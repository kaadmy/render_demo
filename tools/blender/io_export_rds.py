
import bpy
import bpy.props
from bpy_extras.io_utils import ExportHelper

import struct
import time

bl_info = {
    "name": "Renderdemo scene exporter",
    "author": "kaadmy",
    "version": (0, 1, 0),
    "blender": (2, 80, 0),
    "location": "File > Import-Export",
    "description": "Export Renderdemo scene",
    "warning": "Expect stability issues",
    "category": "Import-Export"
}

OBJECT_TYPES = {
    "EMPTY": 0,
    "MESH": 1,
    "ARMATURE": 2,
    "LIGHT": 3
}

# Export a single empty

def export_object_empty(depsgraph, scene, obj, options):
    return bytes()

# Export a single light

def export_object_light(depsgraph, scene, obj, options):
    return bytes()

# Export a single mesh

def export_object_mesh(depsgraph, scene, obj, options):
    mesh = obj.to_mesh(depsgraph, options["use_mesh_modifiers"])
    mesh.calc_loop_triangles()

    # Output array of binary data, these are later joined together

    blob_array = []

    # Mesh header

    # Remove temp mesh

    bpy.data.meshes.remove(mesh)

    # Join blob data and return it

    return bytes().join(blob_array)

def export_scene(depsgraph, scene, objects, options):
    # Objects are already filtered at this point, so they should all
    # be exported

    # Output array of binary data, these are later joined together

    blob_array = []

    # Enumerate objects and call individual object export functions

    for obj_index, obj in enumerate(objects):
        print("Exporting %s(%d)" % (obj.type, OBJECT_TYPES[obj.type]))

        # Object header

        object_header = bytes()
        object_header = struct.pack("<I", (OBJECT_TYPES[obj.type]))

        blob_array.append(object_header)

        # Individual export functions can add their own headers

        if obj.type == "EMPTY":
            blob_array.append(export_object_empty(depsgraph, scene, obj, options))
        elif obj.type == "LIGHT":
            blob_array.append(export_object_light(depsgraph, scene, obj, options))
        elif obj.type == "MESH":
            blob_array.append(export_object_mesh(depsgraph, scene, obj, options))

    # Join blob data and return it

    return bytes().join(blob_array)

def export_rds(context, filepath, options):
    print("Exporting renderdemo scene: %s" % filepath)

    # Force object mode

    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(mode = "OBJECT")

    # Get object array

    if options["use_selection"]:
        objects_all = context.selected_objects
    else:
        objects_all = context.scene.objects

    objects = []

    for obj in objects_all:
        if obj.type in OBJECT_TYPES:
            objects.append(obj)

    # Start export timer

    timer_start = time.time()

    # Open file for writing

    file_out = open(filepath, "wb")

    # Write file header

    blob_header = bytes("RDS001", "utf-8")

    blob_header += struct.pack("<I")

    file_out.write(blob_header)

    # Export the scene

    blob_out = export_scene(context.depsgraph, context.scene, objects, options)

    file_out.write(blob_out)

    # Close file

    file_out.close()

    # Finish export timer

    print("Exported %d objects in %0.4f seconds" % (len(objects), time.time() - timer_start))

    return {"FINISHED"}

class ExportRDS(bpy.types.Operator, ExportHelper):
    """Export Renderdemo scene"""

    bl_idname = "export_scene.rds"
    bl_label = "Export RDS"
    bl_options = {"PRESET"}

    filename_ext = ".rds"

    # File selection

    filter_glob: bpy.props.StringProperty(
        default = "*.rds",
        options = {"HIDDEN"}
    )

    # Object selection

    use_selection: bpy.props.BoolProperty(
        name = "Selection Only",
        description = "Export selected objects only",
        default = False
    )

    # Modifiers

    use_mesh_modifiers: bpy.props.BoolProperty(
        name = "Apply Modifiers",
        description = "Apply modifiers",
        default = True
    )

    # Mesh objects

    use_mesh_normals: bpy.props.BoolProperty(
        name = "Export Normals",
        description = "Export normals for meshes",
        default = True
    )

    use_mesh_uv2: bpy.props.BoolProperty(
        name = "Export UV2",
        description = "Export secondary UV coordinates for meshes",
        default = False
    )

    use_mesh_colors: bpy.props.BoolProperty(
        name = "Export Vertex Colors",
        description = "Export vertex colors for meshes",
        default = False
    )

    use_mesh_weights: bpy.props.BoolProperty(
        name = "Export Vertex Weights",
        description = "Export vertex weights for meshes",
        default = False
    )

    def execute(self, context):
        options = self.as_keywords(ignore = (
            "check_existing",
            "filter_glob",
            "filepath"
        ))

        return export_rds(context, self.filepath, options)

# Register/unregister callbacks

def menu_func_export(self, context):
    self.layout.operator(ExportRDS.bl_idname, text="Renderdemo scene (.rds)")

classes = tuple([
    ExportRDS
])

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

def unregister():
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

    for cls in classes:
        bpy.utils.unregister_class(cls)

# Register addon by default

if __name__ == "__main__":
    register()
