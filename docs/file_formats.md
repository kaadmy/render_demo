
# Material format (.rdt)

Example:

```
material "materials/base/base01" {
    shader vertex "shaders/default/mesh_static.vert"
    shader fragment "shaders/default/mesh"
    texture albedo0 "textures/base/base01_albedo.dds"
    texture normal0 "textures/base/base01_normal.dds"
    texture orm0 "textures/base/base01_orm.dds"
}

material "materials/base/base02" {
    shader vertex "shaders/default/mesh_static.vert"
    shader fragment "shaders/default/mesh"
    texture 0 albedo "textures/base/base02_albedo.dds"
    texture 0 normal "textures/base/base02_normal.dds"
    texture 0 orm "textures/base/base02_orm.dds"
    texture 0 emission "textures/base/base02_emission.dds"
    emission 0 r "0.0 1.0 0.0"
    emission 0 g "1.0 1.0 0.0"
    emission 0 b "0.0 1.0 1.0"
}

material "materials/base/base03" {
    blend add
    shader vertex "shaders/default/mesh_static.vert"
    shader fragment "shaders/default/mesh"
    texture 0 albedo "textures/base/base03_albedo.dds"
    texture 0 normal "textures/base/base03_normal.dds"
    texture 0 orm "textures/base/base03_orm.dds"
}
```

# Scene format (.rds)

Little-endian binary data blob.

```
string[8] magic = "RDS001"
uint32 object_count
struct object_meta {
    uint64 seek_pointer
    uint32 object_type
    string[] object_name

} object_meta_table[object_count]
object_struct objects[object_count]
struct<object_struct> mesh {
    uint32 vertex_attribute_mask // Bitmask of exported vertex attributes
    uint32 vertex_count
    uint32 element_count
    string[] material_name
    struct {
        <attribute data> // Depends on which attributes were exported
    } vertices[vertex_count]
    struct {
       uint32 vertex_index
    } elements[element_count]
}
struct<object_struct> armature
    uint8 bone_count
    struct {
        uint8 parent
        mat4 transform
        string[] bone_name
    } bones[bone_count]
}
struct<object_struct> light {
}
```

# Animation format (.rda)

# Texture format (.dds, .png)

Just regular DDS and PNG loading.

PNG loading will probably use `libpng` or `stb_image` as a lossless
alternative (but DDS will be the most performant and support more
features.)
