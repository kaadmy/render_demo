
# Note: This is mainly a test project and intended as an example rendering codebase, and of course it very possibly has bugs.

# Render Demo

## Rendering Pipeline

- Prepass
  - Render scene depth
    - Sorted front-to-back, compare `LEQUAL`
  - Render decals
    - Decals are precomputed to be floating off the main geometry surface
    - No depth write
    - Render normals
      - Format: RGB16F
        - RG: Screen-space XY normal
        - B: Albedo texture's alpha channel
      - Additive blending
    - Render blend mask
      - Format: R8G8B8
        - RGB: Blend texture's RGB channels
      - Additive blending
- SSAO
  - Half resolution
  - Upscaled to fit during sampling
  - Calculated from prepass scene depth
    - Infer normal/position from depth
  - Randomized scattering
  - Format: R8
    - R: Inverse occlusion (0 = full, 1 = none)
- Geometry
  - Depth compare `EQUAL`
  - Render scene lighting
    - Use prepass decal buffers
    - Use SSAO
- MS resolve
  - Half resolution
  - Resolve scene lighting buffers
- Auto exposure
  - Luminance buffer downscale
    - Source from MS resolve
    - Begin at 256x256, halve with linear filtering until 2x2
- Bloom
  - Bright-pass
    - Source from MS resolve
    - 1/8th window resolution
    - Account for auto exposure
  - Ping-pong blur
    - Separable Gaussian blur
- Composite
  - Source from scene lighting (Not MS resolve)
  - Tonemapping
  - Auto exposure correction
  - Bloom
  - Gamma correction
  - Chromatic aberration
  - Vignette

## OpenGL

### Texture units

- 0: unknown/temporary
- 1-20: material
- 21-30: engine/environment
