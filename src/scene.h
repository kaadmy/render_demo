
#pragma once

#include "render_api.h"

#define BLOOM_LEVELS 7

struct scene_state {
    struct {
        int window[2];
        int size[2];
        int d2[2];
        int d4[2];
        int d8[2];

        int bloom[BLOOM_LEVELS][2];
    } size;

    struct {
        struct {
            struct render_framebuffer fb;
            struct render_texture tex_decal_normal; // RG16F
            struct render_texture tex_decal_blend; // RGB
            struct render_texture tex_depth; // D24
        } fb_prepass; // Prepass for depth and decals (With mutisampling)

        struct {
            struct render_framebuffer fb;
            struct render_texture tex_resolve; // RGB16F
            struct render_texture tex_depth_resolve; // D24
        } fb_resolve; // Multisample resolve for prepass/lighting pass

        struct {
            struct render_framebuffer fb;
            struct render_texture tex_ssao; // R
        } fb_ssao; // SSAO pass

        struct {
            struct render_framebuffer fb;
            struct render_texture tex_lighting; // RGB16F
        } fb_lighting; // Lighting pass (With multisampling)

        struct {
            struct render_framebuffer fb;
            struct render_texture tex_luminance; // RGB16F
        } fb_luminance[8]; // Screen luminance, downsampled to 2x2 (Sources from MS resolve) (Index 0 is 2x2)

        struct {
            struct render_framebuffer fb;
            struct render_texture tex_bloom; // RGB16F
        } fb_bloom_levels[BLOOM_LEVELS][2]; // Bloom blur levels (Sources from MS resolve) [level][blur axis]

        struct {
            struct render_framebuffer fb;
            struct render_texture tex_bloom; // RGB16F
        } fb_bloom; // Bloom extract, after blurring this becomes the bloom final mix

        struct {
            struct render_shader default_vertex;
            struct render_shader default_fragment;
            struct render_shader pp_vertex;
            struct render_shader pp_bloom_extract_fragment;
            struct render_shader pp_bloom_mix_fragment;
            struct render_shader pp_blur_fragment;
            struct render_shader pp_composite_fragment;
        } shaders;

        struct {
            struct render_material material;
            struct render_texture tex_albedo0;
            struct render_texture tex_orm0;
            struct render_texture tex_normal0;
        } material_default;

        struct {
            struct render_material material;
        } material_pp_bloom_extract;

        struct {
            struct render_material material;
        } material_pp_bloom_mix;

        struct {
            struct render_material material;
        } material_pp_blur;

        struct {
            struct render_material material;
        } material_pp_composite;

        struct {
            //struct render_mesh mesh;
            struct render_vertex_buffer vertex_buffer;
        } mesh_pp;

        struct {
            //struct render_mesh mesh;
            struct render_vertex_buffer vertex_buffer;
        } mesh_testing;
    } assets;
};

void scene_state_init(struct scene_state *scene);
void scene_state_deinit(struct scene_state *scene);

void scene_state_resize(struct scene_state *scene, int *window_size, int *size);

void scene_state_render(struct scene_state *scene);
