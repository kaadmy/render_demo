
#include "main.h"

#include "render_api.h"

#include "common/memory.h"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdlib.h>

struct main_state mstate;

static void window_event_resize(GLFWwindow *window, int width, int height) {
    mstate.window_size[0] = width;
    mstate.window_size[1] = height;

    mstate.render_size[0] = width * mstate.render_scale;
    mstate.render_size[1] = height * mstate.render_scale;

    scene_state_resize(&mstate.scene, mstate.window_size, mstate.render_size);
}

static bool main_loop(void) {
    while(!glfwWindowShouldClose(mstate.window)) {
        glfwPollEvents();

        render_context_render_pre();

        scene_state_render(&mstate.scene);

        render_context_render_post();
    }

    return true;
}

static bool main_init(void) {
    if(!glfwInit()) {
        printf("Failed to initialize GLFW\n");
        return false;
    }

#ifdef RENDER_DEMO_USE_OPENGL
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
#endif

    mstate.window_size[0] = 1024;
    mstate.window_size[1] = 600;

    mstate.render_scale = 1.0;

    mstate.render_size[0] = mstate.window_size[0] * mstate.render_scale;
    mstate.render_size[1] = mstate.window_size[1] * mstate.render_scale;

    mstate.window = glfwCreateWindow(mstate.window_size[0], mstate.window_size[1], "Render demo", NULL, NULL);

    if(mstate.window == NULL) {
        printf("Failed to create window\n");
        return false;
    }

    glfwSetWindowSizeCallback(mstate.window, window_event_resize);

#ifdef RENDER_DEMO_USE_OPENGL
    glfwMakeContextCurrent(mstate.window);
#endif

    if(!render_context_init(mstate.window)) {
        return false;
    }

    scene_state_init(&mstate.scene);
    scene_state_resize(&mstate.scene, mstate.window_size, mstate.render_size);

    return main_loop();
}

static void main_atexit(void) {
    scene_state_deinit(&mstate.scene);

    render_context_deinit();

    glfwDestroyWindow(mstate.window);
    glfwTerminate();

    common_memory_audit();
}

int main(int argc, char **argv) {
    if(atexit(main_atexit)) {
        return 1;
    }

    if(!main_init()) {
        return 1;
    }

    return 0;
}
