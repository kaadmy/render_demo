
#pragma once

#include "render_api.h"
#include "scene.h"

struct main_state {
    GLFWwindow *window;
    int window_size[2];

    float render_scale;
    int render_size[2];

    struct scene_state scene;
};

extern struct main_state mstate;
