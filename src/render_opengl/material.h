
#pragma once

#include "glad.h"
#include "shader.h"
#include "texture.h"

#include "cglm/cglm.h"

#include <stdbool.h>

enum render_material_texture {
    RENDER_MATERIAL_TEXTURE_COLOR0 = 0, // COLOR0 is always the same index
    RENDER_MATERIAL_TEXTURE_COLOR1,
    RENDER_MATERIAL_TEXTURE_COLOR2,
    RENDER_MATERIAL_TEXTURE_COLOR3,
    RENDER_MATERIAL_TEXTURE_COLOR4,
    RENDER_MATERIAL_TEXTURE_COLOR5,
    RENDER_MATERIAL_TEXTURE_COLOR6,
    RENDER_MATERIAL_TEXTURE_COLOR7,
    RENDER_MATERIAL_TEXTURE_COLOR8,
    RENDER_MATERIAL_TEXTURE_COLOR9,
    RENDER_MATERIAL_TEXTURE_COLOR10,

    RENDER_MATERIAL_MAX_TEXTURES,
};

enum render_material_texture_uniform_format {
    RENDER_MATERIAL_TEXTURE_UNIFORM_FORMAT_INDEXED = 0,
    RENDER_MATERIAL_TEXTURE_UNIFORM_FORMAT_PBR_BASE,

    RENDER_MATERIAL_MAX_TEXTURE_UNIFORM_FORMATS,
};

struct render_material_options {
    //bool used_attributes[RENDER_VERTEX_BUFFER_MAX_ATTRIBS]; // Which vertex buffer attributes are expected in this material

    enum render_material_texture_uniform_format uniform_format;

    struct render_shader *shaders[RENDER_SHADER_MAX_TYPES];

    struct render_texture *textures[RENDER_MATERIAL_MAX_TEXTURES];
};

struct render_material {
    struct render_material_options options;

    bool usable;

    GLbitfield gl_stages;
    GLint gl_program;
    GLuint gl_program_pipeline;
};

void render_material_options_init(struct render_material_options *options);

void render_material_init(struct render_material *material, struct render_material_options *options);
void render_material_deinit(struct render_material *material);

void render_material_bind(struct render_material *material);

void render_material_uniform_float(struct render_material *material, const char *name, float v0);
void render_material_uniform_vec2(struct render_material *material, const char *name, const vec2 v0);
void render_material_uniform_vec3(struct render_material *material, const char *name, const vec3 v0);
void render_material_uniform_vec4(struct render_material *material, const char *name, const vec4 v0);
void render_material_uniform_int(struct render_material *material, const char *name, int v0);
void render_material_uniform_mat3(struct render_material *material, const char *name, const mat3 v0);
void render_material_uniform_mat4(struct render_material *material, const char *name, const mat4 v0);

void render_material_texture_set(struct render_material *material, enum render_material_texture index,
                                 struct render_texture *texture);
void render_material_texture_bind(struct render_material *material, enum render_material_texture index);
