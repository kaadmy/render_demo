
#include "vertex_buffer.h"

#include "common/memory.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

static const struct {
    const char *name;

    int flag;

    size_t size;
    size_t member_offset;

    GLint count;
    GLenum type;
    GLboolean normalized;
} attribute_data_table[RENDER_VERTEX_BUFFER_MAX_ATTRIBUTES] = {
    {
        .name = "ATTRIB_POSITION",
        .flag = RENDER_VERTEX_BUFFER_ATTRIBUTE_FLAG_POSITION,
        .size = sizeof(float) * 3,
        .member_offset = offsetof(struct render_vertex_data, position),
        .count = 3,
        .type = GL_FLOAT,
        .normalized = GL_FALSE,
    },
    {
        .name = "ATTRIB_NORMAL",
        .flag = RENDER_VERTEX_BUFFER_ATTRIBUTE_FLAG_NORMAL,
        .size = sizeof(float) * 3,
        .member_offset = offsetof(struct render_vertex_data, normal),
        .count = 3,
        .type = GL_FLOAT,
        .normalized = GL_FALSE,
    },
    {
        .name = "ATTRIB_COLOR",
        .flag = RENDER_VERTEX_BUFFER_ATTRIBUTE_FLAG_COLOR,
        .size = sizeof(unsigned char) * 4,
        .member_offset = offsetof(struct render_vertex_data, color),
        .count = 4,
        .type = GL_UNSIGNED_BYTE,
        .normalized = GL_TRUE,
    },
    {
        .name = "ATTRIB_TEXCOORD0",
        .flag = RENDER_VERTEX_BUFFER_ATTRIBUTE_FLAG_TEXCOORD0,
        .size = sizeof(float) * 2,
        .member_offset = offsetof(struct render_vertex_data, texcoord0),
        .count = 2,
        .type = GL_FLOAT,
        .normalized = GL_FALSE,
    },
};

const char *render_vertex_buffer_get_attribute_name(enum render_vertex_buffer_attribute attribute) {
    return attribute_data_table[attribute].name;
}

void render_vertex_data_init(struct render_vertex_data *vertex) {
    vertex->position[0] = 0.0;
    vertex->position[1] = 0.0;
    vertex->position[2] = 0.0;

    vertex->normal[0] = 0.0;
    vertex->normal[1] = 0.0;
    vertex->normal[2] = 0.0;

    vertex->color[0] = 0;
    vertex->color[1] = 0;
    vertex->color[2] = 0;
    vertex->color[3] = 0;

    vertex->texcoord0[0] = 0.0;
    vertex->texcoord0[1] = 0.0;
}

void render_vertex_buffer_init(struct render_vertex_buffer *vertex_buffer, int attribute_flags) {
    vertex_buffer->vertex_count_allocated = 0;
    vertex_buffer->vertex_count = 0;

    vertex_buffer->element_dirty = false;
    vertex_buffer->element_count_allocated = 0;
    vertex_buffer->element_count = 0;
    vertex_buffer->element_data = NULL;

    glGenVertexArrays(1, &vertex_buffer->gl_vao);
    glBindVertexArray(vertex_buffer->gl_vao);

    for(int i=0; i<RENDER_VERTEX_BUFFER_MAX_ATTRIBUTES; i++) {
        vertex_buffer->attributes[i].dirty = false;
        vertex_buffer->attributes[i].attribute_data = NULL;
        vertex_buffer->attributes[i].gl_vbo = 0;

        if(attribute_flags & attribute_data_table[i].flag) {
            vertex_buffer->attributes[i].active = true;

            glGenBuffers(1, &vertex_buffer->attributes[i].gl_vbo);
            glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer->attributes[i].gl_vbo);

            glEnableVertexAttribArray(i);
            glVertexAttribPointer(i, attribute_data_table[i].count, attribute_data_table[i].type,
                                  attribute_data_table[i].normalized, 0, 0);
        } else {
            vertex_buffer->attributes[i].active = false;
        }
    }

    glGenBuffers(1, &vertex_buffer->gl_ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertex_buffer->gl_ebo);
}

void render_vertex_buffer_deinit(struct render_vertex_buffer *vertex_buffer) {
    if(vertex_buffer == NULL) {
        return;
    }

    glDeleteVertexArrays(1, &vertex_buffer->gl_vao);

    glDeleteBuffers(1, &vertex_buffer->gl_ebo);

    for(int i=0; i<RENDER_VERTEX_BUFFER_MAX_ATTRIBUTES; i++) {
        if(vertex_buffer->attributes[i].active) {
            glDeleteBuffers(1, &vertex_buffer->attributes[i].gl_vbo);

            if(vertex_buffer->attributes[i].attribute_data != NULL) {
                common_memory_free(vertex_buffer->attributes[i].attribute_data);
            }
        }
    }

    if(vertex_buffer->element_data != NULL) {
        common_memory_free(vertex_buffer->element_data);
    }
}

void render_vertex_buffer_render(struct render_vertex_buffer *vertex_buffer) {
    // Only render 1 or more vertices, if vertices are removed then
    // this will behave incorrectly.
    if(vertex_buffer->element_count < 1) {
        return;
    }

    glBindVertexArray(vertex_buffer->gl_vao);

    size_t size;

    for(int i=0; i<RENDER_VERTEX_BUFFER_MAX_ATTRIBUTES; i++) {
        if(vertex_buffer->attributes[i].active && vertex_buffer->attributes[i].dirty) {
            size = vertex_buffer->vertex_count * attribute_data_table[i].size;

            glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer->attributes[i].gl_vbo);
            glBufferData(GL_ARRAY_BUFFER, size, vertex_buffer->attributes[i].attribute_data, GL_STATIC_DRAW);

            vertex_buffer->attributes[i].dirty = false;
        }
    }

    if(vertex_buffer->element_dirty) {
        size = vertex_buffer->element_count * sizeof(unsigned int);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertex_buffer->gl_ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, vertex_buffer->element_data, GL_STATIC_DRAW);

        vertex_buffer->element_dirty = false;
    }

    //glDrawArrays(GL_TRIANGLES, 0, vertex_buffer->vertex_count);
    glDrawElements(GL_TRIANGLES, vertex_buffer->element_count, GL_UNSIGNED_INT, 0);
}

void render_vertex_buffer_vertex_allocate(struct render_vertex_buffer *vertex_buffer, size_t count) {
    vertex_buffer->vertex_count_allocated += count;

    size_t size;

    for(int i=0; i<RENDER_VERTEX_BUFFER_MAX_ATTRIBUTES; i++) {
        if(vertex_buffer->attributes[i].active) {
            size = vertex_buffer->vertex_count_allocated * attribute_data_table[i].size;

            vertex_buffer->attributes[i].attribute_data = common_memory_reallocate(vertex_buffer->attributes[i].attribute_data, size);
        }
    }
}

size_t render_vertex_buffer_vertex_add(struct render_vertex_buffer *vertex_buffer, struct render_vertex_data *vertex_data) {
    assert(vertex_buffer->vertex_count < vertex_buffer->vertex_count_allocated);

    size_t index = vertex_buffer->vertex_count;

    for(int i=0; i<RENDER_VERTEX_BUFFER_MAX_ATTRIBUTES; i++) {
        if(vertex_buffer->attributes[i].active) {
            unsigned char *attribute_pointer = &vertex_buffer->attributes[i].attribute_data[attribute_data_table[i].size * index];
            unsigned char *data = &((unsigned char *) vertex_data)[attribute_data_table[i].member_offset];

            memcpy(attribute_pointer, data, attribute_data_table[i].size);

            vertex_buffer->attributes[i].dirty = true;
        }
    }

    vertex_buffer->vertex_count++;

    return index;
}

void render_vertex_buffer_element_allocate(struct render_vertex_buffer *vertex_buffer, size_t count) {
    vertex_buffer->element_count_allocated += count;

    size_t size = vertex_buffer->element_count_allocated * sizeof(unsigned int);

    vertex_buffer->element_data = common_memory_reallocate(vertex_buffer->element_data, size);
}

void render_vertex_buffer_element_add(struct render_vertex_buffer *vertex_buffer, size_t count, unsigned int *vertex_indices) {
    size_t index = vertex_buffer->element_count;

    vertex_buffer->element_count +=  count;
    assert(vertex_buffer->element_count <= vertex_buffer->element_count_allocated);

    memcpy(vertex_buffer->element_data + index, vertex_indices, count * sizeof(unsigned int));

    vertex_buffer->element_dirty = true;
}
