
#pragma once

#include "glad.h"

#include <stdbool.h>

enum render_texture_slot {
    RENDER_TEXTURE_SLOT_TEMP = 0,

    RENDER_TEXTURE_SLOT_MATERIAL0,
    RENDER_TEXTURE_SLOT_MATERIAL1,
    RENDER_TEXTURE_SLOT_MATERIAL2,
    RENDER_TEXTURE_SLOT_MATERIAL3,
    RENDER_TEXTURE_SLOT_MATERIAL4,
    RENDER_TEXTURE_SLOT_MATERIAL5,
    RENDER_TEXTURE_SLOT_MATERIAL6,
    RENDER_TEXTURE_SLOT_MATERIAL7,
    RENDER_TEXTURE_SLOT_MATERIAL8,
    RENDER_TEXTURE_SLOT_MATERIAL9,
    RENDER_TEXTURE_SLOT_MATERIAL10,
    RENDER_TEXTURE_SLOT_MATERIAL11,
    RENDER_TEXTURE_SLOT_MATERIAL12,
    RENDER_TEXTURE_SLOT_MATERIAL13,
    RENDER_TEXTURE_SLOT_MATERIAL14,
    RENDER_TEXTURE_SLOT_MATERIAL15,
    RENDER_TEXTURE_SLOT_MATERIAL16,
    RENDER_TEXTURE_SLOT_MATERIAL17,
    RENDER_TEXTURE_SLOT_MATERIAL18,
    RENDER_TEXTURE_SLOT_MATERIAL19,

    RENDER_TEXTURE_SLOT_ENGINE0,
    RENDER_TEXTURE_SLOT_ENGINE1,
    RENDER_TEXTURE_SLOT_ENGINE2,
    RENDER_TEXTURE_SLOT_ENGINE3,
    RENDER_TEXTURE_SLOT_ENGINE4,
    RENDER_TEXTURE_SLOT_ENGINE5,
    RENDER_TEXTURE_SLOT_ENGINE6,
    RENDER_TEXTURE_SLOT_ENGINE7,
    RENDER_TEXTURE_SLOT_ENGINE8,
    RENDER_TEXTURE_SLOT_ENGINE9,

    RENDER_TEXTURE_MAX_SLOTS,
};

enum render_texture_format {
    RENDER_TEXTURE_FORMAT_R = 0,
    RENDER_TEXTURE_FORMAT_RG,
    RENDER_TEXTURE_FORMAT_RGB,
    RENDER_TEXTURE_FORMAT_RGBA,

    RENDER_TEXTURE_FORMAT_R16F,
    RENDER_TEXTURE_FORMAT_R32F,
    RENDER_TEXTURE_FORMAT_RG16F,
    RENDER_TEXTURE_FORMAT_RG32F,
    RENDER_TEXTURE_FORMAT_RGB16F,
    RENDER_TEXTURE_FORMAT_RGB32F,
    RENDER_TEXTURE_FORMAT_RGBA16F,
    RENDER_TEXTURE_FORMAT_RGBA32F,

    RENDER_TEXTURE_FORMAT_DEPTH,
    RENDER_TEXTURE_FORMAT_DEPTH16,
    RENDER_TEXTURE_FORMAT_DEPTH24,
    RENDER_TEXTURE_FORMAT_DEPTH32,

    RENDER_TEXTURE_MAX_FORMATS,
};

enum render_texture_format_type {
    RENDER_TEXTURE_FORMAT_TYPE_UNSIGNED_BYTE = 0,
    RENDER_TEXTURE_FORMAT_TYPE_BYTE,

    RENDER_TEXTURE_FORMAT_TYPE_FLOAT,
};

struct render_texture_options {
    enum render_texture_format internal_format;

    bool allow_multisample;

    bool allow_anisotropy;

    bool mipmap;
    bool repeat;
};

struct render_texture {
    struct render_texture_options options;

    int size[2];

    GLenum gl_internal_format;
    GLenum gl_internal_format_type;

    GLenum gl_target;
    GLuint gl_texture;
};

void render_texture_options_init(struct render_texture_options *options);

void render_texture_init(struct render_texture *texture, struct render_texture_options *options);
void render_texture_deinit(struct render_texture *texture);

void render_texture_bind(struct render_texture *texture, enum render_texture_slot slot);

void render_texture_resize(struct render_texture *texture, const int *size);
void render_texture_get_size(struct render_texture *texture, int *size);
