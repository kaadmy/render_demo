
#include "glad.h"
#include "state.h"

#include <stdbool.h>
#include <stdio.h>

static struct render_state state;

void render_state_init(void) {
    state.force_set = true;

    render_state_depth_test(true);
    render_state_depth_mask(true);
    render_state_enable_blend(true);

    render_state_gl_active_texture_(GL_TEXTURE0);

    state.force_set = false;
}

void render_state_deinit(void) {
}

void render_state_depth_test(bool depth_test) {
    if(state.force_set || depth_test != state.depth_test) {
        if(depth_test) {
            glEnable(GL_DEPTH_TEST);
        } else {
            glDisable(GL_DEPTH_TEST);
        }
    }

    state.depth_test = depth_test;
}

void render_state_depth_mask(bool depth_mask) {
    if(state.force_set || depth_mask != state.depth_mask) {
        if(depth_mask) {
            glDepthMask(GL_TRUE);
        } else {
            glDepthMask(GL_FALSE);
        }
    }

    state.depth_mask = depth_mask;
}

void render_state_enable_blend(bool enable_blend) {
    if(state.force_set || enable_blend != state.enable_blend) {
        if(enable_blend) {
            glEnable(GL_BLEND);
        } else {
            glDisable(GL_BLEND);
        }
    }

    state.enable_blend = enable_blend;
}

void render_state_gl_active_texture_(GLenum texture) {
    if(state.force_set || texture != state.gl_active_texture) {
        glActiveTexture(texture);
    }
    state.gl_active_texture = texture;
}
