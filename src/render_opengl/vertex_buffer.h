
#pragma once

#include "glad.h"
#include "shader.h"
#include "texture.h"

#include <stdbool.h>

#define RENDER_VERTEX_BUFFER_ATTRIBUTE_FLAG_POSITION (1<<0)
#define RENDER_VERTEX_BUFFER_ATTRIBUTE_FLAG_NORMAL (1<<1)
#define RENDER_VERTEX_BUFFER_ATTRIBUTE_FLAG_COLOR (1<<2)
#define RENDER_VERTEX_BUFFER_ATTRIBUTE_FLAG_TEXCOORD0 (1<<3)

enum render_vertex_buffer_attribute {
    RENDER_VERTEX_BUFFER_ATTRIBUTE_POSITION = 0,
    RENDER_VERTEX_BUFFER_ATTRIBUTE_NORMAL,
    RENDER_VERTEX_BUFFER_ATTRIBUTE_COLOR,
    RENDER_VERTEX_BUFFER_ATTRIBUTE_TEXCOORD0,

    RENDER_VERTEX_BUFFER_MAX_ATTRIBUTES,
};

struct render_vertex_data {
    float position[3];
    float normal[3];
    unsigned char color[4];
    float texcoord0[2];
};

struct render_vertex_buffer {
    int attribute_count;

    struct {
        bool active;

        bool dirty;

        unsigned char *attribute_data; // Just a byte container

        GLuint gl_vbo;
    } attributes[RENDER_VERTEX_BUFFER_MAX_ATTRIBUTES];

    size_t vertex_count_allocated;
    size_t vertex_count;

    bool element_dirty;

    size_t element_count_allocated;
    size_t element_count;
    unsigned int *element_data;

    GLuint gl_ebo;
    GLuint gl_vao;
};

const char *render_vertex_buffer_get_attribute_name(enum render_vertex_buffer_attribute attribute);

void render_vertex_data_init(struct render_vertex_data *vertex);

void render_vertex_buffer_init(struct render_vertex_buffer *vertex_buffer, int attribute_flags);
void render_vertex_buffer_deinit(struct render_vertex_buffer *vertex_buffer);

void render_vertex_buffer_render(struct render_vertex_buffer *vertex_buffer);

void render_vertex_buffer_vertex_allocate(struct render_vertex_buffer *vertex_buffer, size_t count);
size_t render_vertex_buffer_vertex_add(struct render_vertex_buffer *vertex_buffer, struct render_vertex_data *vertex_data);

void render_vertex_buffer_element_allocate(struct render_vertex_buffer *vertex_buffer, size_t count);
void render_vertex_buffer_element_add(struct render_vertex_buffer *vertex_buffer, size_t count, unsigned int *vertex_indices);
