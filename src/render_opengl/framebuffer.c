
#include "context.h"
#include "framebuffer.h"
#include "glad.h"

#include "cglm/cglm.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

static const GLint gl_attachments[] = {
    GL_COLOR_ATTACHMENT0,
    GL_COLOR_ATTACHMENT1,
    GL_COLOR_ATTACHMENT2,
    GL_COLOR_ATTACHMENT3,
    GL_COLOR_ATTACHMENT4,
    GL_COLOR_ATTACHMENT5,
    GL_COLOR_ATTACHMENT6,
    GL_COLOR_ATTACHMENT7,

    GL_DEPTH_ATTACHMENT,
};

void render_framebuffer_init(struct render_framebuffer *framebuffer) {
    assert(framebuffer != NULL);

    framebuffer->size[0] = 64;
    framebuffer->size[1] = 64;

    for(int i=0; i<RENDER_FRAMEBUFFER_MAX_ATTACHMENTS; i++) {
        framebuffer->attachments[i].texture = NULL;
        framebuffer->attachments[i].gl_attachment = gl_attachments[i];
    }

    glGenFramebuffers(1, &framebuffer->gl_fbo);

    // `framebuffer->gl_draw_buffers` only has to be initialized for
    // the specified size, which defaults to zero. If the size is ever
    // increased, the draw buffer indices must also reflect this.
    framebuffer->gl_draw_buffer_count = 0;
}

void render_framebuffer_deinit(struct render_framebuffer *framebuffer) {
    assert(framebuffer != NULL);

    glDeleteFramebuffers(1, &framebuffer->gl_fbo);
}

static void update_draw_buffers(struct render_framebuffer *framebuffer) {
    framebuffer->gl_draw_buffer_count = 0;

    for(int i=0; i<RENDER_FRAMEBUFFER_ATTACHMENT_DEPTH; i++) {
        if(framebuffer->attachments[i].texture != NULL) {

            framebuffer->gl_draw_buffers[framebuffer->gl_draw_buffer_count] = gl_attachments[i];
            framebuffer->gl_draw_buffer_count++;
        }
    }
}

void render_framebuffer_bind(struct render_framebuffer *framebuffer) {
    if(framebuffer == NULL) {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

        return;
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer->gl_fbo);

    GLenum status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
    if(status != GL_FRAMEBUFFER_COMPLETE) {
        printf("Framebuffer incomplete: 0x%x\n", status);
    }

    glDrawBuffers(framebuffer->gl_draw_buffer_count, framebuffer->gl_draw_buffers);
}

void render_framebuffer_viewport(struct render_framebuffer *framebuffer) {
    if(framebuffer == NULL) {
        glViewport(0, 0, render_context_state.window_size[0], render_context_state.window_size[1]);

        return;
    }

    glViewport(0, 0, framebuffer->size[0], framebuffer->size[1]);
}

void render_framebuffer_resize(struct render_framebuffer *framebuffer, const int *size) {
    framebuffer->size[0] = glm_max(size[0], 1);
    framebuffer->size[1] = glm_max(size[1], 1);

    // We should resize any textures we use as well

    for(int i=0; i<RENDER_FRAMEBUFFER_MAX_ATTACHMENTS; i++) {
        if(framebuffer->attachments[i].texture != NULL) {
            render_texture_resize(framebuffer->attachments[i].texture, framebuffer->size);
        }
    }
}

void render_framebuffer_get_size(struct render_framebuffer *framebuffer, int *size) {
    size[0] = framebuffer->size[0];
    size[1] = framebuffer->size[1];
}

void render_framebuffer_blit(struct render_framebuffer *fb_from, enum render_framebuffer_attachment attachment_from,
                             struct render_framebuffer *fb_to, enum render_framebuffer_attachment attachment_to,
                             bool linear_filter) {
    assert(fb_from->attachments[attachment_from].texture != NULL);
    assert(fb_to->attachments[attachment_to].texture != NULL);

    if(fb_from->attachments[attachment_from].texture->options.internal_format !=
       fb_to->attachments[attachment_to].texture->options.internal_format) {
        printf("Cannot blit framebuffer attachments with mismatched texture formats");

        return;
    }

    glBindFramebuffer(GL_READ_FRAMEBUFFER, fb_from->gl_fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fb_to->gl_fbo);

    GLenum mask = GL_COLOR_BUFFER_BIT;

    if(fb_from->attachments[attachment_from].gl_attachment == GL_DEPTH_ATTACHMENT) {
        mask = GL_DEPTH_BUFFER_BIT;

        if(linear_filter) {
            printf("Depth attacahments must be blitted with nearest filtering\n");
        }
    } else {
        glReadBuffer(fb_from->attachments[attachment_from].gl_attachment);
        glDrawBuffer(fb_to->attachments[attachment_to].gl_attachment);
    }

    glBlitFramebuffer(0, 0, fb_from->size[0], fb_from->size[1],
                      0, 0, fb_to->size[0], fb_to->size[1],
                      mask, (linear_filter ? GL_LINEAR : GL_NEAREST));
}

void render_framebuffer_attachment_set(struct render_framebuffer *framebuffer, enum render_framebuffer_attachment index,
                                       struct render_texture *texture) {
    if(texture == NULL) {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer->gl_fbo);
        glFramebufferTexture(GL_DRAW_FRAMEBUFFER, gl_attachments[index], 0, 0);
    } else {
        if(framebuffer->attachments[index].texture != NULL) {
            printf("Trying to override a framebuffer attachment without removing it first\n");
        }

        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer->gl_fbo);
        glFramebufferTexture(GL_DRAW_FRAMEBUFFER, gl_attachments[index], texture->gl_texture, 0);
    }

    framebuffer->attachments[index].texture = texture;

    update_draw_buffers(framebuffer);
}

void render_framebuffer_attachment_clear(struct render_framebuffer *framebuffer, enum render_framebuffer_attachment index) {
    assert(framebuffer->attachments[index].texture != NULL);

    // `render_framebuffer_bind` must be called on the same
    // framebuffer beforehand.

    static const GLfloat zero_f[4] = {0.0, 0.0, 0.0, 0.0};
    static const GLint zero_i[4] = {0, 0, 0, 0};
    static const GLuint zero_ui[4] = {0, 0, 0, 0};

    if(framebuffer->attachments[index].gl_attachment < GL_DEPTH_ATTACHMENT) {
        GLint draw_buffer = framebuffer->attachments[index].gl_attachment - GL_COLOR_ATTACHMENT0;

        if(framebuffer->attachments[index].texture->gl_internal_format_type == GL_FLOAT) {
            glClearBufferfv(GL_COLOR, draw_buffer, zero_f);
        } else if(framebuffer->attachments[index].texture->gl_internal_format_type == GL_BYTE) {
            glClearBufferiv(GL_COLOR, draw_buffer, zero_i);
        } else {
            glClearBufferuiv(GL_COLOR, draw_buffer, zero_ui);
        }
    } else if(framebuffer->attachments[index].gl_attachment == GL_DEPTH_ATTACHMENT) {
        glClearBufferfv(GL_DEPTH, 0, &zero_f[0]);
    }
}

void render_framebuffer_attachment_clear_colorf(struct render_framebuffer *framebuffer, enum render_framebuffer_attachment index,
                                                float *color) {
    assert(framebuffer->attachments[index].texture != NULL);
    assert(framebuffer->attachments[index].gl_attachment < GL_DEPTH_ATTACHMENT);

    // `render_framebuffer_bind` must be called on the same
    // framebuffer beforehand.

    glClearBufferfv(GL_COLOR, framebuffer->attachments[index].gl_attachment - GL_COLOR_ATTACHMENT0, color);
}
