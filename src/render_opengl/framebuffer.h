
#pragma once

#include "glad.h"
#include "texture.h"

enum render_framebuffer_attachment {
    RENDER_FRAMEBUFFER_ATTACHMENT_COLOR0 = 0,
    RENDER_FRAMEBUFFER_ATTACHMENT_COLOR1,
    RENDER_FRAMEBUFFER_ATTACHMENT_COLOR2,
    RENDER_FRAMEBUFFER_ATTACHMENT_COLOR3,
    RENDER_FRAMEBUFFER_ATTACHMENT_COLOR4,
    RENDER_FRAMEBUFFER_ATTACHMENT_COLOR5,
    RENDER_FRAMEBUFFER_ATTACHMENT_COLOR6,
    RENDER_FRAMEBUFFER_ATTACHMENT_COLOR7,

    RENDER_FRAMEBUFFER_ATTACHMENT_DEPTH, // Depth must be directly after the last color attachment index

    RENDER_FRAMEBUFFER_MAX_ATTACHMENTS,
};

struct render_framebuffer {
    struct {
        struct render_texture *texture;

        GLint gl_attachment;
    } attachments[RENDER_FRAMEBUFFER_MAX_ATTACHMENTS];

    int size[2];

    GLuint gl_fbo;

    int gl_draw_buffer_count;
    GLuint gl_draw_buffers[RENDER_FRAMEBUFFER_MAX_ATTACHMENTS];
};

void render_framebuffer_init(struct render_framebuffer *framebuffer);
void render_framebuffer_deinit(struct render_framebuffer *framebuffer);

void render_framebuffer_bind(struct render_framebuffer *framebuffer);
void render_framebuffer_viewport(struct render_framebuffer *framebuffer);

void render_framebuffer_resize(struct render_framebuffer *framebuffer, const int *size);
void render_framebuffer_get_size(struct render_framebuffer *framebuffer, int *size);

void render_framebuffer_blit(struct render_framebuffer *fb_from, enum render_framebuffer_attachment attachment_from,
                             struct render_framebuffer *fb_to, enum render_framebuffer_attachment attachment_to,
                             bool linear_filter);

void render_framebuffer_attachment_set(struct render_framebuffer *framebuffer, enum render_framebuffer_attachment index,
                                       struct render_texture *texture);

void render_framebuffer_attachment_clear(struct render_framebuffer *framebuffer, enum render_framebuffer_attachment index);
void render_framebuffer_attachment_clear_colorf(struct render_framebuffer *framebuffer, enum render_framebuffer_attachment index,
                                                float *color);
