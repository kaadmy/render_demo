
#pragma once

#include "glad.h"

#include <stdbool.h>

// This struct is meant to be only used in state.c, but is here in
// case that changes later
struct render_state {
    bool force_set;

    bool depth_test;
    bool depth_mask;

    bool enable_blend;

    GLenum gl_active_texture;
};

void render_state_init(void);
void render_state_deinit(void);

void render_state_depth_test(bool depth_test);
void render_state_depth_mask(bool depth_mask);
void render_state_enable_blend(bool enable_blend);

// Functions below this point are meant for use only inside the OpenGL backend

void render_state_gl_active_texture_(GLenum texture);
