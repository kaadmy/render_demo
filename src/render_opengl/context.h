
#pragma once

#include "state.h"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <stdbool.h>

struct render_context_state_ {
    GLFWwindow *window;

    int window_size[2];

    int size[2];

    int texture_anisotropy_level;

    int multisample_samples;
};

extern struct render_context_state_ render_context_state;

bool render_context_init(GLFWwindow *window);
void render_context_deinit(void);

void render_context_render_pre(void);
void render_context_render_post(void);

void render_context_resize(int *window_size, int *size);
