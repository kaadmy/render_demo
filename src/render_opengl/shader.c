
#include "glad.h"
#include "shader.h"

#include "common/memory.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

static const GLenum gl_stages[] = {
    GL_VERTEX_SHADER,
    GL_FRAGMENT_SHADER,
};

static const GLenum gl_stage_bitmask[] = {
    GL_VERTEX_SHADER_BIT,
    GL_FRAGMENT_SHADER_BIT,
};

static const char *shader_type_strings[] = {
    "VERTEX",
    "FRAGMENT",
};

const char *render_shader_get_type_string(enum render_shader_type type) {
    return shader_type_strings[type];
}

void render_shader_init(struct render_shader *shader, enum render_shader_type type) {
    shader->type = type;

    shader->is_compiled = false;

    shader->gl_stage = gl_stage_bitmask[type];
    shader->gl_shader = 0;
}

void render_shader_deinit(struct render_shader *shader) {
    if(shader->is_compiled) {
        glDeleteShader(shader->gl_shader);
        shader->gl_shader = 0;
    }
}

void render_shader_load_from_string(struct render_shader *shader, const char *source) {
    assert(!shader->is_compiled);

    shader->gl_shader = glCreateShader(gl_stages[shader->type]);

    glShaderSource(shader->gl_shader, 1, (const GLchar **) &source, NULL);
    glCompileShader(shader->gl_shader);

    GLint success;
    glGetShaderiv(shader->gl_shader, GL_COMPILE_STATUS, &success);

    if(success == GL_FALSE) {
        GLint log_length;
        glGetShaderiv(shader->gl_shader, GL_INFO_LOG_LENGTH, &log_length);

        GLchar *log = common_memory_allocate(sizeof(GLchar) * log_length);
        glGetShaderInfoLog(shader->gl_shader, log_length, &log_length, &log[0]);

        glDeleteShader(shader->gl_shader);
        shader->gl_shader = 0;

        printf("-------------------- Begin shader source --------------------\n");
        printf(source);
        printf("-------------------- End shader source --------------------\n");

        printf("Shader compilation error log (%s):\n%s", render_shader_get_type_string(shader->type), log);

        common_memory_free(log);

        return;
    }

    shader->is_compiled = true;
}
