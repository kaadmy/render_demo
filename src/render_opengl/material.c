
#include "framebuffer.h"
#include "material.h"
#include "vertex_buffer.h"

#include "common/memory.h"

#include "cglm/cglm.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

static const char *material_texture_uniform_names[RENDER_MATERIAL_MAX_TEXTURE_UNIFORM_FORMATS][RENDER_MATERIAL_MAX_TEXTURES] = {
    {
        "TEXTURE_COLOR0",
        "TEXTURE_COLOR1",
        "TEXTURE_COLOR2",
        "TEXTURE_COLOR3",
        "TEXTURE_COLOR4",
        "TEXTURE_COLOR5",
        "TEXTURE_COLOR6",
        "TEXTURE_COLOR7",
        "TEXTURE_COLOR8",
        "TEXTURE_COLOR9",
        "TEXTURE_COLOR10",
    },
    {
        "TEXTURE_COLOR0",
        "TEXTURE_COLOR1",
        "TEXTURE_COLOR2",
        "TEXTURE_COLOR3",
        "TEXTURE_COLOR4",
        "TEXTURE_COLOR5",
        "TEXTURE_COLOR6",
        "TEXTURE_COLOR7",
        "TEXTURE_ALBEDO0",
        "TEXTURE_ORM0",
        "TEXTURE_NORMAL0",
    }
};

static const char *material_fragdata_names[RENDER_FRAMEBUFFER_MAX_ATTACHMENTS] = {
    "FRAGMENT_OUT_COLOR0",
    "FRAGMENT_OUT_COLOR1",
    "FRAGMENT_OUT_COLOR2",
    "FRAGMENT_OUT_COLOR3",
    "FRAGMENT_OUT_COLOR4",
    "FRAGMENT_OUT_COLOR5",
    "FRAGMENT_OUT_COLOR6",
    "FRAGMENT_OUT_COLOR7",
    "FRAGMENT_OUT_COLOR8",
};

static bool link_program(struct render_material *material) {
    material->gl_program = glCreateProgram();

    material->gl_stages = 0;

    for(int i=0; i<RENDER_SHADER_MAX_TYPES; i++) {
        if(material->options.shaders[i] != NULL) {
            if(material->options.shaders[i]->is_compiled) {
                glAttachShader(material->gl_program, material->options.shaders[i]->gl_shader);

                material->gl_stages |= material->options.shaders[i]->gl_stage;
            } else {
                printf("Trying to attach an uncompiled shader to a material\n");
            }
        }
    }

    if(material->gl_stages == 0) {
        printf("Trying to create a material with no shaders\n");

        glDeleteProgram(material->gl_program);
        material->gl_program = 0;

        return false;
    }

    glProgramParameteri(material->gl_program, GL_PROGRAM_SEPARABLE, GL_TRUE);

    if(material->options.shaders[RENDER_SHADER_TYPE_VERTEX] != NULL) {
        for(int i=0; i<RENDER_VERTEX_BUFFER_MAX_ATTRIBUTES; i++) {
            glBindAttribLocation(material->gl_program, i, render_vertex_buffer_get_attribute_name(i));
        }
    }

    if(material->options.shaders[RENDER_SHADER_TYPE_FRAGMENT] != NULL) {
        for(int i=0; i<RENDER_VERTEX_BUFFER_MAX_ATTRIBUTES; i++) {
            glBindFragDataLocation(material->gl_program, i, material_fragdata_names[i]);
        }
    }

    glLinkProgram(material->gl_program);

    GLint success;
    glGetProgramiv(material->gl_program, GL_LINK_STATUS, &success);

    if(success == GL_FALSE) {
        GLint log_length;
        glGetProgramiv(material->gl_program, GL_INFO_LOG_LENGTH, &log_length);

        GLchar *log = common_memory_allocate(sizeof(GLchar) * log_length);
        glGetProgramInfoLog(material->gl_program, log_length, &log_length, &log[0]);

        glDeleteProgram(material->gl_program);
        material->gl_program = 0;

        printf("Shader link error log:\n%s", log);

        common_memory_free(log);

        return false;
    }

    return true;
}

static void bind_texture_uniforms(struct render_material *material) {
    for(int i=0; i<RENDER_MATERIAL_MAX_TEXTURES; i++) {
        if(material->options.textures[i] == NULL) {
            render_material_uniform_int(material, material_texture_uniform_names[material->options.uniform_format][i],
                                        RENDER_TEXTURE_SLOT_MATERIAL0);
        } else {
            render_material_uniform_int(material, material_texture_uniform_names[material->options.uniform_format][i],
                                        RENDER_TEXTURE_SLOT_MATERIAL0 + i);
        }
    }
}

void render_material_options_init(struct render_material_options *options) {
    int i;

    options->uniform_format = RENDER_MATERIAL_TEXTURE_UNIFORM_FORMAT_INDEXED;

    for(i=0; i<RENDER_SHADER_MAX_TYPES; i++) {
        options->shaders[i] = NULL;
    }

    for(i=0; i<RENDER_MATERIAL_MAX_TEXTURES; i++) {
        options->textures[i] = NULL;
    }
}

void render_material_init(struct render_material *material, struct render_material_options *options) {
    assert(material != NULL);

    int i;

    material->options.uniform_format = options->uniform_format;

    for(i=0; i<RENDER_SHADER_MAX_TYPES; i++) {
        material->options.shaders[i] = options->shaders[i];
    }

    for(i=0; i<RENDER_MATERIAL_MAX_TEXTURES; i++) {
        material->options.textures[i] = options->textures[i];
    }

    material->gl_stages = 0;

    if(link_program(material)) {
        material->usable = true;

        glGenProgramPipelines(1, &material->gl_program_pipeline);
        glUseProgramStages(material->gl_program_pipeline, material->gl_stages, material->gl_program);

        bind_texture_uniforms(material);
    } else {
        material->usable = false;
        material->gl_program = 0;
        material->gl_program_pipeline = 0;
    }
}

void render_material_deinit(struct render_material *material) {
    assert(material != NULL);

    if(material->gl_program_pipeline != 0) {
        glBindProgramPipeline(0);
        glDeleteProgramPipelines(1, &material->gl_program_pipeline);
        material->gl_program_pipeline = 0;
    }
}

void render_material_bind(struct render_material *material) {
    if(material == NULL) { // Technically this is UB according to OpenGL, but have this for consistency
        glBindProgramPipeline(0);

        return;
    }

    if(!material->usable) {
        return;
    }

    for(int i=0; i<RENDER_MATERIAL_MAX_TEXTURES; i++) {
        if(material->options.textures[i] != NULL) {
            render_texture_bind(material->options.textures[i], RENDER_TEXTURE_SLOT_MATERIAL0 + i);
        }
    }

    // One-liner hack to get Nvidia drivers to shut up
    glBindProgramPipeline(0);

    glBindProgramPipeline(material->gl_program_pipeline);
}

static GLint get_uniform_location(struct render_material *material, const char *name) {
    return glGetUniformLocation(material->gl_program, name);
}

#define GET_UNIFORM_LOCATION GLint location = get_uniform_location(material, name); if(location == -1) return

void render_material_uniform_float(struct render_material *material, const char *name, float v0) {
    GET_UNIFORM_LOCATION;
    glProgramUniform1f(material->gl_program, location, v0);
}

void render_material_uniform_vec2(struct render_material *material, const char *name, const vec2 v0) {
    GET_UNIFORM_LOCATION;
    glProgramUniform2fv(material->gl_program, location, 1, v0);
}

void render_material_uniform_vec3(struct render_material *material, const char *name, const vec3 v0) {
    GET_UNIFORM_LOCATION;
    glProgramUniform3fv(material->gl_program, location, 1, v0);
}

void render_material_uniform_vec4(struct render_material *material, const char *name, const vec4 v0) {
    GET_UNIFORM_LOCATION;
    glProgramUniform4fv(material->gl_program, location, 1, v0);
}

void render_material_uniform_int(struct render_material *material, const char *name, int v0) {
    GET_UNIFORM_LOCATION;
    glProgramUniform1i(material->gl_program, location, v0);
}

void render_material_uniform_mat3(struct render_material *material, const char *name, const mat3 v0) {
    GET_UNIFORM_LOCATION;
    glProgramUniformMatrix3fv(material->gl_program, location, 1, GL_FALSE, (GLfloat *) v0);
}

void render_material_uniform_mat4(struct render_material *material, const char *name, const mat4 v0) {
    GET_UNIFORM_LOCATION;
    glProgramUniformMatrix4fv(material->gl_program, location, 1, GL_FALSE, (GLfloat *) v0);
}

#undef GET_UNIFORM_LOCATION

void render_material_texture_set(struct render_material *material, enum render_material_texture index,
                                 struct render_texture *texture) {
    if(texture == NULL) {
        index = 0;
    }

    material->options.textures[index] = texture;

    render_material_uniform_int(material, material_texture_uniform_names[material->options.uniform_format][index],
                                RENDER_TEXTURE_SLOT_MATERIAL0 + index);
}

void render_material_texture_bind(struct render_material *material, enum render_material_texture index) {
    // Binds a texture index (Currently this is functionally identical
    // to calling `render_texture_bind`, but is separated in case
    // later down the material needs to track more renderer state)

    if(material->options.textures[index] != NULL) {
        render_texture_bind(material->options.textures[index], RENDER_TEXTURE_SLOT_MATERIAL0 + index);
    }
}
