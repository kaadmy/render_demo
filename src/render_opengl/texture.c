
#include "context.h"
#include "glad.h"
#include "state.h"
#include "texture.h"

#include "cglm/cglm.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

static const GLenum gl_texture_units[RENDER_TEXTURE_MAX_SLOTS] = {
    GL_TEXTURE0,

    GL_TEXTURE1,
    GL_TEXTURE2,
    GL_TEXTURE3,
    GL_TEXTURE4,
    GL_TEXTURE5,
    GL_TEXTURE6,
    GL_TEXTURE7,
    GL_TEXTURE8,
    GL_TEXTURE9,
    GL_TEXTURE10,
    GL_TEXTURE11,
    GL_TEXTURE12,
    GL_TEXTURE13,
    GL_TEXTURE14,
    GL_TEXTURE15,
    GL_TEXTURE16,
    GL_TEXTURE17,
    GL_TEXTURE18,
    GL_TEXTURE19,
    GL_TEXTURE20,

    GL_TEXTURE21,
    GL_TEXTURE22,
    GL_TEXTURE23,
    GL_TEXTURE24,
    GL_TEXTURE25,
    GL_TEXTURE26,
    GL_TEXTURE27,
    GL_TEXTURE28,
    GL_TEXTURE29,
    GL_TEXTURE30,
};

static const GLenum gl_texture_internal_formats[RENDER_TEXTURE_MAX_FORMATS] = {
    GL_RED,
    GL_RG,
    GL_RGB,
    GL_RGBA,

    GL_R16F,
    GL_R32F,
    GL_RG16F,
    GL_RG32F,
    GL_RGB16F,
    GL_RGB32F,
    GL_RGBA16F,
    GL_RGBA32F,

    GL_DEPTH_COMPONENT,
    GL_DEPTH_COMPONENT16,
    GL_DEPTH_COMPONENT24,
    GL_DEPTH_COMPONENT32,
};

void render_texture_options_init(struct render_texture_options *options) {
    options->internal_format = RENDER_TEXTURE_FORMAT_R;

    options->allow_multisample = false;

    options->allow_anisotropy = false;

    options->mipmap = false;
    options->repeat = false;
}

void render_texture_init(struct render_texture *texture, struct render_texture_options *options) {
    texture->options.internal_format = options->internal_format;

    texture->options.allow_multisample = options->allow_multisample;

    texture->options.allow_anisotropy = options->allow_anisotropy;

    texture->options.mipmap = options->mipmap;
    texture->options.repeat = options->repeat;

    texture->size[0] = 64;
    texture->size[1] = 64;

    if(options->internal_format >= RENDER_TEXTURE_FORMAT_R16F &&
       options->internal_format <= RENDER_TEXTURE_FORMAT_RGBA32F) {
        texture->gl_internal_format_type = GL_FLOAT;
    } else if(options->internal_format >= RENDER_TEXTURE_FORMAT_DEPTH &&
              options->internal_format <= RENDER_TEXTURE_FORMAT_DEPTH32) {
        texture->gl_internal_format_type = GL_FLOAT;
    } else {
        texture->gl_internal_format_type = GL_UNSIGNED_BYTE;
    }

    if(options->allow_multisample && render_context_state.multisample_samples > 0) {
        texture->gl_target = GL_TEXTURE_2D_MULTISAMPLE;
    } else {
        texture->gl_target = GL_TEXTURE_2D;
    }

    glGenTextures(1, &texture->gl_texture);

    render_texture_resize(texture, texture->size);
}

void render_texture_deinit(struct render_texture *texture) {
    if(texture->gl_texture != 0) {
        glDeleteTextures(1, &texture->gl_texture);
        texture->gl_texture = 0;
    }
}

void render_texture_bind(struct render_texture *texture, enum render_texture_slot slot) {
    render_state_gl_active_texture_(gl_texture_units[slot]);

    if(texture == NULL) { // This doesn't do a whole lot, but we should handle this case anyway
        glBindTexture(texture->gl_target, 0);

        return;
    }

    glBindTexture(texture->gl_target, texture->gl_texture);
}

void render_texture_resize(struct render_texture *texture, const int *size) {
    texture->size[0] = glm_max(size[0], 1);
    texture->size[1] = glm_max(size[1], 1);

    render_state_gl_active_texture_(gl_texture_units[RENDER_TEXTURE_SLOT_TEMP]);
    glBindTexture(texture->gl_target, texture->gl_texture);

    if(texture->options.allow_multisample && render_context_state.multisample_samples > 0) {
        glTexImage2DMultisample(texture->gl_target, render_context_state.multisample_samples,
                                gl_texture_internal_formats[texture->options.internal_format],
                                texture->size[0], texture->size[1], false);
    } else {
        GLenum format = GL_RGB;

        if(texture->options.internal_format >= RENDER_TEXTURE_FORMAT_DEPTH &&
           texture->options.internal_format <= RENDER_TEXTURE_FORMAT_DEPTH32) {
            format = GL_DEPTH_COMPONENT;
        }

        glTexImage2D(texture->gl_target, 0,
                     gl_texture_internal_formats[texture->options.internal_format],
                     texture->size[0], texture->size[1], 0, format, GL_UNSIGNED_BYTE, NULL);

        glTexParameteri(texture->gl_target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(texture->gl_target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(texture->gl_target, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

#if 1
        glTexParameteri(texture->gl_target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(texture->gl_target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
#else
        glTexParameteri(texture->gl_target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(texture->gl_target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
#endif
    }
}

void render_texture_get_size(struct render_texture *texture, int *size) {
    size[0] = texture->size[0];
    size[1] = texture->size[1];
}
