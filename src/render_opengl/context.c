
#include "context.h"
#include "framebuffer.h"
#include "glad.h"

#include "common/string.h"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <stdbool.h>
#include <stdio.h>

struct render_context_state_ render_context_state;

static void display_gl_version(void) {
    printf("OpenGL vendor: %s\n", glGetString(GL_VENDOR));
    printf("OpenGL renderer: %s\n", glGetString(GL_RENDERER));
    printf("OpenGL version: %s\n", glGetString(GL_VERSION));
}

static void display_gl_extensions(void) {
    static const char ext_sf[] = "found";
    static const char ext_snf[] = "not found";

#define DISPLAY_EXTENSION(ext) printf("OpenGL extension "#ext": %s\n", common_string_display_boolean(GLAD_##ext, ext_sf, ext_snf))

    DISPLAY_EXTENSION(GL_ARB_texture_filter_anisotropic);
    DISPLAY_EXTENSION(GL_ARB_texture_non_power_of_two);

#undef DISPLAY_EXTENSION
}

static void display_gl_variables(void) {
    int var_int;
    float var_float;

#define DISPLAY_INT(var) glGetIntegerv(var, &var_int); printf("OpenGL variable "#var": %d\n", var_int)
#define DISPLAY_FLOAT(var) glGetFloatv(var, &var_float); printf("OpenGL variable "#var": %f\n", var_float)

    DISPLAY_INT(GL_MAX_ARRAY_TEXTURE_LAYERS);
    DISPLAY_INT(GL_MAX_COLOR_ATTACHMENTS);
    DISPLAY_INT(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS);
    DISPLAY_INT(GL_MAX_COMBINED_UNIFORM_BLOCKS);
    DISPLAY_INT(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS);
    DISPLAY_INT(GL_MAX_SAMPLES);
    DISPLAY_INT(GL_MAX_TEXTURE_BUFFER_SIZE);
    DISPLAY_INT(GL_MAX_TEXTURE_IMAGE_UNITS);
    DISPLAY_FLOAT(GL_MAX_TEXTURE_MAX_ANISOTROPY);
    DISPLAY_INT(GL_MAX_TEXTURE_SIZE);
    DISPLAY_INT(GL_MAX_VARYING_COMPONENTS);
    DISPLAY_INT(GL_MAX_VERTEX_ATTRIBS);
    DISPLAY_INT(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS);
    DISPLAY_INT(GL_MAX_VERTEX_UNIFORM_COMPONENTS);
    DISPLAY_INT(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT);

#undef DISPLAY_INT
#undef DISPLAY_FLOAT
}

bool render_context_init(GLFWwindow *window) {
    render_context_state.window = window;

    render_context_state.texture_anisotropy_level = 4;
    render_context_state.multisample_samples = 4;

    if(gladLoadGL((GLADloadfunc) glfwGetProcAddress) == 0) {
        printf("Failed to initialize Glad\n");
        return false;
    }

    display_gl_version();
    display_gl_extensions();
    display_gl_variables();

    glClearColor(0.5, 0.5, 0.5, 1.0);

    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    glBlendEquation(GL_FUNC_ADD);

    render_state_init();

    return true;
}

void render_context_deinit(void) {
    render_state_deinit();
}

void render_context_render_pre(void) {
    glClear(GL_COLOR_BUFFER_BIT);
}

void render_context_render_post(void) {
    glfwSwapBuffers(render_context_state.window);
}

void render_context_resize(int *window_size, int *size) {
    render_context_state.window_size[0] = window_size[0];
    render_context_state.window_size[1] = window_size[1];

    render_context_state.size[0] = size[0];
    render_context_state.size[1] = size[1];
}
