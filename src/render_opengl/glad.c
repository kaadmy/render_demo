#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "glad.h"

#ifndef GLAD_IMPL_UTIL_C_
#define GLAD_IMPL_UTIL_C_

#ifdef _MSC_VER
#define GLAD_IMPL_UTIL_SSCANF sscanf_s
#else
#define GLAD_IMPL_UTIL_SSCANF sscanf
#endif

#endif /* GLAD_IMPL_UTIL_C_ */


int GLAD_GL_VERSION_1_0 = 0;
int GLAD_GL_VERSION_1_1 = 0;
int GLAD_GL_VERSION_1_2 = 0;
int GLAD_GL_VERSION_1_3 = 0;
int GLAD_GL_VERSION_1_4 = 0;
int GLAD_GL_VERSION_1_5 = 0;
int GLAD_GL_VERSION_2_0 = 0;
int GLAD_GL_VERSION_2_1 = 0;
int GLAD_GL_VERSION_3_0 = 0;
int GLAD_GL_VERSION_3_1 = 0;
int GLAD_GL_VERSION_3_2 = 0;
int GLAD_GL_VERSION_3_3 = 0;
int GLAD_GL_VERSION_4_0 = 0;
int GLAD_GL_VERSION_4_1 = 0;
int GLAD_GL_ARB_texture_filter_anisotropic = 0;
int GLAD_GL_ARB_texture_non_power_of_two = 0;


void _pre_call_gl_callback_default(const char *name, GLADapiproc apiproc, int len_args, ...) {
    (void) len_args;

    if (apiproc == NULL) {
        fprintf(stderr, "GLAD: ERROR %s is NULL!\n", name);
        return;
    }
    if (glad_glGetError == NULL) {
        fprintf(stderr, "GLAD: ERROR glGetError is NULL!\n");
        return;
    }

    (void) glad_glGetError();
}
void _post_call_gl_callback_default(void *ret, const char *name, GLADapiproc apiproc, int len_args, ...) {
    GLenum error_code;

    (void) ret;
    (void) apiproc;
    (void) len_args;

    error_code = glad_glGetError();

    if (error_code != GL_NO_ERROR) {
        fprintf(stderr, "GLAD: ERROR %d in %s!\n", error_code, name);
    }
}

static GLADprecallback _pre_call_gl_callback = _pre_call_gl_callback_default;
void gladSetglPreCallback(GLADprecallback cb) {
    _pre_call_gl_callback = cb;
}
static GLADpostcallback _post_call_gl_callback = _post_call_gl_callback_default;
void gladSetglPostCallback(GLADpostcallback cb) {
    _post_call_gl_callback = cb;
}

PFNGLACTIVESHADERPROGRAMPROC glad_glActiveShaderProgram = NULL;
void GLAD_API_PTR glad_debug_impl_glActiveShaderProgram(GLuint arg0, GLuint arg1) {
    _pre_call_gl_callback("glActiveShaderProgram", (GLADapiproc) glActiveShaderProgram, 2, arg0, arg1);
    glActiveShaderProgram(arg0, arg1);
    _post_call_gl_callback(NULL, "glActiveShaderProgram", (GLADapiproc) glActiveShaderProgram, 2, arg0, arg1);

}
PFNGLACTIVESHADERPROGRAMPROC glad_debug_glActiveShaderProgram = glad_debug_impl_glActiveShaderProgram;
PFNGLACTIVETEXTUREPROC glad_glActiveTexture = NULL;
void GLAD_API_PTR glad_debug_impl_glActiveTexture(GLenum arg0) {
    _pre_call_gl_callback("glActiveTexture", (GLADapiproc) glActiveTexture, 1, arg0);
    glActiveTexture(arg0);
    _post_call_gl_callback(NULL, "glActiveTexture", (GLADapiproc) glActiveTexture, 1, arg0);

}
PFNGLACTIVETEXTUREPROC glad_debug_glActiveTexture = glad_debug_impl_glActiveTexture;
PFNGLATTACHSHADERPROC glad_glAttachShader = NULL;
void GLAD_API_PTR glad_debug_impl_glAttachShader(GLuint arg0, GLuint arg1) {
    _pre_call_gl_callback("glAttachShader", (GLADapiproc) glAttachShader, 2, arg0, arg1);
    glAttachShader(arg0, arg1);
    _post_call_gl_callback(NULL, "glAttachShader", (GLADapiproc) glAttachShader, 2, arg0, arg1);

}
PFNGLATTACHSHADERPROC glad_debug_glAttachShader = glad_debug_impl_glAttachShader;
PFNGLBEGINCONDITIONALRENDERPROC glad_glBeginConditionalRender = NULL;
void GLAD_API_PTR glad_debug_impl_glBeginConditionalRender(GLuint arg0, GLenum arg1) {
    _pre_call_gl_callback("glBeginConditionalRender", (GLADapiproc) glBeginConditionalRender, 2, arg0, arg1);
    glBeginConditionalRender(arg0, arg1);
    _post_call_gl_callback(NULL, "glBeginConditionalRender", (GLADapiproc) glBeginConditionalRender, 2, arg0, arg1);

}
PFNGLBEGINCONDITIONALRENDERPROC glad_debug_glBeginConditionalRender = glad_debug_impl_glBeginConditionalRender;
PFNGLBEGINQUERYPROC glad_glBeginQuery = NULL;
void GLAD_API_PTR glad_debug_impl_glBeginQuery(GLenum arg0, GLuint arg1) {
    _pre_call_gl_callback("glBeginQuery", (GLADapiproc) glBeginQuery, 2, arg0, arg1);
    glBeginQuery(arg0, arg1);
    _post_call_gl_callback(NULL, "glBeginQuery", (GLADapiproc) glBeginQuery, 2, arg0, arg1);

}
PFNGLBEGINQUERYPROC glad_debug_glBeginQuery = glad_debug_impl_glBeginQuery;
PFNGLBEGINQUERYINDEXEDPROC glad_glBeginQueryIndexed = NULL;
void GLAD_API_PTR glad_debug_impl_glBeginQueryIndexed(GLenum arg0, GLuint arg1, GLuint arg2) {
    _pre_call_gl_callback("glBeginQueryIndexed", (GLADapiproc) glBeginQueryIndexed, 3, arg0, arg1, arg2);
    glBeginQueryIndexed(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glBeginQueryIndexed", (GLADapiproc) glBeginQueryIndexed, 3, arg0, arg1, arg2);

}
PFNGLBEGINQUERYINDEXEDPROC glad_debug_glBeginQueryIndexed = glad_debug_impl_glBeginQueryIndexed;
PFNGLBEGINTRANSFORMFEEDBACKPROC glad_glBeginTransformFeedback = NULL;
void GLAD_API_PTR glad_debug_impl_glBeginTransformFeedback(GLenum arg0) {
    _pre_call_gl_callback("glBeginTransformFeedback", (GLADapiproc) glBeginTransformFeedback, 1, arg0);
    glBeginTransformFeedback(arg0);
    _post_call_gl_callback(NULL, "glBeginTransformFeedback", (GLADapiproc) glBeginTransformFeedback, 1, arg0);

}
PFNGLBEGINTRANSFORMFEEDBACKPROC glad_debug_glBeginTransformFeedback = glad_debug_impl_glBeginTransformFeedback;
PFNGLBINDATTRIBLOCATIONPROC glad_glBindAttribLocation = NULL;
void GLAD_API_PTR glad_debug_impl_glBindAttribLocation(GLuint arg0, GLuint arg1, const GLchar * arg2) {
    _pre_call_gl_callback("glBindAttribLocation", (GLADapiproc) glBindAttribLocation, 3, arg0, arg1, arg2);
    glBindAttribLocation(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glBindAttribLocation", (GLADapiproc) glBindAttribLocation, 3, arg0, arg1, arg2);

}
PFNGLBINDATTRIBLOCATIONPROC glad_debug_glBindAttribLocation = glad_debug_impl_glBindAttribLocation;
PFNGLBINDBUFFERPROC glad_glBindBuffer = NULL;
void GLAD_API_PTR glad_debug_impl_glBindBuffer(GLenum arg0, GLuint arg1) {
    _pre_call_gl_callback("glBindBuffer", (GLADapiproc) glBindBuffer, 2, arg0, arg1);
    glBindBuffer(arg0, arg1);
    _post_call_gl_callback(NULL, "glBindBuffer", (GLADapiproc) glBindBuffer, 2, arg0, arg1);

}
PFNGLBINDBUFFERPROC glad_debug_glBindBuffer = glad_debug_impl_glBindBuffer;
PFNGLBINDBUFFERBASEPROC glad_glBindBufferBase = NULL;
void GLAD_API_PTR glad_debug_impl_glBindBufferBase(GLenum arg0, GLuint arg1, GLuint arg2) {
    _pre_call_gl_callback("glBindBufferBase", (GLADapiproc) glBindBufferBase, 3, arg0, arg1, arg2);
    glBindBufferBase(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glBindBufferBase", (GLADapiproc) glBindBufferBase, 3, arg0, arg1, arg2);

}
PFNGLBINDBUFFERBASEPROC glad_debug_glBindBufferBase = glad_debug_impl_glBindBufferBase;
PFNGLBINDBUFFERRANGEPROC glad_glBindBufferRange = NULL;
void GLAD_API_PTR glad_debug_impl_glBindBufferRange(GLenum arg0, GLuint arg1, GLuint arg2, GLintptr arg3, GLsizeiptr arg4) {
    _pre_call_gl_callback("glBindBufferRange", (GLADapiproc) glBindBufferRange, 5, arg0, arg1, arg2, arg3, arg4);
    glBindBufferRange(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glBindBufferRange", (GLADapiproc) glBindBufferRange, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLBINDBUFFERRANGEPROC glad_debug_glBindBufferRange = glad_debug_impl_glBindBufferRange;
PFNGLBINDFRAGDATALOCATIONPROC glad_glBindFragDataLocation = NULL;
void GLAD_API_PTR glad_debug_impl_glBindFragDataLocation(GLuint arg0, GLuint arg1, const GLchar * arg2) {
    _pre_call_gl_callback("glBindFragDataLocation", (GLADapiproc) glBindFragDataLocation, 3, arg0, arg1, arg2);
    glBindFragDataLocation(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glBindFragDataLocation", (GLADapiproc) glBindFragDataLocation, 3, arg0, arg1, arg2);

}
PFNGLBINDFRAGDATALOCATIONPROC glad_debug_glBindFragDataLocation = glad_debug_impl_glBindFragDataLocation;
PFNGLBINDFRAGDATALOCATIONINDEXEDPROC glad_glBindFragDataLocationIndexed = NULL;
void GLAD_API_PTR glad_debug_impl_glBindFragDataLocationIndexed(GLuint arg0, GLuint arg1, GLuint arg2, const GLchar * arg3) {
    _pre_call_gl_callback("glBindFragDataLocationIndexed", (GLADapiproc) glBindFragDataLocationIndexed, 4, arg0, arg1, arg2, arg3);
    glBindFragDataLocationIndexed(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glBindFragDataLocationIndexed", (GLADapiproc) glBindFragDataLocationIndexed, 4, arg0, arg1, arg2, arg3);

}
PFNGLBINDFRAGDATALOCATIONINDEXEDPROC glad_debug_glBindFragDataLocationIndexed = glad_debug_impl_glBindFragDataLocationIndexed;
PFNGLBINDFRAMEBUFFERPROC glad_glBindFramebuffer = NULL;
void GLAD_API_PTR glad_debug_impl_glBindFramebuffer(GLenum arg0, GLuint arg1) {
    _pre_call_gl_callback("glBindFramebuffer", (GLADapiproc) glBindFramebuffer, 2, arg0, arg1);
    glBindFramebuffer(arg0, arg1);
    _post_call_gl_callback(NULL, "glBindFramebuffer", (GLADapiproc) glBindFramebuffer, 2, arg0, arg1);

}
PFNGLBINDFRAMEBUFFERPROC glad_debug_glBindFramebuffer = glad_debug_impl_glBindFramebuffer;
PFNGLBINDPROGRAMPIPELINEPROC glad_glBindProgramPipeline = NULL;
void GLAD_API_PTR glad_debug_impl_glBindProgramPipeline(GLuint arg0) {
    _pre_call_gl_callback("glBindProgramPipeline", (GLADapiproc) glBindProgramPipeline, 1, arg0);
    glBindProgramPipeline(arg0);
    _post_call_gl_callback(NULL, "glBindProgramPipeline", (GLADapiproc) glBindProgramPipeline, 1, arg0);

}
PFNGLBINDPROGRAMPIPELINEPROC glad_debug_glBindProgramPipeline = glad_debug_impl_glBindProgramPipeline;
PFNGLBINDRENDERBUFFERPROC glad_glBindRenderbuffer = NULL;
void GLAD_API_PTR glad_debug_impl_glBindRenderbuffer(GLenum arg0, GLuint arg1) {
    _pre_call_gl_callback("glBindRenderbuffer", (GLADapiproc) glBindRenderbuffer, 2, arg0, arg1);
    glBindRenderbuffer(arg0, arg1);
    _post_call_gl_callback(NULL, "glBindRenderbuffer", (GLADapiproc) glBindRenderbuffer, 2, arg0, arg1);

}
PFNGLBINDRENDERBUFFERPROC glad_debug_glBindRenderbuffer = glad_debug_impl_glBindRenderbuffer;
PFNGLBINDSAMPLERPROC glad_glBindSampler = NULL;
void GLAD_API_PTR glad_debug_impl_glBindSampler(GLuint arg0, GLuint arg1) {
    _pre_call_gl_callback("glBindSampler", (GLADapiproc) glBindSampler, 2, arg0, arg1);
    glBindSampler(arg0, arg1);
    _post_call_gl_callback(NULL, "glBindSampler", (GLADapiproc) glBindSampler, 2, arg0, arg1);

}
PFNGLBINDSAMPLERPROC glad_debug_glBindSampler = glad_debug_impl_glBindSampler;
PFNGLBINDTEXTUREPROC glad_glBindTexture = NULL;
void GLAD_API_PTR glad_debug_impl_glBindTexture(GLenum arg0, GLuint arg1) {
    _pre_call_gl_callback("glBindTexture", (GLADapiproc) glBindTexture, 2, arg0, arg1);
    glBindTexture(arg0, arg1);
    _post_call_gl_callback(NULL, "glBindTexture", (GLADapiproc) glBindTexture, 2, arg0, arg1);

}
PFNGLBINDTEXTUREPROC glad_debug_glBindTexture = glad_debug_impl_glBindTexture;
PFNGLBINDTRANSFORMFEEDBACKPROC glad_glBindTransformFeedback = NULL;
void GLAD_API_PTR glad_debug_impl_glBindTransformFeedback(GLenum arg0, GLuint arg1) {
    _pre_call_gl_callback("glBindTransformFeedback", (GLADapiproc) glBindTransformFeedback, 2, arg0, arg1);
    glBindTransformFeedback(arg0, arg1);
    _post_call_gl_callback(NULL, "glBindTransformFeedback", (GLADapiproc) glBindTransformFeedback, 2, arg0, arg1);

}
PFNGLBINDTRANSFORMFEEDBACKPROC glad_debug_glBindTransformFeedback = glad_debug_impl_glBindTransformFeedback;
PFNGLBINDVERTEXARRAYPROC glad_glBindVertexArray = NULL;
void GLAD_API_PTR glad_debug_impl_glBindVertexArray(GLuint arg0) {
    _pre_call_gl_callback("glBindVertexArray", (GLADapiproc) glBindVertexArray, 1, arg0);
    glBindVertexArray(arg0);
    _post_call_gl_callback(NULL, "glBindVertexArray", (GLADapiproc) glBindVertexArray, 1, arg0);

}
PFNGLBINDVERTEXARRAYPROC glad_debug_glBindVertexArray = glad_debug_impl_glBindVertexArray;
PFNGLBLENDCOLORPROC glad_glBlendColor = NULL;
void GLAD_API_PTR glad_debug_impl_glBlendColor(GLfloat arg0, GLfloat arg1, GLfloat arg2, GLfloat arg3) {
    _pre_call_gl_callback("glBlendColor", (GLADapiproc) glBlendColor, 4, arg0, arg1, arg2, arg3);
    glBlendColor(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glBlendColor", (GLADapiproc) glBlendColor, 4, arg0, arg1, arg2, arg3);

}
PFNGLBLENDCOLORPROC glad_debug_glBlendColor = glad_debug_impl_glBlendColor;
PFNGLBLENDEQUATIONPROC glad_glBlendEquation = NULL;
void GLAD_API_PTR glad_debug_impl_glBlendEquation(GLenum arg0) {
    _pre_call_gl_callback("glBlendEquation", (GLADapiproc) glBlendEquation, 1, arg0);
    glBlendEquation(arg0);
    _post_call_gl_callback(NULL, "glBlendEquation", (GLADapiproc) glBlendEquation, 1, arg0);

}
PFNGLBLENDEQUATIONPROC glad_debug_glBlendEquation = glad_debug_impl_glBlendEquation;
PFNGLBLENDEQUATIONSEPARATEPROC glad_glBlendEquationSeparate = NULL;
void GLAD_API_PTR glad_debug_impl_glBlendEquationSeparate(GLenum arg0, GLenum arg1) {
    _pre_call_gl_callback("glBlendEquationSeparate", (GLADapiproc) glBlendEquationSeparate, 2, arg0, arg1);
    glBlendEquationSeparate(arg0, arg1);
    _post_call_gl_callback(NULL, "glBlendEquationSeparate", (GLADapiproc) glBlendEquationSeparate, 2, arg0, arg1);

}
PFNGLBLENDEQUATIONSEPARATEPROC glad_debug_glBlendEquationSeparate = glad_debug_impl_glBlendEquationSeparate;
PFNGLBLENDEQUATIONSEPARATEIPROC glad_glBlendEquationSeparatei = NULL;
void GLAD_API_PTR glad_debug_impl_glBlendEquationSeparatei(GLuint arg0, GLenum arg1, GLenum arg2) {
    _pre_call_gl_callback("glBlendEquationSeparatei", (GLADapiproc) glBlendEquationSeparatei, 3, arg0, arg1, arg2);
    glBlendEquationSeparatei(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glBlendEquationSeparatei", (GLADapiproc) glBlendEquationSeparatei, 3, arg0, arg1, arg2);

}
PFNGLBLENDEQUATIONSEPARATEIPROC glad_debug_glBlendEquationSeparatei = glad_debug_impl_glBlendEquationSeparatei;
PFNGLBLENDEQUATIONIPROC glad_glBlendEquationi = NULL;
void GLAD_API_PTR glad_debug_impl_glBlendEquationi(GLuint arg0, GLenum arg1) {
    _pre_call_gl_callback("glBlendEquationi", (GLADapiproc) glBlendEquationi, 2, arg0, arg1);
    glBlendEquationi(arg0, arg1);
    _post_call_gl_callback(NULL, "glBlendEquationi", (GLADapiproc) glBlendEquationi, 2, arg0, arg1);

}
PFNGLBLENDEQUATIONIPROC glad_debug_glBlendEquationi = glad_debug_impl_glBlendEquationi;
PFNGLBLENDFUNCPROC glad_glBlendFunc = NULL;
void GLAD_API_PTR glad_debug_impl_glBlendFunc(GLenum arg0, GLenum arg1) {
    _pre_call_gl_callback("glBlendFunc", (GLADapiproc) glBlendFunc, 2, arg0, arg1);
    glBlendFunc(arg0, arg1);
    _post_call_gl_callback(NULL, "glBlendFunc", (GLADapiproc) glBlendFunc, 2, arg0, arg1);

}
PFNGLBLENDFUNCPROC glad_debug_glBlendFunc = glad_debug_impl_glBlendFunc;
PFNGLBLENDFUNCSEPARATEPROC glad_glBlendFuncSeparate = NULL;
void GLAD_API_PTR glad_debug_impl_glBlendFuncSeparate(GLenum arg0, GLenum arg1, GLenum arg2, GLenum arg3) {
    _pre_call_gl_callback("glBlendFuncSeparate", (GLADapiproc) glBlendFuncSeparate, 4, arg0, arg1, arg2, arg3);
    glBlendFuncSeparate(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glBlendFuncSeparate", (GLADapiproc) glBlendFuncSeparate, 4, arg0, arg1, arg2, arg3);

}
PFNGLBLENDFUNCSEPARATEPROC glad_debug_glBlendFuncSeparate = glad_debug_impl_glBlendFuncSeparate;
PFNGLBLENDFUNCSEPARATEIPROC glad_glBlendFuncSeparatei = NULL;
void GLAD_API_PTR glad_debug_impl_glBlendFuncSeparatei(GLuint arg0, GLenum arg1, GLenum arg2, GLenum arg3, GLenum arg4) {
    _pre_call_gl_callback("glBlendFuncSeparatei", (GLADapiproc) glBlendFuncSeparatei, 5, arg0, arg1, arg2, arg3, arg4);
    glBlendFuncSeparatei(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glBlendFuncSeparatei", (GLADapiproc) glBlendFuncSeparatei, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLBLENDFUNCSEPARATEIPROC glad_debug_glBlendFuncSeparatei = glad_debug_impl_glBlendFuncSeparatei;
PFNGLBLENDFUNCIPROC glad_glBlendFunci = NULL;
void GLAD_API_PTR glad_debug_impl_glBlendFunci(GLuint arg0, GLenum arg1, GLenum arg2) {
    _pre_call_gl_callback("glBlendFunci", (GLADapiproc) glBlendFunci, 3, arg0, arg1, arg2);
    glBlendFunci(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glBlendFunci", (GLADapiproc) glBlendFunci, 3, arg0, arg1, arg2);

}
PFNGLBLENDFUNCIPROC glad_debug_glBlendFunci = glad_debug_impl_glBlendFunci;
PFNGLBLITFRAMEBUFFERPROC glad_glBlitFramebuffer = NULL;
void GLAD_API_PTR glad_debug_impl_glBlitFramebuffer(GLint arg0, GLint arg1, GLint arg2, GLint arg3, GLint arg4, GLint arg5, GLint arg6, GLint arg7, GLbitfield arg8, GLenum arg9) {
    _pre_call_gl_callback("glBlitFramebuffer", (GLADapiproc) glBlitFramebuffer, 10, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
    glBlitFramebuffer(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
    _post_call_gl_callback(NULL, "glBlitFramebuffer", (GLADapiproc) glBlitFramebuffer, 10, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);

}
PFNGLBLITFRAMEBUFFERPROC glad_debug_glBlitFramebuffer = glad_debug_impl_glBlitFramebuffer;
PFNGLBUFFERDATAPROC glad_glBufferData = NULL;
void GLAD_API_PTR glad_debug_impl_glBufferData(GLenum arg0, GLsizeiptr arg1, const void * arg2, GLenum arg3) {
    _pre_call_gl_callback("glBufferData", (GLADapiproc) glBufferData, 4, arg0, arg1, arg2, arg3);
    glBufferData(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glBufferData", (GLADapiproc) glBufferData, 4, arg0, arg1, arg2, arg3);

}
PFNGLBUFFERDATAPROC glad_debug_glBufferData = glad_debug_impl_glBufferData;
PFNGLBUFFERSUBDATAPROC glad_glBufferSubData = NULL;
void GLAD_API_PTR glad_debug_impl_glBufferSubData(GLenum arg0, GLintptr arg1, GLsizeiptr arg2, const void * arg3) {
    _pre_call_gl_callback("glBufferSubData", (GLADapiproc) glBufferSubData, 4, arg0, arg1, arg2, arg3);
    glBufferSubData(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glBufferSubData", (GLADapiproc) glBufferSubData, 4, arg0, arg1, arg2, arg3);

}
PFNGLBUFFERSUBDATAPROC glad_debug_glBufferSubData = glad_debug_impl_glBufferSubData;
PFNGLCHECKFRAMEBUFFERSTATUSPROC glad_glCheckFramebufferStatus = NULL;
GLenum GLAD_API_PTR glad_debug_impl_glCheckFramebufferStatus(GLenum arg0) {
    GLenum ret;
    _pre_call_gl_callback("glCheckFramebufferStatus", (GLADapiproc) glCheckFramebufferStatus, 1, arg0);
    ret = glCheckFramebufferStatus(arg0);
    _post_call_gl_callback((void*) &ret, "glCheckFramebufferStatus", (GLADapiproc) glCheckFramebufferStatus, 1, arg0);
    return ret;
}
PFNGLCHECKFRAMEBUFFERSTATUSPROC glad_debug_glCheckFramebufferStatus = glad_debug_impl_glCheckFramebufferStatus;
PFNGLCLAMPCOLORPROC glad_glClampColor = NULL;
void GLAD_API_PTR glad_debug_impl_glClampColor(GLenum arg0, GLenum arg1) {
    _pre_call_gl_callback("glClampColor", (GLADapiproc) glClampColor, 2, arg0, arg1);
    glClampColor(arg0, arg1);
    _post_call_gl_callback(NULL, "glClampColor", (GLADapiproc) glClampColor, 2, arg0, arg1);

}
PFNGLCLAMPCOLORPROC glad_debug_glClampColor = glad_debug_impl_glClampColor;
PFNGLCLEARPROC glad_glClear = NULL;
void GLAD_API_PTR glad_debug_impl_glClear(GLbitfield arg0) {
    _pre_call_gl_callback("glClear", (GLADapiproc) glClear, 1, arg0);
    glClear(arg0);
    _post_call_gl_callback(NULL, "glClear", (GLADapiproc) glClear, 1, arg0);

}
PFNGLCLEARPROC glad_debug_glClear = glad_debug_impl_glClear;
PFNGLCLEARBUFFERFIPROC glad_glClearBufferfi = NULL;
void GLAD_API_PTR glad_debug_impl_glClearBufferfi(GLenum arg0, GLint arg1, GLfloat arg2, GLint arg3) {
    _pre_call_gl_callback("glClearBufferfi", (GLADapiproc) glClearBufferfi, 4, arg0, arg1, arg2, arg3);
    glClearBufferfi(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glClearBufferfi", (GLADapiproc) glClearBufferfi, 4, arg0, arg1, arg2, arg3);

}
PFNGLCLEARBUFFERFIPROC glad_debug_glClearBufferfi = glad_debug_impl_glClearBufferfi;
PFNGLCLEARBUFFERFVPROC glad_glClearBufferfv = NULL;
void GLAD_API_PTR glad_debug_impl_glClearBufferfv(GLenum arg0, GLint arg1, const GLfloat * arg2) {
    _pre_call_gl_callback("glClearBufferfv", (GLADapiproc) glClearBufferfv, 3, arg0, arg1, arg2);
    glClearBufferfv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glClearBufferfv", (GLADapiproc) glClearBufferfv, 3, arg0, arg1, arg2);

}
PFNGLCLEARBUFFERFVPROC glad_debug_glClearBufferfv = glad_debug_impl_glClearBufferfv;
PFNGLCLEARBUFFERIVPROC glad_glClearBufferiv = NULL;
void GLAD_API_PTR glad_debug_impl_glClearBufferiv(GLenum arg0, GLint arg1, const GLint * arg2) {
    _pre_call_gl_callback("glClearBufferiv", (GLADapiproc) glClearBufferiv, 3, arg0, arg1, arg2);
    glClearBufferiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glClearBufferiv", (GLADapiproc) glClearBufferiv, 3, arg0, arg1, arg2);

}
PFNGLCLEARBUFFERIVPROC glad_debug_glClearBufferiv = glad_debug_impl_glClearBufferiv;
PFNGLCLEARBUFFERUIVPROC glad_glClearBufferuiv = NULL;
void GLAD_API_PTR glad_debug_impl_glClearBufferuiv(GLenum arg0, GLint arg1, const GLuint * arg2) {
    _pre_call_gl_callback("glClearBufferuiv", (GLADapiproc) glClearBufferuiv, 3, arg0, arg1, arg2);
    glClearBufferuiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glClearBufferuiv", (GLADapiproc) glClearBufferuiv, 3, arg0, arg1, arg2);

}
PFNGLCLEARBUFFERUIVPROC glad_debug_glClearBufferuiv = glad_debug_impl_glClearBufferuiv;
PFNGLCLEARCOLORPROC glad_glClearColor = NULL;
void GLAD_API_PTR glad_debug_impl_glClearColor(GLfloat arg0, GLfloat arg1, GLfloat arg2, GLfloat arg3) {
    _pre_call_gl_callback("glClearColor", (GLADapiproc) glClearColor, 4, arg0, arg1, arg2, arg3);
    glClearColor(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glClearColor", (GLADapiproc) glClearColor, 4, arg0, arg1, arg2, arg3);

}
PFNGLCLEARCOLORPROC glad_debug_glClearColor = glad_debug_impl_glClearColor;
PFNGLCLEARDEPTHPROC glad_glClearDepth = NULL;
void GLAD_API_PTR glad_debug_impl_glClearDepth(GLdouble arg0) {
    _pre_call_gl_callback("glClearDepth", (GLADapiproc) glClearDepth, 1, arg0);
    glClearDepth(arg0);
    _post_call_gl_callback(NULL, "glClearDepth", (GLADapiproc) glClearDepth, 1, arg0);

}
PFNGLCLEARDEPTHPROC glad_debug_glClearDepth = glad_debug_impl_glClearDepth;
PFNGLCLEARDEPTHFPROC glad_glClearDepthf = NULL;
void GLAD_API_PTR glad_debug_impl_glClearDepthf(GLfloat arg0) {
    _pre_call_gl_callback("glClearDepthf", (GLADapiproc) glClearDepthf, 1, arg0);
    glClearDepthf(arg0);
    _post_call_gl_callback(NULL, "glClearDepthf", (GLADapiproc) glClearDepthf, 1, arg0);

}
PFNGLCLEARDEPTHFPROC glad_debug_glClearDepthf = glad_debug_impl_glClearDepthf;
PFNGLCLEARSTENCILPROC glad_glClearStencil = NULL;
void GLAD_API_PTR glad_debug_impl_glClearStencil(GLint arg0) {
    _pre_call_gl_callback("glClearStencil", (GLADapiproc) glClearStencil, 1, arg0);
    glClearStencil(arg0);
    _post_call_gl_callback(NULL, "glClearStencil", (GLADapiproc) glClearStencil, 1, arg0);

}
PFNGLCLEARSTENCILPROC glad_debug_glClearStencil = glad_debug_impl_glClearStencil;
PFNGLCLIENTWAITSYNCPROC glad_glClientWaitSync = NULL;
GLenum GLAD_API_PTR glad_debug_impl_glClientWaitSync(GLsync arg0, GLbitfield arg1, GLuint64 arg2) {
    GLenum ret;
    _pre_call_gl_callback("glClientWaitSync", (GLADapiproc) glClientWaitSync, 3, arg0, arg1, arg2);
    ret = glClientWaitSync(arg0, arg1, arg2);
    _post_call_gl_callback((void*) &ret, "glClientWaitSync", (GLADapiproc) glClientWaitSync, 3, arg0, arg1, arg2);
    return ret;
}
PFNGLCLIENTWAITSYNCPROC glad_debug_glClientWaitSync = glad_debug_impl_glClientWaitSync;
PFNGLCOLORMASKPROC glad_glColorMask = NULL;
void GLAD_API_PTR glad_debug_impl_glColorMask(GLboolean arg0, GLboolean arg1, GLboolean arg2, GLboolean arg3) {
    _pre_call_gl_callback("glColorMask", (GLADapiproc) glColorMask, 4, arg0, arg1, arg2, arg3);
    glColorMask(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glColorMask", (GLADapiproc) glColorMask, 4, arg0, arg1, arg2, arg3);

}
PFNGLCOLORMASKPROC glad_debug_glColorMask = glad_debug_impl_glColorMask;
PFNGLCOLORMASKIPROC glad_glColorMaski = NULL;
void GLAD_API_PTR glad_debug_impl_glColorMaski(GLuint arg0, GLboolean arg1, GLboolean arg2, GLboolean arg3, GLboolean arg4) {
    _pre_call_gl_callback("glColorMaski", (GLADapiproc) glColorMaski, 5, arg0, arg1, arg2, arg3, arg4);
    glColorMaski(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glColorMaski", (GLADapiproc) glColorMaski, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLCOLORMASKIPROC glad_debug_glColorMaski = glad_debug_impl_glColorMaski;
PFNGLCOMPILESHADERPROC glad_glCompileShader = NULL;
void GLAD_API_PTR glad_debug_impl_glCompileShader(GLuint arg0) {
    _pre_call_gl_callback("glCompileShader", (GLADapiproc) glCompileShader, 1, arg0);
    glCompileShader(arg0);
    _post_call_gl_callback(NULL, "glCompileShader", (GLADapiproc) glCompileShader, 1, arg0);

}
PFNGLCOMPILESHADERPROC glad_debug_glCompileShader = glad_debug_impl_glCompileShader;
PFNGLCOMPRESSEDTEXIMAGE1DPROC glad_glCompressedTexImage1D = NULL;
void GLAD_API_PTR glad_debug_impl_glCompressedTexImage1D(GLenum arg0, GLint arg1, GLenum arg2, GLsizei arg3, GLint arg4, GLsizei arg5, const void * arg6) {
    _pre_call_gl_callback("glCompressedTexImage1D", (GLADapiproc) glCompressedTexImage1D, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    glCompressedTexImage1D(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    _post_call_gl_callback(NULL, "glCompressedTexImage1D", (GLADapiproc) glCompressedTexImage1D, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);

}
PFNGLCOMPRESSEDTEXIMAGE1DPROC glad_debug_glCompressedTexImage1D = glad_debug_impl_glCompressedTexImage1D;
PFNGLCOMPRESSEDTEXIMAGE2DPROC glad_glCompressedTexImage2D = NULL;
void GLAD_API_PTR glad_debug_impl_glCompressedTexImage2D(GLenum arg0, GLint arg1, GLenum arg2, GLsizei arg3, GLsizei arg4, GLint arg5, GLsizei arg6, const void * arg7) {
    _pre_call_gl_callback("glCompressedTexImage2D", (GLADapiproc) glCompressedTexImage2D, 8, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    glCompressedTexImage2D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    _post_call_gl_callback(NULL, "glCompressedTexImage2D", (GLADapiproc) glCompressedTexImage2D, 8, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);

}
PFNGLCOMPRESSEDTEXIMAGE2DPROC glad_debug_glCompressedTexImage2D = glad_debug_impl_glCompressedTexImage2D;
PFNGLCOMPRESSEDTEXIMAGE3DPROC glad_glCompressedTexImage3D = NULL;
void GLAD_API_PTR glad_debug_impl_glCompressedTexImage3D(GLenum arg0, GLint arg1, GLenum arg2, GLsizei arg3, GLsizei arg4, GLsizei arg5, GLint arg6, GLsizei arg7, const void * arg8) {
    _pre_call_gl_callback("glCompressedTexImage3D", (GLADapiproc) glCompressedTexImage3D, 9, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    glCompressedTexImage3D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    _post_call_gl_callback(NULL, "glCompressedTexImage3D", (GLADapiproc) glCompressedTexImage3D, 9, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);

}
PFNGLCOMPRESSEDTEXIMAGE3DPROC glad_debug_glCompressedTexImage3D = glad_debug_impl_glCompressedTexImage3D;
PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC glad_glCompressedTexSubImage1D = NULL;
void GLAD_API_PTR glad_debug_impl_glCompressedTexSubImage1D(GLenum arg0, GLint arg1, GLint arg2, GLsizei arg3, GLenum arg4, GLsizei arg5, const void * arg6) {
    _pre_call_gl_callback("glCompressedTexSubImage1D", (GLADapiproc) glCompressedTexSubImage1D, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    glCompressedTexSubImage1D(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    _post_call_gl_callback(NULL, "glCompressedTexSubImage1D", (GLADapiproc) glCompressedTexSubImage1D, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);

}
PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC glad_debug_glCompressedTexSubImage1D = glad_debug_impl_glCompressedTexSubImage1D;
PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC glad_glCompressedTexSubImage2D = NULL;
void GLAD_API_PTR glad_debug_impl_glCompressedTexSubImage2D(GLenum arg0, GLint arg1, GLint arg2, GLint arg3, GLsizei arg4, GLsizei arg5, GLenum arg6, GLsizei arg7, const void * arg8) {
    _pre_call_gl_callback("glCompressedTexSubImage2D", (GLADapiproc) glCompressedTexSubImage2D, 9, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    glCompressedTexSubImage2D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    _post_call_gl_callback(NULL, "glCompressedTexSubImage2D", (GLADapiproc) glCompressedTexSubImage2D, 9, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);

}
PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC glad_debug_glCompressedTexSubImage2D = glad_debug_impl_glCompressedTexSubImage2D;
PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC glad_glCompressedTexSubImage3D = NULL;
void GLAD_API_PTR glad_debug_impl_glCompressedTexSubImage3D(GLenum arg0, GLint arg1, GLint arg2, GLint arg3, GLint arg4, GLsizei arg5, GLsizei arg6, GLsizei arg7, GLenum arg8, GLsizei arg9, const void * arg10) {
    _pre_call_gl_callback("glCompressedTexSubImage3D", (GLADapiproc) glCompressedTexSubImage3D, 11, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
    glCompressedTexSubImage3D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
    _post_call_gl_callback(NULL, "glCompressedTexSubImage3D", (GLADapiproc) glCompressedTexSubImage3D, 11, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);

}
PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC glad_debug_glCompressedTexSubImage3D = glad_debug_impl_glCompressedTexSubImage3D;
PFNGLCOPYBUFFERSUBDATAPROC glad_glCopyBufferSubData = NULL;
void GLAD_API_PTR glad_debug_impl_glCopyBufferSubData(GLenum arg0, GLenum arg1, GLintptr arg2, GLintptr arg3, GLsizeiptr arg4) {
    _pre_call_gl_callback("glCopyBufferSubData", (GLADapiproc) glCopyBufferSubData, 5, arg0, arg1, arg2, arg3, arg4);
    glCopyBufferSubData(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glCopyBufferSubData", (GLADapiproc) glCopyBufferSubData, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLCOPYBUFFERSUBDATAPROC glad_debug_glCopyBufferSubData = glad_debug_impl_glCopyBufferSubData;
PFNGLCOPYTEXIMAGE1DPROC glad_glCopyTexImage1D = NULL;
void GLAD_API_PTR glad_debug_impl_glCopyTexImage1D(GLenum arg0, GLint arg1, GLenum arg2, GLint arg3, GLint arg4, GLsizei arg5, GLint arg6) {
    _pre_call_gl_callback("glCopyTexImage1D", (GLADapiproc) glCopyTexImage1D, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    glCopyTexImage1D(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    _post_call_gl_callback(NULL, "glCopyTexImage1D", (GLADapiproc) glCopyTexImage1D, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);

}
PFNGLCOPYTEXIMAGE1DPROC glad_debug_glCopyTexImage1D = glad_debug_impl_glCopyTexImage1D;
PFNGLCOPYTEXIMAGE2DPROC glad_glCopyTexImage2D = NULL;
void GLAD_API_PTR glad_debug_impl_glCopyTexImage2D(GLenum arg0, GLint arg1, GLenum arg2, GLint arg3, GLint arg4, GLsizei arg5, GLsizei arg6, GLint arg7) {
    _pre_call_gl_callback("glCopyTexImage2D", (GLADapiproc) glCopyTexImage2D, 8, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    glCopyTexImage2D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    _post_call_gl_callback(NULL, "glCopyTexImage2D", (GLADapiproc) glCopyTexImage2D, 8, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);

}
PFNGLCOPYTEXIMAGE2DPROC glad_debug_glCopyTexImage2D = glad_debug_impl_glCopyTexImage2D;
PFNGLCOPYTEXSUBIMAGE1DPROC glad_glCopyTexSubImage1D = NULL;
void GLAD_API_PTR glad_debug_impl_glCopyTexSubImage1D(GLenum arg0, GLint arg1, GLint arg2, GLint arg3, GLint arg4, GLsizei arg5) {
    _pre_call_gl_callback("glCopyTexSubImage1D", (GLADapiproc) glCopyTexSubImage1D, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glCopyTexSubImage1D(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glCopyTexSubImage1D", (GLADapiproc) glCopyTexSubImage1D, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLCOPYTEXSUBIMAGE1DPROC glad_debug_glCopyTexSubImage1D = glad_debug_impl_glCopyTexSubImage1D;
PFNGLCOPYTEXSUBIMAGE2DPROC glad_glCopyTexSubImage2D = NULL;
void GLAD_API_PTR glad_debug_impl_glCopyTexSubImage2D(GLenum arg0, GLint arg1, GLint arg2, GLint arg3, GLint arg4, GLint arg5, GLsizei arg6, GLsizei arg7) {
    _pre_call_gl_callback("glCopyTexSubImage2D", (GLADapiproc) glCopyTexSubImage2D, 8, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    glCopyTexSubImage2D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    _post_call_gl_callback(NULL, "glCopyTexSubImage2D", (GLADapiproc) glCopyTexSubImage2D, 8, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);

}
PFNGLCOPYTEXSUBIMAGE2DPROC glad_debug_glCopyTexSubImage2D = glad_debug_impl_glCopyTexSubImage2D;
PFNGLCOPYTEXSUBIMAGE3DPROC glad_glCopyTexSubImage3D = NULL;
void GLAD_API_PTR glad_debug_impl_glCopyTexSubImage3D(GLenum arg0, GLint arg1, GLint arg2, GLint arg3, GLint arg4, GLint arg5, GLint arg6, GLsizei arg7, GLsizei arg8) {
    _pre_call_gl_callback("glCopyTexSubImage3D", (GLADapiproc) glCopyTexSubImage3D, 9, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    glCopyTexSubImage3D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    _post_call_gl_callback(NULL, "glCopyTexSubImage3D", (GLADapiproc) glCopyTexSubImage3D, 9, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);

}
PFNGLCOPYTEXSUBIMAGE3DPROC glad_debug_glCopyTexSubImage3D = glad_debug_impl_glCopyTexSubImage3D;
PFNGLCREATEPROGRAMPROC glad_glCreateProgram = NULL;
GLuint GLAD_API_PTR glad_debug_impl_glCreateProgram(void) {
    GLuint ret;
    _pre_call_gl_callback("glCreateProgram", (GLADapiproc) glCreateProgram, 0);
    ret = glCreateProgram();
    _post_call_gl_callback((void*) &ret, "glCreateProgram", (GLADapiproc) glCreateProgram, 0);
    return ret;
}
PFNGLCREATEPROGRAMPROC glad_debug_glCreateProgram = glad_debug_impl_glCreateProgram;
PFNGLCREATESHADERPROC glad_glCreateShader = NULL;
GLuint GLAD_API_PTR glad_debug_impl_glCreateShader(GLenum arg0) {
    GLuint ret;
    _pre_call_gl_callback("glCreateShader", (GLADapiproc) glCreateShader, 1, arg0);
    ret = glCreateShader(arg0);
    _post_call_gl_callback((void*) &ret, "glCreateShader", (GLADapiproc) glCreateShader, 1, arg0);
    return ret;
}
PFNGLCREATESHADERPROC glad_debug_glCreateShader = glad_debug_impl_glCreateShader;
PFNGLCREATESHADERPROGRAMVPROC glad_glCreateShaderProgramv = NULL;
GLuint GLAD_API_PTR glad_debug_impl_glCreateShaderProgramv(GLenum arg0, GLsizei arg1, const GLchar *const* arg2) {
    GLuint ret;
    _pre_call_gl_callback("glCreateShaderProgramv", (GLADapiproc) glCreateShaderProgramv, 3, arg0, arg1, arg2);
    ret = glCreateShaderProgramv(arg0, arg1, arg2);
    _post_call_gl_callback((void*) &ret, "glCreateShaderProgramv", (GLADapiproc) glCreateShaderProgramv, 3, arg0, arg1, arg2);
    return ret;
}
PFNGLCREATESHADERPROGRAMVPROC glad_debug_glCreateShaderProgramv = glad_debug_impl_glCreateShaderProgramv;
PFNGLCULLFACEPROC glad_glCullFace = NULL;
void GLAD_API_PTR glad_debug_impl_glCullFace(GLenum arg0) {
    _pre_call_gl_callback("glCullFace", (GLADapiproc) glCullFace, 1, arg0);
    glCullFace(arg0);
    _post_call_gl_callback(NULL, "glCullFace", (GLADapiproc) glCullFace, 1, arg0);

}
PFNGLCULLFACEPROC glad_debug_glCullFace = glad_debug_impl_glCullFace;
PFNGLDELETEBUFFERSPROC glad_glDeleteBuffers = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteBuffers(GLsizei arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glDeleteBuffers", (GLADapiproc) glDeleteBuffers, 2, arg0, arg1);
    glDeleteBuffers(arg0, arg1);
    _post_call_gl_callback(NULL, "glDeleteBuffers", (GLADapiproc) glDeleteBuffers, 2, arg0, arg1);

}
PFNGLDELETEBUFFERSPROC glad_debug_glDeleteBuffers = glad_debug_impl_glDeleteBuffers;
PFNGLDELETEFRAMEBUFFERSPROC glad_glDeleteFramebuffers = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteFramebuffers(GLsizei arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glDeleteFramebuffers", (GLADapiproc) glDeleteFramebuffers, 2, arg0, arg1);
    glDeleteFramebuffers(arg0, arg1);
    _post_call_gl_callback(NULL, "glDeleteFramebuffers", (GLADapiproc) glDeleteFramebuffers, 2, arg0, arg1);

}
PFNGLDELETEFRAMEBUFFERSPROC glad_debug_glDeleteFramebuffers = glad_debug_impl_glDeleteFramebuffers;
PFNGLDELETEPROGRAMPROC glad_glDeleteProgram = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteProgram(GLuint arg0) {
    _pre_call_gl_callback("glDeleteProgram", (GLADapiproc) glDeleteProgram, 1, arg0);
    glDeleteProgram(arg0);
    _post_call_gl_callback(NULL, "glDeleteProgram", (GLADapiproc) glDeleteProgram, 1, arg0);

}
PFNGLDELETEPROGRAMPROC glad_debug_glDeleteProgram = glad_debug_impl_glDeleteProgram;
PFNGLDELETEPROGRAMPIPELINESPROC glad_glDeleteProgramPipelines = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteProgramPipelines(GLsizei arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glDeleteProgramPipelines", (GLADapiproc) glDeleteProgramPipelines, 2, arg0, arg1);
    glDeleteProgramPipelines(arg0, arg1);
    _post_call_gl_callback(NULL, "glDeleteProgramPipelines", (GLADapiproc) glDeleteProgramPipelines, 2, arg0, arg1);

}
PFNGLDELETEPROGRAMPIPELINESPROC glad_debug_glDeleteProgramPipelines = glad_debug_impl_glDeleteProgramPipelines;
PFNGLDELETEQUERIESPROC glad_glDeleteQueries = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteQueries(GLsizei arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glDeleteQueries", (GLADapiproc) glDeleteQueries, 2, arg0, arg1);
    glDeleteQueries(arg0, arg1);
    _post_call_gl_callback(NULL, "glDeleteQueries", (GLADapiproc) glDeleteQueries, 2, arg0, arg1);

}
PFNGLDELETEQUERIESPROC glad_debug_glDeleteQueries = glad_debug_impl_glDeleteQueries;
PFNGLDELETERENDERBUFFERSPROC glad_glDeleteRenderbuffers = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteRenderbuffers(GLsizei arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glDeleteRenderbuffers", (GLADapiproc) glDeleteRenderbuffers, 2, arg0, arg1);
    glDeleteRenderbuffers(arg0, arg1);
    _post_call_gl_callback(NULL, "glDeleteRenderbuffers", (GLADapiproc) glDeleteRenderbuffers, 2, arg0, arg1);

}
PFNGLDELETERENDERBUFFERSPROC glad_debug_glDeleteRenderbuffers = glad_debug_impl_glDeleteRenderbuffers;
PFNGLDELETESAMPLERSPROC glad_glDeleteSamplers = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteSamplers(GLsizei arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glDeleteSamplers", (GLADapiproc) glDeleteSamplers, 2, arg0, arg1);
    glDeleteSamplers(arg0, arg1);
    _post_call_gl_callback(NULL, "glDeleteSamplers", (GLADapiproc) glDeleteSamplers, 2, arg0, arg1);

}
PFNGLDELETESAMPLERSPROC glad_debug_glDeleteSamplers = glad_debug_impl_glDeleteSamplers;
PFNGLDELETESHADERPROC glad_glDeleteShader = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteShader(GLuint arg0) {
    _pre_call_gl_callback("glDeleteShader", (GLADapiproc) glDeleteShader, 1, arg0);
    glDeleteShader(arg0);
    _post_call_gl_callback(NULL, "glDeleteShader", (GLADapiproc) glDeleteShader, 1, arg0);

}
PFNGLDELETESHADERPROC glad_debug_glDeleteShader = glad_debug_impl_glDeleteShader;
PFNGLDELETESYNCPROC glad_glDeleteSync = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteSync(GLsync arg0) {
    _pre_call_gl_callback("glDeleteSync", (GLADapiproc) glDeleteSync, 1, arg0);
    glDeleteSync(arg0);
    _post_call_gl_callback(NULL, "glDeleteSync", (GLADapiproc) glDeleteSync, 1, arg0);

}
PFNGLDELETESYNCPROC glad_debug_glDeleteSync = glad_debug_impl_glDeleteSync;
PFNGLDELETETEXTURESPROC glad_glDeleteTextures = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteTextures(GLsizei arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glDeleteTextures", (GLADapiproc) glDeleteTextures, 2, arg0, arg1);
    glDeleteTextures(arg0, arg1);
    _post_call_gl_callback(NULL, "glDeleteTextures", (GLADapiproc) glDeleteTextures, 2, arg0, arg1);

}
PFNGLDELETETEXTURESPROC glad_debug_glDeleteTextures = glad_debug_impl_glDeleteTextures;
PFNGLDELETETRANSFORMFEEDBACKSPROC glad_glDeleteTransformFeedbacks = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteTransformFeedbacks(GLsizei arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glDeleteTransformFeedbacks", (GLADapiproc) glDeleteTransformFeedbacks, 2, arg0, arg1);
    glDeleteTransformFeedbacks(arg0, arg1);
    _post_call_gl_callback(NULL, "glDeleteTransformFeedbacks", (GLADapiproc) glDeleteTransformFeedbacks, 2, arg0, arg1);

}
PFNGLDELETETRANSFORMFEEDBACKSPROC glad_debug_glDeleteTransformFeedbacks = glad_debug_impl_glDeleteTransformFeedbacks;
PFNGLDELETEVERTEXARRAYSPROC glad_glDeleteVertexArrays = NULL;
void GLAD_API_PTR glad_debug_impl_glDeleteVertexArrays(GLsizei arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glDeleteVertexArrays", (GLADapiproc) glDeleteVertexArrays, 2, arg0, arg1);
    glDeleteVertexArrays(arg0, arg1);
    _post_call_gl_callback(NULL, "glDeleteVertexArrays", (GLADapiproc) glDeleteVertexArrays, 2, arg0, arg1);

}
PFNGLDELETEVERTEXARRAYSPROC glad_debug_glDeleteVertexArrays = glad_debug_impl_glDeleteVertexArrays;
PFNGLDEPTHFUNCPROC glad_glDepthFunc = NULL;
void GLAD_API_PTR glad_debug_impl_glDepthFunc(GLenum arg0) {
    _pre_call_gl_callback("glDepthFunc", (GLADapiproc) glDepthFunc, 1, arg0);
    glDepthFunc(arg0);
    _post_call_gl_callback(NULL, "glDepthFunc", (GLADapiproc) glDepthFunc, 1, arg0);

}
PFNGLDEPTHFUNCPROC glad_debug_glDepthFunc = glad_debug_impl_glDepthFunc;
PFNGLDEPTHMASKPROC glad_glDepthMask = NULL;
void GLAD_API_PTR glad_debug_impl_glDepthMask(GLboolean arg0) {
    _pre_call_gl_callback("glDepthMask", (GLADapiproc) glDepthMask, 1, arg0);
    glDepthMask(arg0);
    _post_call_gl_callback(NULL, "glDepthMask", (GLADapiproc) glDepthMask, 1, arg0);

}
PFNGLDEPTHMASKPROC glad_debug_glDepthMask = glad_debug_impl_glDepthMask;
PFNGLDEPTHRANGEPROC glad_glDepthRange = NULL;
void GLAD_API_PTR glad_debug_impl_glDepthRange(GLdouble arg0, GLdouble arg1) {
    _pre_call_gl_callback("glDepthRange", (GLADapiproc) glDepthRange, 2, arg0, arg1);
    glDepthRange(arg0, arg1);
    _post_call_gl_callback(NULL, "glDepthRange", (GLADapiproc) glDepthRange, 2, arg0, arg1);

}
PFNGLDEPTHRANGEPROC glad_debug_glDepthRange = glad_debug_impl_glDepthRange;
PFNGLDEPTHRANGEARRAYVPROC glad_glDepthRangeArrayv = NULL;
void GLAD_API_PTR glad_debug_impl_glDepthRangeArrayv(GLuint arg0, GLsizei arg1, const GLdouble * arg2) {
    _pre_call_gl_callback("glDepthRangeArrayv", (GLADapiproc) glDepthRangeArrayv, 3, arg0, arg1, arg2);
    glDepthRangeArrayv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glDepthRangeArrayv", (GLADapiproc) glDepthRangeArrayv, 3, arg0, arg1, arg2);

}
PFNGLDEPTHRANGEARRAYVPROC glad_debug_glDepthRangeArrayv = glad_debug_impl_glDepthRangeArrayv;
PFNGLDEPTHRANGEINDEXEDPROC glad_glDepthRangeIndexed = NULL;
void GLAD_API_PTR glad_debug_impl_glDepthRangeIndexed(GLuint arg0, GLdouble arg1, GLdouble arg2) {
    _pre_call_gl_callback("glDepthRangeIndexed", (GLADapiproc) glDepthRangeIndexed, 3, arg0, arg1, arg2);
    glDepthRangeIndexed(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glDepthRangeIndexed", (GLADapiproc) glDepthRangeIndexed, 3, arg0, arg1, arg2);

}
PFNGLDEPTHRANGEINDEXEDPROC glad_debug_glDepthRangeIndexed = glad_debug_impl_glDepthRangeIndexed;
PFNGLDEPTHRANGEFPROC glad_glDepthRangef = NULL;
void GLAD_API_PTR glad_debug_impl_glDepthRangef(GLfloat arg0, GLfloat arg1) {
    _pre_call_gl_callback("glDepthRangef", (GLADapiproc) glDepthRangef, 2, arg0, arg1);
    glDepthRangef(arg0, arg1);
    _post_call_gl_callback(NULL, "glDepthRangef", (GLADapiproc) glDepthRangef, 2, arg0, arg1);

}
PFNGLDEPTHRANGEFPROC glad_debug_glDepthRangef = glad_debug_impl_glDepthRangef;
PFNGLDETACHSHADERPROC glad_glDetachShader = NULL;
void GLAD_API_PTR glad_debug_impl_glDetachShader(GLuint arg0, GLuint arg1) {
    _pre_call_gl_callback("glDetachShader", (GLADapiproc) glDetachShader, 2, arg0, arg1);
    glDetachShader(arg0, arg1);
    _post_call_gl_callback(NULL, "glDetachShader", (GLADapiproc) glDetachShader, 2, arg0, arg1);

}
PFNGLDETACHSHADERPROC glad_debug_glDetachShader = glad_debug_impl_glDetachShader;
PFNGLDISABLEPROC glad_glDisable = NULL;
void GLAD_API_PTR glad_debug_impl_glDisable(GLenum arg0) {
    _pre_call_gl_callback("glDisable", (GLADapiproc) glDisable, 1, arg0);
    glDisable(arg0);
    _post_call_gl_callback(NULL, "glDisable", (GLADapiproc) glDisable, 1, arg0);

}
PFNGLDISABLEPROC glad_debug_glDisable = glad_debug_impl_glDisable;
PFNGLDISABLEVERTEXATTRIBARRAYPROC glad_glDisableVertexAttribArray = NULL;
void GLAD_API_PTR glad_debug_impl_glDisableVertexAttribArray(GLuint arg0) {
    _pre_call_gl_callback("glDisableVertexAttribArray", (GLADapiproc) glDisableVertexAttribArray, 1, arg0);
    glDisableVertexAttribArray(arg0);
    _post_call_gl_callback(NULL, "glDisableVertexAttribArray", (GLADapiproc) glDisableVertexAttribArray, 1, arg0);

}
PFNGLDISABLEVERTEXATTRIBARRAYPROC glad_debug_glDisableVertexAttribArray = glad_debug_impl_glDisableVertexAttribArray;
PFNGLDISABLEIPROC glad_glDisablei = NULL;
void GLAD_API_PTR glad_debug_impl_glDisablei(GLenum arg0, GLuint arg1) {
    _pre_call_gl_callback("glDisablei", (GLADapiproc) glDisablei, 2, arg0, arg1);
    glDisablei(arg0, arg1);
    _post_call_gl_callback(NULL, "glDisablei", (GLADapiproc) glDisablei, 2, arg0, arg1);

}
PFNGLDISABLEIPROC glad_debug_glDisablei = glad_debug_impl_glDisablei;
PFNGLDRAWARRAYSPROC glad_glDrawArrays = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawArrays(GLenum arg0, GLint arg1, GLsizei arg2) {
    _pre_call_gl_callback("glDrawArrays", (GLADapiproc) glDrawArrays, 3, arg0, arg1, arg2);
    glDrawArrays(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glDrawArrays", (GLADapiproc) glDrawArrays, 3, arg0, arg1, arg2);

}
PFNGLDRAWARRAYSPROC glad_debug_glDrawArrays = glad_debug_impl_glDrawArrays;
PFNGLDRAWARRAYSINDIRECTPROC glad_glDrawArraysIndirect = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawArraysIndirect(GLenum arg0, const void * arg1) {
    _pre_call_gl_callback("glDrawArraysIndirect", (GLADapiproc) glDrawArraysIndirect, 2, arg0, arg1);
    glDrawArraysIndirect(arg0, arg1);
    _post_call_gl_callback(NULL, "glDrawArraysIndirect", (GLADapiproc) glDrawArraysIndirect, 2, arg0, arg1);

}
PFNGLDRAWARRAYSINDIRECTPROC glad_debug_glDrawArraysIndirect = glad_debug_impl_glDrawArraysIndirect;
PFNGLDRAWARRAYSINSTANCEDPROC glad_glDrawArraysInstanced = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawArraysInstanced(GLenum arg0, GLint arg1, GLsizei arg2, GLsizei arg3) {
    _pre_call_gl_callback("glDrawArraysInstanced", (GLADapiproc) glDrawArraysInstanced, 4, arg0, arg1, arg2, arg3);
    glDrawArraysInstanced(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glDrawArraysInstanced", (GLADapiproc) glDrawArraysInstanced, 4, arg0, arg1, arg2, arg3);

}
PFNGLDRAWARRAYSINSTANCEDPROC glad_debug_glDrawArraysInstanced = glad_debug_impl_glDrawArraysInstanced;
PFNGLDRAWBUFFERPROC glad_glDrawBuffer = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawBuffer(GLenum arg0) {
    _pre_call_gl_callback("glDrawBuffer", (GLADapiproc) glDrawBuffer, 1, arg0);
    glDrawBuffer(arg0);
    _post_call_gl_callback(NULL, "glDrawBuffer", (GLADapiproc) glDrawBuffer, 1, arg0);

}
PFNGLDRAWBUFFERPROC glad_debug_glDrawBuffer = glad_debug_impl_glDrawBuffer;
PFNGLDRAWBUFFERSPROC glad_glDrawBuffers = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawBuffers(GLsizei arg0, const GLenum * arg1) {
    _pre_call_gl_callback("glDrawBuffers", (GLADapiproc) glDrawBuffers, 2, arg0, arg1);
    glDrawBuffers(arg0, arg1);
    _post_call_gl_callback(NULL, "glDrawBuffers", (GLADapiproc) glDrawBuffers, 2, arg0, arg1);

}
PFNGLDRAWBUFFERSPROC glad_debug_glDrawBuffers = glad_debug_impl_glDrawBuffers;
PFNGLDRAWELEMENTSPROC glad_glDrawElements = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawElements(GLenum arg0, GLsizei arg1, GLenum arg2, const void * arg3) {
    _pre_call_gl_callback("glDrawElements", (GLADapiproc) glDrawElements, 4, arg0, arg1, arg2, arg3);
    glDrawElements(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glDrawElements", (GLADapiproc) glDrawElements, 4, arg0, arg1, arg2, arg3);

}
PFNGLDRAWELEMENTSPROC glad_debug_glDrawElements = glad_debug_impl_glDrawElements;
PFNGLDRAWELEMENTSBASEVERTEXPROC glad_glDrawElementsBaseVertex = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawElementsBaseVertex(GLenum arg0, GLsizei arg1, GLenum arg2, const void * arg3, GLint arg4) {
    _pre_call_gl_callback("glDrawElementsBaseVertex", (GLADapiproc) glDrawElementsBaseVertex, 5, arg0, arg1, arg2, arg3, arg4);
    glDrawElementsBaseVertex(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glDrawElementsBaseVertex", (GLADapiproc) glDrawElementsBaseVertex, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLDRAWELEMENTSBASEVERTEXPROC glad_debug_glDrawElementsBaseVertex = glad_debug_impl_glDrawElementsBaseVertex;
PFNGLDRAWELEMENTSINDIRECTPROC glad_glDrawElementsIndirect = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawElementsIndirect(GLenum arg0, GLenum arg1, const void * arg2) {
    _pre_call_gl_callback("glDrawElementsIndirect", (GLADapiproc) glDrawElementsIndirect, 3, arg0, arg1, arg2);
    glDrawElementsIndirect(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glDrawElementsIndirect", (GLADapiproc) glDrawElementsIndirect, 3, arg0, arg1, arg2);

}
PFNGLDRAWELEMENTSINDIRECTPROC glad_debug_glDrawElementsIndirect = glad_debug_impl_glDrawElementsIndirect;
PFNGLDRAWELEMENTSINSTANCEDPROC glad_glDrawElementsInstanced = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawElementsInstanced(GLenum arg0, GLsizei arg1, GLenum arg2, const void * arg3, GLsizei arg4) {
    _pre_call_gl_callback("glDrawElementsInstanced", (GLADapiproc) glDrawElementsInstanced, 5, arg0, arg1, arg2, arg3, arg4);
    glDrawElementsInstanced(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glDrawElementsInstanced", (GLADapiproc) glDrawElementsInstanced, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLDRAWELEMENTSINSTANCEDPROC glad_debug_glDrawElementsInstanced = glad_debug_impl_glDrawElementsInstanced;
PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC glad_glDrawElementsInstancedBaseVertex = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawElementsInstancedBaseVertex(GLenum arg0, GLsizei arg1, GLenum arg2, const void * arg3, GLsizei arg4, GLint arg5) {
    _pre_call_gl_callback("glDrawElementsInstancedBaseVertex", (GLADapiproc) glDrawElementsInstancedBaseVertex, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glDrawElementsInstancedBaseVertex(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glDrawElementsInstancedBaseVertex", (GLADapiproc) glDrawElementsInstancedBaseVertex, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC glad_debug_glDrawElementsInstancedBaseVertex = glad_debug_impl_glDrawElementsInstancedBaseVertex;
PFNGLDRAWRANGEELEMENTSPROC glad_glDrawRangeElements = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawRangeElements(GLenum arg0, GLuint arg1, GLuint arg2, GLsizei arg3, GLenum arg4, const void * arg5) {
    _pre_call_gl_callback("glDrawRangeElements", (GLADapiproc) glDrawRangeElements, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glDrawRangeElements(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glDrawRangeElements", (GLADapiproc) glDrawRangeElements, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLDRAWRANGEELEMENTSPROC glad_debug_glDrawRangeElements = glad_debug_impl_glDrawRangeElements;
PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC glad_glDrawRangeElementsBaseVertex = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawRangeElementsBaseVertex(GLenum arg0, GLuint arg1, GLuint arg2, GLsizei arg3, GLenum arg4, const void * arg5, GLint arg6) {
    _pre_call_gl_callback("glDrawRangeElementsBaseVertex", (GLADapiproc) glDrawRangeElementsBaseVertex, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    glDrawRangeElementsBaseVertex(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    _post_call_gl_callback(NULL, "glDrawRangeElementsBaseVertex", (GLADapiproc) glDrawRangeElementsBaseVertex, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);

}
PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC glad_debug_glDrawRangeElementsBaseVertex = glad_debug_impl_glDrawRangeElementsBaseVertex;
PFNGLDRAWTRANSFORMFEEDBACKPROC glad_glDrawTransformFeedback = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawTransformFeedback(GLenum arg0, GLuint arg1) {
    _pre_call_gl_callback("glDrawTransformFeedback", (GLADapiproc) glDrawTransformFeedback, 2, arg0, arg1);
    glDrawTransformFeedback(arg0, arg1);
    _post_call_gl_callback(NULL, "glDrawTransformFeedback", (GLADapiproc) glDrawTransformFeedback, 2, arg0, arg1);

}
PFNGLDRAWTRANSFORMFEEDBACKPROC glad_debug_glDrawTransformFeedback = glad_debug_impl_glDrawTransformFeedback;
PFNGLDRAWTRANSFORMFEEDBACKSTREAMPROC glad_glDrawTransformFeedbackStream = NULL;
void GLAD_API_PTR glad_debug_impl_glDrawTransformFeedbackStream(GLenum arg0, GLuint arg1, GLuint arg2) {
    _pre_call_gl_callback("glDrawTransformFeedbackStream", (GLADapiproc) glDrawTransformFeedbackStream, 3, arg0, arg1, arg2);
    glDrawTransformFeedbackStream(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glDrawTransformFeedbackStream", (GLADapiproc) glDrawTransformFeedbackStream, 3, arg0, arg1, arg2);

}
PFNGLDRAWTRANSFORMFEEDBACKSTREAMPROC glad_debug_glDrawTransformFeedbackStream = glad_debug_impl_glDrawTransformFeedbackStream;
PFNGLENABLEPROC glad_glEnable = NULL;
void GLAD_API_PTR glad_debug_impl_glEnable(GLenum arg0) {
    _pre_call_gl_callback("glEnable", (GLADapiproc) glEnable, 1, arg0);
    glEnable(arg0);
    _post_call_gl_callback(NULL, "glEnable", (GLADapiproc) glEnable, 1, arg0);

}
PFNGLENABLEPROC glad_debug_glEnable = glad_debug_impl_glEnable;
PFNGLENABLEVERTEXATTRIBARRAYPROC glad_glEnableVertexAttribArray = NULL;
void GLAD_API_PTR glad_debug_impl_glEnableVertexAttribArray(GLuint arg0) {
    _pre_call_gl_callback("glEnableVertexAttribArray", (GLADapiproc) glEnableVertexAttribArray, 1, arg0);
    glEnableVertexAttribArray(arg0);
    _post_call_gl_callback(NULL, "glEnableVertexAttribArray", (GLADapiproc) glEnableVertexAttribArray, 1, arg0);

}
PFNGLENABLEVERTEXATTRIBARRAYPROC glad_debug_glEnableVertexAttribArray = glad_debug_impl_glEnableVertexAttribArray;
PFNGLENABLEIPROC glad_glEnablei = NULL;
void GLAD_API_PTR glad_debug_impl_glEnablei(GLenum arg0, GLuint arg1) {
    _pre_call_gl_callback("glEnablei", (GLADapiproc) glEnablei, 2, arg0, arg1);
    glEnablei(arg0, arg1);
    _post_call_gl_callback(NULL, "glEnablei", (GLADapiproc) glEnablei, 2, arg0, arg1);

}
PFNGLENABLEIPROC glad_debug_glEnablei = glad_debug_impl_glEnablei;
PFNGLENDCONDITIONALRENDERPROC glad_glEndConditionalRender = NULL;
void GLAD_API_PTR glad_debug_impl_glEndConditionalRender(void) {
    _pre_call_gl_callback("glEndConditionalRender", (GLADapiproc) glEndConditionalRender, 0);
    glEndConditionalRender();
    _post_call_gl_callback(NULL, "glEndConditionalRender", (GLADapiproc) glEndConditionalRender, 0);

}
PFNGLENDCONDITIONALRENDERPROC glad_debug_glEndConditionalRender = glad_debug_impl_glEndConditionalRender;
PFNGLENDQUERYPROC glad_glEndQuery = NULL;
void GLAD_API_PTR glad_debug_impl_glEndQuery(GLenum arg0) {
    _pre_call_gl_callback("glEndQuery", (GLADapiproc) glEndQuery, 1, arg0);
    glEndQuery(arg0);
    _post_call_gl_callback(NULL, "glEndQuery", (GLADapiproc) glEndQuery, 1, arg0);

}
PFNGLENDQUERYPROC glad_debug_glEndQuery = glad_debug_impl_glEndQuery;
PFNGLENDQUERYINDEXEDPROC glad_glEndQueryIndexed = NULL;
void GLAD_API_PTR glad_debug_impl_glEndQueryIndexed(GLenum arg0, GLuint arg1) {
    _pre_call_gl_callback("glEndQueryIndexed", (GLADapiproc) glEndQueryIndexed, 2, arg0, arg1);
    glEndQueryIndexed(arg0, arg1);
    _post_call_gl_callback(NULL, "glEndQueryIndexed", (GLADapiproc) glEndQueryIndexed, 2, arg0, arg1);

}
PFNGLENDQUERYINDEXEDPROC glad_debug_glEndQueryIndexed = glad_debug_impl_glEndQueryIndexed;
PFNGLENDTRANSFORMFEEDBACKPROC glad_glEndTransformFeedback = NULL;
void GLAD_API_PTR glad_debug_impl_glEndTransformFeedback(void) {
    _pre_call_gl_callback("glEndTransformFeedback", (GLADapiproc) glEndTransformFeedback, 0);
    glEndTransformFeedback();
    _post_call_gl_callback(NULL, "glEndTransformFeedback", (GLADapiproc) glEndTransformFeedback, 0);

}
PFNGLENDTRANSFORMFEEDBACKPROC glad_debug_glEndTransformFeedback = glad_debug_impl_glEndTransformFeedback;
PFNGLFENCESYNCPROC glad_glFenceSync = NULL;
GLsync GLAD_API_PTR glad_debug_impl_glFenceSync(GLenum arg0, GLbitfield arg1) {
    GLsync ret;
    _pre_call_gl_callback("glFenceSync", (GLADapiproc) glFenceSync, 2, arg0, arg1);
    ret = glFenceSync(arg0, arg1);
    _post_call_gl_callback((void*) &ret, "glFenceSync", (GLADapiproc) glFenceSync, 2, arg0, arg1);
    return ret;
}
PFNGLFENCESYNCPROC glad_debug_glFenceSync = glad_debug_impl_glFenceSync;
PFNGLFINISHPROC glad_glFinish = NULL;
void GLAD_API_PTR glad_debug_impl_glFinish(void) {
    _pre_call_gl_callback("glFinish", (GLADapiproc) glFinish, 0);
    glFinish();
    _post_call_gl_callback(NULL, "glFinish", (GLADapiproc) glFinish, 0);

}
PFNGLFINISHPROC glad_debug_glFinish = glad_debug_impl_glFinish;
PFNGLFLUSHPROC glad_glFlush = NULL;
void GLAD_API_PTR glad_debug_impl_glFlush(void) {
    _pre_call_gl_callback("glFlush", (GLADapiproc) glFlush, 0);
    glFlush();
    _post_call_gl_callback(NULL, "glFlush", (GLADapiproc) glFlush, 0);

}
PFNGLFLUSHPROC glad_debug_glFlush = glad_debug_impl_glFlush;
PFNGLFLUSHMAPPEDBUFFERRANGEPROC glad_glFlushMappedBufferRange = NULL;
void GLAD_API_PTR glad_debug_impl_glFlushMappedBufferRange(GLenum arg0, GLintptr arg1, GLsizeiptr arg2) {
    _pre_call_gl_callback("glFlushMappedBufferRange", (GLADapiproc) glFlushMappedBufferRange, 3, arg0, arg1, arg2);
    glFlushMappedBufferRange(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glFlushMappedBufferRange", (GLADapiproc) glFlushMappedBufferRange, 3, arg0, arg1, arg2);

}
PFNGLFLUSHMAPPEDBUFFERRANGEPROC glad_debug_glFlushMappedBufferRange = glad_debug_impl_glFlushMappedBufferRange;
PFNGLFRAMEBUFFERRENDERBUFFERPROC glad_glFramebufferRenderbuffer = NULL;
void GLAD_API_PTR glad_debug_impl_glFramebufferRenderbuffer(GLenum arg0, GLenum arg1, GLenum arg2, GLuint arg3) {
    _pre_call_gl_callback("glFramebufferRenderbuffer", (GLADapiproc) glFramebufferRenderbuffer, 4, arg0, arg1, arg2, arg3);
    glFramebufferRenderbuffer(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glFramebufferRenderbuffer", (GLADapiproc) glFramebufferRenderbuffer, 4, arg0, arg1, arg2, arg3);

}
PFNGLFRAMEBUFFERRENDERBUFFERPROC glad_debug_glFramebufferRenderbuffer = glad_debug_impl_glFramebufferRenderbuffer;
PFNGLFRAMEBUFFERTEXTUREPROC glad_glFramebufferTexture = NULL;
void GLAD_API_PTR glad_debug_impl_glFramebufferTexture(GLenum arg0, GLenum arg1, GLuint arg2, GLint arg3) {
    _pre_call_gl_callback("glFramebufferTexture", (GLADapiproc) glFramebufferTexture, 4, arg0, arg1, arg2, arg3);
    glFramebufferTexture(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glFramebufferTexture", (GLADapiproc) glFramebufferTexture, 4, arg0, arg1, arg2, arg3);

}
PFNGLFRAMEBUFFERTEXTUREPROC glad_debug_glFramebufferTexture = glad_debug_impl_glFramebufferTexture;
PFNGLFRAMEBUFFERTEXTURE1DPROC glad_glFramebufferTexture1D = NULL;
void GLAD_API_PTR glad_debug_impl_glFramebufferTexture1D(GLenum arg0, GLenum arg1, GLenum arg2, GLuint arg3, GLint arg4) {
    _pre_call_gl_callback("glFramebufferTexture1D", (GLADapiproc) glFramebufferTexture1D, 5, arg0, arg1, arg2, arg3, arg4);
    glFramebufferTexture1D(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glFramebufferTexture1D", (GLADapiproc) glFramebufferTexture1D, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLFRAMEBUFFERTEXTURE1DPROC glad_debug_glFramebufferTexture1D = glad_debug_impl_glFramebufferTexture1D;
PFNGLFRAMEBUFFERTEXTURE2DPROC glad_glFramebufferTexture2D = NULL;
void GLAD_API_PTR glad_debug_impl_glFramebufferTexture2D(GLenum arg0, GLenum arg1, GLenum arg2, GLuint arg3, GLint arg4) {
    _pre_call_gl_callback("glFramebufferTexture2D", (GLADapiproc) glFramebufferTexture2D, 5, arg0, arg1, arg2, arg3, arg4);
    glFramebufferTexture2D(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glFramebufferTexture2D", (GLADapiproc) glFramebufferTexture2D, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLFRAMEBUFFERTEXTURE2DPROC glad_debug_glFramebufferTexture2D = glad_debug_impl_glFramebufferTexture2D;
PFNGLFRAMEBUFFERTEXTURE3DPROC glad_glFramebufferTexture3D = NULL;
void GLAD_API_PTR glad_debug_impl_glFramebufferTexture3D(GLenum arg0, GLenum arg1, GLenum arg2, GLuint arg3, GLint arg4, GLint arg5) {
    _pre_call_gl_callback("glFramebufferTexture3D", (GLADapiproc) glFramebufferTexture3D, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glFramebufferTexture3D(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glFramebufferTexture3D", (GLADapiproc) glFramebufferTexture3D, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLFRAMEBUFFERTEXTURE3DPROC glad_debug_glFramebufferTexture3D = glad_debug_impl_glFramebufferTexture3D;
PFNGLFRAMEBUFFERTEXTURELAYERPROC glad_glFramebufferTextureLayer = NULL;
void GLAD_API_PTR glad_debug_impl_glFramebufferTextureLayer(GLenum arg0, GLenum arg1, GLuint arg2, GLint arg3, GLint arg4) {
    _pre_call_gl_callback("glFramebufferTextureLayer", (GLADapiproc) glFramebufferTextureLayer, 5, arg0, arg1, arg2, arg3, arg4);
    glFramebufferTextureLayer(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glFramebufferTextureLayer", (GLADapiproc) glFramebufferTextureLayer, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLFRAMEBUFFERTEXTURELAYERPROC glad_debug_glFramebufferTextureLayer = glad_debug_impl_glFramebufferTextureLayer;
PFNGLFRONTFACEPROC glad_glFrontFace = NULL;
void GLAD_API_PTR glad_debug_impl_glFrontFace(GLenum arg0) {
    _pre_call_gl_callback("glFrontFace", (GLADapiproc) glFrontFace, 1, arg0);
    glFrontFace(arg0);
    _post_call_gl_callback(NULL, "glFrontFace", (GLADapiproc) glFrontFace, 1, arg0);

}
PFNGLFRONTFACEPROC glad_debug_glFrontFace = glad_debug_impl_glFrontFace;
PFNGLGENBUFFERSPROC glad_glGenBuffers = NULL;
void GLAD_API_PTR glad_debug_impl_glGenBuffers(GLsizei arg0, GLuint * arg1) {
    _pre_call_gl_callback("glGenBuffers", (GLADapiproc) glGenBuffers, 2, arg0, arg1);
    glGenBuffers(arg0, arg1);
    _post_call_gl_callback(NULL, "glGenBuffers", (GLADapiproc) glGenBuffers, 2, arg0, arg1);

}
PFNGLGENBUFFERSPROC glad_debug_glGenBuffers = glad_debug_impl_glGenBuffers;
PFNGLGENFRAMEBUFFERSPROC glad_glGenFramebuffers = NULL;
void GLAD_API_PTR glad_debug_impl_glGenFramebuffers(GLsizei arg0, GLuint * arg1) {
    _pre_call_gl_callback("glGenFramebuffers", (GLADapiproc) glGenFramebuffers, 2, arg0, arg1);
    glGenFramebuffers(arg0, arg1);
    _post_call_gl_callback(NULL, "glGenFramebuffers", (GLADapiproc) glGenFramebuffers, 2, arg0, arg1);

}
PFNGLGENFRAMEBUFFERSPROC glad_debug_glGenFramebuffers = glad_debug_impl_glGenFramebuffers;
PFNGLGENPROGRAMPIPELINESPROC glad_glGenProgramPipelines = NULL;
void GLAD_API_PTR glad_debug_impl_glGenProgramPipelines(GLsizei arg0, GLuint * arg1) {
    _pre_call_gl_callback("glGenProgramPipelines", (GLADapiproc) glGenProgramPipelines, 2, arg0, arg1);
    glGenProgramPipelines(arg0, arg1);
    _post_call_gl_callback(NULL, "glGenProgramPipelines", (GLADapiproc) glGenProgramPipelines, 2, arg0, arg1);

}
PFNGLGENPROGRAMPIPELINESPROC glad_debug_glGenProgramPipelines = glad_debug_impl_glGenProgramPipelines;
PFNGLGENQUERIESPROC glad_glGenQueries = NULL;
void GLAD_API_PTR glad_debug_impl_glGenQueries(GLsizei arg0, GLuint * arg1) {
    _pre_call_gl_callback("glGenQueries", (GLADapiproc) glGenQueries, 2, arg0, arg1);
    glGenQueries(arg0, arg1);
    _post_call_gl_callback(NULL, "glGenQueries", (GLADapiproc) glGenQueries, 2, arg0, arg1);

}
PFNGLGENQUERIESPROC glad_debug_glGenQueries = glad_debug_impl_glGenQueries;
PFNGLGENRENDERBUFFERSPROC glad_glGenRenderbuffers = NULL;
void GLAD_API_PTR glad_debug_impl_glGenRenderbuffers(GLsizei arg0, GLuint * arg1) {
    _pre_call_gl_callback("glGenRenderbuffers", (GLADapiproc) glGenRenderbuffers, 2, arg0, arg1);
    glGenRenderbuffers(arg0, arg1);
    _post_call_gl_callback(NULL, "glGenRenderbuffers", (GLADapiproc) glGenRenderbuffers, 2, arg0, arg1);

}
PFNGLGENRENDERBUFFERSPROC glad_debug_glGenRenderbuffers = glad_debug_impl_glGenRenderbuffers;
PFNGLGENSAMPLERSPROC glad_glGenSamplers = NULL;
void GLAD_API_PTR glad_debug_impl_glGenSamplers(GLsizei arg0, GLuint * arg1) {
    _pre_call_gl_callback("glGenSamplers", (GLADapiproc) glGenSamplers, 2, arg0, arg1);
    glGenSamplers(arg0, arg1);
    _post_call_gl_callback(NULL, "glGenSamplers", (GLADapiproc) glGenSamplers, 2, arg0, arg1);

}
PFNGLGENSAMPLERSPROC glad_debug_glGenSamplers = glad_debug_impl_glGenSamplers;
PFNGLGENTEXTURESPROC glad_glGenTextures = NULL;
void GLAD_API_PTR glad_debug_impl_glGenTextures(GLsizei arg0, GLuint * arg1) {
    _pre_call_gl_callback("glGenTextures", (GLADapiproc) glGenTextures, 2, arg0, arg1);
    glGenTextures(arg0, arg1);
    _post_call_gl_callback(NULL, "glGenTextures", (GLADapiproc) glGenTextures, 2, arg0, arg1);

}
PFNGLGENTEXTURESPROC glad_debug_glGenTextures = glad_debug_impl_glGenTextures;
PFNGLGENTRANSFORMFEEDBACKSPROC glad_glGenTransformFeedbacks = NULL;
void GLAD_API_PTR glad_debug_impl_glGenTransformFeedbacks(GLsizei arg0, GLuint * arg1) {
    _pre_call_gl_callback("glGenTransformFeedbacks", (GLADapiproc) glGenTransformFeedbacks, 2, arg0, arg1);
    glGenTransformFeedbacks(arg0, arg1);
    _post_call_gl_callback(NULL, "glGenTransformFeedbacks", (GLADapiproc) glGenTransformFeedbacks, 2, arg0, arg1);

}
PFNGLGENTRANSFORMFEEDBACKSPROC glad_debug_glGenTransformFeedbacks = glad_debug_impl_glGenTransformFeedbacks;
PFNGLGENVERTEXARRAYSPROC glad_glGenVertexArrays = NULL;
void GLAD_API_PTR glad_debug_impl_glGenVertexArrays(GLsizei arg0, GLuint * arg1) {
    _pre_call_gl_callback("glGenVertexArrays", (GLADapiproc) glGenVertexArrays, 2, arg0, arg1);
    glGenVertexArrays(arg0, arg1);
    _post_call_gl_callback(NULL, "glGenVertexArrays", (GLADapiproc) glGenVertexArrays, 2, arg0, arg1);

}
PFNGLGENVERTEXARRAYSPROC glad_debug_glGenVertexArrays = glad_debug_impl_glGenVertexArrays;
PFNGLGENERATEMIPMAPPROC glad_glGenerateMipmap = NULL;
void GLAD_API_PTR glad_debug_impl_glGenerateMipmap(GLenum arg0) {
    _pre_call_gl_callback("glGenerateMipmap", (GLADapiproc) glGenerateMipmap, 1, arg0);
    glGenerateMipmap(arg0);
    _post_call_gl_callback(NULL, "glGenerateMipmap", (GLADapiproc) glGenerateMipmap, 1, arg0);

}
PFNGLGENERATEMIPMAPPROC glad_debug_glGenerateMipmap = glad_debug_impl_glGenerateMipmap;
PFNGLGETACTIVEATTRIBPROC glad_glGetActiveAttrib = NULL;
void GLAD_API_PTR glad_debug_impl_glGetActiveAttrib(GLuint arg0, GLuint arg1, GLsizei arg2, GLsizei * arg3, GLint * arg4, GLenum * arg5, GLchar * arg6) {
    _pre_call_gl_callback("glGetActiveAttrib", (GLADapiproc) glGetActiveAttrib, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    glGetActiveAttrib(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    _post_call_gl_callback(NULL, "glGetActiveAttrib", (GLADapiproc) glGetActiveAttrib, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);

}
PFNGLGETACTIVEATTRIBPROC glad_debug_glGetActiveAttrib = glad_debug_impl_glGetActiveAttrib;
PFNGLGETACTIVESUBROUTINENAMEPROC glad_glGetActiveSubroutineName = NULL;
void GLAD_API_PTR glad_debug_impl_glGetActiveSubroutineName(GLuint arg0, GLenum arg1, GLuint arg2, GLsizei arg3, GLsizei * arg4, GLchar * arg5) {
    _pre_call_gl_callback("glGetActiveSubroutineName", (GLADapiproc) glGetActiveSubroutineName, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glGetActiveSubroutineName(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glGetActiveSubroutineName", (GLADapiproc) glGetActiveSubroutineName, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLGETACTIVESUBROUTINENAMEPROC glad_debug_glGetActiveSubroutineName = glad_debug_impl_glGetActiveSubroutineName;
PFNGLGETACTIVESUBROUTINEUNIFORMNAMEPROC glad_glGetActiveSubroutineUniformName = NULL;
void GLAD_API_PTR glad_debug_impl_glGetActiveSubroutineUniformName(GLuint arg0, GLenum arg1, GLuint arg2, GLsizei arg3, GLsizei * arg4, GLchar * arg5) {
    _pre_call_gl_callback("glGetActiveSubroutineUniformName", (GLADapiproc) glGetActiveSubroutineUniformName, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glGetActiveSubroutineUniformName(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glGetActiveSubroutineUniformName", (GLADapiproc) glGetActiveSubroutineUniformName, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLGETACTIVESUBROUTINEUNIFORMNAMEPROC glad_debug_glGetActiveSubroutineUniformName = glad_debug_impl_glGetActiveSubroutineUniformName;
PFNGLGETACTIVESUBROUTINEUNIFORMIVPROC glad_glGetActiveSubroutineUniformiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetActiveSubroutineUniformiv(GLuint arg0, GLenum arg1, GLuint arg2, GLenum arg3, GLint * arg4) {
    _pre_call_gl_callback("glGetActiveSubroutineUniformiv", (GLADapiproc) glGetActiveSubroutineUniformiv, 5, arg0, arg1, arg2, arg3, arg4);
    glGetActiveSubroutineUniformiv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glGetActiveSubroutineUniformiv", (GLADapiproc) glGetActiveSubroutineUniformiv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLGETACTIVESUBROUTINEUNIFORMIVPROC glad_debug_glGetActiveSubroutineUniformiv = glad_debug_impl_glGetActiveSubroutineUniformiv;
PFNGLGETACTIVEUNIFORMPROC glad_glGetActiveUniform = NULL;
void GLAD_API_PTR glad_debug_impl_glGetActiveUniform(GLuint arg0, GLuint arg1, GLsizei arg2, GLsizei * arg3, GLint * arg4, GLenum * arg5, GLchar * arg6) {
    _pre_call_gl_callback("glGetActiveUniform", (GLADapiproc) glGetActiveUniform, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    glGetActiveUniform(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    _post_call_gl_callback(NULL, "glGetActiveUniform", (GLADapiproc) glGetActiveUniform, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);

}
PFNGLGETACTIVEUNIFORMPROC glad_debug_glGetActiveUniform = glad_debug_impl_glGetActiveUniform;
PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC glad_glGetActiveUniformBlockName = NULL;
void GLAD_API_PTR glad_debug_impl_glGetActiveUniformBlockName(GLuint arg0, GLuint arg1, GLsizei arg2, GLsizei * arg3, GLchar * arg4) {
    _pre_call_gl_callback("glGetActiveUniformBlockName", (GLADapiproc) glGetActiveUniformBlockName, 5, arg0, arg1, arg2, arg3, arg4);
    glGetActiveUniformBlockName(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glGetActiveUniformBlockName", (GLADapiproc) glGetActiveUniformBlockName, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC glad_debug_glGetActiveUniformBlockName = glad_debug_impl_glGetActiveUniformBlockName;
PFNGLGETACTIVEUNIFORMBLOCKIVPROC glad_glGetActiveUniformBlockiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetActiveUniformBlockiv(GLuint arg0, GLuint arg1, GLenum arg2, GLint * arg3) {
    _pre_call_gl_callback("glGetActiveUniformBlockiv", (GLADapiproc) glGetActiveUniformBlockiv, 4, arg0, arg1, arg2, arg3);
    glGetActiveUniformBlockiv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetActiveUniformBlockiv", (GLADapiproc) glGetActiveUniformBlockiv, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETACTIVEUNIFORMBLOCKIVPROC glad_debug_glGetActiveUniformBlockiv = glad_debug_impl_glGetActiveUniformBlockiv;
PFNGLGETACTIVEUNIFORMNAMEPROC glad_glGetActiveUniformName = NULL;
void GLAD_API_PTR glad_debug_impl_glGetActiveUniformName(GLuint arg0, GLuint arg1, GLsizei arg2, GLsizei * arg3, GLchar * arg4) {
    _pre_call_gl_callback("glGetActiveUniformName", (GLADapiproc) glGetActiveUniformName, 5, arg0, arg1, arg2, arg3, arg4);
    glGetActiveUniformName(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glGetActiveUniformName", (GLADapiproc) glGetActiveUniformName, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLGETACTIVEUNIFORMNAMEPROC glad_debug_glGetActiveUniformName = glad_debug_impl_glGetActiveUniformName;
PFNGLGETACTIVEUNIFORMSIVPROC glad_glGetActiveUniformsiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetActiveUniformsiv(GLuint arg0, GLsizei arg1, const GLuint * arg2, GLenum arg3, GLint * arg4) {
    _pre_call_gl_callback("glGetActiveUniformsiv", (GLADapiproc) glGetActiveUniformsiv, 5, arg0, arg1, arg2, arg3, arg4);
    glGetActiveUniformsiv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glGetActiveUniformsiv", (GLADapiproc) glGetActiveUniformsiv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLGETACTIVEUNIFORMSIVPROC glad_debug_glGetActiveUniformsiv = glad_debug_impl_glGetActiveUniformsiv;
PFNGLGETATTACHEDSHADERSPROC glad_glGetAttachedShaders = NULL;
void GLAD_API_PTR glad_debug_impl_glGetAttachedShaders(GLuint arg0, GLsizei arg1, GLsizei * arg2, GLuint * arg3) {
    _pre_call_gl_callback("glGetAttachedShaders", (GLADapiproc) glGetAttachedShaders, 4, arg0, arg1, arg2, arg3);
    glGetAttachedShaders(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetAttachedShaders", (GLADapiproc) glGetAttachedShaders, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETATTACHEDSHADERSPROC glad_debug_glGetAttachedShaders = glad_debug_impl_glGetAttachedShaders;
PFNGLGETATTRIBLOCATIONPROC glad_glGetAttribLocation = NULL;
GLint GLAD_API_PTR glad_debug_impl_glGetAttribLocation(GLuint arg0, const GLchar * arg1) {
    GLint ret;
    _pre_call_gl_callback("glGetAttribLocation", (GLADapiproc) glGetAttribLocation, 2, arg0, arg1);
    ret = glGetAttribLocation(arg0, arg1);
    _post_call_gl_callback((void*) &ret, "glGetAttribLocation", (GLADapiproc) glGetAttribLocation, 2, arg0, arg1);
    return ret;
}
PFNGLGETATTRIBLOCATIONPROC glad_debug_glGetAttribLocation = glad_debug_impl_glGetAttribLocation;
PFNGLGETBOOLEANI_VPROC glad_glGetBooleani_v = NULL;
void GLAD_API_PTR glad_debug_impl_glGetBooleani_v(GLenum arg0, GLuint arg1, GLboolean * arg2) {
    _pre_call_gl_callback("glGetBooleani_v", (GLADapiproc) glGetBooleani_v, 3, arg0, arg1, arg2);
    glGetBooleani_v(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetBooleani_v", (GLADapiproc) glGetBooleani_v, 3, arg0, arg1, arg2);

}
PFNGLGETBOOLEANI_VPROC glad_debug_glGetBooleani_v = glad_debug_impl_glGetBooleani_v;
PFNGLGETBOOLEANVPROC glad_glGetBooleanv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetBooleanv(GLenum arg0, GLboolean * arg1) {
    _pre_call_gl_callback("glGetBooleanv", (GLADapiproc) glGetBooleanv, 2, arg0, arg1);
    glGetBooleanv(arg0, arg1);
    _post_call_gl_callback(NULL, "glGetBooleanv", (GLADapiproc) glGetBooleanv, 2, arg0, arg1);

}
PFNGLGETBOOLEANVPROC glad_debug_glGetBooleanv = glad_debug_impl_glGetBooleanv;
PFNGLGETBUFFERPARAMETERI64VPROC glad_glGetBufferParameteri64v = NULL;
void GLAD_API_PTR glad_debug_impl_glGetBufferParameteri64v(GLenum arg0, GLenum arg1, GLint64 * arg2) {
    _pre_call_gl_callback("glGetBufferParameteri64v", (GLADapiproc) glGetBufferParameteri64v, 3, arg0, arg1, arg2);
    glGetBufferParameteri64v(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetBufferParameteri64v", (GLADapiproc) glGetBufferParameteri64v, 3, arg0, arg1, arg2);

}
PFNGLGETBUFFERPARAMETERI64VPROC glad_debug_glGetBufferParameteri64v = glad_debug_impl_glGetBufferParameteri64v;
PFNGLGETBUFFERPARAMETERIVPROC glad_glGetBufferParameteriv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetBufferParameteriv(GLenum arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetBufferParameteriv", (GLADapiproc) glGetBufferParameteriv, 3, arg0, arg1, arg2);
    glGetBufferParameteriv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetBufferParameteriv", (GLADapiproc) glGetBufferParameteriv, 3, arg0, arg1, arg2);

}
PFNGLGETBUFFERPARAMETERIVPROC glad_debug_glGetBufferParameteriv = glad_debug_impl_glGetBufferParameteriv;
PFNGLGETBUFFERPOINTERVPROC glad_glGetBufferPointerv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetBufferPointerv(GLenum arg0, GLenum arg1, void ** arg2) {
    _pre_call_gl_callback("glGetBufferPointerv", (GLADapiproc) glGetBufferPointerv, 3, arg0, arg1, arg2);
    glGetBufferPointerv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetBufferPointerv", (GLADapiproc) glGetBufferPointerv, 3, arg0, arg1, arg2);

}
PFNGLGETBUFFERPOINTERVPROC glad_debug_glGetBufferPointerv = glad_debug_impl_glGetBufferPointerv;
PFNGLGETBUFFERSUBDATAPROC glad_glGetBufferSubData = NULL;
void GLAD_API_PTR glad_debug_impl_glGetBufferSubData(GLenum arg0, GLintptr arg1, GLsizeiptr arg2, void * arg3) {
    _pre_call_gl_callback("glGetBufferSubData", (GLADapiproc) glGetBufferSubData, 4, arg0, arg1, arg2, arg3);
    glGetBufferSubData(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetBufferSubData", (GLADapiproc) glGetBufferSubData, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETBUFFERSUBDATAPROC glad_debug_glGetBufferSubData = glad_debug_impl_glGetBufferSubData;
PFNGLGETCOMPRESSEDTEXIMAGEPROC glad_glGetCompressedTexImage = NULL;
void GLAD_API_PTR glad_debug_impl_glGetCompressedTexImage(GLenum arg0, GLint arg1, void * arg2) {
    _pre_call_gl_callback("glGetCompressedTexImage", (GLADapiproc) glGetCompressedTexImage, 3, arg0, arg1, arg2);
    glGetCompressedTexImage(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetCompressedTexImage", (GLADapiproc) glGetCompressedTexImage, 3, arg0, arg1, arg2);

}
PFNGLGETCOMPRESSEDTEXIMAGEPROC glad_debug_glGetCompressedTexImage = glad_debug_impl_glGetCompressedTexImage;
PFNGLGETDOUBLEI_VPROC glad_glGetDoublei_v = NULL;
void GLAD_API_PTR glad_debug_impl_glGetDoublei_v(GLenum arg0, GLuint arg1, GLdouble * arg2) {
    _pre_call_gl_callback("glGetDoublei_v", (GLADapiproc) glGetDoublei_v, 3, arg0, arg1, arg2);
    glGetDoublei_v(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetDoublei_v", (GLADapiproc) glGetDoublei_v, 3, arg0, arg1, arg2);

}
PFNGLGETDOUBLEI_VPROC glad_debug_glGetDoublei_v = glad_debug_impl_glGetDoublei_v;
PFNGLGETDOUBLEVPROC glad_glGetDoublev = NULL;
void GLAD_API_PTR glad_debug_impl_glGetDoublev(GLenum arg0, GLdouble * arg1) {
    _pre_call_gl_callback("glGetDoublev", (GLADapiproc) glGetDoublev, 2, arg0, arg1);
    glGetDoublev(arg0, arg1);
    _post_call_gl_callback(NULL, "glGetDoublev", (GLADapiproc) glGetDoublev, 2, arg0, arg1);

}
PFNGLGETDOUBLEVPROC glad_debug_glGetDoublev = glad_debug_impl_glGetDoublev;
PFNGLGETERRORPROC glad_glGetError = NULL;
GLenum GLAD_API_PTR glad_debug_impl_glGetError(void) {
    GLenum ret;
    _pre_call_gl_callback("glGetError", (GLADapiproc) glGetError, 0);
    ret = glGetError();
    _post_call_gl_callback((void*) &ret, "glGetError", (GLADapiproc) glGetError, 0);
    return ret;
}
PFNGLGETERRORPROC glad_debug_glGetError = glad_debug_impl_glGetError;
PFNGLGETFLOATI_VPROC glad_glGetFloati_v = NULL;
void GLAD_API_PTR glad_debug_impl_glGetFloati_v(GLenum arg0, GLuint arg1, GLfloat * arg2) {
    _pre_call_gl_callback("glGetFloati_v", (GLADapiproc) glGetFloati_v, 3, arg0, arg1, arg2);
    glGetFloati_v(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetFloati_v", (GLADapiproc) glGetFloati_v, 3, arg0, arg1, arg2);

}
PFNGLGETFLOATI_VPROC glad_debug_glGetFloati_v = glad_debug_impl_glGetFloati_v;
PFNGLGETFLOATVPROC glad_glGetFloatv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetFloatv(GLenum arg0, GLfloat * arg1) {
    _pre_call_gl_callback("glGetFloatv", (GLADapiproc) glGetFloatv, 2, arg0, arg1);
    glGetFloatv(arg0, arg1);
    _post_call_gl_callback(NULL, "glGetFloatv", (GLADapiproc) glGetFloatv, 2, arg0, arg1);

}
PFNGLGETFLOATVPROC glad_debug_glGetFloatv = glad_debug_impl_glGetFloatv;
PFNGLGETFRAGDATAINDEXPROC glad_glGetFragDataIndex = NULL;
GLint GLAD_API_PTR glad_debug_impl_glGetFragDataIndex(GLuint arg0, const GLchar * arg1) {
    GLint ret;
    _pre_call_gl_callback("glGetFragDataIndex", (GLADapiproc) glGetFragDataIndex, 2, arg0, arg1);
    ret = glGetFragDataIndex(arg0, arg1);
    _post_call_gl_callback((void*) &ret, "glGetFragDataIndex", (GLADapiproc) glGetFragDataIndex, 2, arg0, arg1);
    return ret;
}
PFNGLGETFRAGDATAINDEXPROC glad_debug_glGetFragDataIndex = glad_debug_impl_glGetFragDataIndex;
PFNGLGETFRAGDATALOCATIONPROC glad_glGetFragDataLocation = NULL;
GLint GLAD_API_PTR glad_debug_impl_glGetFragDataLocation(GLuint arg0, const GLchar * arg1) {
    GLint ret;
    _pre_call_gl_callback("glGetFragDataLocation", (GLADapiproc) glGetFragDataLocation, 2, arg0, arg1);
    ret = glGetFragDataLocation(arg0, arg1);
    _post_call_gl_callback((void*) &ret, "glGetFragDataLocation", (GLADapiproc) glGetFragDataLocation, 2, arg0, arg1);
    return ret;
}
PFNGLGETFRAGDATALOCATIONPROC glad_debug_glGetFragDataLocation = glad_debug_impl_glGetFragDataLocation;
PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC glad_glGetFramebufferAttachmentParameteriv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetFramebufferAttachmentParameteriv(GLenum arg0, GLenum arg1, GLenum arg2, GLint * arg3) {
    _pre_call_gl_callback("glGetFramebufferAttachmentParameteriv", (GLADapiproc) glGetFramebufferAttachmentParameteriv, 4, arg0, arg1, arg2, arg3);
    glGetFramebufferAttachmentParameteriv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetFramebufferAttachmentParameteriv", (GLADapiproc) glGetFramebufferAttachmentParameteriv, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC glad_debug_glGetFramebufferAttachmentParameteriv = glad_debug_impl_glGetFramebufferAttachmentParameteriv;
PFNGLGETINTEGER64I_VPROC glad_glGetInteger64i_v = NULL;
void GLAD_API_PTR glad_debug_impl_glGetInteger64i_v(GLenum arg0, GLuint arg1, GLint64 * arg2) {
    _pre_call_gl_callback("glGetInteger64i_v", (GLADapiproc) glGetInteger64i_v, 3, arg0, arg1, arg2);
    glGetInteger64i_v(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetInteger64i_v", (GLADapiproc) glGetInteger64i_v, 3, arg0, arg1, arg2);

}
PFNGLGETINTEGER64I_VPROC glad_debug_glGetInteger64i_v = glad_debug_impl_glGetInteger64i_v;
PFNGLGETINTEGER64VPROC glad_glGetInteger64v = NULL;
void GLAD_API_PTR glad_debug_impl_glGetInteger64v(GLenum arg0, GLint64 * arg1) {
    _pre_call_gl_callback("glGetInteger64v", (GLADapiproc) glGetInteger64v, 2, arg0, arg1);
    glGetInteger64v(arg0, arg1);
    _post_call_gl_callback(NULL, "glGetInteger64v", (GLADapiproc) glGetInteger64v, 2, arg0, arg1);

}
PFNGLGETINTEGER64VPROC glad_debug_glGetInteger64v = glad_debug_impl_glGetInteger64v;
PFNGLGETINTEGERI_VPROC glad_glGetIntegeri_v = NULL;
void GLAD_API_PTR glad_debug_impl_glGetIntegeri_v(GLenum arg0, GLuint arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetIntegeri_v", (GLADapiproc) glGetIntegeri_v, 3, arg0, arg1, arg2);
    glGetIntegeri_v(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetIntegeri_v", (GLADapiproc) glGetIntegeri_v, 3, arg0, arg1, arg2);

}
PFNGLGETINTEGERI_VPROC glad_debug_glGetIntegeri_v = glad_debug_impl_glGetIntegeri_v;
PFNGLGETINTEGERVPROC glad_glGetIntegerv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetIntegerv(GLenum arg0, GLint * arg1) {
    _pre_call_gl_callback("glGetIntegerv", (GLADapiproc) glGetIntegerv, 2, arg0, arg1);
    glGetIntegerv(arg0, arg1);
    _post_call_gl_callback(NULL, "glGetIntegerv", (GLADapiproc) glGetIntegerv, 2, arg0, arg1);

}
PFNGLGETINTEGERVPROC glad_debug_glGetIntegerv = glad_debug_impl_glGetIntegerv;
PFNGLGETMULTISAMPLEFVPROC glad_glGetMultisamplefv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetMultisamplefv(GLenum arg0, GLuint arg1, GLfloat * arg2) {
    _pre_call_gl_callback("glGetMultisamplefv", (GLADapiproc) glGetMultisamplefv, 3, arg0, arg1, arg2);
    glGetMultisamplefv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetMultisamplefv", (GLADapiproc) glGetMultisamplefv, 3, arg0, arg1, arg2);

}
PFNGLGETMULTISAMPLEFVPROC glad_debug_glGetMultisamplefv = glad_debug_impl_glGetMultisamplefv;
PFNGLGETPROGRAMBINARYPROC glad_glGetProgramBinary = NULL;
void GLAD_API_PTR glad_debug_impl_glGetProgramBinary(GLuint arg0, GLsizei arg1, GLsizei * arg2, GLenum * arg3, void * arg4) {
    _pre_call_gl_callback("glGetProgramBinary", (GLADapiproc) glGetProgramBinary, 5, arg0, arg1, arg2, arg3, arg4);
    glGetProgramBinary(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glGetProgramBinary", (GLADapiproc) glGetProgramBinary, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLGETPROGRAMBINARYPROC glad_debug_glGetProgramBinary = glad_debug_impl_glGetProgramBinary;
PFNGLGETPROGRAMINFOLOGPROC glad_glGetProgramInfoLog = NULL;
void GLAD_API_PTR glad_debug_impl_glGetProgramInfoLog(GLuint arg0, GLsizei arg1, GLsizei * arg2, GLchar * arg3) {
    _pre_call_gl_callback("glGetProgramInfoLog", (GLADapiproc) glGetProgramInfoLog, 4, arg0, arg1, arg2, arg3);
    glGetProgramInfoLog(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetProgramInfoLog", (GLADapiproc) glGetProgramInfoLog, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETPROGRAMINFOLOGPROC glad_debug_glGetProgramInfoLog = glad_debug_impl_glGetProgramInfoLog;
PFNGLGETPROGRAMPIPELINEINFOLOGPROC glad_glGetProgramPipelineInfoLog = NULL;
void GLAD_API_PTR glad_debug_impl_glGetProgramPipelineInfoLog(GLuint arg0, GLsizei arg1, GLsizei * arg2, GLchar * arg3) {
    _pre_call_gl_callback("glGetProgramPipelineInfoLog", (GLADapiproc) glGetProgramPipelineInfoLog, 4, arg0, arg1, arg2, arg3);
    glGetProgramPipelineInfoLog(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetProgramPipelineInfoLog", (GLADapiproc) glGetProgramPipelineInfoLog, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETPROGRAMPIPELINEINFOLOGPROC glad_debug_glGetProgramPipelineInfoLog = glad_debug_impl_glGetProgramPipelineInfoLog;
PFNGLGETPROGRAMPIPELINEIVPROC glad_glGetProgramPipelineiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetProgramPipelineiv(GLuint arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetProgramPipelineiv", (GLADapiproc) glGetProgramPipelineiv, 3, arg0, arg1, arg2);
    glGetProgramPipelineiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetProgramPipelineiv", (GLADapiproc) glGetProgramPipelineiv, 3, arg0, arg1, arg2);

}
PFNGLGETPROGRAMPIPELINEIVPROC glad_debug_glGetProgramPipelineiv = glad_debug_impl_glGetProgramPipelineiv;
PFNGLGETPROGRAMSTAGEIVPROC glad_glGetProgramStageiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetProgramStageiv(GLuint arg0, GLenum arg1, GLenum arg2, GLint * arg3) {
    _pre_call_gl_callback("glGetProgramStageiv", (GLADapiproc) glGetProgramStageiv, 4, arg0, arg1, arg2, arg3);
    glGetProgramStageiv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetProgramStageiv", (GLADapiproc) glGetProgramStageiv, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETPROGRAMSTAGEIVPROC glad_debug_glGetProgramStageiv = glad_debug_impl_glGetProgramStageiv;
PFNGLGETPROGRAMIVPROC glad_glGetProgramiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetProgramiv(GLuint arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetProgramiv", (GLADapiproc) glGetProgramiv, 3, arg0, arg1, arg2);
    glGetProgramiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetProgramiv", (GLADapiproc) glGetProgramiv, 3, arg0, arg1, arg2);

}
PFNGLGETPROGRAMIVPROC glad_debug_glGetProgramiv = glad_debug_impl_glGetProgramiv;
PFNGLGETQUERYINDEXEDIVPROC glad_glGetQueryIndexediv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetQueryIndexediv(GLenum arg0, GLuint arg1, GLenum arg2, GLint * arg3) {
    _pre_call_gl_callback("glGetQueryIndexediv", (GLADapiproc) glGetQueryIndexediv, 4, arg0, arg1, arg2, arg3);
    glGetQueryIndexediv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetQueryIndexediv", (GLADapiproc) glGetQueryIndexediv, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETQUERYINDEXEDIVPROC glad_debug_glGetQueryIndexediv = glad_debug_impl_glGetQueryIndexediv;
PFNGLGETQUERYOBJECTI64VPROC glad_glGetQueryObjecti64v = NULL;
void GLAD_API_PTR glad_debug_impl_glGetQueryObjecti64v(GLuint arg0, GLenum arg1, GLint64 * arg2) {
    _pre_call_gl_callback("glGetQueryObjecti64v", (GLADapiproc) glGetQueryObjecti64v, 3, arg0, arg1, arg2);
    glGetQueryObjecti64v(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetQueryObjecti64v", (GLADapiproc) glGetQueryObjecti64v, 3, arg0, arg1, arg2);

}
PFNGLGETQUERYOBJECTI64VPROC glad_debug_glGetQueryObjecti64v = glad_debug_impl_glGetQueryObjecti64v;
PFNGLGETQUERYOBJECTIVPROC glad_glGetQueryObjectiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetQueryObjectiv(GLuint arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetQueryObjectiv", (GLADapiproc) glGetQueryObjectiv, 3, arg0, arg1, arg2);
    glGetQueryObjectiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetQueryObjectiv", (GLADapiproc) glGetQueryObjectiv, 3, arg0, arg1, arg2);

}
PFNGLGETQUERYOBJECTIVPROC glad_debug_glGetQueryObjectiv = glad_debug_impl_glGetQueryObjectiv;
PFNGLGETQUERYOBJECTUI64VPROC glad_glGetQueryObjectui64v = NULL;
void GLAD_API_PTR glad_debug_impl_glGetQueryObjectui64v(GLuint arg0, GLenum arg1, GLuint64 * arg2) {
    _pre_call_gl_callback("glGetQueryObjectui64v", (GLADapiproc) glGetQueryObjectui64v, 3, arg0, arg1, arg2);
    glGetQueryObjectui64v(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetQueryObjectui64v", (GLADapiproc) glGetQueryObjectui64v, 3, arg0, arg1, arg2);

}
PFNGLGETQUERYOBJECTUI64VPROC glad_debug_glGetQueryObjectui64v = glad_debug_impl_glGetQueryObjectui64v;
PFNGLGETQUERYOBJECTUIVPROC glad_glGetQueryObjectuiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetQueryObjectuiv(GLuint arg0, GLenum arg1, GLuint * arg2) {
    _pre_call_gl_callback("glGetQueryObjectuiv", (GLADapiproc) glGetQueryObjectuiv, 3, arg0, arg1, arg2);
    glGetQueryObjectuiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetQueryObjectuiv", (GLADapiproc) glGetQueryObjectuiv, 3, arg0, arg1, arg2);

}
PFNGLGETQUERYOBJECTUIVPROC glad_debug_glGetQueryObjectuiv = glad_debug_impl_glGetQueryObjectuiv;
PFNGLGETQUERYIVPROC glad_glGetQueryiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetQueryiv(GLenum arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetQueryiv", (GLADapiproc) glGetQueryiv, 3, arg0, arg1, arg2);
    glGetQueryiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetQueryiv", (GLADapiproc) glGetQueryiv, 3, arg0, arg1, arg2);

}
PFNGLGETQUERYIVPROC glad_debug_glGetQueryiv = glad_debug_impl_glGetQueryiv;
PFNGLGETRENDERBUFFERPARAMETERIVPROC glad_glGetRenderbufferParameteriv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetRenderbufferParameteriv(GLenum arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetRenderbufferParameteriv", (GLADapiproc) glGetRenderbufferParameteriv, 3, arg0, arg1, arg2);
    glGetRenderbufferParameteriv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetRenderbufferParameteriv", (GLADapiproc) glGetRenderbufferParameteriv, 3, arg0, arg1, arg2);

}
PFNGLGETRENDERBUFFERPARAMETERIVPROC glad_debug_glGetRenderbufferParameteriv = glad_debug_impl_glGetRenderbufferParameteriv;
PFNGLGETSAMPLERPARAMETERIIVPROC glad_glGetSamplerParameterIiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetSamplerParameterIiv(GLuint arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetSamplerParameterIiv", (GLADapiproc) glGetSamplerParameterIiv, 3, arg0, arg1, arg2);
    glGetSamplerParameterIiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetSamplerParameterIiv", (GLADapiproc) glGetSamplerParameterIiv, 3, arg0, arg1, arg2);

}
PFNGLGETSAMPLERPARAMETERIIVPROC glad_debug_glGetSamplerParameterIiv = glad_debug_impl_glGetSamplerParameterIiv;
PFNGLGETSAMPLERPARAMETERIUIVPROC glad_glGetSamplerParameterIuiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetSamplerParameterIuiv(GLuint arg0, GLenum arg1, GLuint * arg2) {
    _pre_call_gl_callback("glGetSamplerParameterIuiv", (GLADapiproc) glGetSamplerParameterIuiv, 3, arg0, arg1, arg2);
    glGetSamplerParameterIuiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetSamplerParameterIuiv", (GLADapiproc) glGetSamplerParameterIuiv, 3, arg0, arg1, arg2);

}
PFNGLGETSAMPLERPARAMETERIUIVPROC glad_debug_glGetSamplerParameterIuiv = glad_debug_impl_glGetSamplerParameterIuiv;
PFNGLGETSAMPLERPARAMETERFVPROC glad_glGetSamplerParameterfv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetSamplerParameterfv(GLuint arg0, GLenum arg1, GLfloat * arg2) {
    _pre_call_gl_callback("glGetSamplerParameterfv", (GLADapiproc) glGetSamplerParameterfv, 3, arg0, arg1, arg2);
    glGetSamplerParameterfv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetSamplerParameterfv", (GLADapiproc) glGetSamplerParameterfv, 3, arg0, arg1, arg2);

}
PFNGLGETSAMPLERPARAMETERFVPROC glad_debug_glGetSamplerParameterfv = glad_debug_impl_glGetSamplerParameterfv;
PFNGLGETSAMPLERPARAMETERIVPROC glad_glGetSamplerParameteriv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetSamplerParameteriv(GLuint arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetSamplerParameteriv", (GLADapiproc) glGetSamplerParameteriv, 3, arg0, arg1, arg2);
    glGetSamplerParameteriv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetSamplerParameteriv", (GLADapiproc) glGetSamplerParameteriv, 3, arg0, arg1, arg2);

}
PFNGLGETSAMPLERPARAMETERIVPROC glad_debug_glGetSamplerParameteriv = glad_debug_impl_glGetSamplerParameteriv;
PFNGLGETSHADERINFOLOGPROC glad_glGetShaderInfoLog = NULL;
void GLAD_API_PTR glad_debug_impl_glGetShaderInfoLog(GLuint arg0, GLsizei arg1, GLsizei * arg2, GLchar * arg3) {
    _pre_call_gl_callback("glGetShaderInfoLog", (GLADapiproc) glGetShaderInfoLog, 4, arg0, arg1, arg2, arg3);
    glGetShaderInfoLog(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetShaderInfoLog", (GLADapiproc) glGetShaderInfoLog, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETSHADERINFOLOGPROC glad_debug_glGetShaderInfoLog = glad_debug_impl_glGetShaderInfoLog;
PFNGLGETSHADERPRECISIONFORMATPROC glad_glGetShaderPrecisionFormat = NULL;
void GLAD_API_PTR glad_debug_impl_glGetShaderPrecisionFormat(GLenum arg0, GLenum arg1, GLint * arg2, GLint * arg3) {
    _pre_call_gl_callback("glGetShaderPrecisionFormat", (GLADapiproc) glGetShaderPrecisionFormat, 4, arg0, arg1, arg2, arg3);
    glGetShaderPrecisionFormat(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetShaderPrecisionFormat", (GLADapiproc) glGetShaderPrecisionFormat, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETSHADERPRECISIONFORMATPROC glad_debug_glGetShaderPrecisionFormat = glad_debug_impl_glGetShaderPrecisionFormat;
PFNGLGETSHADERSOURCEPROC glad_glGetShaderSource = NULL;
void GLAD_API_PTR glad_debug_impl_glGetShaderSource(GLuint arg0, GLsizei arg1, GLsizei * arg2, GLchar * arg3) {
    _pre_call_gl_callback("glGetShaderSource", (GLADapiproc) glGetShaderSource, 4, arg0, arg1, arg2, arg3);
    glGetShaderSource(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetShaderSource", (GLADapiproc) glGetShaderSource, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETSHADERSOURCEPROC glad_debug_glGetShaderSource = glad_debug_impl_glGetShaderSource;
PFNGLGETSHADERIVPROC glad_glGetShaderiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetShaderiv(GLuint arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetShaderiv", (GLADapiproc) glGetShaderiv, 3, arg0, arg1, arg2);
    glGetShaderiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetShaderiv", (GLADapiproc) glGetShaderiv, 3, arg0, arg1, arg2);

}
PFNGLGETSHADERIVPROC glad_debug_glGetShaderiv = glad_debug_impl_glGetShaderiv;
PFNGLGETSTRINGPROC glad_glGetString = NULL;
const GLubyte * GLAD_API_PTR glad_debug_impl_glGetString(GLenum arg0) {
    const GLubyte * ret;
    _pre_call_gl_callback("glGetString", (GLADapiproc) glGetString, 1, arg0);
    ret = glGetString(arg0);
    _post_call_gl_callback((void*) &ret, "glGetString", (GLADapiproc) glGetString, 1, arg0);
    return ret;
}
PFNGLGETSTRINGPROC glad_debug_glGetString = glad_debug_impl_glGetString;
PFNGLGETSTRINGIPROC glad_glGetStringi = NULL;
const GLubyte * GLAD_API_PTR glad_debug_impl_glGetStringi(GLenum arg0, GLuint arg1) {
    const GLubyte * ret;
    _pre_call_gl_callback("glGetStringi", (GLADapiproc) glGetStringi, 2, arg0, arg1);
    ret = glGetStringi(arg0, arg1);
    _post_call_gl_callback((void*) &ret, "glGetStringi", (GLADapiproc) glGetStringi, 2, arg0, arg1);
    return ret;
}
PFNGLGETSTRINGIPROC glad_debug_glGetStringi = glad_debug_impl_glGetStringi;
PFNGLGETSUBROUTINEINDEXPROC glad_glGetSubroutineIndex = NULL;
GLuint GLAD_API_PTR glad_debug_impl_glGetSubroutineIndex(GLuint arg0, GLenum arg1, const GLchar * arg2) {
    GLuint ret;
    _pre_call_gl_callback("glGetSubroutineIndex", (GLADapiproc) glGetSubroutineIndex, 3, arg0, arg1, arg2);
    ret = glGetSubroutineIndex(arg0, arg1, arg2);
    _post_call_gl_callback((void*) &ret, "glGetSubroutineIndex", (GLADapiproc) glGetSubroutineIndex, 3, arg0, arg1, arg2);
    return ret;
}
PFNGLGETSUBROUTINEINDEXPROC glad_debug_glGetSubroutineIndex = glad_debug_impl_glGetSubroutineIndex;
PFNGLGETSUBROUTINEUNIFORMLOCATIONPROC glad_glGetSubroutineUniformLocation = NULL;
GLint GLAD_API_PTR glad_debug_impl_glGetSubroutineUniformLocation(GLuint arg0, GLenum arg1, const GLchar * arg2) {
    GLint ret;
    _pre_call_gl_callback("glGetSubroutineUniformLocation", (GLADapiproc) glGetSubroutineUniformLocation, 3, arg0, arg1, arg2);
    ret = glGetSubroutineUniformLocation(arg0, arg1, arg2);
    _post_call_gl_callback((void*) &ret, "glGetSubroutineUniformLocation", (GLADapiproc) glGetSubroutineUniformLocation, 3, arg0, arg1, arg2);
    return ret;
}
PFNGLGETSUBROUTINEUNIFORMLOCATIONPROC glad_debug_glGetSubroutineUniformLocation = glad_debug_impl_glGetSubroutineUniformLocation;
PFNGLGETSYNCIVPROC glad_glGetSynciv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetSynciv(GLsync arg0, GLenum arg1, GLsizei arg2, GLsizei * arg3, GLint * arg4) {
    _pre_call_gl_callback("glGetSynciv", (GLADapiproc) glGetSynciv, 5, arg0, arg1, arg2, arg3, arg4);
    glGetSynciv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glGetSynciv", (GLADapiproc) glGetSynciv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLGETSYNCIVPROC glad_debug_glGetSynciv = glad_debug_impl_glGetSynciv;
PFNGLGETTEXIMAGEPROC glad_glGetTexImage = NULL;
void GLAD_API_PTR glad_debug_impl_glGetTexImage(GLenum arg0, GLint arg1, GLenum arg2, GLenum arg3, void * arg4) {
    _pre_call_gl_callback("glGetTexImage", (GLADapiproc) glGetTexImage, 5, arg0, arg1, arg2, arg3, arg4);
    glGetTexImage(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glGetTexImage", (GLADapiproc) glGetTexImage, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLGETTEXIMAGEPROC glad_debug_glGetTexImage = glad_debug_impl_glGetTexImage;
PFNGLGETTEXLEVELPARAMETERFVPROC glad_glGetTexLevelParameterfv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetTexLevelParameterfv(GLenum arg0, GLint arg1, GLenum arg2, GLfloat * arg3) {
    _pre_call_gl_callback("glGetTexLevelParameterfv", (GLADapiproc) glGetTexLevelParameterfv, 4, arg0, arg1, arg2, arg3);
    glGetTexLevelParameterfv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetTexLevelParameterfv", (GLADapiproc) glGetTexLevelParameterfv, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETTEXLEVELPARAMETERFVPROC glad_debug_glGetTexLevelParameterfv = glad_debug_impl_glGetTexLevelParameterfv;
PFNGLGETTEXLEVELPARAMETERIVPROC glad_glGetTexLevelParameteriv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetTexLevelParameteriv(GLenum arg0, GLint arg1, GLenum arg2, GLint * arg3) {
    _pre_call_gl_callback("glGetTexLevelParameteriv", (GLADapiproc) glGetTexLevelParameteriv, 4, arg0, arg1, arg2, arg3);
    glGetTexLevelParameteriv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetTexLevelParameteriv", (GLADapiproc) glGetTexLevelParameteriv, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETTEXLEVELPARAMETERIVPROC glad_debug_glGetTexLevelParameteriv = glad_debug_impl_glGetTexLevelParameteriv;
PFNGLGETTEXPARAMETERIIVPROC glad_glGetTexParameterIiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetTexParameterIiv(GLenum arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetTexParameterIiv", (GLADapiproc) glGetTexParameterIiv, 3, arg0, arg1, arg2);
    glGetTexParameterIiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetTexParameterIiv", (GLADapiproc) glGetTexParameterIiv, 3, arg0, arg1, arg2);

}
PFNGLGETTEXPARAMETERIIVPROC glad_debug_glGetTexParameterIiv = glad_debug_impl_glGetTexParameterIiv;
PFNGLGETTEXPARAMETERIUIVPROC glad_glGetTexParameterIuiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetTexParameterIuiv(GLenum arg0, GLenum arg1, GLuint * arg2) {
    _pre_call_gl_callback("glGetTexParameterIuiv", (GLADapiproc) glGetTexParameterIuiv, 3, arg0, arg1, arg2);
    glGetTexParameterIuiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetTexParameterIuiv", (GLADapiproc) glGetTexParameterIuiv, 3, arg0, arg1, arg2);

}
PFNGLGETTEXPARAMETERIUIVPROC glad_debug_glGetTexParameterIuiv = glad_debug_impl_glGetTexParameterIuiv;
PFNGLGETTEXPARAMETERFVPROC glad_glGetTexParameterfv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetTexParameterfv(GLenum arg0, GLenum arg1, GLfloat * arg2) {
    _pre_call_gl_callback("glGetTexParameterfv", (GLADapiproc) glGetTexParameterfv, 3, arg0, arg1, arg2);
    glGetTexParameterfv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetTexParameterfv", (GLADapiproc) glGetTexParameterfv, 3, arg0, arg1, arg2);

}
PFNGLGETTEXPARAMETERFVPROC glad_debug_glGetTexParameterfv = glad_debug_impl_glGetTexParameterfv;
PFNGLGETTEXPARAMETERIVPROC glad_glGetTexParameteriv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetTexParameteriv(GLenum arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetTexParameteriv", (GLADapiproc) glGetTexParameteriv, 3, arg0, arg1, arg2);
    glGetTexParameteriv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetTexParameteriv", (GLADapiproc) glGetTexParameteriv, 3, arg0, arg1, arg2);

}
PFNGLGETTEXPARAMETERIVPROC glad_debug_glGetTexParameteriv = glad_debug_impl_glGetTexParameteriv;
PFNGLGETTRANSFORMFEEDBACKVARYINGPROC glad_glGetTransformFeedbackVarying = NULL;
void GLAD_API_PTR glad_debug_impl_glGetTransformFeedbackVarying(GLuint arg0, GLuint arg1, GLsizei arg2, GLsizei * arg3, GLsizei * arg4, GLenum * arg5, GLchar * arg6) {
    _pre_call_gl_callback("glGetTransformFeedbackVarying", (GLADapiproc) glGetTransformFeedbackVarying, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    glGetTransformFeedbackVarying(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    _post_call_gl_callback(NULL, "glGetTransformFeedbackVarying", (GLADapiproc) glGetTransformFeedbackVarying, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);

}
PFNGLGETTRANSFORMFEEDBACKVARYINGPROC glad_debug_glGetTransformFeedbackVarying = glad_debug_impl_glGetTransformFeedbackVarying;
PFNGLGETUNIFORMBLOCKINDEXPROC glad_glGetUniformBlockIndex = NULL;
GLuint GLAD_API_PTR glad_debug_impl_glGetUniformBlockIndex(GLuint arg0, const GLchar * arg1) {
    GLuint ret;
    _pre_call_gl_callback("glGetUniformBlockIndex", (GLADapiproc) glGetUniformBlockIndex, 2, arg0, arg1);
    ret = glGetUniformBlockIndex(arg0, arg1);
    _post_call_gl_callback((void*) &ret, "glGetUniformBlockIndex", (GLADapiproc) glGetUniformBlockIndex, 2, arg0, arg1);
    return ret;
}
PFNGLGETUNIFORMBLOCKINDEXPROC glad_debug_glGetUniformBlockIndex = glad_debug_impl_glGetUniformBlockIndex;
PFNGLGETUNIFORMINDICESPROC glad_glGetUniformIndices = NULL;
void GLAD_API_PTR glad_debug_impl_glGetUniformIndices(GLuint arg0, GLsizei arg1, const GLchar *const* arg2, GLuint * arg3) {
    _pre_call_gl_callback("glGetUniformIndices", (GLADapiproc) glGetUniformIndices, 4, arg0, arg1, arg2, arg3);
    glGetUniformIndices(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glGetUniformIndices", (GLADapiproc) glGetUniformIndices, 4, arg0, arg1, arg2, arg3);

}
PFNGLGETUNIFORMINDICESPROC glad_debug_glGetUniformIndices = glad_debug_impl_glGetUniformIndices;
PFNGLGETUNIFORMLOCATIONPROC glad_glGetUniformLocation = NULL;
GLint GLAD_API_PTR glad_debug_impl_glGetUniformLocation(GLuint arg0, const GLchar * arg1) {
    GLint ret;
    _pre_call_gl_callback("glGetUniformLocation", (GLADapiproc) glGetUniformLocation, 2, arg0, arg1);
    ret = glGetUniformLocation(arg0, arg1);
    _post_call_gl_callback((void*) &ret, "glGetUniformLocation", (GLADapiproc) glGetUniformLocation, 2, arg0, arg1);
    return ret;
}
PFNGLGETUNIFORMLOCATIONPROC glad_debug_glGetUniformLocation = glad_debug_impl_glGetUniformLocation;
PFNGLGETUNIFORMSUBROUTINEUIVPROC glad_glGetUniformSubroutineuiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetUniformSubroutineuiv(GLenum arg0, GLint arg1, GLuint * arg2) {
    _pre_call_gl_callback("glGetUniformSubroutineuiv", (GLADapiproc) glGetUniformSubroutineuiv, 3, arg0, arg1, arg2);
    glGetUniformSubroutineuiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetUniformSubroutineuiv", (GLADapiproc) glGetUniformSubroutineuiv, 3, arg0, arg1, arg2);

}
PFNGLGETUNIFORMSUBROUTINEUIVPROC glad_debug_glGetUniformSubroutineuiv = glad_debug_impl_glGetUniformSubroutineuiv;
PFNGLGETUNIFORMDVPROC glad_glGetUniformdv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetUniformdv(GLuint arg0, GLint arg1, GLdouble * arg2) {
    _pre_call_gl_callback("glGetUniformdv", (GLADapiproc) glGetUniformdv, 3, arg0, arg1, arg2);
    glGetUniformdv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetUniformdv", (GLADapiproc) glGetUniformdv, 3, arg0, arg1, arg2);

}
PFNGLGETUNIFORMDVPROC glad_debug_glGetUniformdv = glad_debug_impl_glGetUniformdv;
PFNGLGETUNIFORMFVPROC glad_glGetUniformfv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetUniformfv(GLuint arg0, GLint arg1, GLfloat * arg2) {
    _pre_call_gl_callback("glGetUniformfv", (GLADapiproc) glGetUniformfv, 3, arg0, arg1, arg2);
    glGetUniformfv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetUniformfv", (GLADapiproc) glGetUniformfv, 3, arg0, arg1, arg2);

}
PFNGLGETUNIFORMFVPROC glad_debug_glGetUniformfv = glad_debug_impl_glGetUniformfv;
PFNGLGETUNIFORMIVPROC glad_glGetUniformiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetUniformiv(GLuint arg0, GLint arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetUniformiv", (GLADapiproc) glGetUniformiv, 3, arg0, arg1, arg2);
    glGetUniformiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetUniformiv", (GLADapiproc) glGetUniformiv, 3, arg0, arg1, arg2);

}
PFNGLGETUNIFORMIVPROC glad_debug_glGetUniformiv = glad_debug_impl_glGetUniformiv;
PFNGLGETUNIFORMUIVPROC glad_glGetUniformuiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetUniformuiv(GLuint arg0, GLint arg1, GLuint * arg2) {
    _pre_call_gl_callback("glGetUniformuiv", (GLADapiproc) glGetUniformuiv, 3, arg0, arg1, arg2);
    glGetUniformuiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetUniformuiv", (GLADapiproc) glGetUniformuiv, 3, arg0, arg1, arg2);

}
PFNGLGETUNIFORMUIVPROC glad_debug_glGetUniformuiv = glad_debug_impl_glGetUniformuiv;
PFNGLGETVERTEXATTRIBIIVPROC glad_glGetVertexAttribIiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetVertexAttribIiv(GLuint arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetVertexAttribIiv", (GLADapiproc) glGetVertexAttribIiv, 3, arg0, arg1, arg2);
    glGetVertexAttribIiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetVertexAttribIiv", (GLADapiproc) glGetVertexAttribIiv, 3, arg0, arg1, arg2);

}
PFNGLGETVERTEXATTRIBIIVPROC glad_debug_glGetVertexAttribIiv = glad_debug_impl_glGetVertexAttribIiv;
PFNGLGETVERTEXATTRIBIUIVPROC glad_glGetVertexAttribIuiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetVertexAttribIuiv(GLuint arg0, GLenum arg1, GLuint * arg2) {
    _pre_call_gl_callback("glGetVertexAttribIuiv", (GLADapiproc) glGetVertexAttribIuiv, 3, arg0, arg1, arg2);
    glGetVertexAttribIuiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetVertexAttribIuiv", (GLADapiproc) glGetVertexAttribIuiv, 3, arg0, arg1, arg2);

}
PFNGLGETVERTEXATTRIBIUIVPROC glad_debug_glGetVertexAttribIuiv = glad_debug_impl_glGetVertexAttribIuiv;
PFNGLGETVERTEXATTRIBLDVPROC glad_glGetVertexAttribLdv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetVertexAttribLdv(GLuint arg0, GLenum arg1, GLdouble * arg2) {
    _pre_call_gl_callback("glGetVertexAttribLdv", (GLADapiproc) glGetVertexAttribLdv, 3, arg0, arg1, arg2);
    glGetVertexAttribLdv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetVertexAttribLdv", (GLADapiproc) glGetVertexAttribLdv, 3, arg0, arg1, arg2);

}
PFNGLGETVERTEXATTRIBLDVPROC glad_debug_glGetVertexAttribLdv = glad_debug_impl_glGetVertexAttribLdv;
PFNGLGETVERTEXATTRIBPOINTERVPROC glad_glGetVertexAttribPointerv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetVertexAttribPointerv(GLuint arg0, GLenum arg1, void ** arg2) {
    _pre_call_gl_callback("glGetVertexAttribPointerv", (GLADapiproc) glGetVertexAttribPointerv, 3, arg0, arg1, arg2);
    glGetVertexAttribPointerv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetVertexAttribPointerv", (GLADapiproc) glGetVertexAttribPointerv, 3, arg0, arg1, arg2);

}
PFNGLGETVERTEXATTRIBPOINTERVPROC glad_debug_glGetVertexAttribPointerv = glad_debug_impl_glGetVertexAttribPointerv;
PFNGLGETVERTEXATTRIBDVPROC glad_glGetVertexAttribdv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetVertexAttribdv(GLuint arg0, GLenum arg1, GLdouble * arg2) {
    _pre_call_gl_callback("glGetVertexAttribdv", (GLADapiproc) glGetVertexAttribdv, 3, arg0, arg1, arg2);
    glGetVertexAttribdv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetVertexAttribdv", (GLADapiproc) glGetVertexAttribdv, 3, arg0, arg1, arg2);

}
PFNGLGETVERTEXATTRIBDVPROC glad_debug_glGetVertexAttribdv = glad_debug_impl_glGetVertexAttribdv;
PFNGLGETVERTEXATTRIBFVPROC glad_glGetVertexAttribfv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetVertexAttribfv(GLuint arg0, GLenum arg1, GLfloat * arg2) {
    _pre_call_gl_callback("glGetVertexAttribfv", (GLADapiproc) glGetVertexAttribfv, 3, arg0, arg1, arg2);
    glGetVertexAttribfv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetVertexAttribfv", (GLADapiproc) glGetVertexAttribfv, 3, arg0, arg1, arg2);

}
PFNGLGETVERTEXATTRIBFVPROC glad_debug_glGetVertexAttribfv = glad_debug_impl_glGetVertexAttribfv;
PFNGLGETVERTEXATTRIBIVPROC glad_glGetVertexAttribiv = NULL;
void GLAD_API_PTR glad_debug_impl_glGetVertexAttribiv(GLuint arg0, GLenum arg1, GLint * arg2) {
    _pre_call_gl_callback("glGetVertexAttribiv", (GLADapiproc) glGetVertexAttribiv, 3, arg0, arg1, arg2);
    glGetVertexAttribiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glGetVertexAttribiv", (GLADapiproc) glGetVertexAttribiv, 3, arg0, arg1, arg2);

}
PFNGLGETVERTEXATTRIBIVPROC glad_debug_glGetVertexAttribiv = glad_debug_impl_glGetVertexAttribiv;
PFNGLHINTPROC glad_glHint = NULL;
void GLAD_API_PTR glad_debug_impl_glHint(GLenum arg0, GLenum arg1) {
    _pre_call_gl_callback("glHint", (GLADapiproc) glHint, 2, arg0, arg1);
    glHint(arg0, arg1);
    _post_call_gl_callback(NULL, "glHint", (GLADapiproc) glHint, 2, arg0, arg1);

}
PFNGLHINTPROC glad_debug_glHint = glad_debug_impl_glHint;
PFNGLISBUFFERPROC glad_glIsBuffer = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsBuffer(GLuint arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsBuffer", (GLADapiproc) glIsBuffer, 1, arg0);
    ret = glIsBuffer(arg0);
    _post_call_gl_callback((void*) &ret, "glIsBuffer", (GLADapiproc) glIsBuffer, 1, arg0);
    return ret;
}
PFNGLISBUFFERPROC glad_debug_glIsBuffer = glad_debug_impl_glIsBuffer;
PFNGLISENABLEDPROC glad_glIsEnabled = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsEnabled(GLenum arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsEnabled", (GLADapiproc) glIsEnabled, 1, arg0);
    ret = glIsEnabled(arg0);
    _post_call_gl_callback((void*) &ret, "glIsEnabled", (GLADapiproc) glIsEnabled, 1, arg0);
    return ret;
}
PFNGLISENABLEDPROC glad_debug_glIsEnabled = glad_debug_impl_glIsEnabled;
PFNGLISENABLEDIPROC glad_glIsEnabledi = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsEnabledi(GLenum arg0, GLuint arg1) {
    GLboolean ret;
    _pre_call_gl_callback("glIsEnabledi", (GLADapiproc) glIsEnabledi, 2, arg0, arg1);
    ret = glIsEnabledi(arg0, arg1);
    _post_call_gl_callback((void*) &ret, "glIsEnabledi", (GLADapiproc) glIsEnabledi, 2, arg0, arg1);
    return ret;
}
PFNGLISENABLEDIPROC glad_debug_glIsEnabledi = glad_debug_impl_glIsEnabledi;
PFNGLISFRAMEBUFFERPROC glad_glIsFramebuffer = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsFramebuffer(GLuint arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsFramebuffer", (GLADapiproc) glIsFramebuffer, 1, arg0);
    ret = glIsFramebuffer(arg0);
    _post_call_gl_callback((void*) &ret, "glIsFramebuffer", (GLADapiproc) glIsFramebuffer, 1, arg0);
    return ret;
}
PFNGLISFRAMEBUFFERPROC glad_debug_glIsFramebuffer = glad_debug_impl_glIsFramebuffer;
PFNGLISPROGRAMPROC glad_glIsProgram = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsProgram(GLuint arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsProgram", (GLADapiproc) glIsProgram, 1, arg0);
    ret = glIsProgram(arg0);
    _post_call_gl_callback((void*) &ret, "glIsProgram", (GLADapiproc) glIsProgram, 1, arg0);
    return ret;
}
PFNGLISPROGRAMPROC glad_debug_glIsProgram = glad_debug_impl_glIsProgram;
PFNGLISPROGRAMPIPELINEPROC glad_glIsProgramPipeline = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsProgramPipeline(GLuint arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsProgramPipeline", (GLADapiproc) glIsProgramPipeline, 1, arg0);
    ret = glIsProgramPipeline(arg0);
    _post_call_gl_callback((void*) &ret, "glIsProgramPipeline", (GLADapiproc) glIsProgramPipeline, 1, arg0);
    return ret;
}
PFNGLISPROGRAMPIPELINEPROC glad_debug_glIsProgramPipeline = glad_debug_impl_glIsProgramPipeline;
PFNGLISQUERYPROC glad_glIsQuery = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsQuery(GLuint arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsQuery", (GLADapiproc) glIsQuery, 1, arg0);
    ret = glIsQuery(arg0);
    _post_call_gl_callback((void*) &ret, "glIsQuery", (GLADapiproc) glIsQuery, 1, arg0);
    return ret;
}
PFNGLISQUERYPROC glad_debug_glIsQuery = glad_debug_impl_glIsQuery;
PFNGLISRENDERBUFFERPROC glad_glIsRenderbuffer = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsRenderbuffer(GLuint arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsRenderbuffer", (GLADapiproc) glIsRenderbuffer, 1, arg0);
    ret = glIsRenderbuffer(arg0);
    _post_call_gl_callback((void*) &ret, "glIsRenderbuffer", (GLADapiproc) glIsRenderbuffer, 1, arg0);
    return ret;
}
PFNGLISRENDERBUFFERPROC glad_debug_glIsRenderbuffer = glad_debug_impl_glIsRenderbuffer;
PFNGLISSAMPLERPROC glad_glIsSampler = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsSampler(GLuint arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsSampler", (GLADapiproc) glIsSampler, 1, arg0);
    ret = glIsSampler(arg0);
    _post_call_gl_callback((void*) &ret, "glIsSampler", (GLADapiproc) glIsSampler, 1, arg0);
    return ret;
}
PFNGLISSAMPLERPROC glad_debug_glIsSampler = glad_debug_impl_glIsSampler;
PFNGLISSHADERPROC glad_glIsShader = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsShader(GLuint arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsShader", (GLADapiproc) glIsShader, 1, arg0);
    ret = glIsShader(arg0);
    _post_call_gl_callback((void*) &ret, "glIsShader", (GLADapiproc) glIsShader, 1, arg0);
    return ret;
}
PFNGLISSHADERPROC glad_debug_glIsShader = glad_debug_impl_glIsShader;
PFNGLISSYNCPROC glad_glIsSync = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsSync(GLsync arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsSync", (GLADapiproc) glIsSync, 1, arg0);
    ret = glIsSync(arg0);
    _post_call_gl_callback((void*) &ret, "glIsSync", (GLADapiproc) glIsSync, 1, arg0);
    return ret;
}
PFNGLISSYNCPROC glad_debug_glIsSync = glad_debug_impl_glIsSync;
PFNGLISTEXTUREPROC glad_glIsTexture = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsTexture(GLuint arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsTexture", (GLADapiproc) glIsTexture, 1, arg0);
    ret = glIsTexture(arg0);
    _post_call_gl_callback((void*) &ret, "glIsTexture", (GLADapiproc) glIsTexture, 1, arg0);
    return ret;
}
PFNGLISTEXTUREPROC glad_debug_glIsTexture = glad_debug_impl_glIsTexture;
PFNGLISTRANSFORMFEEDBACKPROC glad_glIsTransformFeedback = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsTransformFeedback(GLuint arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsTransformFeedback", (GLADapiproc) glIsTransformFeedback, 1, arg0);
    ret = glIsTransformFeedback(arg0);
    _post_call_gl_callback((void*) &ret, "glIsTransformFeedback", (GLADapiproc) glIsTransformFeedback, 1, arg0);
    return ret;
}
PFNGLISTRANSFORMFEEDBACKPROC glad_debug_glIsTransformFeedback = glad_debug_impl_glIsTransformFeedback;
PFNGLISVERTEXARRAYPROC glad_glIsVertexArray = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glIsVertexArray(GLuint arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glIsVertexArray", (GLADapiproc) glIsVertexArray, 1, arg0);
    ret = glIsVertexArray(arg0);
    _post_call_gl_callback((void*) &ret, "glIsVertexArray", (GLADapiproc) glIsVertexArray, 1, arg0);
    return ret;
}
PFNGLISVERTEXARRAYPROC glad_debug_glIsVertexArray = glad_debug_impl_glIsVertexArray;
PFNGLLINEWIDTHPROC glad_glLineWidth = NULL;
void GLAD_API_PTR glad_debug_impl_glLineWidth(GLfloat arg0) {
    _pre_call_gl_callback("glLineWidth", (GLADapiproc) glLineWidth, 1, arg0);
    glLineWidth(arg0);
    _post_call_gl_callback(NULL, "glLineWidth", (GLADapiproc) glLineWidth, 1, arg0);

}
PFNGLLINEWIDTHPROC glad_debug_glLineWidth = glad_debug_impl_glLineWidth;
PFNGLLINKPROGRAMPROC glad_glLinkProgram = NULL;
void GLAD_API_PTR glad_debug_impl_glLinkProgram(GLuint arg0) {
    _pre_call_gl_callback("glLinkProgram", (GLADapiproc) glLinkProgram, 1, arg0);
    glLinkProgram(arg0);
    _post_call_gl_callback(NULL, "glLinkProgram", (GLADapiproc) glLinkProgram, 1, arg0);

}
PFNGLLINKPROGRAMPROC glad_debug_glLinkProgram = glad_debug_impl_glLinkProgram;
PFNGLLOGICOPPROC glad_glLogicOp = NULL;
void GLAD_API_PTR glad_debug_impl_glLogicOp(GLenum arg0) {
    _pre_call_gl_callback("glLogicOp", (GLADapiproc) glLogicOp, 1, arg0);
    glLogicOp(arg0);
    _post_call_gl_callback(NULL, "glLogicOp", (GLADapiproc) glLogicOp, 1, arg0);

}
PFNGLLOGICOPPROC glad_debug_glLogicOp = glad_debug_impl_glLogicOp;
PFNGLMAPBUFFERPROC glad_glMapBuffer = NULL;
void * GLAD_API_PTR glad_debug_impl_glMapBuffer(GLenum arg0, GLenum arg1) {
    void * ret;
    _pre_call_gl_callback("glMapBuffer", (GLADapiproc) glMapBuffer, 2, arg0, arg1);
    ret = glMapBuffer(arg0, arg1);
    _post_call_gl_callback((void*) &ret, "glMapBuffer", (GLADapiproc) glMapBuffer, 2, arg0, arg1);
    return ret;
}
PFNGLMAPBUFFERPROC glad_debug_glMapBuffer = glad_debug_impl_glMapBuffer;
PFNGLMAPBUFFERRANGEPROC glad_glMapBufferRange = NULL;
void * GLAD_API_PTR glad_debug_impl_glMapBufferRange(GLenum arg0, GLintptr arg1, GLsizeiptr arg2, GLbitfield arg3) {
    void * ret;
    _pre_call_gl_callback("glMapBufferRange", (GLADapiproc) glMapBufferRange, 4, arg0, arg1, arg2, arg3);
    ret = glMapBufferRange(arg0, arg1, arg2, arg3);
    _post_call_gl_callback((void*) &ret, "glMapBufferRange", (GLADapiproc) glMapBufferRange, 4, arg0, arg1, arg2, arg3);
    return ret;
}
PFNGLMAPBUFFERRANGEPROC glad_debug_glMapBufferRange = glad_debug_impl_glMapBufferRange;
PFNGLMINSAMPLESHADINGPROC glad_glMinSampleShading = NULL;
void GLAD_API_PTR glad_debug_impl_glMinSampleShading(GLfloat arg0) {
    _pre_call_gl_callback("glMinSampleShading", (GLADapiproc) glMinSampleShading, 1, arg0);
    glMinSampleShading(arg0);
    _post_call_gl_callback(NULL, "glMinSampleShading", (GLADapiproc) glMinSampleShading, 1, arg0);

}
PFNGLMINSAMPLESHADINGPROC glad_debug_glMinSampleShading = glad_debug_impl_glMinSampleShading;
PFNGLMULTIDRAWARRAYSPROC glad_glMultiDrawArrays = NULL;
void GLAD_API_PTR glad_debug_impl_glMultiDrawArrays(GLenum arg0, const GLint * arg1, const GLsizei * arg2, GLsizei arg3) {
    _pre_call_gl_callback("glMultiDrawArrays", (GLADapiproc) glMultiDrawArrays, 4, arg0, arg1, arg2, arg3);
    glMultiDrawArrays(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glMultiDrawArrays", (GLADapiproc) glMultiDrawArrays, 4, arg0, arg1, arg2, arg3);

}
PFNGLMULTIDRAWARRAYSPROC glad_debug_glMultiDrawArrays = glad_debug_impl_glMultiDrawArrays;
PFNGLMULTIDRAWELEMENTSPROC glad_glMultiDrawElements = NULL;
void GLAD_API_PTR glad_debug_impl_glMultiDrawElements(GLenum arg0, const GLsizei * arg1, GLenum arg2, const void *const* arg3, GLsizei arg4) {
    _pre_call_gl_callback("glMultiDrawElements", (GLADapiproc) glMultiDrawElements, 5, arg0, arg1, arg2, arg3, arg4);
    glMultiDrawElements(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glMultiDrawElements", (GLADapiproc) glMultiDrawElements, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLMULTIDRAWELEMENTSPROC glad_debug_glMultiDrawElements = glad_debug_impl_glMultiDrawElements;
PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC glad_glMultiDrawElementsBaseVertex = NULL;
void GLAD_API_PTR glad_debug_impl_glMultiDrawElementsBaseVertex(GLenum arg0, const GLsizei * arg1, GLenum arg2, const void *const* arg3, GLsizei arg4, const GLint * arg5) {
    _pre_call_gl_callback("glMultiDrawElementsBaseVertex", (GLADapiproc) glMultiDrawElementsBaseVertex, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glMultiDrawElementsBaseVertex(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glMultiDrawElementsBaseVertex", (GLADapiproc) glMultiDrawElementsBaseVertex, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC glad_debug_glMultiDrawElementsBaseVertex = glad_debug_impl_glMultiDrawElementsBaseVertex;
PFNGLPATCHPARAMETERFVPROC glad_glPatchParameterfv = NULL;
void GLAD_API_PTR glad_debug_impl_glPatchParameterfv(GLenum arg0, const GLfloat * arg1) {
    _pre_call_gl_callback("glPatchParameterfv", (GLADapiproc) glPatchParameterfv, 2, arg0, arg1);
    glPatchParameterfv(arg0, arg1);
    _post_call_gl_callback(NULL, "glPatchParameterfv", (GLADapiproc) glPatchParameterfv, 2, arg0, arg1);

}
PFNGLPATCHPARAMETERFVPROC glad_debug_glPatchParameterfv = glad_debug_impl_glPatchParameterfv;
PFNGLPATCHPARAMETERIPROC glad_glPatchParameteri = NULL;
void GLAD_API_PTR glad_debug_impl_glPatchParameteri(GLenum arg0, GLint arg1) {
    _pre_call_gl_callback("glPatchParameteri", (GLADapiproc) glPatchParameteri, 2, arg0, arg1);
    glPatchParameteri(arg0, arg1);
    _post_call_gl_callback(NULL, "glPatchParameteri", (GLADapiproc) glPatchParameteri, 2, arg0, arg1);

}
PFNGLPATCHPARAMETERIPROC glad_debug_glPatchParameteri = glad_debug_impl_glPatchParameteri;
PFNGLPAUSETRANSFORMFEEDBACKPROC glad_glPauseTransformFeedback = NULL;
void GLAD_API_PTR glad_debug_impl_glPauseTransformFeedback(void) {
    _pre_call_gl_callback("glPauseTransformFeedback", (GLADapiproc) glPauseTransformFeedback, 0);
    glPauseTransformFeedback();
    _post_call_gl_callback(NULL, "glPauseTransformFeedback", (GLADapiproc) glPauseTransformFeedback, 0);

}
PFNGLPAUSETRANSFORMFEEDBACKPROC glad_debug_glPauseTransformFeedback = glad_debug_impl_glPauseTransformFeedback;
PFNGLPIXELSTOREFPROC glad_glPixelStoref = NULL;
void GLAD_API_PTR glad_debug_impl_glPixelStoref(GLenum arg0, GLfloat arg1) {
    _pre_call_gl_callback("glPixelStoref", (GLADapiproc) glPixelStoref, 2, arg0, arg1);
    glPixelStoref(arg0, arg1);
    _post_call_gl_callback(NULL, "glPixelStoref", (GLADapiproc) glPixelStoref, 2, arg0, arg1);

}
PFNGLPIXELSTOREFPROC glad_debug_glPixelStoref = glad_debug_impl_glPixelStoref;
PFNGLPIXELSTOREIPROC glad_glPixelStorei = NULL;
void GLAD_API_PTR glad_debug_impl_glPixelStorei(GLenum arg0, GLint arg1) {
    _pre_call_gl_callback("glPixelStorei", (GLADapiproc) glPixelStorei, 2, arg0, arg1);
    glPixelStorei(arg0, arg1);
    _post_call_gl_callback(NULL, "glPixelStorei", (GLADapiproc) glPixelStorei, 2, arg0, arg1);

}
PFNGLPIXELSTOREIPROC glad_debug_glPixelStorei = glad_debug_impl_glPixelStorei;
PFNGLPOINTPARAMETERFPROC glad_glPointParameterf = NULL;
void GLAD_API_PTR glad_debug_impl_glPointParameterf(GLenum arg0, GLfloat arg1) {
    _pre_call_gl_callback("glPointParameterf", (GLADapiproc) glPointParameterf, 2, arg0, arg1);
    glPointParameterf(arg0, arg1);
    _post_call_gl_callback(NULL, "glPointParameterf", (GLADapiproc) glPointParameterf, 2, arg0, arg1);

}
PFNGLPOINTPARAMETERFPROC glad_debug_glPointParameterf = glad_debug_impl_glPointParameterf;
PFNGLPOINTPARAMETERFVPROC glad_glPointParameterfv = NULL;
void GLAD_API_PTR glad_debug_impl_glPointParameterfv(GLenum arg0, const GLfloat * arg1) {
    _pre_call_gl_callback("glPointParameterfv", (GLADapiproc) glPointParameterfv, 2, arg0, arg1);
    glPointParameterfv(arg0, arg1);
    _post_call_gl_callback(NULL, "glPointParameterfv", (GLADapiproc) glPointParameterfv, 2, arg0, arg1);

}
PFNGLPOINTPARAMETERFVPROC glad_debug_glPointParameterfv = glad_debug_impl_glPointParameterfv;
PFNGLPOINTPARAMETERIPROC glad_glPointParameteri = NULL;
void GLAD_API_PTR glad_debug_impl_glPointParameteri(GLenum arg0, GLint arg1) {
    _pre_call_gl_callback("glPointParameteri", (GLADapiproc) glPointParameteri, 2, arg0, arg1);
    glPointParameteri(arg0, arg1);
    _post_call_gl_callback(NULL, "glPointParameteri", (GLADapiproc) glPointParameteri, 2, arg0, arg1);

}
PFNGLPOINTPARAMETERIPROC glad_debug_glPointParameteri = glad_debug_impl_glPointParameteri;
PFNGLPOINTPARAMETERIVPROC glad_glPointParameteriv = NULL;
void GLAD_API_PTR glad_debug_impl_glPointParameteriv(GLenum arg0, const GLint * arg1) {
    _pre_call_gl_callback("glPointParameteriv", (GLADapiproc) glPointParameteriv, 2, arg0, arg1);
    glPointParameteriv(arg0, arg1);
    _post_call_gl_callback(NULL, "glPointParameteriv", (GLADapiproc) glPointParameteriv, 2, arg0, arg1);

}
PFNGLPOINTPARAMETERIVPROC glad_debug_glPointParameteriv = glad_debug_impl_glPointParameteriv;
PFNGLPOINTSIZEPROC glad_glPointSize = NULL;
void GLAD_API_PTR glad_debug_impl_glPointSize(GLfloat arg0) {
    _pre_call_gl_callback("glPointSize", (GLADapiproc) glPointSize, 1, arg0);
    glPointSize(arg0);
    _post_call_gl_callback(NULL, "glPointSize", (GLADapiproc) glPointSize, 1, arg0);

}
PFNGLPOINTSIZEPROC glad_debug_glPointSize = glad_debug_impl_glPointSize;
PFNGLPOLYGONMODEPROC glad_glPolygonMode = NULL;
void GLAD_API_PTR glad_debug_impl_glPolygonMode(GLenum arg0, GLenum arg1) {
    _pre_call_gl_callback("glPolygonMode", (GLADapiproc) glPolygonMode, 2, arg0, arg1);
    glPolygonMode(arg0, arg1);
    _post_call_gl_callback(NULL, "glPolygonMode", (GLADapiproc) glPolygonMode, 2, arg0, arg1);

}
PFNGLPOLYGONMODEPROC glad_debug_glPolygonMode = glad_debug_impl_glPolygonMode;
PFNGLPOLYGONOFFSETPROC glad_glPolygonOffset = NULL;
void GLAD_API_PTR glad_debug_impl_glPolygonOffset(GLfloat arg0, GLfloat arg1) {
    _pre_call_gl_callback("glPolygonOffset", (GLADapiproc) glPolygonOffset, 2, arg0, arg1);
    glPolygonOffset(arg0, arg1);
    _post_call_gl_callback(NULL, "glPolygonOffset", (GLADapiproc) glPolygonOffset, 2, arg0, arg1);

}
PFNGLPOLYGONOFFSETPROC glad_debug_glPolygonOffset = glad_debug_impl_glPolygonOffset;
PFNGLPRIMITIVERESTARTINDEXPROC glad_glPrimitiveRestartIndex = NULL;
void GLAD_API_PTR glad_debug_impl_glPrimitiveRestartIndex(GLuint arg0) {
    _pre_call_gl_callback("glPrimitiveRestartIndex", (GLADapiproc) glPrimitiveRestartIndex, 1, arg0);
    glPrimitiveRestartIndex(arg0);
    _post_call_gl_callback(NULL, "glPrimitiveRestartIndex", (GLADapiproc) glPrimitiveRestartIndex, 1, arg0);

}
PFNGLPRIMITIVERESTARTINDEXPROC glad_debug_glPrimitiveRestartIndex = glad_debug_impl_glPrimitiveRestartIndex;
PFNGLPROGRAMBINARYPROC glad_glProgramBinary = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramBinary(GLuint arg0, GLenum arg1, const void * arg2, GLsizei arg3) {
    _pre_call_gl_callback("glProgramBinary", (GLADapiproc) glProgramBinary, 4, arg0, arg1, arg2, arg3);
    glProgramBinary(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramBinary", (GLADapiproc) glProgramBinary, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMBINARYPROC glad_debug_glProgramBinary = glad_debug_impl_glProgramBinary;
PFNGLPROGRAMPARAMETERIPROC glad_glProgramParameteri = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramParameteri(GLuint arg0, GLenum arg1, GLint arg2) {
    _pre_call_gl_callback("glProgramParameteri", (GLADapiproc) glProgramParameteri, 3, arg0, arg1, arg2);
    glProgramParameteri(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glProgramParameteri", (GLADapiproc) glProgramParameteri, 3, arg0, arg1, arg2);

}
PFNGLPROGRAMPARAMETERIPROC glad_debug_glProgramParameteri = glad_debug_impl_glProgramParameteri;
PFNGLPROGRAMUNIFORM1DPROC glad_glProgramUniform1d = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform1d(GLuint arg0, GLint arg1, GLdouble arg2) {
    _pre_call_gl_callback("glProgramUniform1d", (GLADapiproc) glProgramUniform1d, 3, arg0, arg1, arg2);
    glProgramUniform1d(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glProgramUniform1d", (GLADapiproc) glProgramUniform1d, 3, arg0, arg1, arg2);

}
PFNGLPROGRAMUNIFORM1DPROC glad_debug_glProgramUniform1d = glad_debug_impl_glProgramUniform1d;
PFNGLPROGRAMUNIFORM1DVPROC glad_glProgramUniform1dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform1dv(GLuint arg0, GLint arg1, GLsizei arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glProgramUniform1dv", (GLADapiproc) glProgramUniform1dv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform1dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform1dv", (GLADapiproc) glProgramUniform1dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM1DVPROC glad_debug_glProgramUniform1dv = glad_debug_impl_glProgramUniform1dv;
PFNGLPROGRAMUNIFORM1FPROC glad_glProgramUniform1f = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform1f(GLuint arg0, GLint arg1, GLfloat arg2) {
    _pre_call_gl_callback("glProgramUniform1f", (GLADapiproc) glProgramUniform1f, 3, arg0, arg1, arg2);
    glProgramUniform1f(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glProgramUniform1f", (GLADapiproc) glProgramUniform1f, 3, arg0, arg1, arg2);

}
PFNGLPROGRAMUNIFORM1FPROC glad_debug_glProgramUniform1f = glad_debug_impl_glProgramUniform1f;
PFNGLPROGRAMUNIFORM1FVPROC glad_glProgramUniform1fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform1fv(GLuint arg0, GLint arg1, GLsizei arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glProgramUniform1fv", (GLADapiproc) glProgramUniform1fv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform1fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform1fv", (GLADapiproc) glProgramUniform1fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM1FVPROC glad_debug_glProgramUniform1fv = glad_debug_impl_glProgramUniform1fv;
PFNGLPROGRAMUNIFORM1IPROC glad_glProgramUniform1i = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform1i(GLuint arg0, GLint arg1, GLint arg2) {
    _pre_call_gl_callback("glProgramUniform1i", (GLADapiproc) glProgramUniform1i, 3, arg0, arg1, arg2);
    glProgramUniform1i(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glProgramUniform1i", (GLADapiproc) glProgramUniform1i, 3, arg0, arg1, arg2);

}
PFNGLPROGRAMUNIFORM1IPROC glad_debug_glProgramUniform1i = glad_debug_impl_glProgramUniform1i;
PFNGLPROGRAMUNIFORM1IVPROC glad_glProgramUniform1iv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform1iv(GLuint arg0, GLint arg1, GLsizei arg2, const GLint * arg3) {
    _pre_call_gl_callback("glProgramUniform1iv", (GLADapiproc) glProgramUniform1iv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform1iv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform1iv", (GLADapiproc) glProgramUniform1iv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM1IVPROC glad_debug_glProgramUniform1iv = glad_debug_impl_glProgramUniform1iv;
PFNGLPROGRAMUNIFORM1UIPROC glad_glProgramUniform1ui = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform1ui(GLuint arg0, GLint arg1, GLuint arg2) {
    _pre_call_gl_callback("glProgramUniform1ui", (GLADapiproc) glProgramUniform1ui, 3, arg0, arg1, arg2);
    glProgramUniform1ui(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glProgramUniform1ui", (GLADapiproc) glProgramUniform1ui, 3, arg0, arg1, arg2);

}
PFNGLPROGRAMUNIFORM1UIPROC glad_debug_glProgramUniform1ui = glad_debug_impl_glProgramUniform1ui;
PFNGLPROGRAMUNIFORM1UIVPROC glad_glProgramUniform1uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform1uiv(GLuint arg0, GLint arg1, GLsizei arg2, const GLuint * arg3) {
    _pre_call_gl_callback("glProgramUniform1uiv", (GLADapiproc) glProgramUniform1uiv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform1uiv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform1uiv", (GLADapiproc) glProgramUniform1uiv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM1UIVPROC glad_debug_glProgramUniform1uiv = glad_debug_impl_glProgramUniform1uiv;
PFNGLPROGRAMUNIFORM2DPROC glad_glProgramUniform2d = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform2d(GLuint arg0, GLint arg1, GLdouble arg2, GLdouble arg3) {
    _pre_call_gl_callback("glProgramUniform2d", (GLADapiproc) glProgramUniform2d, 4, arg0, arg1, arg2, arg3);
    glProgramUniform2d(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform2d", (GLADapiproc) glProgramUniform2d, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM2DPROC glad_debug_glProgramUniform2d = glad_debug_impl_glProgramUniform2d;
PFNGLPROGRAMUNIFORM2DVPROC glad_glProgramUniform2dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform2dv(GLuint arg0, GLint arg1, GLsizei arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glProgramUniform2dv", (GLADapiproc) glProgramUniform2dv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform2dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform2dv", (GLADapiproc) glProgramUniform2dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM2DVPROC glad_debug_glProgramUniform2dv = glad_debug_impl_glProgramUniform2dv;
PFNGLPROGRAMUNIFORM2FPROC glad_glProgramUniform2f = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform2f(GLuint arg0, GLint arg1, GLfloat arg2, GLfloat arg3) {
    _pre_call_gl_callback("glProgramUniform2f", (GLADapiproc) glProgramUniform2f, 4, arg0, arg1, arg2, arg3);
    glProgramUniform2f(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform2f", (GLADapiproc) glProgramUniform2f, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM2FPROC glad_debug_glProgramUniform2f = glad_debug_impl_glProgramUniform2f;
PFNGLPROGRAMUNIFORM2FVPROC glad_glProgramUniform2fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform2fv(GLuint arg0, GLint arg1, GLsizei arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glProgramUniform2fv", (GLADapiproc) glProgramUniform2fv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform2fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform2fv", (GLADapiproc) glProgramUniform2fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM2FVPROC glad_debug_glProgramUniform2fv = glad_debug_impl_glProgramUniform2fv;
PFNGLPROGRAMUNIFORM2IPROC glad_glProgramUniform2i = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform2i(GLuint arg0, GLint arg1, GLint arg2, GLint arg3) {
    _pre_call_gl_callback("glProgramUniform2i", (GLADapiproc) glProgramUniform2i, 4, arg0, arg1, arg2, arg3);
    glProgramUniform2i(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform2i", (GLADapiproc) glProgramUniform2i, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM2IPROC glad_debug_glProgramUniform2i = glad_debug_impl_glProgramUniform2i;
PFNGLPROGRAMUNIFORM2IVPROC glad_glProgramUniform2iv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform2iv(GLuint arg0, GLint arg1, GLsizei arg2, const GLint * arg3) {
    _pre_call_gl_callback("glProgramUniform2iv", (GLADapiproc) glProgramUniform2iv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform2iv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform2iv", (GLADapiproc) glProgramUniform2iv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM2IVPROC glad_debug_glProgramUniform2iv = glad_debug_impl_glProgramUniform2iv;
PFNGLPROGRAMUNIFORM2UIPROC glad_glProgramUniform2ui = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform2ui(GLuint arg0, GLint arg1, GLuint arg2, GLuint arg3) {
    _pre_call_gl_callback("glProgramUniform2ui", (GLADapiproc) glProgramUniform2ui, 4, arg0, arg1, arg2, arg3);
    glProgramUniform2ui(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform2ui", (GLADapiproc) glProgramUniform2ui, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM2UIPROC glad_debug_glProgramUniform2ui = glad_debug_impl_glProgramUniform2ui;
PFNGLPROGRAMUNIFORM2UIVPROC glad_glProgramUniform2uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform2uiv(GLuint arg0, GLint arg1, GLsizei arg2, const GLuint * arg3) {
    _pre_call_gl_callback("glProgramUniform2uiv", (GLADapiproc) glProgramUniform2uiv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform2uiv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform2uiv", (GLADapiproc) glProgramUniform2uiv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM2UIVPROC glad_debug_glProgramUniform2uiv = glad_debug_impl_glProgramUniform2uiv;
PFNGLPROGRAMUNIFORM3DPROC glad_glProgramUniform3d = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform3d(GLuint arg0, GLint arg1, GLdouble arg2, GLdouble arg3, GLdouble arg4) {
    _pre_call_gl_callback("glProgramUniform3d", (GLADapiproc) glProgramUniform3d, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniform3d(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniform3d", (GLADapiproc) glProgramUniform3d, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORM3DPROC glad_debug_glProgramUniform3d = glad_debug_impl_glProgramUniform3d;
PFNGLPROGRAMUNIFORM3DVPROC glad_glProgramUniform3dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform3dv(GLuint arg0, GLint arg1, GLsizei arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glProgramUniform3dv", (GLADapiproc) glProgramUniform3dv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform3dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform3dv", (GLADapiproc) glProgramUniform3dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM3DVPROC glad_debug_glProgramUniform3dv = glad_debug_impl_glProgramUniform3dv;
PFNGLPROGRAMUNIFORM3FPROC glad_glProgramUniform3f = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform3f(GLuint arg0, GLint arg1, GLfloat arg2, GLfloat arg3, GLfloat arg4) {
    _pre_call_gl_callback("glProgramUniform3f", (GLADapiproc) glProgramUniform3f, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniform3f(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniform3f", (GLADapiproc) glProgramUniform3f, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORM3FPROC glad_debug_glProgramUniform3f = glad_debug_impl_glProgramUniform3f;
PFNGLPROGRAMUNIFORM3FVPROC glad_glProgramUniform3fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform3fv(GLuint arg0, GLint arg1, GLsizei arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glProgramUniform3fv", (GLADapiproc) glProgramUniform3fv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform3fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform3fv", (GLADapiproc) glProgramUniform3fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM3FVPROC glad_debug_glProgramUniform3fv = glad_debug_impl_glProgramUniform3fv;
PFNGLPROGRAMUNIFORM3IPROC glad_glProgramUniform3i = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform3i(GLuint arg0, GLint arg1, GLint arg2, GLint arg3, GLint arg4) {
    _pre_call_gl_callback("glProgramUniform3i", (GLADapiproc) glProgramUniform3i, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniform3i(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniform3i", (GLADapiproc) glProgramUniform3i, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORM3IPROC glad_debug_glProgramUniform3i = glad_debug_impl_glProgramUniform3i;
PFNGLPROGRAMUNIFORM3IVPROC glad_glProgramUniform3iv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform3iv(GLuint arg0, GLint arg1, GLsizei arg2, const GLint * arg3) {
    _pre_call_gl_callback("glProgramUniform3iv", (GLADapiproc) glProgramUniform3iv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform3iv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform3iv", (GLADapiproc) glProgramUniform3iv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM3IVPROC glad_debug_glProgramUniform3iv = glad_debug_impl_glProgramUniform3iv;
PFNGLPROGRAMUNIFORM3UIPROC glad_glProgramUniform3ui = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform3ui(GLuint arg0, GLint arg1, GLuint arg2, GLuint arg3, GLuint arg4) {
    _pre_call_gl_callback("glProgramUniform3ui", (GLADapiproc) glProgramUniform3ui, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniform3ui(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniform3ui", (GLADapiproc) glProgramUniform3ui, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORM3UIPROC glad_debug_glProgramUniform3ui = glad_debug_impl_glProgramUniform3ui;
PFNGLPROGRAMUNIFORM3UIVPROC glad_glProgramUniform3uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform3uiv(GLuint arg0, GLint arg1, GLsizei arg2, const GLuint * arg3) {
    _pre_call_gl_callback("glProgramUniform3uiv", (GLADapiproc) glProgramUniform3uiv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform3uiv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform3uiv", (GLADapiproc) glProgramUniform3uiv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM3UIVPROC glad_debug_glProgramUniform3uiv = glad_debug_impl_glProgramUniform3uiv;
PFNGLPROGRAMUNIFORM4DPROC glad_glProgramUniform4d = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform4d(GLuint arg0, GLint arg1, GLdouble arg2, GLdouble arg3, GLdouble arg4, GLdouble arg5) {
    _pre_call_gl_callback("glProgramUniform4d", (GLADapiproc) glProgramUniform4d, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glProgramUniform4d(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glProgramUniform4d", (GLADapiproc) glProgramUniform4d, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLPROGRAMUNIFORM4DPROC glad_debug_glProgramUniform4d = glad_debug_impl_glProgramUniform4d;
PFNGLPROGRAMUNIFORM4DVPROC glad_glProgramUniform4dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform4dv(GLuint arg0, GLint arg1, GLsizei arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glProgramUniform4dv", (GLADapiproc) glProgramUniform4dv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform4dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform4dv", (GLADapiproc) glProgramUniform4dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM4DVPROC glad_debug_glProgramUniform4dv = glad_debug_impl_glProgramUniform4dv;
PFNGLPROGRAMUNIFORM4FPROC glad_glProgramUniform4f = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform4f(GLuint arg0, GLint arg1, GLfloat arg2, GLfloat arg3, GLfloat arg4, GLfloat arg5) {
    _pre_call_gl_callback("glProgramUniform4f", (GLADapiproc) glProgramUniform4f, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glProgramUniform4f(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glProgramUniform4f", (GLADapiproc) glProgramUniform4f, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLPROGRAMUNIFORM4FPROC glad_debug_glProgramUniform4f = glad_debug_impl_glProgramUniform4f;
PFNGLPROGRAMUNIFORM4FVPROC glad_glProgramUniform4fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform4fv(GLuint arg0, GLint arg1, GLsizei arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glProgramUniform4fv", (GLADapiproc) glProgramUniform4fv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform4fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform4fv", (GLADapiproc) glProgramUniform4fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM4FVPROC glad_debug_glProgramUniform4fv = glad_debug_impl_glProgramUniform4fv;
PFNGLPROGRAMUNIFORM4IPROC glad_glProgramUniform4i = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform4i(GLuint arg0, GLint arg1, GLint arg2, GLint arg3, GLint arg4, GLint arg5) {
    _pre_call_gl_callback("glProgramUniform4i", (GLADapiproc) glProgramUniform4i, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glProgramUniform4i(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glProgramUniform4i", (GLADapiproc) glProgramUniform4i, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLPROGRAMUNIFORM4IPROC glad_debug_glProgramUniform4i = glad_debug_impl_glProgramUniform4i;
PFNGLPROGRAMUNIFORM4IVPROC glad_glProgramUniform4iv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform4iv(GLuint arg0, GLint arg1, GLsizei arg2, const GLint * arg3) {
    _pre_call_gl_callback("glProgramUniform4iv", (GLADapiproc) glProgramUniform4iv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform4iv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform4iv", (GLADapiproc) glProgramUniform4iv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM4IVPROC glad_debug_glProgramUniform4iv = glad_debug_impl_glProgramUniform4iv;
PFNGLPROGRAMUNIFORM4UIPROC glad_glProgramUniform4ui = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform4ui(GLuint arg0, GLint arg1, GLuint arg2, GLuint arg3, GLuint arg4, GLuint arg5) {
    _pre_call_gl_callback("glProgramUniform4ui", (GLADapiproc) glProgramUniform4ui, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glProgramUniform4ui(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glProgramUniform4ui", (GLADapiproc) glProgramUniform4ui, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLPROGRAMUNIFORM4UIPROC glad_debug_glProgramUniform4ui = glad_debug_impl_glProgramUniform4ui;
PFNGLPROGRAMUNIFORM4UIVPROC glad_glProgramUniform4uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniform4uiv(GLuint arg0, GLint arg1, GLsizei arg2, const GLuint * arg3) {
    _pre_call_gl_callback("glProgramUniform4uiv", (GLADapiproc) glProgramUniform4uiv, 4, arg0, arg1, arg2, arg3);
    glProgramUniform4uiv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glProgramUniform4uiv", (GLADapiproc) glProgramUniform4uiv, 4, arg0, arg1, arg2, arg3);

}
PFNGLPROGRAMUNIFORM4UIVPROC glad_debug_glProgramUniform4uiv = glad_debug_impl_glProgramUniform4uiv;
PFNGLPROGRAMUNIFORMMATRIX2DVPROC glad_glProgramUniformMatrix2dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix2dv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLdouble * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix2dv", (GLADapiproc) glProgramUniformMatrix2dv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix2dv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix2dv", (GLADapiproc) glProgramUniformMatrix2dv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX2DVPROC glad_debug_glProgramUniformMatrix2dv = glad_debug_impl_glProgramUniformMatrix2dv;
PFNGLPROGRAMUNIFORMMATRIX2FVPROC glad_glProgramUniformMatrix2fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix2fv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLfloat * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix2fv", (GLADapiproc) glProgramUniformMatrix2fv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix2fv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix2fv", (GLADapiproc) glProgramUniformMatrix2fv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX2FVPROC glad_debug_glProgramUniformMatrix2fv = glad_debug_impl_glProgramUniformMatrix2fv;
PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC glad_glProgramUniformMatrix2x3dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix2x3dv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLdouble * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix2x3dv", (GLADapiproc) glProgramUniformMatrix2x3dv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix2x3dv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix2x3dv", (GLADapiproc) glProgramUniformMatrix2x3dv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC glad_debug_glProgramUniformMatrix2x3dv = glad_debug_impl_glProgramUniformMatrix2x3dv;
PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC glad_glProgramUniformMatrix2x3fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix2x3fv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLfloat * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix2x3fv", (GLADapiproc) glProgramUniformMatrix2x3fv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix2x3fv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix2x3fv", (GLADapiproc) glProgramUniformMatrix2x3fv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC glad_debug_glProgramUniformMatrix2x3fv = glad_debug_impl_glProgramUniformMatrix2x3fv;
PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC glad_glProgramUniformMatrix2x4dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix2x4dv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLdouble * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix2x4dv", (GLADapiproc) glProgramUniformMatrix2x4dv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix2x4dv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix2x4dv", (GLADapiproc) glProgramUniformMatrix2x4dv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC glad_debug_glProgramUniformMatrix2x4dv = glad_debug_impl_glProgramUniformMatrix2x4dv;
PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC glad_glProgramUniformMatrix2x4fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix2x4fv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLfloat * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix2x4fv", (GLADapiproc) glProgramUniformMatrix2x4fv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix2x4fv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix2x4fv", (GLADapiproc) glProgramUniformMatrix2x4fv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC glad_debug_glProgramUniformMatrix2x4fv = glad_debug_impl_glProgramUniformMatrix2x4fv;
PFNGLPROGRAMUNIFORMMATRIX3DVPROC glad_glProgramUniformMatrix3dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix3dv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLdouble * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix3dv", (GLADapiproc) glProgramUniformMatrix3dv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix3dv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix3dv", (GLADapiproc) glProgramUniformMatrix3dv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX3DVPROC glad_debug_glProgramUniformMatrix3dv = glad_debug_impl_glProgramUniformMatrix3dv;
PFNGLPROGRAMUNIFORMMATRIX3FVPROC glad_glProgramUniformMatrix3fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix3fv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLfloat * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix3fv", (GLADapiproc) glProgramUniformMatrix3fv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix3fv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix3fv", (GLADapiproc) glProgramUniformMatrix3fv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX3FVPROC glad_debug_glProgramUniformMatrix3fv = glad_debug_impl_glProgramUniformMatrix3fv;
PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC glad_glProgramUniformMatrix3x2dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix3x2dv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLdouble * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix3x2dv", (GLADapiproc) glProgramUniformMatrix3x2dv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix3x2dv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix3x2dv", (GLADapiproc) glProgramUniformMatrix3x2dv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC glad_debug_glProgramUniformMatrix3x2dv = glad_debug_impl_glProgramUniformMatrix3x2dv;
PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC glad_glProgramUniformMatrix3x2fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix3x2fv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLfloat * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix3x2fv", (GLADapiproc) glProgramUniformMatrix3x2fv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix3x2fv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix3x2fv", (GLADapiproc) glProgramUniformMatrix3x2fv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC glad_debug_glProgramUniformMatrix3x2fv = glad_debug_impl_glProgramUniformMatrix3x2fv;
PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC glad_glProgramUniformMatrix3x4dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix3x4dv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLdouble * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix3x4dv", (GLADapiproc) glProgramUniformMatrix3x4dv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix3x4dv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix3x4dv", (GLADapiproc) glProgramUniformMatrix3x4dv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC glad_debug_glProgramUniformMatrix3x4dv = glad_debug_impl_glProgramUniformMatrix3x4dv;
PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC glad_glProgramUniformMatrix3x4fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix3x4fv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLfloat * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix3x4fv", (GLADapiproc) glProgramUniformMatrix3x4fv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix3x4fv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix3x4fv", (GLADapiproc) glProgramUniformMatrix3x4fv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC glad_debug_glProgramUniformMatrix3x4fv = glad_debug_impl_glProgramUniformMatrix3x4fv;
PFNGLPROGRAMUNIFORMMATRIX4DVPROC glad_glProgramUniformMatrix4dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix4dv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLdouble * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix4dv", (GLADapiproc) glProgramUniformMatrix4dv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix4dv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix4dv", (GLADapiproc) glProgramUniformMatrix4dv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX4DVPROC glad_debug_glProgramUniformMatrix4dv = glad_debug_impl_glProgramUniformMatrix4dv;
PFNGLPROGRAMUNIFORMMATRIX4FVPROC glad_glProgramUniformMatrix4fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix4fv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLfloat * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix4fv", (GLADapiproc) glProgramUniformMatrix4fv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix4fv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix4fv", (GLADapiproc) glProgramUniformMatrix4fv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX4FVPROC glad_debug_glProgramUniformMatrix4fv = glad_debug_impl_glProgramUniformMatrix4fv;
PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC glad_glProgramUniformMatrix4x2dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix4x2dv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLdouble * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix4x2dv", (GLADapiproc) glProgramUniformMatrix4x2dv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix4x2dv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix4x2dv", (GLADapiproc) glProgramUniformMatrix4x2dv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC glad_debug_glProgramUniformMatrix4x2dv = glad_debug_impl_glProgramUniformMatrix4x2dv;
PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC glad_glProgramUniformMatrix4x2fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix4x2fv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLfloat * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix4x2fv", (GLADapiproc) glProgramUniformMatrix4x2fv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix4x2fv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix4x2fv", (GLADapiproc) glProgramUniformMatrix4x2fv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC glad_debug_glProgramUniformMatrix4x2fv = glad_debug_impl_glProgramUniformMatrix4x2fv;
PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC glad_glProgramUniformMatrix4x3dv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix4x3dv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLdouble * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix4x3dv", (GLADapiproc) glProgramUniformMatrix4x3dv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix4x3dv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix4x3dv", (GLADapiproc) glProgramUniformMatrix4x3dv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC glad_debug_glProgramUniformMatrix4x3dv = glad_debug_impl_glProgramUniformMatrix4x3dv;
PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC glad_glProgramUniformMatrix4x3fv = NULL;
void GLAD_API_PTR glad_debug_impl_glProgramUniformMatrix4x3fv(GLuint arg0, GLint arg1, GLsizei arg2, GLboolean arg3, const GLfloat * arg4) {
    _pre_call_gl_callback("glProgramUniformMatrix4x3fv", (GLADapiproc) glProgramUniformMatrix4x3fv, 5, arg0, arg1, arg2, arg3, arg4);
    glProgramUniformMatrix4x3fv(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glProgramUniformMatrix4x3fv", (GLADapiproc) glProgramUniformMatrix4x3fv, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC glad_debug_glProgramUniformMatrix4x3fv = glad_debug_impl_glProgramUniformMatrix4x3fv;
PFNGLPROVOKINGVERTEXPROC glad_glProvokingVertex = NULL;
void GLAD_API_PTR glad_debug_impl_glProvokingVertex(GLenum arg0) {
    _pre_call_gl_callback("glProvokingVertex", (GLADapiproc) glProvokingVertex, 1, arg0);
    glProvokingVertex(arg0);
    _post_call_gl_callback(NULL, "glProvokingVertex", (GLADapiproc) glProvokingVertex, 1, arg0);

}
PFNGLPROVOKINGVERTEXPROC glad_debug_glProvokingVertex = glad_debug_impl_glProvokingVertex;
PFNGLQUERYCOUNTERPROC glad_glQueryCounter = NULL;
void GLAD_API_PTR glad_debug_impl_glQueryCounter(GLuint arg0, GLenum arg1) {
    _pre_call_gl_callback("glQueryCounter", (GLADapiproc) glQueryCounter, 2, arg0, arg1);
    glQueryCounter(arg0, arg1);
    _post_call_gl_callback(NULL, "glQueryCounter", (GLADapiproc) glQueryCounter, 2, arg0, arg1);

}
PFNGLQUERYCOUNTERPROC glad_debug_glQueryCounter = glad_debug_impl_glQueryCounter;
PFNGLREADBUFFERPROC glad_glReadBuffer = NULL;
void GLAD_API_PTR glad_debug_impl_glReadBuffer(GLenum arg0) {
    _pre_call_gl_callback("glReadBuffer", (GLADapiproc) glReadBuffer, 1, arg0);
    glReadBuffer(arg0);
    _post_call_gl_callback(NULL, "glReadBuffer", (GLADapiproc) glReadBuffer, 1, arg0);

}
PFNGLREADBUFFERPROC glad_debug_glReadBuffer = glad_debug_impl_glReadBuffer;
PFNGLREADPIXELSPROC glad_glReadPixels = NULL;
void GLAD_API_PTR glad_debug_impl_glReadPixels(GLint arg0, GLint arg1, GLsizei arg2, GLsizei arg3, GLenum arg4, GLenum arg5, void * arg6) {
    _pre_call_gl_callback("glReadPixels", (GLADapiproc) glReadPixels, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    glReadPixels(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    _post_call_gl_callback(NULL, "glReadPixels", (GLADapiproc) glReadPixels, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);

}
PFNGLREADPIXELSPROC glad_debug_glReadPixels = glad_debug_impl_glReadPixels;
PFNGLRELEASESHADERCOMPILERPROC glad_glReleaseShaderCompiler = NULL;
void GLAD_API_PTR glad_debug_impl_glReleaseShaderCompiler(void) {
    _pre_call_gl_callback("glReleaseShaderCompiler", (GLADapiproc) glReleaseShaderCompiler, 0);
    glReleaseShaderCompiler();
    _post_call_gl_callback(NULL, "glReleaseShaderCompiler", (GLADapiproc) glReleaseShaderCompiler, 0);

}
PFNGLRELEASESHADERCOMPILERPROC glad_debug_glReleaseShaderCompiler = glad_debug_impl_glReleaseShaderCompiler;
PFNGLRENDERBUFFERSTORAGEPROC glad_glRenderbufferStorage = NULL;
void GLAD_API_PTR glad_debug_impl_glRenderbufferStorage(GLenum arg0, GLenum arg1, GLsizei arg2, GLsizei arg3) {
    _pre_call_gl_callback("glRenderbufferStorage", (GLADapiproc) glRenderbufferStorage, 4, arg0, arg1, arg2, arg3);
    glRenderbufferStorage(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glRenderbufferStorage", (GLADapiproc) glRenderbufferStorage, 4, arg0, arg1, arg2, arg3);

}
PFNGLRENDERBUFFERSTORAGEPROC glad_debug_glRenderbufferStorage = glad_debug_impl_glRenderbufferStorage;
PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC glad_glRenderbufferStorageMultisample = NULL;
void GLAD_API_PTR glad_debug_impl_glRenderbufferStorageMultisample(GLenum arg0, GLsizei arg1, GLenum arg2, GLsizei arg3, GLsizei arg4) {
    _pre_call_gl_callback("glRenderbufferStorageMultisample", (GLADapiproc) glRenderbufferStorageMultisample, 5, arg0, arg1, arg2, arg3, arg4);
    glRenderbufferStorageMultisample(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glRenderbufferStorageMultisample", (GLADapiproc) glRenderbufferStorageMultisample, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC glad_debug_glRenderbufferStorageMultisample = glad_debug_impl_glRenderbufferStorageMultisample;
PFNGLRESUMETRANSFORMFEEDBACKPROC glad_glResumeTransformFeedback = NULL;
void GLAD_API_PTR glad_debug_impl_glResumeTransformFeedback(void) {
    _pre_call_gl_callback("glResumeTransformFeedback", (GLADapiproc) glResumeTransformFeedback, 0);
    glResumeTransformFeedback();
    _post_call_gl_callback(NULL, "glResumeTransformFeedback", (GLADapiproc) glResumeTransformFeedback, 0);

}
PFNGLRESUMETRANSFORMFEEDBACKPROC glad_debug_glResumeTransformFeedback = glad_debug_impl_glResumeTransformFeedback;
PFNGLSAMPLECOVERAGEPROC glad_glSampleCoverage = NULL;
void GLAD_API_PTR glad_debug_impl_glSampleCoverage(GLfloat arg0, GLboolean arg1) {
    _pre_call_gl_callback("glSampleCoverage", (GLADapiproc) glSampleCoverage, 2, arg0, arg1);
    glSampleCoverage(arg0, arg1);
    _post_call_gl_callback(NULL, "glSampleCoverage", (GLADapiproc) glSampleCoverage, 2, arg0, arg1);

}
PFNGLSAMPLECOVERAGEPROC glad_debug_glSampleCoverage = glad_debug_impl_glSampleCoverage;
PFNGLSAMPLEMASKIPROC glad_glSampleMaski = NULL;
void GLAD_API_PTR glad_debug_impl_glSampleMaski(GLuint arg0, GLbitfield arg1) {
    _pre_call_gl_callback("glSampleMaski", (GLADapiproc) glSampleMaski, 2, arg0, arg1);
    glSampleMaski(arg0, arg1);
    _post_call_gl_callback(NULL, "glSampleMaski", (GLADapiproc) glSampleMaski, 2, arg0, arg1);

}
PFNGLSAMPLEMASKIPROC glad_debug_glSampleMaski = glad_debug_impl_glSampleMaski;
PFNGLSAMPLERPARAMETERIIVPROC glad_glSamplerParameterIiv = NULL;
void GLAD_API_PTR glad_debug_impl_glSamplerParameterIiv(GLuint arg0, GLenum arg1, const GLint * arg2) {
    _pre_call_gl_callback("glSamplerParameterIiv", (GLADapiproc) glSamplerParameterIiv, 3, arg0, arg1, arg2);
    glSamplerParameterIiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glSamplerParameterIiv", (GLADapiproc) glSamplerParameterIiv, 3, arg0, arg1, arg2);

}
PFNGLSAMPLERPARAMETERIIVPROC glad_debug_glSamplerParameterIiv = glad_debug_impl_glSamplerParameterIiv;
PFNGLSAMPLERPARAMETERIUIVPROC glad_glSamplerParameterIuiv = NULL;
void GLAD_API_PTR glad_debug_impl_glSamplerParameterIuiv(GLuint arg0, GLenum arg1, const GLuint * arg2) {
    _pre_call_gl_callback("glSamplerParameterIuiv", (GLADapiproc) glSamplerParameterIuiv, 3, arg0, arg1, arg2);
    glSamplerParameterIuiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glSamplerParameterIuiv", (GLADapiproc) glSamplerParameterIuiv, 3, arg0, arg1, arg2);

}
PFNGLSAMPLERPARAMETERIUIVPROC glad_debug_glSamplerParameterIuiv = glad_debug_impl_glSamplerParameterIuiv;
PFNGLSAMPLERPARAMETERFPROC glad_glSamplerParameterf = NULL;
void GLAD_API_PTR glad_debug_impl_glSamplerParameterf(GLuint arg0, GLenum arg1, GLfloat arg2) {
    _pre_call_gl_callback("glSamplerParameterf", (GLADapiproc) glSamplerParameterf, 3, arg0, arg1, arg2);
    glSamplerParameterf(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glSamplerParameterf", (GLADapiproc) glSamplerParameterf, 3, arg0, arg1, arg2);

}
PFNGLSAMPLERPARAMETERFPROC glad_debug_glSamplerParameterf = glad_debug_impl_glSamplerParameterf;
PFNGLSAMPLERPARAMETERFVPROC glad_glSamplerParameterfv = NULL;
void GLAD_API_PTR glad_debug_impl_glSamplerParameterfv(GLuint arg0, GLenum arg1, const GLfloat * arg2) {
    _pre_call_gl_callback("glSamplerParameterfv", (GLADapiproc) glSamplerParameterfv, 3, arg0, arg1, arg2);
    glSamplerParameterfv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glSamplerParameterfv", (GLADapiproc) glSamplerParameterfv, 3, arg0, arg1, arg2);

}
PFNGLSAMPLERPARAMETERFVPROC glad_debug_glSamplerParameterfv = glad_debug_impl_glSamplerParameterfv;
PFNGLSAMPLERPARAMETERIPROC glad_glSamplerParameteri = NULL;
void GLAD_API_PTR glad_debug_impl_glSamplerParameteri(GLuint arg0, GLenum arg1, GLint arg2) {
    _pre_call_gl_callback("glSamplerParameteri", (GLADapiproc) glSamplerParameteri, 3, arg0, arg1, arg2);
    glSamplerParameteri(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glSamplerParameteri", (GLADapiproc) glSamplerParameteri, 3, arg0, arg1, arg2);

}
PFNGLSAMPLERPARAMETERIPROC glad_debug_glSamplerParameteri = glad_debug_impl_glSamplerParameteri;
PFNGLSAMPLERPARAMETERIVPROC glad_glSamplerParameteriv = NULL;
void GLAD_API_PTR glad_debug_impl_glSamplerParameteriv(GLuint arg0, GLenum arg1, const GLint * arg2) {
    _pre_call_gl_callback("glSamplerParameteriv", (GLADapiproc) glSamplerParameteriv, 3, arg0, arg1, arg2);
    glSamplerParameteriv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glSamplerParameteriv", (GLADapiproc) glSamplerParameteriv, 3, arg0, arg1, arg2);

}
PFNGLSAMPLERPARAMETERIVPROC glad_debug_glSamplerParameteriv = glad_debug_impl_glSamplerParameteriv;
PFNGLSCISSORPROC glad_glScissor = NULL;
void GLAD_API_PTR glad_debug_impl_glScissor(GLint arg0, GLint arg1, GLsizei arg2, GLsizei arg3) {
    _pre_call_gl_callback("glScissor", (GLADapiproc) glScissor, 4, arg0, arg1, arg2, arg3);
    glScissor(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glScissor", (GLADapiproc) glScissor, 4, arg0, arg1, arg2, arg3);

}
PFNGLSCISSORPROC glad_debug_glScissor = glad_debug_impl_glScissor;
PFNGLSCISSORARRAYVPROC glad_glScissorArrayv = NULL;
void GLAD_API_PTR glad_debug_impl_glScissorArrayv(GLuint arg0, GLsizei arg1, const GLint * arg2) {
    _pre_call_gl_callback("glScissorArrayv", (GLADapiproc) glScissorArrayv, 3, arg0, arg1, arg2);
    glScissorArrayv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glScissorArrayv", (GLADapiproc) glScissorArrayv, 3, arg0, arg1, arg2);

}
PFNGLSCISSORARRAYVPROC glad_debug_glScissorArrayv = glad_debug_impl_glScissorArrayv;
PFNGLSCISSORINDEXEDPROC glad_glScissorIndexed = NULL;
void GLAD_API_PTR glad_debug_impl_glScissorIndexed(GLuint arg0, GLint arg1, GLint arg2, GLsizei arg3, GLsizei arg4) {
    _pre_call_gl_callback("glScissorIndexed", (GLADapiproc) glScissorIndexed, 5, arg0, arg1, arg2, arg3, arg4);
    glScissorIndexed(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glScissorIndexed", (GLADapiproc) glScissorIndexed, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLSCISSORINDEXEDPROC glad_debug_glScissorIndexed = glad_debug_impl_glScissorIndexed;
PFNGLSCISSORINDEXEDVPROC glad_glScissorIndexedv = NULL;
void GLAD_API_PTR glad_debug_impl_glScissorIndexedv(GLuint arg0, const GLint * arg1) {
    _pre_call_gl_callback("glScissorIndexedv", (GLADapiproc) glScissorIndexedv, 2, arg0, arg1);
    glScissorIndexedv(arg0, arg1);
    _post_call_gl_callback(NULL, "glScissorIndexedv", (GLADapiproc) glScissorIndexedv, 2, arg0, arg1);

}
PFNGLSCISSORINDEXEDVPROC glad_debug_glScissorIndexedv = glad_debug_impl_glScissorIndexedv;
PFNGLSHADERBINARYPROC glad_glShaderBinary = NULL;
void GLAD_API_PTR glad_debug_impl_glShaderBinary(GLsizei arg0, const GLuint * arg1, GLenum arg2, const void * arg3, GLsizei arg4) {
    _pre_call_gl_callback("glShaderBinary", (GLADapiproc) glShaderBinary, 5, arg0, arg1, arg2, arg3, arg4);
    glShaderBinary(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glShaderBinary", (GLADapiproc) glShaderBinary, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLSHADERBINARYPROC glad_debug_glShaderBinary = glad_debug_impl_glShaderBinary;
PFNGLSHADERSOURCEPROC glad_glShaderSource = NULL;
void GLAD_API_PTR glad_debug_impl_glShaderSource(GLuint arg0, GLsizei arg1, const GLchar *const* arg2, const GLint * arg3) {
    _pre_call_gl_callback("glShaderSource", (GLADapiproc) glShaderSource, 4, arg0, arg1, arg2, arg3);
    glShaderSource(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glShaderSource", (GLADapiproc) glShaderSource, 4, arg0, arg1, arg2, arg3);

}
PFNGLSHADERSOURCEPROC glad_debug_glShaderSource = glad_debug_impl_glShaderSource;
PFNGLSTENCILFUNCPROC glad_glStencilFunc = NULL;
void GLAD_API_PTR glad_debug_impl_glStencilFunc(GLenum arg0, GLint arg1, GLuint arg2) {
    _pre_call_gl_callback("glStencilFunc", (GLADapiproc) glStencilFunc, 3, arg0, arg1, arg2);
    glStencilFunc(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glStencilFunc", (GLADapiproc) glStencilFunc, 3, arg0, arg1, arg2);

}
PFNGLSTENCILFUNCPROC glad_debug_glStencilFunc = glad_debug_impl_glStencilFunc;
PFNGLSTENCILFUNCSEPARATEPROC glad_glStencilFuncSeparate = NULL;
void GLAD_API_PTR glad_debug_impl_glStencilFuncSeparate(GLenum arg0, GLenum arg1, GLint arg2, GLuint arg3) {
    _pre_call_gl_callback("glStencilFuncSeparate", (GLADapiproc) glStencilFuncSeparate, 4, arg0, arg1, arg2, arg3);
    glStencilFuncSeparate(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glStencilFuncSeparate", (GLADapiproc) glStencilFuncSeparate, 4, arg0, arg1, arg2, arg3);

}
PFNGLSTENCILFUNCSEPARATEPROC glad_debug_glStencilFuncSeparate = glad_debug_impl_glStencilFuncSeparate;
PFNGLSTENCILMASKPROC glad_glStencilMask = NULL;
void GLAD_API_PTR glad_debug_impl_glStencilMask(GLuint arg0) {
    _pre_call_gl_callback("glStencilMask", (GLADapiproc) glStencilMask, 1, arg0);
    glStencilMask(arg0);
    _post_call_gl_callback(NULL, "glStencilMask", (GLADapiproc) glStencilMask, 1, arg0);

}
PFNGLSTENCILMASKPROC glad_debug_glStencilMask = glad_debug_impl_glStencilMask;
PFNGLSTENCILMASKSEPARATEPROC glad_glStencilMaskSeparate = NULL;
void GLAD_API_PTR glad_debug_impl_glStencilMaskSeparate(GLenum arg0, GLuint arg1) {
    _pre_call_gl_callback("glStencilMaskSeparate", (GLADapiproc) glStencilMaskSeparate, 2, arg0, arg1);
    glStencilMaskSeparate(arg0, arg1);
    _post_call_gl_callback(NULL, "glStencilMaskSeparate", (GLADapiproc) glStencilMaskSeparate, 2, arg0, arg1);

}
PFNGLSTENCILMASKSEPARATEPROC glad_debug_glStencilMaskSeparate = glad_debug_impl_glStencilMaskSeparate;
PFNGLSTENCILOPPROC glad_glStencilOp = NULL;
void GLAD_API_PTR glad_debug_impl_glStencilOp(GLenum arg0, GLenum arg1, GLenum arg2) {
    _pre_call_gl_callback("glStencilOp", (GLADapiproc) glStencilOp, 3, arg0, arg1, arg2);
    glStencilOp(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glStencilOp", (GLADapiproc) glStencilOp, 3, arg0, arg1, arg2);

}
PFNGLSTENCILOPPROC glad_debug_glStencilOp = glad_debug_impl_glStencilOp;
PFNGLSTENCILOPSEPARATEPROC glad_glStencilOpSeparate = NULL;
void GLAD_API_PTR glad_debug_impl_glStencilOpSeparate(GLenum arg0, GLenum arg1, GLenum arg2, GLenum arg3) {
    _pre_call_gl_callback("glStencilOpSeparate", (GLADapiproc) glStencilOpSeparate, 4, arg0, arg1, arg2, arg3);
    glStencilOpSeparate(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glStencilOpSeparate", (GLADapiproc) glStencilOpSeparate, 4, arg0, arg1, arg2, arg3);

}
PFNGLSTENCILOPSEPARATEPROC glad_debug_glStencilOpSeparate = glad_debug_impl_glStencilOpSeparate;
PFNGLTEXBUFFERPROC glad_glTexBuffer = NULL;
void GLAD_API_PTR glad_debug_impl_glTexBuffer(GLenum arg0, GLenum arg1, GLuint arg2) {
    _pre_call_gl_callback("glTexBuffer", (GLADapiproc) glTexBuffer, 3, arg0, arg1, arg2);
    glTexBuffer(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glTexBuffer", (GLADapiproc) glTexBuffer, 3, arg0, arg1, arg2);

}
PFNGLTEXBUFFERPROC glad_debug_glTexBuffer = glad_debug_impl_glTexBuffer;
PFNGLTEXIMAGE1DPROC glad_glTexImage1D = NULL;
void GLAD_API_PTR glad_debug_impl_glTexImage1D(GLenum arg0, GLint arg1, GLint arg2, GLsizei arg3, GLint arg4, GLenum arg5, GLenum arg6, const void * arg7) {
    _pre_call_gl_callback("glTexImage1D", (GLADapiproc) glTexImage1D, 8, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    glTexImage1D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    _post_call_gl_callback(NULL, "glTexImage1D", (GLADapiproc) glTexImage1D, 8, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);

}
PFNGLTEXIMAGE1DPROC glad_debug_glTexImage1D = glad_debug_impl_glTexImage1D;
PFNGLTEXIMAGE2DPROC glad_glTexImage2D = NULL;
void GLAD_API_PTR glad_debug_impl_glTexImage2D(GLenum arg0, GLint arg1, GLint arg2, GLsizei arg3, GLsizei arg4, GLint arg5, GLenum arg6, GLenum arg7, const void * arg8) {
    _pre_call_gl_callback("glTexImage2D", (GLADapiproc) glTexImage2D, 9, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    glTexImage2D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    _post_call_gl_callback(NULL, "glTexImage2D", (GLADapiproc) glTexImage2D, 9, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);

}
PFNGLTEXIMAGE2DPROC glad_debug_glTexImage2D = glad_debug_impl_glTexImage2D;
PFNGLTEXIMAGE2DMULTISAMPLEPROC glad_glTexImage2DMultisample = NULL;
void GLAD_API_PTR glad_debug_impl_glTexImage2DMultisample(GLenum arg0, GLsizei arg1, GLenum arg2, GLsizei arg3, GLsizei arg4, GLboolean arg5) {
    _pre_call_gl_callback("glTexImage2DMultisample", (GLADapiproc) glTexImage2DMultisample, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glTexImage2DMultisample(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glTexImage2DMultisample", (GLADapiproc) glTexImage2DMultisample, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLTEXIMAGE2DMULTISAMPLEPROC glad_debug_glTexImage2DMultisample = glad_debug_impl_glTexImage2DMultisample;
PFNGLTEXIMAGE3DPROC glad_glTexImage3D = NULL;
void GLAD_API_PTR glad_debug_impl_glTexImage3D(GLenum arg0, GLint arg1, GLint arg2, GLsizei arg3, GLsizei arg4, GLsizei arg5, GLint arg6, GLenum arg7, GLenum arg8, const void * arg9) {
    _pre_call_gl_callback("glTexImage3D", (GLADapiproc) glTexImage3D, 10, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
    glTexImage3D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
    _post_call_gl_callback(NULL, "glTexImage3D", (GLADapiproc) glTexImage3D, 10, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);

}
PFNGLTEXIMAGE3DPROC glad_debug_glTexImage3D = glad_debug_impl_glTexImage3D;
PFNGLTEXIMAGE3DMULTISAMPLEPROC glad_glTexImage3DMultisample = NULL;
void GLAD_API_PTR glad_debug_impl_glTexImage3DMultisample(GLenum arg0, GLsizei arg1, GLenum arg2, GLsizei arg3, GLsizei arg4, GLsizei arg5, GLboolean arg6) {
    _pre_call_gl_callback("glTexImage3DMultisample", (GLADapiproc) glTexImage3DMultisample, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    glTexImage3DMultisample(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    _post_call_gl_callback(NULL, "glTexImage3DMultisample", (GLADapiproc) glTexImage3DMultisample, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);

}
PFNGLTEXIMAGE3DMULTISAMPLEPROC glad_debug_glTexImage3DMultisample = glad_debug_impl_glTexImage3DMultisample;
PFNGLTEXPARAMETERIIVPROC glad_glTexParameterIiv = NULL;
void GLAD_API_PTR glad_debug_impl_glTexParameterIiv(GLenum arg0, GLenum arg1, const GLint * arg2) {
    _pre_call_gl_callback("glTexParameterIiv", (GLADapiproc) glTexParameterIiv, 3, arg0, arg1, arg2);
    glTexParameterIiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glTexParameterIiv", (GLADapiproc) glTexParameterIiv, 3, arg0, arg1, arg2);

}
PFNGLTEXPARAMETERIIVPROC glad_debug_glTexParameterIiv = glad_debug_impl_glTexParameterIiv;
PFNGLTEXPARAMETERIUIVPROC glad_glTexParameterIuiv = NULL;
void GLAD_API_PTR glad_debug_impl_glTexParameterIuiv(GLenum arg0, GLenum arg1, const GLuint * arg2) {
    _pre_call_gl_callback("glTexParameterIuiv", (GLADapiproc) glTexParameterIuiv, 3, arg0, arg1, arg2);
    glTexParameterIuiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glTexParameterIuiv", (GLADapiproc) glTexParameterIuiv, 3, arg0, arg1, arg2);

}
PFNGLTEXPARAMETERIUIVPROC glad_debug_glTexParameterIuiv = glad_debug_impl_glTexParameterIuiv;
PFNGLTEXPARAMETERFPROC glad_glTexParameterf = NULL;
void GLAD_API_PTR glad_debug_impl_glTexParameterf(GLenum arg0, GLenum arg1, GLfloat arg2) {
    _pre_call_gl_callback("glTexParameterf", (GLADapiproc) glTexParameterf, 3, arg0, arg1, arg2);
    glTexParameterf(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glTexParameterf", (GLADapiproc) glTexParameterf, 3, arg0, arg1, arg2);

}
PFNGLTEXPARAMETERFPROC glad_debug_glTexParameterf = glad_debug_impl_glTexParameterf;
PFNGLTEXPARAMETERFVPROC glad_glTexParameterfv = NULL;
void GLAD_API_PTR glad_debug_impl_glTexParameterfv(GLenum arg0, GLenum arg1, const GLfloat * arg2) {
    _pre_call_gl_callback("glTexParameterfv", (GLADapiproc) glTexParameterfv, 3, arg0, arg1, arg2);
    glTexParameterfv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glTexParameterfv", (GLADapiproc) glTexParameterfv, 3, arg0, arg1, arg2);

}
PFNGLTEXPARAMETERFVPROC glad_debug_glTexParameterfv = glad_debug_impl_glTexParameterfv;
PFNGLTEXPARAMETERIPROC glad_glTexParameteri = NULL;
void GLAD_API_PTR glad_debug_impl_glTexParameteri(GLenum arg0, GLenum arg1, GLint arg2) {
    _pre_call_gl_callback("glTexParameteri", (GLADapiproc) glTexParameteri, 3, arg0, arg1, arg2);
    glTexParameteri(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glTexParameteri", (GLADapiproc) glTexParameteri, 3, arg0, arg1, arg2);

}
PFNGLTEXPARAMETERIPROC glad_debug_glTexParameteri = glad_debug_impl_glTexParameteri;
PFNGLTEXPARAMETERIVPROC glad_glTexParameteriv = NULL;
void GLAD_API_PTR glad_debug_impl_glTexParameteriv(GLenum arg0, GLenum arg1, const GLint * arg2) {
    _pre_call_gl_callback("glTexParameteriv", (GLADapiproc) glTexParameteriv, 3, arg0, arg1, arg2);
    glTexParameteriv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glTexParameteriv", (GLADapiproc) glTexParameteriv, 3, arg0, arg1, arg2);

}
PFNGLTEXPARAMETERIVPROC glad_debug_glTexParameteriv = glad_debug_impl_glTexParameteriv;
PFNGLTEXSUBIMAGE1DPROC glad_glTexSubImage1D = NULL;
void GLAD_API_PTR glad_debug_impl_glTexSubImage1D(GLenum arg0, GLint arg1, GLint arg2, GLsizei arg3, GLenum arg4, GLenum arg5, const void * arg6) {
    _pre_call_gl_callback("glTexSubImage1D", (GLADapiproc) glTexSubImage1D, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    glTexSubImage1D(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    _post_call_gl_callback(NULL, "glTexSubImage1D", (GLADapiproc) glTexSubImage1D, 7, arg0, arg1, arg2, arg3, arg4, arg5, arg6);

}
PFNGLTEXSUBIMAGE1DPROC glad_debug_glTexSubImage1D = glad_debug_impl_glTexSubImage1D;
PFNGLTEXSUBIMAGE2DPROC glad_glTexSubImage2D = NULL;
void GLAD_API_PTR glad_debug_impl_glTexSubImage2D(GLenum arg0, GLint arg1, GLint arg2, GLint arg3, GLsizei arg4, GLsizei arg5, GLenum arg6, GLenum arg7, const void * arg8) {
    _pre_call_gl_callback("glTexSubImage2D", (GLADapiproc) glTexSubImage2D, 9, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    glTexSubImage2D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    _post_call_gl_callback(NULL, "glTexSubImage2D", (GLADapiproc) glTexSubImage2D, 9, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);

}
PFNGLTEXSUBIMAGE2DPROC glad_debug_glTexSubImage2D = glad_debug_impl_glTexSubImage2D;
PFNGLTEXSUBIMAGE3DPROC glad_glTexSubImage3D = NULL;
void GLAD_API_PTR glad_debug_impl_glTexSubImage3D(GLenum arg0, GLint arg1, GLint arg2, GLint arg3, GLint arg4, GLsizei arg5, GLsizei arg6, GLsizei arg7, GLenum arg8, GLenum arg9, const void * arg10) {
    _pre_call_gl_callback("glTexSubImage3D", (GLADapiproc) glTexSubImage3D, 11, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
    glTexSubImage3D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
    _post_call_gl_callback(NULL, "glTexSubImage3D", (GLADapiproc) glTexSubImage3D, 11, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);

}
PFNGLTEXSUBIMAGE3DPROC glad_debug_glTexSubImage3D = glad_debug_impl_glTexSubImage3D;
PFNGLTRANSFORMFEEDBACKVARYINGSPROC glad_glTransformFeedbackVaryings = NULL;
void GLAD_API_PTR glad_debug_impl_glTransformFeedbackVaryings(GLuint arg0, GLsizei arg1, const GLchar *const* arg2, GLenum arg3) {
    _pre_call_gl_callback("glTransformFeedbackVaryings", (GLADapiproc) glTransformFeedbackVaryings, 4, arg0, arg1, arg2, arg3);
    glTransformFeedbackVaryings(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glTransformFeedbackVaryings", (GLADapiproc) glTransformFeedbackVaryings, 4, arg0, arg1, arg2, arg3);

}
PFNGLTRANSFORMFEEDBACKVARYINGSPROC glad_debug_glTransformFeedbackVaryings = glad_debug_impl_glTransformFeedbackVaryings;
PFNGLUNIFORM1DPROC glad_glUniform1d = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform1d(GLint arg0, GLdouble arg1) {
    _pre_call_gl_callback("glUniform1d", (GLADapiproc) glUniform1d, 2, arg0, arg1);
    glUniform1d(arg0, arg1);
    _post_call_gl_callback(NULL, "glUniform1d", (GLADapiproc) glUniform1d, 2, arg0, arg1);

}
PFNGLUNIFORM1DPROC glad_debug_glUniform1d = glad_debug_impl_glUniform1d;
PFNGLUNIFORM1DVPROC glad_glUniform1dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform1dv(GLint arg0, GLsizei arg1, const GLdouble * arg2) {
    _pre_call_gl_callback("glUniform1dv", (GLADapiproc) glUniform1dv, 3, arg0, arg1, arg2);
    glUniform1dv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform1dv", (GLADapiproc) glUniform1dv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM1DVPROC glad_debug_glUniform1dv = glad_debug_impl_glUniform1dv;
PFNGLUNIFORM1FPROC glad_glUniform1f = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform1f(GLint arg0, GLfloat arg1) {
    _pre_call_gl_callback("glUniform1f", (GLADapiproc) glUniform1f, 2, arg0, arg1);
    glUniform1f(arg0, arg1);
    _post_call_gl_callback(NULL, "glUniform1f", (GLADapiproc) glUniform1f, 2, arg0, arg1);

}
PFNGLUNIFORM1FPROC glad_debug_glUniform1f = glad_debug_impl_glUniform1f;
PFNGLUNIFORM1FVPROC glad_glUniform1fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform1fv(GLint arg0, GLsizei arg1, const GLfloat * arg2) {
    _pre_call_gl_callback("glUniform1fv", (GLADapiproc) glUniform1fv, 3, arg0, arg1, arg2);
    glUniform1fv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform1fv", (GLADapiproc) glUniform1fv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM1FVPROC glad_debug_glUniform1fv = glad_debug_impl_glUniform1fv;
PFNGLUNIFORM1IPROC glad_glUniform1i = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform1i(GLint arg0, GLint arg1) {
    _pre_call_gl_callback("glUniform1i", (GLADapiproc) glUniform1i, 2, arg0, arg1);
    glUniform1i(arg0, arg1);
    _post_call_gl_callback(NULL, "glUniform1i", (GLADapiproc) glUniform1i, 2, arg0, arg1);

}
PFNGLUNIFORM1IPROC glad_debug_glUniform1i = glad_debug_impl_glUniform1i;
PFNGLUNIFORM1IVPROC glad_glUniform1iv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform1iv(GLint arg0, GLsizei arg1, const GLint * arg2) {
    _pre_call_gl_callback("glUniform1iv", (GLADapiproc) glUniform1iv, 3, arg0, arg1, arg2);
    glUniform1iv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform1iv", (GLADapiproc) glUniform1iv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM1IVPROC glad_debug_glUniform1iv = glad_debug_impl_glUniform1iv;
PFNGLUNIFORM1UIPROC glad_glUniform1ui = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform1ui(GLint arg0, GLuint arg1) {
    _pre_call_gl_callback("glUniform1ui", (GLADapiproc) glUniform1ui, 2, arg0, arg1);
    glUniform1ui(arg0, arg1);
    _post_call_gl_callback(NULL, "glUniform1ui", (GLADapiproc) glUniform1ui, 2, arg0, arg1);

}
PFNGLUNIFORM1UIPROC glad_debug_glUniform1ui = glad_debug_impl_glUniform1ui;
PFNGLUNIFORM1UIVPROC glad_glUniform1uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform1uiv(GLint arg0, GLsizei arg1, const GLuint * arg2) {
    _pre_call_gl_callback("glUniform1uiv", (GLADapiproc) glUniform1uiv, 3, arg0, arg1, arg2);
    glUniform1uiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform1uiv", (GLADapiproc) glUniform1uiv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM1UIVPROC glad_debug_glUniform1uiv = glad_debug_impl_glUniform1uiv;
PFNGLUNIFORM2DPROC glad_glUniform2d = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform2d(GLint arg0, GLdouble arg1, GLdouble arg2) {
    _pre_call_gl_callback("glUniform2d", (GLADapiproc) glUniform2d, 3, arg0, arg1, arg2);
    glUniform2d(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform2d", (GLADapiproc) glUniform2d, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM2DPROC glad_debug_glUniform2d = glad_debug_impl_glUniform2d;
PFNGLUNIFORM2DVPROC glad_glUniform2dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform2dv(GLint arg0, GLsizei arg1, const GLdouble * arg2) {
    _pre_call_gl_callback("glUniform2dv", (GLADapiproc) glUniform2dv, 3, arg0, arg1, arg2);
    glUniform2dv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform2dv", (GLADapiproc) glUniform2dv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM2DVPROC glad_debug_glUniform2dv = glad_debug_impl_glUniform2dv;
PFNGLUNIFORM2FPROC glad_glUniform2f = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform2f(GLint arg0, GLfloat arg1, GLfloat arg2) {
    _pre_call_gl_callback("glUniform2f", (GLADapiproc) glUniform2f, 3, arg0, arg1, arg2);
    glUniform2f(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform2f", (GLADapiproc) glUniform2f, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM2FPROC glad_debug_glUniform2f = glad_debug_impl_glUniform2f;
PFNGLUNIFORM2FVPROC glad_glUniform2fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform2fv(GLint arg0, GLsizei arg1, const GLfloat * arg2) {
    _pre_call_gl_callback("glUniform2fv", (GLADapiproc) glUniform2fv, 3, arg0, arg1, arg2);
    glUniform2fv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform2fv", (GLADapiproc) glUniform2fv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM2FVPROC glad_debug_glUniform2fv = glad_debug_impl_glUniform2fv;
PFNGLUNIFORM2IPROC glad_glUniform2i = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform2i(GLint arg0, GLint arg1, GLint arg2) {
    _pre_call_gl_callback("glUniform2i", (GLADapiproc) glUniform2i, 3, arg0, arg1, arg2);
    glUniform2i(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform2i", (GLADapiproc) glUniform2i, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM2IPROC glad_debug_glUniform2i = glad_debug_impl_glUniform2i;
PFNGLUNIFORM2IVPROC glad_glUniform2iv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform2iv(GLint arg0, GLsizei arg1, const GLint * arg2) {
    _pre_call_gl_callback("glUniform2iv", (GLADapiproc) glUniform2iv, 3, arg0, arg1, arg2);
    glUniform2iv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform2iv", (GLADapiproc) glUniform2iv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM2IVPROC glad_debug_glUniform2iv = glad_debug_impl_glUniform2iv;
PFNGLUNIFORM2UIPROC glad_glUniform2ui = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform2ui(GLint arg0, GLuint arg1, GLuint arg2) {
    _pre_call_gl_callback("glUniform2ui", (GLADapiproc) glUniform2ui, 3, arg0, arg1, arg2);
    glUniform2ui(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform2ui", (GLADapiproc) glUniform2ui, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM2UIPROC glad_debug_glUniform2ui = glad_debug_impl_glUniform2ui;
PFNGLUNIFORM2UIVPROC glad_glUniform2uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform2uiv(GLint arg0, GLsizei arg1, const GLuint * arg2) {
    _pre_call_gl_callback("glUniform2uiv", (GLADapiproc) glUniform2uiv, 3, arg0, arg1, arg2);
    glUniform2uiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform2uiv", (GLADapiproc) glUniform2uiv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM2UIVPROC glad_debug_glUniform2uiv = glad_debug_impl_glUniform2uiv;
PFNGLUNIFORM3DPROC glad_glUniform3d = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform3d(GLint arg0, GLdouble arg1, GLdouble arg2, GLdouble arg3) {
    _pre_call_gl_callback("glUniform3d", (GLADapiproc) glUniform3d, 4, arg0, arg1, arg2, arg3);
    glUniform3d(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniform3d", (GLADapiproc) glUniform3d, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORM3DPROC glad_debug_glUniform3d = glad_debug_impl_glUniform3d;
PFNGLUNIFORM3DVPROC glad_glUniform3dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform3dv(GLint arg0, GLsizei arg1, const GLdouble * arg2) {
    _pre_call_gl_callback("glUniform3dv", (GLADapiproc) glUniform3dv, 3, arg0, arg1, arg2);
    glUniform3dv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform3dv", (GLADapiproc) glUniform3dv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM3DVPROC glad_debug_glUniform3dv = glad_debug_impl_glUniform3dv;
PFNGLUNIFORM3FPROC glad_glUniform3f = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform3f(GLint arg0, GLfloat arg1, GLfloat arg2, GLfloat arg3) {
    _pre_call_gl_callback("glUniform3f", (GLADapiproc) glUniform3f, 4, arg0, arg1, arg2, arg3);
    glUniform3f(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniform3f", (GLADapiproc) glUniform3f, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORM3FPROC glad_debug_glUniform3f = glad_debug_impl_glUniform3f;
PFNGLUNIFORM3FVPROC glad_glUniform3fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform3fv(GLint arg0, GLsizei arg1, const GLfloat * arg2) {
    _pre_call_gl_callback("glUniform3fv", (GLADapiproc) glUniform3fv, 3, arg0, arg1, arg2);
    glUniform3fv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform3fv", (GLADapiproc) glUniform3fv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM3FVPROC glad_debug_glUniform3fv = glad_debug_impl_glUniform3fv;
PFNGLUNIFORM3IPROC glad_glUniform3i = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform3i(GLint arg0, GLint arg1, GLint arg2, GLint arg3) {
    _pre_call_gl_callback("glUniform3i", (GLADapiproc) glUniform3i, 4, arg0, arg1, arg2, arg3);
    glUniform3i(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniform3i", (GLADapiproc) glUniform3i, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORM3IPROC glad_debug_glUniform3i = glad_debug_impl_glUniform3i;
PFNGLUNIFORM3IVPROC glad_glUniform3iv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform3iv(GLint arg0, GLsizei arg1, const GLint * arg2) {
    _pre_call_gl_callback("glUniform3iv", (GLADapiproc) glUniform3iv, 3, arg0, arg1, arg2);
    glUniform3iv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform3iv", (GLADapiproc) glUniform3iv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM3IVPROC glad_debug_glUniform3iv = glad_debug_impl_glUniform3iv;
PFNGLUNIFORM3UIPROC glad_glUniform3ui = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform3ui(GLint arg0, GLuint arg1, GLuint arg2, GLuint arg3) {
    _pre_call_gl_callback("glUniform3ui", (GLADapiproc) glUniform3ui, 4, arg0, arg1, arg2, arg3);
    glUniform3ui(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniform3ui", (GLADapiproc) glUniform3ui, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORM3UIPROC glad_debug_glUniform3ui = glad_debug_impl_glUniform3ui;
PFNGLUNIFORM3UIVPROC glad_glUniform3uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform3uiv(GLint arg0, GLsizei arg1, const GLuint * arg2) {
    _pre_call_gl_callback("glUniform3uiv", (GLADapiproc) glUniform3uiv, 3, arg0, arg1, arg2);
    glUniform3uiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform3uiv", (GLADapiproc) glUniform3uiv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM3UIVPROC glad_debug_glUniform3uiv = glad_debug_impl_glUniform3uiv;
PFNGLUNIFORM4DPROC glad_glUniform4d = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform4d(GLint arg0, GLdouble arg1, GLdouble arg2, GLdouble arg3, GLdouble arg4) {
    _pre_call_gl_callback("glUniform4d", (GLADapiproc) glUniform4d, 5, arg0, arg1, arg2, arg3, arg4);
    glUniform4d(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glUniform4d", (GLADapiproc) glUniform4d, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLUNIFORM4DPROC glad_debug_glUniform4d = glad_debug_impl_glUniform4d;
PFNGLUNIFORM4DVPROC glad_glUniform4dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform4dv(GLint arg0, GLsizei arg1, const GLdouble * arg2) {
    _pre_call_gl_callback("glUniform4dv", (GLADapiproc) glUniform4dv, 3, arg0, arg1, arg2);
    glUniform4dv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform4dv", (GLADapiproc) glUniform4dv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM4DVPROC glad_debug_glUniform4dv = glad_debug_impl_glUniform4dv;
PFNGLUNIFORM4FPROC glad_glUniform4f = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform4f(GLint arg0, GLfloat arg1, GLfloat arg2, GLfloat arg3, GLfloat arg4) {
    _pre_call_gl_callback("glUniform4f", (GLADapiproc) glUniform4f, 5, arg0, arg1, arg2, arg3, arg4);
    glUniform4f(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glUniform4f", (GLADapiproc) glUniform4f, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLUNIFORM4FPROC glad_debug_glUniform4f = glad_debug_impl_glUniform4f;
PFNGLUNIFORM4FVPROC glad_glUniform4fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform4fv(GLint arg0, GLsizei arg1, const GLfloat * arg2) {
    _pre_call_gl_callback("glUniform4fv", (GLADapiproc) glUniform4fv, 3, arg0, arg1, arg2);
    glUniform4fv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform4fv", (GLADapiproc) glUniform4fv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM4FVPROC glad_debug_glUniform4fv = glad_debug_impl_glUniform4fv;
PFNGLUNIFORM4IPROC glad_glUniform4i = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform4i(GLint arg0, GLint arg1, GLint arg2, GLint arg3, GLint arg4) {
    _pre_call_gl_callback("glUniform4i", (GLADapiproc) glUniform4i, 5, arg0, arg1, arg2, arg3, arg4);
    glUniform4i(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glUniform4i", (GLADapiproc) glUniform4i, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLUNIFORM4IPROC glad_debug_glUniform4i = glad_debug_impl_glUniform4i;
PFNGLUNIFORM4IVPROC glad_glUniform4iv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform4iv(GLint arg0, GLsizei arg1, const GLint * arg2) {
    _pre_call_gl_callback("glUniform4iv", (GLADapiproc) glUniform4iv, 3, arg0, arg1, arg2);
    glUniform4iv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform4iv", (GLADapiproc) glUniform4iv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM4IVPROC glad_debug_glUniform4iv = glad_debug_impl_glUniform4iv;
PFNGLUNIFORM4UIPROC glad_glUniform4ui = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform4ui(GLint arg0, GLuint arg1, GLuint arg2, GLuint arg3, GLuint arg4) {
    _pre_call_gl_callback("glUniform4ui", (GLADapiproc) glUniform4ui, 5, arg0, arg1, arg2, arg3, arg4);
    glUniform4ui(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glUniform4ui", (GLADapiproc) glUniform4ui, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLUNIFORM4UIPROC glad_debug_glUniform4ui = glad_debug_impl_glUniform4ui;
PFNGLUNIFORM4UIVPROC glad_glUniform4uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniform4uiv(GLint arg0, GLsizei arg1, const GLuint * arg2) {
    _pre_call_gl_callback("glUniform4uiv", (GLADapiproc) glUniform4uiv, 3, arg0, arg1, arg2);
    glUniform4uiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniform4uiv", (GLADapiproc) glUniform4uiv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORM4UIVPROC glad_debug_glUniform4uiv = glad_debug_impl_glUniform4uiv;
PFNGLUNIFORMBLOCKBINDINGPROC glad_glUniformBlockBinding = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformBlockBinding(GLuint arg0, GLuint arg1, GLuint arg2) {
    _pre_call_gl_callback("glUniformBlockBinding", (GLADapiproc) glUniformBlockBinding, 3, arg0, arg1, arg2);
    glUniformBlockBinding(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniformBlockBinding", (GLADapiproc) glUniformBlockBinding, 3, arg0, arg1, arg2);

}
PFNGLUNIFORMBLOCKBINDINGPROC glad_debug_glUniformBlockBinding = glad_debug_impl_glUniformBlockBinding;
PFNGLUNIFORMMATRIX2DVPROC glad_glUniformMatrix2dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix2dv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glUniformMatrix2dv", (GLADapiproc) glUniformMatrix2dv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix2dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix2dv", (GLADapiproc) glUniformMatrix2dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX2DVPROC glad_debug_glUniformMatrix2dv = glad_debug_impl_glUniformMatrix2dv;
PFNGLUNIFORMMATRIX2FVPROC glad_glUniformMatrix2fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix2fv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glUniformMatrix2fv", (GLADapiproc) glUniformMatrix2fv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix2fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix2fv", (GLADapiproc) glUniformMatrix2fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX2FVPROC glad_debug_glUniformMatrix2fv = glad_debug_impl_glUniformMatrix2fv;
PFNGLUNIFORMMATRIX2X3DVPROC glad_glUniformMatrix2x3dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix2x3dv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glUniformMatrix2x3dv", (GLADapiproc) glUniformMatrix2x3dv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix2x3dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix2x3dv", (GLADapiproc) glUniformMatrix2x3dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX2X3DVPROC glad_debug_glUniformMatrix2x3dv = glad_debug_impl_glUniformMatrix2x3dv;
PFNGLUNIFORMMATRIX2X3FVPROC glad_glUniformMatrix2x3fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix2x3fv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glUniformMatrix2x3fv", (GLADapiproc) glUniformMatrix2x3fv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix2x3fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix2x3fv", (GLADapiproc) glUniformMatrix2x3fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX2X3FVPROC glad_debug_glUniformMatrix2x3fv = glad_debug_impl_glUniformMatrix2x3fv;
PFNGLUNIFORMMATRIX2X4DVPROC glad_glUniformMatrix2x4dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix2x4dv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glUniformMatrix2x4dv", (GLADapiproc) glUniformMatrix2x4dv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix2x4dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix2x4dv", (GLADapiproc) glUniformMatrix2x4dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX2X4DVPROC glad_debug_glUniformMatrix2x4dv = glad_debug_impl_glUniformMatrix2x4dv;
PFNGLUNIFORMMATRIX2X4FVPROC glad_glUniformMatrix2x4fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix2x4fv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glUniformMatrix2x4fv", (GLADapiproc) glUniformMatrix2x4fv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix2x4fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix2x4fv", (GLADapiproc) glUniformMatrix2x4fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX2X4FVPROC glad_debug_glUniformMatrix2x4fv = glad_debug_impl_glUniformMatrix2x4fv;
PFNGLUNIFORMMATRIX3DVPROC glad_glUniformMatrix3dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix3dv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glUniformMatrix3dv", (GLADapiproc) glUniformMatrix3dv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix3dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix3dv", (GLADapiproc) glUniformMatrix3dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX3DVPROC glad_debug_glUniformMatrix3dv = glad_debug_impl_glUniformMatrix3dv;
PFNGLUNIFORMMATRIX3FVPROC glad_glUniformMatrix3fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix3fv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glUniformMatrix3fv", (GLADapiproc) glUniformMatrix3fv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix3fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix3fv", (GLADapiproc) glUniformMatrix3fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX3FVPROC glad_debug_glUniformMatrix3fv = glad_debug_impl_glUniformMatrix3fv;
PFNGLUNIFORMMATRIX3X2DVPROC glad_glUniformMatrix3x2dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix3x2dv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glUniformMatrix3x2dv", (GLADapiproc) glUniformMatrix3x2dv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix3x2dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix3x2dv", (GLADapiproc) glUniformMatrix3x2dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX3X2DVPROC glad_debug_glUniformMatrix3x2dv = glad_debug_impl_glUniformMatrix3x2dv;
PFNGLUNIFORMMATRIX3X2FVPROC glad_glUniformMatrix3x2fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix3x2fv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glUniformMatrix3x2fv", (GLADapiproc) glUniformMatrix3x2fv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix3x2fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix3x2fv", (GLADapiproc) glUniformMatrix3x2fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX3X2FVPROC glad_debug_glUniformMatrix3x2fv = glad_debug_impl_glUniformMatrix3x2fv;
PFNGLUNIFORMMATRIX3X4DVPROC glad_glUniformMatrix3x4dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix3x4dv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glUniformMatrix3x4dv", (GLADapiproc) glUniformMatrix3x4dv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix3x4dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix3x4dv", (GLADapiproc) glUniformMatrix3x4dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX3X4DVPROC glad_debug_glUniformMatrix3x4dv = glad_debug_impl_glUniformMatrix3x4dv;
PFNGLUNIFORMMATRIX3X4FVPROC glad_glUniformMatrix3x4fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix3x4fv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glUniformMatrix3x4fv", (GLADapiproc) glUniformMatrix3x4fv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix3x4fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix3x4fv", (GLADapiproc) glUniformMatrix3x4fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX3X4FVPROC glad_debug_glUniformMatrix3x4fv = glad_debug_impl_glUniformMatrix3x4fv;
PFNGLUNIFORMMATRIX4DVPROC glad_glUniformMatrix4dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix4dv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glUniformMatrix4dv", (GLADapiproc) glUniformMatrix4dv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix4dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix4dv", (GLADapiproc) glUniformMatrix4dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX4DVPROC glad_debug_glUniformMatrix4dv = glad_debug_impl_glUniformMatrix4dv;
PFNGLUNIFORMMATRIX4FVPROC glad_glUniformMatrix4fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix4fv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glUniformMatrix4fv", (GLADapiproc) glUniformMatrix4fv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix4fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix4fv", (GLADapiproc) glUniformMatrix4fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX4FVPROC glad_debug_glUniformMatrix4fv = glad_debug_impl_glUniformMatrix4fv;
PFNGLUNIFORMMATRIX4X2DVPROC glad_glUniformMatrix4x2dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix4x2dv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glUniformMatrix4x2dv", (GLADapiproc) glUniformMatrix4x2dv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix4x2dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix4x2dv", (GLADapiproc) glUniformMatrix4x2dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX4X2DVPROC glad_debug_glUniformMatrix4x2dv = glad_debug_impl_glUniformMatrix4x2dv;
PFNGLUNIFORMMATRIX4X2FVPROC glad_glUniformMatrix4x2fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix4x2fv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glUniformMatrix4x2fv", (GLADapiproc) glUniformMatrix4x2fv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix4x2fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix4x2fv", (GLADapiproc) glUniformMatrix4x2fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX4X2FVPROC glad_debug_glUniformMatrix4x2fv = glad_debug_impl_glUniformMatrix4x2fv;
PFNGLUNIFORMMATRIX4X3DVPROC glad_glUniformMatrix4x3dv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix4x3dv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLdouble * arg3) {
    _pre_call_gl_callback("glUniformMatrix4x3dv", (GLADapiproc) glUniformMatrix4x3dv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix4x3dv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix4x3dv", (GLADapiproc) glUniformMatrix4x3dv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX4X3DVPROC glad_debug_glUniformMatrix4x3dv = glad_debug_impl_glUniformMatrix4x3dv;
PFNGLUNIFORMMATRIX4X3FVPROC glad_glUniformMatrix4x3fv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformMatrix4x3fv(GLint arg0, GLsizei arg1, GLboolean arg2, const GLfloat * arg3) {
    _pre_call_gl_callback("glUniformMatrix4x3fv", (GLADapiproc) glUniformMatrix4x3fv, 4, arg0, arg1, arg2, arg3);
    glUniformMatrix4x3fv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glUniformMatrix4x3fv", (GLADapiproc) glUniformMatrix4x3fv, 4, arg0, arg1, arg2, arg3);

}
PFNGLUNIFORMMATRIX4X3FVPROC glad_debug_glUniformMatrix4x3fv = glad_debug_impl_glUniformMatrix4x3fv;
PFNGLUNIFORMSUBROUTINESUIVPROC glad_glUniformSubroutinesuiv = NULL;
void GLAD_API_PTR glad_debug_impl_glUniformSubroutinesuiv(GLenum arg0, GLsizei arg1, const GLuint * arg2) {
    _pre_call_gl_callback("glUniformSubroutinesuiv", (GLADapiproc) glUniformSubroutinesuiv, 3, arg0, arg1, arg2);
    glUniformSubroutinesuiv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUniformSubroutinesuiv", (GLADapiproc) glUniformSubroutinesuiv, 3, arg0, arg1, arg2);

}
PFNGLUNIFORMSUBROUTINESUIVPROC glad_debug_glUniformSubroutinesuiv = glad_debug_impl_glUniformSubroutinesuiv;
PFNGLUNMAPBUFFERPROC glad_glUnmapBuffer = NULL;
GLboolean GLAD_API_PTR glad_debug_impl_glUnmapBuffer(GLenum arg0) {
    GLboolean ret;
    _pre_call_gl_callback("glUnmapBuffer", (GLADapiproc) glUnmapBuffer, 1, arg0);
    ret = glUnmapBuffer(arg0);
    _post_call_gl_callback((void*) &ret, "glUnmapBuffer", (GLADapiproc) glUnmapBuffer, 1, arg0);
    return ret;
}
PFNGLUNMAPBUFFERPROC glad_debug_glUnmapBuffer = glad_debug_impl_glUnmapBuffer;
PFNGLUSEPROGRAMPROC glad_glUseProgram = NULL;
void GLAD_API_PTR glad_debug_impl_glUseProgram(GLuint arg0) {
    _pre_call_gl_callback("glUseProgram", (GLADapiproc) glUseProgram, 1, arg0);
    glUseProgram(arg0);
    _post_call_gl_callback(NULL, "glUseProgram", (GLADapiproc) glUseProgram, 1, arg0);

}
PFNGLUSEPROGRAMPROC glad_debug_glUseProgram = glad_debug_impl_glUseProgram;
PFNGLUSEPROGRAMSTAGESPROC glad_glUseProgramStages = NULL;
void GLAD_API_PTR glad_debug_impl_glUseProgramStages(GLuint arg0, GLbitfield arg1, GLuint arg2) {
    _pre_call_gl_callback("glUseProgramStages", (GLADapiproc) glUseProgramStages, 3, arg0, arg1, arg2);
    glUseProgramStages(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glUseProgramStages", (GLADapiproc) glUseProgramStages, 3, arg0, arg1, arg2);

}
PFNGLUSEPROGRAMSTAGESPROC glad_debug_glUseProgramStages = glad_debug_impl_glUseProgramStages;
PFNGLVALIDATEPROGRAMPROC glad_glValidateProgram = NULL;
void GLAD_API_PTR glad_debug_impl_glValidateProgram(GLuint arg0) {
    _pre_call_gl_callback("glValidateProgram", (GLADapiproc) glValidateProgram, 1, arg0);
    glValidateProgram(arg0);
    _post_call_gl_callback(NULL, "glValidateProgram", (GLADapiproc) glValidateProgram, 1, arg0);

}
PFNGLVALIDATEPROGRAMPROC glad_debug_glValidateProgram = glad_debug_impl_glValidateProgram;
PFNGLVALIDATEPROGRAMPIPELINEPROC glad_glValidateProgramPipeline = NULL;
void GLAD_API_PTR glad_debug_impl_glValidateProgramPipeline(GLuint arg0) {
    _pre_call_gl_callback("glValidateProgramPipeline", (GLADapiproc) glValidateProgramPipeline, 1, arg0);
    glValidateProgramPipeline(arg0);
    _post_call_gl_callback(NULL, "glValidateProgramPipeline", (GLADapiproc) glValidateProgramPipeline, 1, arg0);

}
PFNGLVALIDATEPROGRAMPIPELINEPROC glad_debug_glValidateProgramPipeline = glad_debug_impl_glValidateProgramPipeline;
PFNGLVERTEXATTRIB1DPROC glad_glVertexAttrib1d = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib1d(GLuint arg0, GLdouble arg1) {
    _pre_call_gl_callback("glVertexAttrib1d", (GLADapiproc) glVertexAttrib1d, 2, arg0, arg1);
    glVertexAttrib1d(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib1d", (GLADapiproc) glVertexAttrib1d, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB1DPROC glad_debug_glVertexAttrib1d = glad_debug_impl_glVertexAttrib1d;
PFNGLVERTEXATTRIB1DVPROC glad_glVertexAttrib1dv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib1dv(GLuint arg0, const GLdouble * arg1) {
    _pre_call_gl_callback("glVertexAttrib1dv", (GLADapiproc) glVertexAttrib1dv, 2, arg0, arg1);
    glVertexAttrib1dv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib1dv", (GLADapiproc) glVertexAttrib1dv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB1DVPROC glad_debug_glVertexAttrib1dv = glad_debug_impl_glVertexAttrib1dv;
PFNGLVERTEXATTRIB1FPROC glad_glVertexAttrib1f = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib1f(GLuint arg0, GLfloat arg1) {
    _pre_call_gl_callback("glVertexAttrib1f", (GLADapiproc) glVertexAttrib1f, 2, arg0, arg1);
    glVertexAttrib1f(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib1f", (GLADapiproc) glVertexAttrib1f, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB1FPROC glad_debug_glVertexAttrib1f = glad_debug_impl_glVertexAttrib1f;
PFNGLVERTEXATTRIB1FVPROC glad_glVertexAttrib1fv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib1fv(GLuint arg0, const GLfloat * arg1) {
    _pre_call_gl_callback("glVertexAttrib1fv", (GLADapiproc) glVertexAttrib1fv, 2, arg0, arg1);
    glVertexAttrib1fv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib1fv", (GLADapiproc) glVertexAttrib1fv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB1FVPROC glad_debug_glVertexAttrib1fv = glad_debug_impl_glVertexAttrib1fv;
PFNGLVERTEXATTRIB1SPROC glad_glVertexAttrib1s = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib1s(GLuint arg0, GLshort arg1) {
    _pre_call_gl_callback("glVertexAttrib1s", (GLADapiproc) glVertexAttrib1s, 2, arg0, arg1);
    glVertexAttrib1s(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib1s", (GLADapiproc) glVertexAttrib1s, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB1SPROC glad_debug_glVertexAttrib1s = glad_debug_impl_glVertexAttrib1s;
PFNGLVERTEXATTRIB1SVPROC glad_glVertexAttrib1sv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib1sv(GLuint arg0, const GLshort * arg1) {
    _pre_call_gl_callback("glVertexAttrib1sv", (GLADapiproc) glVertexAttrib1sv, 2, arg0, arg1);
    glVertexAttrib1sv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib1sv", (GLADapiproc) glVertexAttrib1sv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB1SVPROC glad_debug_glVertexAttrib1sv = glad_debug_impl_glVertexAttrib1sv;
PFNGLVERTEXATTRIB2DPROC glad_glVertexAttrib2d = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib2d(GLuint arg0, GLdouble arg1, GLdouble arg2) {
    _pre_call_gl_callback("glVertexAttrib2d", (GLADapiproc) glVertexAttrib2d, 3, arg0, arg1, arg2);
    glVertexAttrib2d(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glVertexAttrib2d", (GLADapiproc) glVertexAttrib2d, 3, arg0, arg1, arg2);

}
PFNGLVERTEXATTRIB2DPROC glad_debug_glVertexAttrib2d = glad_debug_impl_glVertexAttrib2d;
PFNGLVERTEXATTRIB2DVPROC glad_glVertexAttrib2dv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib2dv(GLuint arg0, const GLdouble * arg1) {
    _pre_call_gl_callback("glVertexAttrib2dv", (GLADapiproc) glVertexAttrib2dv, 2, arg0, arg1);
    glVertexAttrib2dv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib2dv", (GLADapiproc) glVertexAttrib2dv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB2DVPROC glad_debug_glVertexAttrib2dv = glad_debug_impl_glVertexAttrib2dv;
PFNGLVERTEXATTRIB2FPROC glad_glVertexAttrib2f = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib2f(GLuint arg0, GLfloat arg1, GLfloat arg2) {
    _pre_call_gl_callback("glVertexAttrib2f", (GLADapiproc) glVertexAttrib2f, 3, arg0, arg1, arg2);
    glVertexAttrib2f(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glVertexAttrib2f", (GLADapiproc) glVertexAttrib2f, 3, arg0, arg1, arg2);

}
PFNGLVERTEXATTRIB2FPROC glad_debug_glVertexAttrib2f = glad_debug_impl_glVertexAttrib2f;
PFNGLVERTEXATTRIB2FVPROC glad_glVertexAttrib2fv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib2fv(GLuint arg0, const GLfloat * arg1) {
    _pre_call_gl_callback("glVertexAttrib2fv", (GLADapiproc) glVertexAttrib2fv, 2, arg0, arg1);
    glVertexAttrib2fv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib2fv", (GLADapiproc) glVertexAttrib2fv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB2FVPROC glad_debug_glVertexAttrib2fv = glad_debug_impl_glVertexAttrib2fv;
PFNGLVERTEXATTRIB2SPROC glad_glVertexAttrib2s = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib2s(GLuint arg0, GLshort arg1, GLshort arg2) {
    _pre_call_gl_callback("glVertexAttrib2s", (GLADapiproc) glVertexAttrib2s, 3, arg0, arg1, arg2);
    glVertexAttrib2s(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glVertexAttrib2s", (GLADapiproc) glVertexAttrib2s, 3, arg0, arg1, arg2);

}
PFNGLVERTEXATTRIB2SPROC glad_debug_glVertexAttrib2s = glad_debug_impl_glVertexAttrib2s;
PFNGLVERTEXATTRIB2SVPROC glad_glVertexAttrib2sv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib2sv(GLuint arg0, const GLshort * arg1) {
    _pre_call_gl_callback("glVertexAttrib2sv", (GLADapiproc) glVertexAttrib2sv, 2, arg0, arg1);
    glVertexAttrib2sv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib2sv", (GLADapiproc) glVertexAttrib2sv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB2SVPROC glad_debug_glVertexAttrib2sv = glad_debug_impl_glVertexAttrib2sv;
PFNGLVERTEXATTRIB3DPROC glad_glVertexAttrib3d = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib3d(GLuint arg0, GLdouble arg1, GLdouble arg2, GLdouble arg3) {
    _pre_call_gl_callback("glVertexAttrib3d", (GLADapiproc) glVertexAttrib3d, 4, arg0, arg1, arg2, arg3);
    glVertexAttrib3d(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttrib3d", (GLADapiproc) glVertexAttrib3d, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIB3DPROC glad_debug_glVertexAttrib3d = glad_debug_impl_glVertexAttrib3d;
PFNGLVERTEXATTRIB3DVPROC glad_glVertexAttrib3dv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib3dv(GLuint arg0, const GLdouble * arg1) {
    _pre_call_gl_callback("glVertexAttrib3dv", (GLADapiproc) glVertexAttrib3dv, 2, arg0, arg1);
    glVertexAttrib3dv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib3dv", (GLADapiproc) glVertexAttrib3dv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB3DVPROC glad_debug_glVertexAttrib3dv = glad_debug_impl_glVertexAttrib3dv;
PFNGLVERTEXATTRIB3FPROC glad_glVertexAttrib3f = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib3f(GLuint arg0, GLfloat arg1, GLfloat arg2, GLfloat arg3) {
    _pre_call_gl_callback("glVertexAttrib3f", (GLADapiproc) glVertexAttrib3f, 4, arg0, arg1, arg2, arg3);
    glVertexAttrib3f(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttrib3f", (GLADapiproc) glVertexAttrib3f, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIB3FPROC glad_debug_glVertexAttrib3f = glad_debug_impl_glVertexAttrib3f;
PFNGLVERTEXATTRIB3FVPROC glad_glVertexAttrib3fv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib3fv(GLuint arg0, const GLfloat * arg1) {
    _pre_call_gl_callback("glVertexAttrib3fv", (GLADapiproc) glVertexAttrib3fv, 2, arg0, arg1);
    glVertexAttrib3fv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib3fv", (GLADapiproc) glVertexAttrib3fv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB3FVPROC glad_debug_glVertexAttrib3fv = glad_debug_impl_glVertexAttrib3fv;
PFNGLVERTEXATTRIB3SPROC glad_glVertexAttrib3s = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib3s(GLuint arg0, GLshort arg1, GLshort arg2, GLshort arg3) {
    _pre_call_gl_callback("glVertexAttrib3s", (GLADapiproc) glVertexAttrib3s, 4, arg0, arg1, arg2, arg3);
    glVertexAttrib3s(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttrib3s", (GLADapiproc) glVertexAttrib3s, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIB3SPROC glad_debug_glVertexAttrib3s = glad_debug_impl_glVertexAttrib3s;
PFNGLVERTEXATTRIB3SVPROC glad_glVertexAttrib3sv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib3sv(GLuint arg0, const GLshort * arg1) {
    _pre_call_gl_callback("glVertexAttrib3sv", (GLADapiproc) glVertexAttrib3sv, 2, arg0, arg1);
    glVertexAttrib3sv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib3sv", (GLADapiproc) glVertexAttrib3sv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB3SVPROC glad_debug_glVertexAttrib3sv = glad_debug_impl_glVertexAttrib3sv;
PFNGLVERTEXATTRIB4NBVPROC glad_glVertexAttrib4Nbv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4Nbv(GLuint arg0, const GLbyte * arg1) {
    _pre_call_gl_callback("glVertexAttrib4Nbv", (GLADapiproc) glVertexAttrib4Nbv, 2, arg0, arg1);
    glVertexAttrib4Nbv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4Nbv", (GLADapiproc) glVertexAttrib4Nbv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4NBVPROC glad_debug_glVertexAttrib4Nbv = glad_debug_impl_glVertexAttrib4Nbv;
PFNGLVERTEXATTRIB4NIVPROC glad_glVertexAttrib4Niv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4Niv(GLuint arg0, const GLint * arg1) {
    _pre_call_gl_callback("glVertexAttrib4Niv", (GLADapiproc) glVertexAttrib4Niv, 2, arg0, arg1);
    glVertexAttrib4Niv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4Niv", (GLADapiproc) glVertexAttrib4Niv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4NIVPROC glad_debug_glVertexAttrib4Niv = glad_debug_impl_glVertexAttrib4Niv;
PFNGLVERTEXATTRIB4NSVPROC glad_glVertexAttrib4Nsv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4Nsv(GLuint arg0, const GLshort * arg1) {
    _pre_call_gl_callback("glVertexAttrib4Nsv", (GLADapiproc) glVertexAttrib4Nsv, 2, arg0, arg1);
    glVertexAttrib4Nsv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4Nsv", (GLADapiproc) glVertexAttrib4Nsv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4NSVPROC glad_debug_glVertexAttrib4Nsv = glad_debug_impl_glVertexAttrib4Nsv;
PFNGLVERTEXATTRIB4NUBPROC glad_glVertexAttrib4Nub = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4Nub(GLuint arg0, GLubyte arg1, GLubyte arg2, GLubyte arg3, GLubyte arg4) {
    _pre_call_gl_callback("glVertexAttrib4Nub", (GLADapiproc) glVertexAttrib4Nub, 5, arg0, arg1, arg2, arg3, arg4);
    glVertexAttrib4Nub(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glVertexAttrib4Nub", (GLADapiproc) glVertexAttrib4Nub, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLVERTEXATTRIB4NUBPROC glad_debug_glVertexAttrib4Nub = glad_debug_impl_glVertexAttrib4Nub;
PFNGLVERTEXATTRIB4NUBVPROC glad_glVertexAttrib4Nubv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4Nubv(GLuint arg0, const GLubyte * arg1) {
    _pre_call_gl_callback("glVertexAttrib4Nubv", (GLADapiproc) glVertexAttrib4Nubv, 2, arg0, arg1);
    glVertexAttrib4Nubv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4Nubv", (GLADapiproc) glVertexAttrib4Nubv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4NUBVPROC glad_debug_glVertexAttrib4Nubv = glad_debug_impl_glVertexAttrib4Nubv;
PFNGLVERTEXATTRIB4NUIVPROC glad_glVertexAttrib4Nuiv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4Nuiv(GLuint arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glVertexAttrib4Nuiv", (GLADapiproc) glVertexAttrib4Nuiv, 2, arg0, arg1);
    glVertexAttrib4Nuiv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4Nuiv", (GLADapiproc) glVertexAttrib4Nuiv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4NUIVPROC glad_debug_glVertexAttrib4Nuiv = glad_debug_impl_glVertexAttrib4Nuiv;
PFNGLVERTEXATTRIB4NUSVPROC glad_glVertexAttrib4Nusv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4Nusv(GLuint arg0, const GLushort * arg1) {
    _pre_call_gl_callback("glVertexAttrib4Nusv", (GLADapiproc) glVertexAttrib4Nusv, 2, arg0, arg1);
    glVertexAttrib4Nusv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4Nusv", (GLADapiproc) glVertexAttrib4Nusv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4NUSVPROC glad_debug_glVertexAttrib4Nusv = glad_debug_impl_glVertexAttrib4Nusv;
PFNGLVERTEXATTRIB4BVPROC glad_glVertexAttrib4bv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4bv(GLuint arg0, const GLbyte * arg1) {
    _pre_call_gl_callback("glVertexAttrib4bv", (GLADapiproc) glVertexAttrib4bv, 2, arg0, arg1);
    glVertexAttrib4bv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4bv", (GLADapiproc) glVertexAttrib4bv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4BVPROC glad_debug_glVertexAttrib4bv = glad_debug_impl_glVertexAttrib4bv;
PFNGLVERTEXATTRIB4DPROC glad_glVertexAttrib4d = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4d(GLuint arg0, GLdouble arg1, GLdouble arg2, GLdouble arg3, GLdouble arg4) {
    _pre_call_gl_callback("glVertexAttrib4d", (GLADapiproc) glVertexAttrib4d, 5, arg0, arg1, arg2, arg3, arg4);
    glVertexAttrib4d(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glVertexAttrib4d", (GLADapiproc) glVertexAttrib4d, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLVERTEXATTRIB4DPROC glad_debug_glVertexAttrib4d = glad_debug_impl_glVertexAttrib4d;
PFNGLVERTEXATTRIB4DVPROC glad_glVertexAttrib4dv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4dv(GLuint arg0, const GLdouble * arg1) {
    _pre_call_gl_callback("glVertexAttrib4dv", (GLADapiproc) glVertexAttrib4dv, 2, arg0, arg1);
    glVertexAttrib4dv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4dv", (GLADapiproc) glVertexAttrib4dv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4DVPROC glad_debug_glVertexAttrib4dv = glad_debug_impl_glVertexAttrib4dv;
PFNGLVERTEXATTRIB4FPROC glad_glVertexAttrib4f = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4f(GLuint arg0, GLfloat arg1, GLfloat arg2, GLfloat arg3, GLfloat arg4) {
    _pre_call_gl_callback("glVertexAttrib4f", (GLADapiproc) glVertexAttrib4f, 5, arg0, arg1, arg2, arg3, arg4);
    glVertexAttrib4f(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glVertexAttrib4f", (GLADapiproc) glVertexAttrib4f, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLVERTEXATTRIB4FPROC glad_debug_glVertexAttrib4f = glad_debug_impl_glVertexAttrib4f;
PFNGLVERTEXATTRIB4FVPROC glad_glVertexAttrib4fv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4fv(GLuint arg0, const GLfloat * arg1) {
    _pre_call_gl_callback("glVertexAttrib4fv", (GLADapiproc) glVertexAttrib4fv, 2, arg0, arg1);
    glVertexAttrib4fv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4fv", (GLADapiproc) glVertexAttrib4fv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4FVPROC glad_debug_glVertexAttrib4fv = glad_debug_impl_glVertexAttrib4fv;
PFNGLVERTEXATTRIB4IVPROC glad_glVertexAttrib4iv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4iv(GLuint arg0, const GLint * arg1) {
    _pre_call_gl_callback("glVertexAttrib4iv", (GLADapiproc) glVertexAttrib4iv, 2, arg0, arg1);
    glVertexAttrib4iv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4iv", (GLADapiproc) glVertexAttrib4iv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4IVPROC glad_debug_glVertexAttrib4iv = glad_debug_impl_glVertexAttrib4iv;
PFNGLVERTEXATTRIB4SPROC glad_glVertexAttrib4s = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4s(GLuint arg0, GLshort arg1, GLshort arg2, GLshort arg3, GLshort arg4) {
    _pre_call_gl_callback("glVertexAttrib4s", (GLADapiproc) glVertexAttrib4s, 5, arg0, arg1, arg2, arg3, arg4);
    glVertexAttrib4s(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glVertexAttrib4s", (GLADapiproc) glVertexAttrib4s, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLVERTEXATTRIB4SPROC glad_debug_glVertexAttrib4s = glad_debug_impl_glVertexAttrib4s;
PFNGLVERTEXATTRIB4SVPROC glad_glVertexAttrib4sv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4sv(GLuint arg0, const GLshort * arg1) {
    _pre_call_gl_callback("glVertexAttrib4sv", (GLADapiproc) glVertexAttrib4sv, 2, arg0, arg1);
    glVertexAttrib4sv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4sv", (GLADapiproc) glVertexAttrib4sv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4SVPROC glad_debug_glVertexAttrib4sv = glad_debug_impl_glVertexAttrib4sv;
PFNGLVERTEXATTRIB4UBVPROC glad_glVertexAttrib4ubv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4ubv(GLuint arg0, const GLubyte * arg1) {
    _pre_call_gl_callback("glVertexAttrib4ubv", (GLADapiproc) glVertexAttrib4ubv, 2, arg0, arg1);
    glVertexAttrib4ubv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4ubv", (GLADapiproc) glVertexAttrib4ubv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4UBVPROC glad_debug_glVertexAttrib4ubv = glad_debug_impl_glVertexAttrib4ubv;
PFNGLVERTEXATTRIB4UIVPROC glad_glVertexAttrib4uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4uiv(GLuint arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glVertexAttrib4uiv", (GLADapiproc) glVertexAttrib4uiv, 2, arg0, arg1);
    glVertexAttrib4uiv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4uiv", (GLADapiproc) glVertexAttrib4uiv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4UIVPROC glad_debug_glVertexAttrib4uiv = glad_debug_impl_glVertexAttrib4uiv;
PFNGLVERTEXATTRIB4USVPROC glad_glVertexAttrib4usv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttrib4usv(GLuint arg0, const GLushort * arg1) {
    _pre_call_gl_callback("glVertexAttrib4usv", (GLADapiproc) glVertexAttrib4usv, 2, arg0, arg1);
    glVertexAttrib4usv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttrib4usv", (GLADapiproc) glVertexAttrib4usv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIB4USVPROC glad_debug_glVertexAttrib4usv = glad_debug_impl_glVertexAttrib4usv;
PFNGLVERTEXATTRIBDIVISORPROC glad_glVertexAttribDivisor = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribDivisor(GLuint arg0, GLuint arg1) {
    _pre_call_gl_callback("glVertexAttribDivisor", (GLADapiproc) glVertexAttribDivisor, 2, arg0, arg1);
    glVertexAttribDivisor(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribDivisor", (GLADapiproc) glVertexAttribDivisor, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBDIVISORPROC glad_debug_glVertexAttribDivisor = glad_debug_impl_glVertexAttribDivisor;
PFNGLVERTEXATTRIBI1IPROC glad_glVertexAttribI1i = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI1i(GLuint arg0, GLint arg1) {
    _pre_call_gl_callback("glVertexAttribI1i", (GLADapiproc) glVertexAttribI1i, 2, arg0, arg1);
    glVertexAttribI1i(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI1i", (GLADapiproc) glVertexAttribI1i, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI1IPROC glad_debug_glVertexAttribI1i = glad_debug_impl_glVertexAttribI1i;
PFNGLVERTEXATTRIBI1IVPROC glad_glVertexAttribI1iv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI1iv(GLuint arg0, const GLint * arg1) {
    _pre_call_gl_callback("glVertexAttribI1iv", (GLADapiproc) glVertexAttribI1iv, 2, arg0, arg1);
    glVertexAttribI1iv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI1iv", (GLADapiproc) glVertexAttribI1iv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI1IVPROC glad_debug_glVertexAttribI1iv = glad_debug_impl_glVertexAttribI1iv;
PFNGLVERTEXATTRIBI1UIPROC glad_glVertexAttribI1ui = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI1ui(GLuint arg0, GLuint arg1) {
    _pre_call_gl_callback("glVertexAttribI1ui", (GLADapiproc) glVertexAttribI1ui, 2, arg0, arg1);
    glVertexAttribI1ui(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI1ui", (GLADapiproc) glVertexAttribI1ui, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI1UIPROC glad_debug_glVertexAttribI1ui = glad_debug_impl_glVertexAttribI1ui;
PFNGLVERTEXATTRIBI1UIVPROC glad_glVertexAttribI1uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI1uiv(GLuint arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glVertexAttribI1uiv", (GLADapiproc) glVertexAttribI1uiv, 2, arg0, arg1);
    glVertexAttribI1uiv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI1uiv", (GLADapiproc) glVertexAttribI1uiv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI1UIVPROC glad_debug_glVertexAttribI1uiv = glad_debug_impl_glVertexAttribI1uiv;
PFNGLVERTEXATTRIBI2IPROC glad_glVertexAttribI2i = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI2i(GLuint arg0, GLint arg1, GLint arg2) {
    _pre_call_gl_callback("glVertexAttribI2i", (GLADapiproc) glVertexAttribI2i, 3, arg0, arg1, arg2);
    glVertexAttribI2i(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glVertexAttribI2i", (GLADapiproc) glVertexAttribI2i, 3, arg0, arg1, arg2);

}
PFNGLVERTEXATTRIBI2IPROC glad_debug_glVertexAttribI2i = glad_debug_impl_glVertexAttribI2i;
PFNGLVERTEXATTRIBI2IVPROC glad_glVertexAttribI2iv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI2iv(GLuint arg0, const GLint * arg1) {
    _pre_call_gl_callback("glVertexAttribI2iv", (GLADapiproc) glVertexAttribI2iv, 2, arg0, arg1);
    glVertexAttribI2iv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI2iv", (GLADapiproc) glVertexAttribI2iv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI2IVPROC glad_debug_glVertexAttribI2iv = glad_debug_impl_glVertexAttribI2iv;
PFNGLVERTEXATTRIBI2UIPROC glad_glVertexAttribI2ui = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI2ui(GLuint arg0, GLuint arg1, GLuint arg2) {
    _pre_call_gl_callback("glVertexAttribI2ui", (GLADapiproc) glVertexAttribI2ui, 3, arg0, arg1, arg2);
    glVertexAttribI2ui(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glVertexAttribI2ui", (GLADapiproc) glVertexAttribI2ui, 3, arg0, arg1, arg2);

}
PFNGLVERTEXATTRIBI2UIPROC glad_debug_glVertexAttribI2ui = glad_debug_impl_glVertexAttribI2ui;
PFNGLVERTEXATTRIBI2UIVPROC glad_glVertexAttribI2uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI2uiv(GLuint arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glVertexAttribI2uiv", (GLADapiproc) glVertexAttribI2uiv, 2, arg0, arg1);
    glVertexAttribI2uiv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI2uiv", (GLADapiproc) glVertexAttribI2uiv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI2UIVPROC glad_debug_glVertexAttribI2uiv = glad_debug_impl_glVertexAttribI2uiv;
PFNGLVERTEXATTRIBI3IPROC glad_glVertexAttribI3i = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI3i(GLuint arg0, GLint arg1, GLint arg2, GLint arg3) {
    _pre_call_gl_callback("glVertexAttribI3i", (GLADapiproc) glVertexAttribI3i, 4, arg0, arg1, arg2, arg3);
    glVertexAttribI3i(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttribI3i", (GLADapiproc) glVertexAttribI3i, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIBI3IPROC glad_debug_glVertexAttribI3i = glad_debug_impl_glVertexAttribI3i;
PFNGLVERTEXATTRIBI3IVPROC glad_glVertexAttribI3iv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI3iv(GLuint arg0, const GLint * arg1) {
    _pre_call_gl_callback("glVertexAttribI3iv", (GLADapiproc) glVertexAttribI3iv, 2, arg0, arg1);
    glVertexAttribI3iv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI3iv", (GLADapiproc) glVertexAttribI3iv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI3IVPROC glad_debug_glVertexAttribI3iv = glad_debug_impl_glVertexAttribI3iv;
PFNGLVERTEXATTRIBI3UIPROC glad_glVertexAttribI3ui = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI3ui(GLuint arg0, GLuint arg1, GLuint arg2, GLuint arg3) {
    _pre_call_gl_callback("glVertexAttribI3ui", (GLADapiproc) glVertexAttribI3ui, 4, arg0, arg1, arg2, arg3);
    glVertexAttribI3ui(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttribI3ui", (GLADapiproc) glVertexAttribI3ui, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIBI3UIPROC glad_debug_glVertexAttribI3ui = glad_debug_impl_glVertexAttribI3ui;
PFNGLVERTEXATTRIBI3UIVPROC glad_glVertexAttribI3uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI3uiv(GLuint arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glVertexAttribI3uiv", (GLADapiproc) glVertexAttribI3uiv, 2, arg0, arg1);
    glVertexAttribI3uiv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI3uiv", (GLADapiproc) glVertexAttribI3uiv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI3UIVPROC glad_debug_glVertexAttribI3uiv = glad_debug_impl_glVertexAttribI3uiv;
PFNGLVERTEXATTRIBI4BVPROC glad_glVertexAttribI4bv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI4bv(GLuint arg0, const GLbyte * arg1) {
    _pre_call_gl_callback("glVertexAttribI4bv", (GLADapiproc) glVertexAttribI4bv, 2, arg0, arg1);
    glVertexAttribI4bv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI4bv", (GLADapiproc) glVertexAttribI4bv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI4BVPROC glad_debug_glVertexAttribI4bv = glad_debug_impl_glVertexAttribI4bv;
PFNGLVERTEXATTRIBI4IPROC glad_glVertexAttribI4i = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI4i(GLuint arg0, GLint arg1, GLint arg2, GLint arg3, GLint arg4) {
    _pre_call_gl_callback("glVertexAttribI4i", (GLADapiproc) glVertexAttribI4i, 5, arg0, arg1, arg2, arg3, arg4);
    glVertexAttribI4i(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glVertexAttribI4i", (GLADapiproc) glVertexAttribI4i, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLVERTEXATTRIBI4IPROC glad_debug_glVertexAttribI4i = glad_debug_impl_glVertexAttribI4i;
PFNGLVERTEXATTRIBI4IVPROC glad_glVertexAttribI4iv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI4iv(GLuint arg0, const GLint * arg1) {
    _pre_call_gl_callback("glVertexAttribI4iv", (GLADapiproc) glVertexAttribI4iv, 2, arg0, arg1);
    glVertexAttribI4iv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI4iv", (GLADapiproc) glVertexAttribI4iv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI4IVPROC glad_debug_glVertexAttribI4iv = glad_debug_impl_glVertexAttribI4iv;
PFNGLVERTEXATTRIBI4SVPROC glad_glVertexAttribI4sv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI4sv(GLuint arg0, const GLshort * arg1) {
    _pre_call_gl_callback("glVertexAttribI4sv", (GLADapiproc) glVertexAttribI4sv, 2, arg0, arg1);
    glVertexAttribI4sv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI4sv", (GLADapiproc) glVertexAttribI4sv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI4SVPROC glad_debug_glVertexAttribI4sv = glad_debug_impl_glVertexAttribI4sv;
PFNGLVERTEXATTRIBI4UBVPROC glad_glVertexAttribI4ubv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI4ubv(GLuint arg0, const GLubyte * arg1) {
    _pre_call_gl_callback("glVertexAttribI4ubv", (GLADapiproc) glVertexAttribI4ubv, 2, arg0, arg1);
    glVertexAttribI4ubv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI4ubv", (GLADapiproc) glVertexAttribI4ubv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI4UBVPROC glad_debug_glVertexAttribI4ubv = glad_debug_impl_glVertexAttribI4ubv;
PFNGLVERTEXATTRIBI4UIPROC glad_glVertexAttribI4ui = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI4ui(GLuint arg0, GLuint arg1, GLuint arg2, GLuint arg3, GLuint arg4) {
    _pre_call_gl_callback("glVertexAttribI4ui", (GLADapiproc) glVertexAttribI4ui, 5, arg0, arg1, arg2, arg3, arg4);
    glVertexAttribI4ui(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glVertexAttribI4ui", (GLADapiproc) glVertexAttribI4ui, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLVERTEXATTRIBI4UIPROC glad_debug_glVertexAttribI4ui = glad_debug_impl_glVertexAttribI4ui;
PFNGLVERTEXATTRIBI4UIVPROC glad_glVertexAttribI4uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI4uiv(GLuint arg0, const GLuint * arg1) {
    _pre_call_gl_callback("glVertexAttribI4uiv", (GLADapiproc) glVertexAttribI4uiv, 2, arg0, arg1);
    glVertexAttribI4uiv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI4uiv", (GLADapiproc) glVertexAttribI4uiv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI4UIVPROC glad_debug_glVertexAttribI4uiv = glad_debug_impl_glVertexAttribI4uiv;
PFNGLVERTEXATTRIBI4USVPROC glad_glVertexAttribI4usv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribI4usv(GLuint arg0, const GLushort * arg1) {
    _pre_call_gl_callback("glVertexAttribI4usv", (GLADapiproc) glVertexAttribI4usv, 2, arg0, arg1);
    glVertexAttribI4usv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribI4usv", (GLADapiproc) glVertexAttribI4usv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBI4USVPROC glad_debug_glVertexAttribI4usv = glad_debug_impl_glVertexAttribI4usv;
PFNGLVERTEXATTRIBIPOINTERPROC glad_glVertexAttribIPointer = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribIPointer(GLuint arg0, GLint arg1, GLenum arg2, GLsizei arg3, const void * arg4) {
    _pre_call_gl_callback("glVertexAttribIPointer", (GLADapiproc) glVertexAttribIPointer, 5, arg0, arg1, arg2, arg3, arg4);
    glVertexAttribIPointer(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glVertexAttribIPointer", (GLADapiproc) glVertexAttribIPointer, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLVERTEXATTRIBIPOINTERPROC glad_debug_glVertexAttribIPointer = glad_debug_impl_glVertexAttribIPointer;
PFNGLVERTEXATTRIBL1DPROC glad_glVertexAttribL1d = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribL1d(GLuint arg0, GLdouble arg1) {
    _pre_call_gl_callback("glVertexAttribL1d", (GLADapiproc) glVertexAttribL1d, 2, arg0, arg1);
    glVertexAttribL1d(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribL1d", (GLADapiproc) glVertexAttribL1d, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBL1DPROC glad_debug_glVertexAttribL1d = glad_debug_impl_glVertexAttribL1d;
PFNGLVERTEXATTRIBL1DVPROC glad_glVertexAttribL1dv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribL1dv(GLuint arg0, const GLdouble * arg1) {
    _pre_call_gl_callback("glVertexAttribL1dv", (GLADapiproc) glVertexAttribL1dv, 2, arg0, arg1);
    glVertexAttribL1dv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribL1dv", (GLADapiproc) glVertexAttribL1dv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBL1DVPROC glad_debug_glVertexAttribL1dv = glad_debug_impl_glVertexAttribL1dv;
PFNGLVERTEXATTRIBL2DPROC glad_glVertexAttribL2d = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribL2d(GLuint arg0, GLdouble arg1, GLdouble arg2) {
    _pre_call_gl_callback("glVertexAttribL2d", (GLADapiproc) glVertexAttribL2d, 3, arg0, arg1, arg2);
    glVertexAttribL2d(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glVertexAttribL2d", (GLADapiproc) glVertexAttribL2d, 3, arg0, arg1, arg2);

}
PFNGLVERTEXATTRIBL2DPROC glad_debug_glVertexAttribL2d = glad_debug_impl_glVertexAttribL2d;
PFNGLVERTEXATTRIBL2DVPROC glad_glVertexAttribL2dv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribL2dv(GLuint arg0, const GLdouble * arg1) {
    _pre_call_gl_callback("glVertexAttribL2dv", (GLADapiproc) glVertexAttribL2dv, 2, arg0, arg1);
    glVertexAttribL2dv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribL2dv", (GLADapiproc) glVertexAttribL2dv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBL2DVPROC glad_debug_glVertexAttribL2dv = glad_debug_impl_glVertexAttribL2dv;
PFNGLVERTEXATTRIBL3DPROC glad_glVertexAttribL3d = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribL3d(GLuint arg0, GLdouble arg1, GLdouble arg2, GLdouble arg3) {
    _pre_call_gl_callback("glVertexAttribL3d", (GLADapiproc) glVertexAttribL3d, 4, arg0, arg1, arg2, arg3);
    glVertexAttribL3d(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttribL3d", (GLADapiproc) glVertexAttribL3d, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIBL3DPROC glad_debug_glVertexAttribL3d = glad_debug_impl_glVertexAttribL3d;
PFNGLVERTEXATTRIBL3DVPROC glad_glVertexAttribL3dv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribL3dv(GLuint arg0, const GLdouble * arg1) {
    _pre_call_gl_callback("glVertexAttribL3dv", (GLADapiproc) glVertexAttribL3dv, 2, arg0, arg1);
    glVertexAttribL3dv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribL3dv", (GLADapiproc) glVertexAttribL3dv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBL3DVPROC glad_debug_glVertexAttribL3dv = glad_debug_impl_glVertexAttribL3dv;
PFNGLVERTEXATTRIBL4DPROC glad_glVertexAttribL4d = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribL4d(GLuint arg0, GLdouble arg1, GLdouble arg2, GLdouble arg3, GLdouble arg4) {
    _pre_call_gl_callback("glVertexAttribL4d", (GLADapiproc) glVertexAttribL4d, 5, arg0, arg1, arg2, arg3, arg4);
    glVertexAttribL4d(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glVertexAttribL4d", (GLADapiproc) glVertexAttribL4d, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLVERTEXATTRIBL4DPROC glad_debug_glVertexAttribL4d = glad_debug_impl_glVertexAttribL4d;
PFNGLVERTEXATTRIBL4DVPROC glad_glVertexAttribL4dv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribL4dv(GLuint arg0, const GLdouble * arg1) {
    _pre_call_gl_callback("glVertexAttribL4dv", (GLADapiproc) glVertexAttribL4dv, 2, arg0, arg1);
    glVertexAttribL4dv(arg0, arg1);
    _post_call_gl_callback(NULL, "glVertexAttribL4dv", (GLADapiproc) glVertexAttribL4dv, 2, arg0, arg1);

}
PFNGLVERTEXATTRIBL4DVPROC glad_debug_glVertexAttribL4dv = glad_debug_impl_glVertexAttribL4dv;
PFNGLVERTEXATTRIBLPOINTERPROC glad_glVertexAttribLPointer = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribLPointer(GLuint arg0, GLint arg1, GLenum arg2, GLsizei arg3, const void * arg4) {
    _pre_call_gl_callback("glVertexAttribLPointer", (GLADapiproc) glVertexAttribLPointer, 5, arg0, arg1, arg2, arg3, arg4);
    glVertexAttribLPointer(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glVertexAttribLPointer", (GLADapiproc) glVertexAttribLPointer, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLVERTEXATTRIBLPOINTERPROC glad_debug_glVertexAttribLPointer = glad_debug_impl_glVertexAttribLPointer;
PFNGLVERTEXATTRIBP1UIPROC glad_glVertexAttribP1ui = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribP1ui(GLuint arg0, GLenum arg1, GLboolean arg2, GLuint arg3) {
    _pre_call_gl_callback("glVertexAttribP1ui", (GLADapiproc) glVertexAttribP1ui, 4, arg0, arg1, arg2, arg3);
    glVertexAttribP1ui(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttribP1ui", (GLADapiproc) glVertexAttribP1ui, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIBP1UIPROC glad_debug_glVertexAttribP1ui = glad_debug_impl_glVertexAttribP1ui;
PFNGLVERTEXATTRIBP1UIVPROC glad_glVertexAttribP1uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribP1uiv(GLuint arg0, GLenum arg1, GLboolean arg2, const GLuint * arg3) {
    _pre_call_gl_callback("glVertexAttribP1uiv", (GLADapiproc) glVertexAttribP1uiv, 4, arg0, arg1, arg2, arg3);
    glVertexAttribP1uiv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttribP1uiv", (GLADapiproc) glVertexAttribP1uiv, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIBP1UIVPROC glad_debug_glVertexAttribP1uiv = glad_debug_impl_glVertexAttribP1uiv;
PFNGLVERTEXATTRIBP2UIPROC glad_glVertexAttribP2ui = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribP2ui(GLuint arg0, GLenum arg1, GLboolean arg2, GLuint arg3) {
    _pre_call_gl_callback("glVertexAttribP2ui", (GLADapiproc) glVertexAttribP2ui, 4, arg0, arg1, arg2, arg3);
    glVertexAttribP2ui(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttribP2ui", (GLADapiproc) glVertexAttribP2ui, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIBP2UIPROC glad_debug_glVertexAttribP2ui = glad_debug_impl_glVertexAttribP2ui;
PFNGLVERTEXATTRIBP2UIVPROC glad_glVertexAttribP2uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribP2uiv(GLuint arg0, GLenum arg1, GLboolean arg2, const GLuint * arg3) {
    _pre_call_gl_callback("glVertexAttribP2uiv", (GLADapiproc) glVertexAttribP2uiv, 4, arg0, arg1, arg2, arg3);
    glVertexAttribP2uiv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttribP2uiv", (GLADapiproc) glVertexAttribP2uiv, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIBP2UIVPROC glad_debug_glVertexAttribP2uiv = glad_debug_impl_glVertexAttribP2uiv;
PFNGLVERTEXATTRIBP3UIPROC glad_glVertexAttribP3ui = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribP3ui(GLuint arg0, GLenum arg1, GLboolean arg2, GLuint arg3) {
    _pre_call_gl_callback("glVertexAttribP3ui", (GLADapiproc) glVertexAttribP3ui, 4, arg0, arg1, arg2, arg3);
    glVertexAttribP3ui(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttribP3ui", (GLADapiproc) glVertexAttribP3ui, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIBP3UIPROC glad_debug_glVertexAttribP3ui = glad_debug_impl_glVertexAttribP3ui;
PFNGLVERTEXATTRIBP3UIVPROC glad_glVertexAttribP3uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribP3uiv(GLuint arg0, GLenum arg1, GLboolean arg2, const GLuint * arg3) {
    _pre_call_gl_callback("glVertexAttribP3uiv", (GLADapiproc) glVertexAttribP3uiv, 4, arg0, arg1, arg2, arg3);
    glVertexAttribP3uiv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttribP3uiv", (GLADapiproc) glVertexAttribP3uiv, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIBP3UIVPROC glad_debug_glVertexAttribP3uiv = glad_debug_impl_glVertexAttribP3uiv;
PFNGLVERTEXATTRIBP4UIPROC glad_glVertexAttribP4ui = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribP4ui(GLuint arg0, GLenum arg1, GLboolean arg2, GLuint arg3) {
    _pre_call_gl_callback("glVertexAttribP4ui", (GLADapiproc) glVertexAttribP4ui, 4, arg0, arg1, arg2, arg3);
    glVertexAttribP4ui(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttribP4ui", (GLADapiproc) glVertexAttribP4ui, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIBP4UIPROC glad_debug_glVertexAttribP4ui = glad_debug_impl_glVertexAttribP4ui;
PFNGLVERTEXATTRIBP4UIVPROC glad_glVertexAttribP4uiv = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribP4uiv(GLuint arg0, GLenum arg1, GLboolean arg2, const GLuint * arg3) {
    _pre_call_gl_callback("glVertexAttribP4uiv", (GLADapiproc) glVertexAttribP4uiv, 4, arg0, arg1, arg2, arg3);
    glVertexAttribP4uiv(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glVertexAttribP4uiv", (GLADapiproc) glVertexAttribP4uiv, 4, arg0, arg1, arg2, arg3);

}
PFNGLVERTEXATTRIBP4UIVPROC glad_debug_glVertexAttribP4uiv = glad_debug_impl_glVertexAttribP4uiv;
PFNGLVERTEXATTRIBPOINTERPROC glad_glVertexAttribPointer = NULL;
void GLAD_API_PTR glad_debug_impl_glVertexAttribPointer(GLuint arg0, GLint arg1, GLenum arg2, GLboolean arg3, GLsizei arg4, const void * arg5) {
    _pre_call_gl_callback("glVertexAttribPointer", (GLADapiproc) glVertexAttribPointer, 6, arg0, arg1, arg2, arg3, arg4, arg5);
    glVertexAttribPointer(arg0, arg1, arg2, arg3, arg4, arg5);
    _post_call_gl_callback(NULL, "glVertexAttribPointer", (GLADapiproc) glVertexAttribPointer, 6, arg0, arg1, arg2, arg3, arg4, arg5);

}
PFNGLVERTEXATTRIBPOINTERPROC glad_debug_glVertexAttribPointer = glad_debug_impl_glVertexAttribPointer;
PFNGLVIEWPORTPROC glad_glViewport = NULL;
void GLAD_API_PTR glad_debug_impl_glViewport(GLint arg0, GLint arg1, GLsizei arg2, GLsizei arg3) {
    _pre_call_gl_callback("glViewport", (GLADapiproc) glViewport, 4, arg0, arg1, arg2, arg3);
    glViewport(arg0, arg1, arg2, arg3);
    _post_call_gl_callback(NULL, "glViewport", (GLADapiproc) glViewport, 4, arg0, arg1, arg2, arg3);

}
PFNGLVIEWPORTPROC glad_debug_glViewport = glad_debug_impl_glViewport;
PFNGLVIEWPORTARRAYVPROC glad_glViewportArrayv = NULL;
void GLAD_API_PTR glad_debug_impl_glViewportArrayv(GLuint arg0, GLsizei arg1, const GLfloat * arg2) {
    _pre_call_gl_callback("glViewportArrayv", (GLADapiproc) glViewportArrayv, 3, arg0, arg1, arg2);
    glViewportArrayv(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glViewportArrayv", (GLADapiproc) glViewportArrayv, 3, arg0, arg1, arg2);

}
PFNGLVIEWPORTARRAYVPROC glad_debug_glViewportArrayv = glad_debug_impl_glViewportArrayv;
PFNGLVIEWPORTINDEXEDFPROC glad_glViewportIndexedf = NULL;
void GLAD_API_PTR glad_debug_impl_glViewportIndexedf(GLuint arg0, GLfloat arg1, GLfloat arg2, GLfloat arg3, GLfloat arg4) {
    _pre_call_gl_callback("glViewportIndexedf", (GLADapiproc) glViewportIndexedf, 5, arg0, arg1, arg2, arg3, arg4);
    glViewportIndexedf(arg0, arg1, arg2, arg3, arg4);
    _post_call_gl_callback(NULL, "glViewportIndexedf", (GLADapiproc) glViewportIndexedf, 5, arg0, arg1, arg2, arg3, arg4);

}
PFNGLVIEWPORTINDEXEDFPROC glad_debug_glViewportIndexedf = glad_debug_impl_glViewportIndexedf;
PFNGLVIEWPORTINDEXEDFVPROC glad_glViewportIndexedfv = NULL;
void GLAD_API_PTR glad_debug_impl_glViewportIndexedfv(GLuint arg0, const GLfloat * arg1) {
    _pre_call_gl_callback("glViewportIndexedfv", (GLADapiproc) glViewportIndexedfv, 2, arg0, arg1);
    glViewportIndexedfv(arg0, arg1);
    _post_call_gl_callback(NULL, "glViewportIndexedfv", (GLADapiproc) glViewportIndexedfv, 2, arg0, arg1);

}
PFNGLVIEWPORTINDEXEDFVPROC glad_debug_glViewportIndexedfv = glad_debug_impl_glViewportIndexedfv;
PFNGLWAITSYNCPROC glad_glWaitSync = NULL;
void GLAD_API_PTR glad_debug_impl_glWaitSync(GLsync arg0, GLbitfield arg1, GLuint64 arg2) {
    _pre_call_gl_callback("glWaitSync", (GLADapiproc) glWaitSync, 3, arg0, arg1, arg2);
    glWaitSync(arg0, arg1, arg2);
    _post_call_gl_callback(NULL, "glWaitSync", (GLADapiproc) glWaitSync, 3, arg0, arg1, arg2);

}
PFNGLWAITSYNCPROC glad_debug_glWaitSync = glad_debug_impl_glWaitSync;


static void glad_gl_load_GL_VERSION_1_0( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_1_0) return;
    glBlendFunc = (PFNGLBLENDFUNCPROC) load("glBlendFunc", userptr);
    glClear = (PFNGLCLEARPROC) load("glClear", userptr);
    glClearColor = (PFNGLCLEARCOLORPROC) load("glClearColor", userptr);
    glClearDepth = (PFNGLCLEARDEPTHPROC) load("glClearDepth", userptr);
    glClearStencil = (PFNGLCLEARSTENCILPROC) load("glClearStencil", userptr);
    glColorMask = (PFNGLCOLORMASKPROC) load("glColorMask", userptr);
    glCullFace = (PFNGLCULLFACEPROC) load("glCullFace", userptr);
    glDepthFunc = (PFNGLDEPTHFUNCPROC) load("glDepthFunc", userptr);
    glDepthMask = (PFNGLDEPTHMASKPROC) load("glDepthMask", userptr);
    glDepthRange = (PFNGLDEPTHRANGEPROC) load("glDepthRange", userptr);
    glDisable = (PFNGLDISABLEPROC) load("glDisable", userptr);
    glDrawBuffer = (PFNGLDRAWBUFFERPROC) load("glDrawBuffer", userptr);
    glEnable = (PFNGLENABLEPROC) load("glEnable", userptr);
    glFinish = (PFNGLFINISHPROC) load("glFinish", userptr);
    glFlush = (PFNGLFLUSHPROC) load("glFlush", userptr);
    glFrontFace = (PFNGLFRONTFACEPROC) load("glFrontFace", userptr);
    glGetBooleanv = (PFNGLGETBOOLEANVPROC) load("glGetBooleanv", userptr);
    glGetDoublev = (PFNGLGETDOUBLEVPROC) load("glGetDoublev", userptr);
    glGetError = (PFNGLGETERRORPROC) load("glGetError", userptr);
    glGetFloatv = (PFNGLGETFLOATVPROC) load("glGetFloatv", userptr);
    glGetIntegerv = (PFNGLGETINTEGERVPROC) load("glGetIntegerv", userptr);
    glGetString = (PFNGLGETSTRINGPROC) load("glGetString", userptr);
    glGetTexImage = (PFNGLGETTEXIMAGEPROC) load("glGetTexImage", userptr);
    glGetTexLevelParameterfv = (PFNGLGETTEXLEVELPARAMETERFVPROC) load("glGetTexLevelParameterfv", userptr);
    glGetTexLevelParameteriv = (PFNGLGETTEXLEVELPARAMETERIVPROC) load("glGetTexLevelParameteriv", userptr);
    glGetTexParameterfv = (PFNGLGETTEXPARAMETERFVPROC) load("glGetTexParameterfv", userptr);
    glGetTexParameteriv = (PFNGLGETTEXPARAMETERIVPROC) load("glGetTexParameteriv", userptr);
    glHint = (PFNGLHINTPROC) load("glHint", userptr);
    glIsEnabled = (PFNGLISENABLEDPROC) load("glIsEnabled", userptr);
    glLineWidth = (PFNGLLINEWIDTHPROC) load("glLineWidth", userptr);
    glLogicOp = (PFNGLLOGICOPPROC) load("glLogicOp", userptr);
    glPixelStoref = (PFNGLPIXELSTOREFPROC) load("glPixelStoref", userptr);
    glPixelStorei = (PFNGLPIXELSTOREIPROC) load("glPixelStorei", userptr);
    glPointSize = (PFNGLPOINTSIZEPROC) load("glPointSize", userptr);
    glPolygonMode = (PFNGLPOLYGONMODEPROC) load("glPolygonMode", userptr);
    glReadBuffer = (PFNGLREADBUFFERPROC) load("glReadBuffer", userptr);
    glReadPixels = (PFNGLREADPIXELSPROC) load("glReadPixels", userptr);
    glScissor = (PFNGLSCISSORPROC) load("glScissor", userptr);
    glStencilFunc = (PFNGLSTENCILFUNCPROC) load("glStencilFunc", userptr);
    glStencilMask = (PFNGLSTENCILMASKPROC) load("glStencilMask", userptr);
    glStencilOp = (PFNGLSTENCILOPPROC) load("glStencilOp", userptr);
    glTexImage1D = (PFNGLTEXIMAGE1DPROC) load("glTexImage1D", userptr);
    glTexImage2D = (PFNGLTEXIMAGE2DPROC) load("glTexImage2D", userptr);
    glTexParameterf = (PFNGLTEXPARAMETERFPROC) load("glTexParameterf", userptr);
    glTexParameterfv = (PFNGLTEXPARAMETERFVPROC) load("glTexParameterfv", userptr);
    glTexParameteri = (PFNGLTEXPARAMETERIPROC) load("glTexParameteri", userptr);
    glTexParameteriv = (PFNGLTEXPARAMETERIVPROC) load("glTexParameteriv", userptr);
    glViewport = (PFNGLVIEWPORTPROC) load("glViewport", userptr);
}
static void glad_gl_load_GL_VERSION_1_1( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_1_1) return;
    glBindTexture = (PFNGLBINDTEXTUREPROC) load("glBindTexture", userptr);
    glCopyTexImage1D = (PFNGLCOPYTEXIMAGE1DPROC) load("glCopyTexImage1D", userptr);
    glCopyTexImage2D = (PFNGLCOPYTEXIMAGE2DPROC) load("glCopyTexImage2D", userptr);
    glCopyTexSubImage1D = (PFNGLCOPYTEXSUBIMAGE1DPROC) load("glCopyTexSubImage1D", userptr);
    glCopyTexSubImage2D = (PFNGLCOPYTEXSUBIMAGE2DPROC) load("glCopyTexSubImage2D", userptr);
    glDeleteTextures = (PFNGLDELETETEXTURESPROC) load("glDeleteTextures", userptr);
    glDrawArrays = (PFNGLDRAWARRAYSPROC) load("glDrawArrays", userptr);
    glDrawElements = (PFNGLDRAWELEMENTSPROC) load("glDrawElements", userptr);
    glGenTextures = (PFNGLGENTEXTURESPROC) load("glGenTextures", userptr);
    glIsTexture = (PFNGLISTEXTUREPROC) load("glIsTexture", userptr);
    glPolygonOffset = (PFNGLPOLYGONOFFSETPROC) load("glPolygonOffset", userptr);
    glTexSubImage1D = (PFNGLTEXSUBIMAGE1DPROC) load("glTexSubImage1D", userptr);
    glTexSubImage2D = (PFNGLTEXSUBIMAGE2DPROC) load("glTexSubImage2D", userptr);
}
static void glad_gl_load_GL_VERSION_1_2( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_1_2) return;
    glCopyTexSubImage3D = (PFNGLCOPYTEXSUBIMAGE3DPROC) load("glCopyTexSubImage3D", userptr);
    glDrawRangeElements = (PFNGLDRAWRANGEELEMENTSPROC) load("glDrawRangeElements", userptr);
    glTexImage3D = (PFNGLTEXIMAGE3DPROC) load("glTexImage3D", userptr);
    glTexSubImage3D = (PFNGLTEXSUBIMAGE3DPROC) load("glTexSubImage3D", userptr);
}
static void glad_gl_load_GL_VERSION_1_3( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_1_3) return;
    glActiveTexture = (PFNGLACTIVETEXTUREPROC) load("glActiveTexture", userptr);
    glCompressedTexImage1D = (PFNGLCOMPRESSEDTEXIMAGE1DPROC) load("glCompressedTexImage1D", userptr);
    glCompressedTexImage2D = (PFNGLCOMPRESSEDTEXIMAGE2DPROC) load("glCompressedTexImage2D", userptr);
    glCompressedTexImage3D = (PFNGLCOMPRESSEDTEXIMAGE3DPROC) load("glCompressedTexImage3D", userptr);
    glCompressedTexSubImage1D = (PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC) load("glCompressedTexSubImage1D", userptr);
    glCompressedTexSubImage2D = (PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC) load("glCompressedTexSubImage2D", userptr);
    glCompressedTexSubImage3D = (PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC) load("glCompressedTexSubImage3D", userptr);
    glGetCompressedTexImage = (PFNGLGETCOMPRESSEDTEXIMAGEPROC) load("glGetCompressedTexImage", userptr);
    glSampleCoverage = (PFNGLSAMPLECOVERAGEPROC) load("glSampleCoverage", userptr);
}
static void glad_gl_load_GL_VERSION_1_4( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_1_4) return;
    glBlendColor = (PFNGLBLENDCOLORPROC) load("glBlendColor", userptr);
    glBlendEquation = (PFNGLBLENDEQUATIONPROC) load("glBlendEquation", userptr);
    glBlendFuncSeparate = (PFNGLBLENDFUNCSEPARATEPROC) load("glBlendFuncSeparate", userptr);
    glMultiDrawArrays = (PFNGLMULTIDRAWARRAYSPROC) load("glMultiDrawArrays", userptr);
    glMultiDrawElements = (PFNGLMULTIDRAWELEMENTSPROC) load("glMultiDrawElements", userptr);
    glPointParameterf = (PFNGLPOINTPARAMETERFPROC) load("glPointParameterf", userptr);
    glPointParameterfv = (PFNGLPOINTPARAMETERFVPROC) load("glPointParameterfv", userptr);
    glPointParameteri = (PFNGLPOINTPARAMETERIPROC) load("glPointParameteri", userptr);
    glPointParameteriv = (PFNGLPOINTPARAMETERIVPROC) load("glPointParameteriv", userptr);
}
static void glad_gl_load_GL_VERSION_1_5( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_1_5) return;
    glBeginQuery = (PFNGLBEGINQUERYPROC) load("glBeginQuery", userptr);
    glBindBuffer = (PFNGLBINDBUFFERPROC) load("glBindBuffer", userptr);
    glBufferData = (PFNGLBUFFERDATAPROC) load("glBufferData", userptr);
    glBufferSubData = (PFNGLBUFFERSUBDATAPROC) load("glBufferSubData", userptr);
    glDeleteBuffers = (PFNGLDELETEBUFFERSPROC) load("glDeleteBuffers", userptr);
    glDeleteQueries = (PFNGLDELETEQUERIESPROC) load("glDeleteQueries", userptr);
    glEndQuery = (PFNGLENDQUERYPROC) load("glEndQuery", userptr);
    glGenBuffers = (PFNGLGENBUFFERSPROC) load("glGenBuffers", userptr);
    glGenQueries = (PFNGLGENQUERIESPROC) load("glGenQueries", userptr);
    glGetBufferParameteriv = (PFNGLGETBUFFERPARAMETERIVPROC) load("glGetBufferParameteriv", userptr);
    glGetBufferPointerv = (PFNGLGETBUFFERPOINTERVPROC) load("glGetBufferPointerv", userptr);
    glGetBufferSubData = (PFNGLGETBUFFERSUBDATAPROC) load("glGetBufferSubData", userptr);
    glGetQueryObjectiv = (PFNGLGETQUERYOBJECTIVPROC) load("glGetQueryObjectiv", userptr);
    glGetQueryObjectuiv = (PFNGLGETQUERYOBJECTUIVPROC) load("glGetQueryObjectuiv", userptr);
    glGetQueryiv = (PFNGLGETQUERYIVPROC) load("glGetQueryiv", userptr);
    glIsBuffer = (PFNGLISBUFFERPROC) load("glIsBuffer", userptr);
    glIsQuery = (PFNGLISQUERYPROC) load("glIsQuery", userptr);
    glMapBuffer = (PFNGLMAPBUFFERPROC) load("glMapBuffer", userptr);
    glUnmapBuffer = (PFNGLUNMAPBUFFERPROC) load("glUnmapBuffer", userptr);
}
static void glad_gl_load_GL_VERSION_2_0( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_2_0) return;
    glAttachShader = (PFNGLATTACHSHADERPROC) load("glAttachShader", userptr);
    glBindAttribLocation = (PFNGLBINDATTRIBLOCATIONPROC) load("glBindAttribLocation", userptr);
    glBlendEquationSeparate = (PFNGLBLENDEQUATIONSEPARATEPROC) load("glBlendEquationSeparate", userptr);
    glCompileShader = (PFNGLCOMPILESHADERPROC) load("glCompileShader", userptr);
    glCreateProgram = (PFNGLCREATEPROGRAMPROC) load("glCreateProgram", userptr);
    glCreateShader = (PFNGLCREATESHADERPROC) load("glCreateShader", userptr);
    glDeleteProgram = (PFNGLDELETEPROGRAMPROC) load("glDeleteProgram", userptr);
    glDeleteShader = (PFNGLDELETESHADERPROC) load("glDeleteShader", userptr);
    glDetachShader = (PFNGLDETACHSHADERPROC) load("glDetachShader", userptr);
    glDisableVertexAttribArray = (PFNGLDISABLEVERTEXATTRIBARRAYPROC) load("glDisableVertexAttribArray", userptr);
    glDrawBuffers = (PFNGLDRAWBUFFERSPROC) load("glDrawBuffers", userptr);
    glEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC) load("glEnableVertexAttribArray", userptr);
    glGetActiveAttrib = (PFNGLGETACTIVEATTRIBPROC) load("glGetActiveAttrib", userptr);
    glGetActiveUniform = (PFNGLGETACTIVEUNIFORMPROC) load("glGetActiveUniform", userptr);
    glGetAttachedShaders = (PFNGLGETATTACHEDSHADERSPROC) load("glGetAttachedShaders", userptr);
    glGetAttribLocation = (PFNGLGETATTRIBLOCATIONPROC) load("glGetAttribLocation", userptr);
    glGetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC) load("glGetProgramInfoLog", userptr);
    glGetProgramiv = (PFNGLGETPROGRAMIVPROC) load("glGetProgramiv", userptr);
    glGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC) load("glGetShaderInfoLog", userptr);
    glGetShaderSource = (PFNGLGETSHADERSOURCEPROC) load("glGetShaderSource", userptr);
    glGetShaderiv = (PFNGLGETSHADERIVPROC) load("glGetShaderiv", userptr);
    glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC) load("glGetUniformLocation", userptr);
    glGetUniformfv = (PFNGLGETUNIFORMFVPROC) load("glGetUniformfv", userptr);
    glGetUniformiv = (PFNGLGETUNIFORMIVPROC) load("glGetUniformiv", userptr);
    glGetVertexAttribPointerv = (PFNGLGETVERTEXATTRIBPOINTERVPROC) load("glGetVertexAttribPointerv", userptr);
    glGetVertexAttribdv = (PFNGLGETVERTEXATTRIBDVPROC) load("glGetVertexAttribdv", userptr);
    glGetVertexAttribfv = (PFNGLGETVERTEXATTRIBFVPROC) load("glGetVertexAttribfv", userptr);
    glGetVertexAttribiv = (PFNGLGETVERTEXATTRIBIVPROC) load("glGetVertexAttribiv", userptr);
    glIsProgram = (PFNGLISPROGRAMPROC) load("glIsProgram", userptr);
    glIsShader = (PFNGLISSHADERPROC) load("glIsShader", userptr);
    glLinkProgram = (PFNGLLINKPROGRAMPROC) load("glLinkProgram", userptr);
    glShaderSource = (PFNGLSHADERSOURCEPROC) load("glShaderSource", userptr);
    glStencilFuncSeparate = (PFNGLSTENCILFUNCSEPARATEPROC) load("glStencilFuncSeparate", userptr);
    glStencilMaskSeparate = (PFNGLSTENCILMASKSEPARATEPROC) load("glStencilMaskSeparate", userptr);
    glStencilOpSeparate = (PFNGLSTENCILOPSEPARATEPROC) load("glStencilOpSeparate", userptr);
    glUniform1f = (PFNGLUNIFORM1FPROC) load("glUniform1f", userptr);
    glUniform1fv = (PFNGLUNIFORM1FVPROC) load("glUniform1fv", userptr);
    glUniform1i = (PFNGLUNIFORM1IPROC) load("glUniform1i", userptr);
    glUniform1iv = (PFNGLUNIFORM1IVPROC) load("glUniform1iv", userptr);
    glUniform2f = (PFNGLUNIFORM2FPROC) load("glUniform2f", userptr);
    glUniform2fv = (PFNGLUNIFORM2FVPROC) load("glUniform2fv", userptr);
    glUniform2i = (PFNGLUNIFORM2IPROC) load("glUniform2i", userptr);
    glUniform2iv = (PFNGLUNIFORM2IVPROC) load("glUniform2iv", userptr);
    glUniform3f = (PFNGLUNIFORM3FPROC) load("glUniform3f", userptr);
    glUniform3fv = (PFNGLUNIFORM3FVPROC) load("glUniform3fv", userptr);
    glUniform3i = (PFNGLUNIFORM3IPROC) load("glUniform3i", userptr);
    glUniform3iv = (PFNGLUNIFORM3IVPROC) load("glUniform3iv", userptr);
    glUniform4f = (PFNGLUNIFORM4FPROC) load("glUniform4f", userptr);
    glUniform4fv = (PFNGLUNIFORM4FVPROC) load("glUniform4fv", userptr);
    glUniform4i = (PFNGLUNIFORM4IPROC) load("glUniform4i", userptr);
    glUniform4iv = (PFNGLUNIFORM4IVPROC) load("glUniform4iv", userptr);
    glUniformMatrix2fv = (PFNGLUNIFORMMATRIX2FVPROC) load("glUniformMatrix2fv", userptr);
    glUniformMatrix3fv = (PFNGLUNIFORMMATRIX3FVPROC) load("glUniformMatrix3fv", userptr);
    glUniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC) load("glUniformMatrix4fv", userptr);
    glUseProgram = (PFNGLUSEPROGRAMPROC) load("glUseProgram", userptr);
    glValidateProgram = (PFNGLVALIDATEPROGRAMPROC) load("glValidateProgram", userptr);
    glVertexAttrib1d = (PFNGLVERTEXATTRIB1DPROC) load("glVertexAttrib1d", userptr);
    glVertexAttrib1dv = (PFNGLVERTEXATTRIB1DVPROC) load("glVertexAttrib1dv", userptr);
    glVertexAttrib1f = (PFNGLVERTEXATTRIB1FPROC) load("glVertexAttrib1f", userptr);
    glVertexAttrib1fv = (PFNGLVERTEXATTRIB1FVPROC) load("glVertexAttrib1fv", userptr);
    glVertexAttrib1s = (PFNGLVERTEXATTRIB1SPROC) load("glVertexAttrib1s", userptr);
    glVertexAttrib1sv = (PFNGLVERTEXATTRIB1SVPROC) load("glVertexAttrib1sv", userptr);
    glVertexAttrib2d = (PFNGLVERTEXATTRIB2DPROC) load("glVertexAttrib2d", userptr);
    glVertexAttrib2dv = (PFNGLVERTEXATTRIB2DVPROC) load("glVertexAttrib2dv", userptr);
    glVertexAttrib2f = (PFNGLVERTEXATTRIB2FPROC) load("glVertexAttrib2f", userptr);
    glVertexAttrib2fv = (PFNGLVERTEXATTRIB2FVPROC) load("glVertexAttrib2fv", userptr);
    glVertexAttrib2s = (PFNGLVERTEXATTRIB2SPROC) load("glVertexAttrib2s", userptr);
    glVertexAttrib2sv = (PFNGLVERTEXATTRIB2SVPROC) load("glVertexAttrib2sv", userptr);
    glVertexAttrib3d = (PFNGLVERTEXATTRIB3DPROC) load("glVertexAttrib3d", userptr);
    glVertexAttrib3dv = (PFNGLVERTEXATTRIB3DVPROC) load("glVertexAttrib3dv", userptr);
    glVertexAttrib3f = (PFNGLVERTEXATTRIB3FPROC) load("glVertexAttrib3f", userptr);
    glVertexAttrib3fv = (PFNGLVERTEXATTRIB3FVPROC) load("glVertexAttrib3fv", userptr);
    glVertexAttrib3s = (PFNGLVERTEXATTRIB3SPROC) load("glVertexAttrib3s", userptr);
    glVertexAttrib3sv = (PFNGLVERTEXATTRIB3SVPROC) load("glVertexAttrib3sv", userptr);
    glVertexAttrib4Nbv = (PFNGLVERTEXATTRIB4NBVPROC) load("glVertexAttrib4Nbv", userptr);
    glVertexAttrib4Niv = (PFNGLVERTEXATTRIB4NIVPROC) load("glVertexAttrib4Niv", userptr);
    glVertexAttrib4Nsv = (PFNGLVERTEXATTRIB4NSVPROC) load("glVertexAttrib4Nsv", userptr);
    glVertexAttrib4Nub = (PFNGLVERTEXATTRIB4NUBPROC) load("glVertexAttrib4Nub", userptr);
    glVertexAttrib4Nubv = (PFNGLVERTEXATTRIB4NUBVPROC) load("glVertexAttrib4Nubv", userptr);
    glVertexAttrib4Nuiv = (PFNGLVERTEXATTRIB4NUIVPROC) load("glVertexAttrib4Nuiv", userptr);
    glVertexAttrib4Nusv = (PFNGLVERTEXATTRIB4NUSVPROC) load("glVertexAttrib4Nusv", userptr);
    glVertexAttrib4bv = (PFNGLVERTEXATTRIB4BVPROC) load("glVertexAttrib4bv", userptr);
    glVertexAttrib4d = (PFNGLVERTEXATTRIB4DPROC) load("glVertexAttrib4d", userptr);
    glVertexAttrib4dv = (PFNGLVERTEXATTRIB4DVPROC) load("glVertexAttrib4dv", userptr);
    glVertexAttrib4f = (PFNGLVERTEXATTRIB4FPROC) load("glVertexAttrib4f", userptr);
    glVertexAttrib4fv = (PFNGLVERTEXATTRIB4FVPROC) load("glVertexAttrib4fv", userptr);
    glVertexAttrib4iv = (PFNGLVERTEXATTRIB4IVPROC) load("glVertexAttrib4iv", userptr);
    glVertexAttrib4s = (PFNGLVERTEXATTRIB4SPROC) load("glVertexAttrib4s", userptr);
    glVertexAttrib4sv = (PFNGLVERTEXATTRIB4SVPROC) load("glVertexAttrib4sv", userptr);
    glVertexAttrib4ubv = (PFNGLVERTEXATTRIB4UBVPROC) load("glVertexAttrib4ubv", userptr);
    glVertexAttrib4uiv = (PFNGLVERTEXATTRIB4UIVPROC) load("glVertexAttrib4uiv", userptr);
    glVertexAttrib4usv = (PFNGLVERTEXATTRIB4USVPROC) load("glVertexAttrib4usv", userptr);
    glVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC) load("glVertexAttribPointer", userptr);
}
static void glad_gl_load_GL_VERSION_2_1( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_2_1) return;
    glUniformMatrix2x3fv = (PFNGLUNIFORMMATRIX2X3FVPROC) load("glUniformMatrix2x3fv", userptr);
    glUniformMatrix2x4fv = (PFNGLUNIFORMMATRIX2X4FVPROC) load("glUniformMatrix2x4fv", userptr);
    glUniformMatrix3x2fv = (PFNGLUNIFORMMATRIX3X2FVPROC) load("glUniformMatrix3x2fv", userptr);
    glUniformMatrix3x4fv = (PFNGLUNIFORMMATRIX3X4FVPROC) load("glUniformMatrix3x4fv", userptr);
    glUniformMatrix4x2fv = (PFNGLUNIFORMMATRIX4X2FVPROC) load("glUniformMatrix4x2fv", userptr);
    glUniformMatrix4x3fv = (PFNGLUNIFORMMATRIX4X3FVPROC) load("glUniformMatrix4x3fv", userptr);
}
static void glad_gl_load_GL_VERSION_3_0( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_3_0) return;
    glBeginConditionalRender = (PFNGLBEGINCONDITIONALRENDERPROC) load("glBeginConditionalRender", userptr);
    glBeginTransformFeedback = (PFNGLBEGINTRANSFORMFEEDBACKPROC) load("glBeginTransformFeedback", userptr);
    glBindBufferBase = (PFNGLBINDBUFFERBASEPROC) load("glBindBufferBase", userptr);
    glBindBufferRange = (PFNGLBINDBUFFERRANGEPROC) load("glBindBufferRange", userptr);
    glBindFragDataLocation = (PFNGLBINDFRAGDATALOCATIONPROC) load("glBindFragDataLocation", userptr);
    glBindFramebuffer = (PFNGLBINDFRAMEBUFFERPROC) load("glBindFramebuffer", userptr);
    glBindRenderbuffer = (PFNGLBINDRENDERBUFFERPROC) load("glBindRenderbuffer", userptr);
    glBindVertexArray = (PFNGLBINDVERTEXARRAYPROC) load("glBindVertexArray", userptr);
    glBlitFramebuffer = (PFNGLBLITFRAMEBUFFERPROC) load("glBlitFramebuffer", userptr);
    glCheckFramebufferStatus = (PFNGLCHECKFRAMEBUFFERSTATUSPROC) load("glCheckFramebufferStatus", userptr);
    glClampColor = (PFNGLCLAMPCOLORPROC) load("glClampColor", userptr);
    glClearBufferfi = (PFNGLCLEARBUFFERFIPROC) load("glClearBufferfi", userptr);
    glClearBufferfv = (PFNGLCLEARBUFFERFVPROC) load("glClearBufferfv", userptr);
    glClearBufferiv = (PFNGLCLEARBUFFERIVPROC) load("glClearBufferiv", userptr);
    glClearBufferuiv = (PFNGLCLEARBUFFERUIVPROC) load("glClearBufferuiv", userptr);
    glColorMaski = (PFNGLCOLORMASKIPROC) load("glColorMaski", userptr);
    glDeleteFramebuffers = (PFNGLDELETEFRAMEBUFFERSPROC) load("glDeleteFramebuffers", userptr);
    glDeleteRenderbuffers = (PFNGLDELETERENDERBUFFERSPROC) load("glDeleteRenderbuffers", userptr);
    glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC) load("glDeleteVertexArrays", userptr);
    glDisablei = (PFNGLDISABLEIPROC) load("glDisablei", userptr);
    glEnablei = (PFNGLENABLEIPROC) load("glEnablei", userptr);
    glEndConditionalRender = (PFNGLENDCONDITIONALRENDERPROC) load("glEndConditionalRender", userptr);
    glEndTransformFeedback = (PFNGLENDTRANSFORMFEEDBACKPROC) load("glEndTransformFeedback", userptr);
    glFlushMappedBufferRange = (PFNGLFLUSHMAPPEDBUFFERRANGEPROC) load("glFlushMappedBufferRange", userptr);
    glFramebufferRenderbuffer = (PFNGLFRAMEBUFFERRENDERBUFFERPROC) load("glFramebufferRenderbuffer", userptr);
    glFramebufferTexture1D = (PFNGLFRAMEBUFFERTEXTURE1DPROC) load("glFramebufferTexture1D", userptr);
    glFramebufferTexture2D = (PFNGLFRAMEBUFFERTEXTURE2DPROC) load("glFramebufferTexture2D", userptr);
    glFramebufferTexture3D = (PFNGLFRAMEBUFFERTEXTURE3DPROC) load("glFramebufferTexture3D", userptr);
    glFramebufferTextureLayer = (PFNGLFRAMEBUFFERTEXTURELAYERPROC) load("glFramebufferTextureLayer", userptr);
    glGenFramebuffers = (PFNGLGENFRAMEBUFFERSPROC) load("glGenFramebuffers", userptr);
    glGenRenderbuffers = (PFNGLGENRENDERBUFFERSPROC) load("glGenRenderbuffers", userptr);
    glGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC) load("glGenVertexArrays", userptr);
    glGenerateMipmap = (PFNGLGENERATEMIPMAPPROC) load("glGenerateMipmap", userptr);
    glGetBooleani_v = (PFNGLGETBOOLEANI_VPROC) load("glGetBooleani_v", userptr);
    glGetFragDataLocation = (PFNGLGETFRAGDATALOCATIONPROC) load("glGetFragDataLocation", userptr);
    glGetFramebufferAttachmentParameteriv = (PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC) load("glGetFramebufferAttachmentParameteriv", userptr);
    glGetIntegeri_v = (PFNGLGETINTEGERI_VPROC) load("glGetIntegeri_v", userptr);
    glGetRenderbufferParameteriv = (PFNGLGETRENDERBUFFERPARAMETERIVPROC) load("glGetRenderbufferParameteriv", userptr);
    glGetStringi = (PFNGLGETSTRINGIPROC) load("glGetStringi", userptr);
    glGetTexParameterIiv = (PFNGLGETTEXPARAMETERIIVPROC) load("glGetTexParameterIiv", userptr);
    glGetTexParameterIuiv = (PFNGLGETTEXPARAMETERIUIVPROC) load("glGetTexParameterIuiv", userptr);
    glGetTransformFeedbackVarying = (PFNGLGETTRANSFORMFEEDBACKVARYINGPROC) load("glGetTransformFeedbackVarying", userptr);
    glGetUniformuiv = (PFNGLGETUNIFORMUIVPROC) load("glGetUniformuiv", userptr);
    glGetVertexAttribIiv = (PFNGLGETVERTEXATTRIBIIVPROC) load("glGetVertexAttribIiv", userptr);
    glGetVertexAttribIuiv = (PFNGLGETVERTEXATTRIBIUIVPROC) load("glGetVertexAttribIuiv", userptr);
    glIsEnabledi = (PFNGLISENABLEDIPROC) load("glIsEnabledi", userptr);
    glIsFramebuffer = (PFNGLISFRAMEBUFFERPROC) load("glIsFramebuffer", userptr);
    glIsRenderbuffer = (PFNGLISRENDERBUFFERPROC) load("glIsRenderbuffer", userptr);
    glIsVertexArray = (PFNGLISVERTEXARRAYPROC) load("glIsVertexArray", userptr);
    glMapBufferRange = (PFNGLMAPBUFFERRANGEPROC) load("glMapBufferRange", userptr);
    glRenderbufferStorage = (PFNGLRENDERBUFFERSTORAGEPROC) load("glRenderbufferStorage", userptr);
    glRenderbufferStorageMultisample = (PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC) load("glRenderbufferStorageMultisample", userptr);
    glTexParameterIiv = (PFNGLTEXPARAMETERIIVPROC) load("glTexParameterIiv", userptr);
    glTexParameterIuiv = (PFNGLTEXPARAMETERIUIVPROC) load("glTexParameterIuiv", userptr);
    glTransformFeedbackVaryings = (PFNGLTRANSFORMFEEDBACKVARYINGSPROC) load("glTransformFeedbackVaryings", userptr);
    glUniform1ui = (PFNGLUNIFORM1UIPROC) load("glUniform1ui", userptr);
    glUniform1uiv = (PFNGLUNIFORM1UIVPROC) load("glUniform1uiv", userptr);
    glUniform2ui = (PFNGLUNIFORM2UIPROC) load("glUniform2ui", userptr);
    glUniform2uiv = (PFNGLUNIFORM2UIVPROC) load("glUniform2uiv", userptr);
    glUniform3ui = (PFNGLUNIFORM3UIPROC) load("glUniform3ui", userptr);
    glUniform3uiv = (PFNGLUNIFORM3UIVPROC) load("glUniform3uiv", userptr);
    glUniform4ui = (PFNGLUNIFORM4UIPROC) load("glUniform4ui", userptr);
    glUniform4uiv = (PFNGLUNIFORM4UIVPROC) load("glUniform4uiv", userptr);
    glVertexAttribI1i = (PFNGLVERTEXATTRIBI1IPROC) load("glVertexAttribI1i", userptr);
    glVertexAttribI1iv = (PFNGLVERTEXATTRIBI1IVPROC) load("glVertexAttribI1iv", userptr);
    glVertexAttribI1ui = (PFNGLVERTEXATTRIBI1UIPROC) load("glVertexAttribI1ui", userptr);
    glVertexAttribI1uiv = (PFNGLVERTEXATTRIBI1UIVPROC) load("glVertexAttribI1uiv", userptr);
    glVertexAttribI2i = (PFNGLVERTEXATTRIBI2IPROC) load("glVertexAttribI2i", userptr);
    glVertexAttribI2iv = (PFNGLVERTEXATTRIBI2IVPROC) load("glVertexAttribI2iv", userptr);
    glVertexAttribI2ui = (PFNGLVERTEXATTRIBI2UIPROC) load("glVertexAttribI2ui", userptr);
    glVertexAttribI2uiv = (PFNGLVERTEXATTRIBI2UIVPROC) load("glVertexAttribI2uiv", userptr);
    glVertexAttribI3i = (PFNGLVERTEXATTRIBI3IPROC) load("glVertexAttribI3i", userptr);
    glVertexAttribI3iv = (PFNGLVERTEXATTRIBI3IVPROC) load("glVertexAttribI3iv", userptr);
    glVertexAttribI3ui = (PFNGLVERTEXATTRIBI3UIPROC) load("glVertexAttribI3ui", userptr);
    glVertexAttribI3uiv = (PFNGLVERTEXATTRIBI3UIVPROC) load("glVertexAttribI3uiv", userptr);
    glVertexAttribI4bv = (PFNGLVERTEXATTRIBI4BVPROC) load("glVertexAttribI4bv", userptr);
    glVertexAttribI4i = (PFNGLVERTEXATTRIBI4IPROC) load("glVertexAttribI4i", userptr);
    glVertexAttribI4iv = (PFNGLVERTEXATTRIBI4IVPROC) load("glVertexAttribI4iv", userptr);
    glVertexAttribI4sv = (PFNGLVERTEXATTRIBI4SVPROC) load("glVertexAttribI4sv", userptr);
    glVertexAttribI4ubv = (PFNGLVERTEXATTRIBI4UBVPROC) load("glVertexAttribI4ubv", userptr);
    glVertexAttribI4ui = (PFNGLVERTEXATTRIBI4UIPROC) load("glVertexAttribI4ui", userptr);
    glVertexAttribI4uiv = (PFNGLVERTEXATTRIBI4UIVPROC) load("glVertexAttribI4uiv", userptr);
    glVertexAttribI4usv = (PFNGLVERTEXATTRIBI4USVPROC) load("glVertexAttribI4usv", userptr);
    glVertexAttribIPointer = (PFNGLVERTEXATTRIBIPOINTERPROC) load("glVertexAttribIPointer", userptr);
}
static void glad_gl_load_GL_VERSION_3_1( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_3_1) return;
    glBindBufferBase = (PFNGLBINDBUFFERBASEPROC) load("glBindBufferBase", userptr);
    glBindBufferRange = (PFNGLBINDBUFFERRANGEPROC) load("glBindBufferRange", userptr);
    glCopyBufferSubData = (PFNGLCOPYBUFFERSUBDATAPROC) load("glCopyBufferSubData", userptr);
    glDrawArraysInstanced = (PFNGLDRAWARRAYSINSTANCEDPROC) load("glDrawArraysInstanced", userptr);
    glDrawElementsInstanced = (PFNGLDRAWELEMENTSINSTANCEDPROC) load("glDrawElementsInstanced", userptr);
    glGetActiveUniformBlockName = (PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC) load("glGetActiveUniformBlockName", userptr);
    glGetActiveUniformBlockiv = (PFNGLGETACTIVEUNIFORMBLOCKIVPROC) load("glGetActiveUniformBlockiv", userptr);
    glGetActiveUniformName = (PFNGLGETACTIVEUNIFORMNAMEPROC) load("glGetActiveUniformName", userptr);
    glGetActiveUniformsiv = (PFNGLGETACTIVEUNIFORMSIVPROC) load("glGetActiveUniformsiv", userptr);
    glGetIntegeri_v = (PFNGLGETINTEGERI_VPROC) load("glGetIntegeri_v", userptr);
    glGetUniformBlockIndex = (PFNGLGETUNIFORMBLOCKINDEXPROC) load("glGetUniformBlockIndex", userptr);
    glGetUniformIndices = (PFNGLGETUNIFORMINDICESPROC) load("glGetUniformIndices", userptr);
    glPrimitiveRestartIndex = (PFNGLPRIMITIVERESTARTINDEXPROC) load("glPrimitiveRestartIndex", userptr);
    glTexBuffer = (PFNGLTEXBUFFERPROC) load("glTexBuffer", userptr);
    glUniformBlockBinding = (PFNGLUNIFORMBLOCKBINDINGPROC) load("glUniformBlockBinding", userptr);
}
static void glad_gl_load_GL_VERSION_3_2( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_3_2) return;
    glClientWaitSync = (PFNGLCLIENTWAITSYNCPROC) load("glClientWaitSync", userptr);
    glDeleteSync = (PFNGLDELETESYNCPROC) load("glDeleteSync", userptr);
    glDrawElementsBaseVertex = (PFNGLDRAWELEMENTSBASEVERTEXPROC) load("glDrawElementsBaseVertex", userptr);
    glDrawElementsInstancedBaseVertex = (PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC) load("glDrawElementsInstancedBaseVertex", userptr);
    glDrawRangeElementsBaseVertex = (PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC) load("glDrawRangeElementsBaseVertex", userptr);
    glFenceSync = (PFNGLFENCESYNCPROC) load("glFenceSync", userptr);
    glFramebufferTexture = (PFNGLFRAMEBUFFERTEXTUREPROC) load("glFramebufferTexture", userptr);
    glGetBufferParameteri64v = (PFNGLGETBUFFERPARAMETERI64VPROC) load("glGetBufferParameteri64v", userptr);
    glGetInteger64i_v = (PFNGLGETINTEGER64I_VPROC) load("glGetInteger64i_v", userptr);
    glGetInteger64v = (PFNGLGETINTEGER64VPROC) load("glGetInteger64v", userptr);
    glGetMultisamplefv = (PFNGLGETMULTISAMPLEFVPROC) load("glGetMultisamplefv", userptr);
    glGetSynciv = (PFNGLGETSYNCIVPROC) load("glGetSynciv", userptr);
    glIsSync = (PFNGLISSYNCPROC) load("glIsSync", userptr);
    glMultiDrawElementsBaseVertex = (PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC) load("glMultiDrawElementsBaseVertex", userptr);
    glProvokingVertex = (PFNGLPROVOKINGVERTEXPROC) load("glProvokingVertex", userptr);
    glSampleMaski = (PFNGLSAMPLEMASKIPROC) load("glSampleMaski", userptr);
    glTexImage2DMultisample = (PFNGLTEXIMAGE2DMULTISAMPLEPROC) load("glTexImage2DMultisample", userptr);
    glTexImage3DMultisample = (PFNGLTEXIMAGE3DMULTISAMPLEPROC) load("glTexImage3DMultisample", userptr);
    glWaitSync = (PFNGLWAITSYNCPROC) load("glWaitSync", userptr);
}
static void glad_gl_load_GL_VERSION_3_3( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_3_3) return;
    glBindFragDataLocationIndexed = (PFNGLBINDFRAGDATALOCATIONINDEXEDPROC) load("glBindFragDataLocationIndexed", userptr);
    glBindSampler = (PFNGLBINDSAMPLERPROC) load("glBindSampler", userptr);
    glDeleteSamplers = (PFNGLDELETESAMPLERSPROC) load("glDeleteSamplers", userptr);
    glGenSamplers = (PFNGLGENSAMPLERSPROC) load("glGenSamplers", userptr);
    glGetFragDataIndex = (PFNGLGETFRAGDATAINDEXPROC) load("glGetFragDataIndex", userptr);
    glGetQueryObjecti64v = (PFNGLGETQUERYOBJECTI64VPROC) load("glGetQueryObjecti64v", userptr);
    glGetQueryObjectui64v = (PFNGLGETQUERYOBJECTUI64VPROC) load("glGetQueryObjectui64v", userptr);
    glGetSamplerParameterIiv = (PFNGLGETSAMPLERPARAMETERIIVPROC) load("glGetSamplerParameterIiv", userptr);
    glGetSamplerParameterIuiv = (PFNGLGETSAMPLERPARAMETERIUIVPROC) load("glGetSamplerParameterIuiv", userptr);
    glGetSamplerParameterfv = (PFNGLGETSAMPLERPARAMETERFVPROC) load("glGetSamplerParameterfv", userptr);
    glGetSamplerParameteriv = (PFNGLGETSAMPLERPARAMETERIVPROC) load("glGetSamplerParameteriv", userptr);
    glIsSampler = (PFNGLISSAMPLERPROC) load("glIsSampler", userptr);
    glQueryCounter = (PFNGLQUERYCOUNTERPROC) load("glQueryCounter", userptr);
    glSamplerParameterIiv = (PFNGLSAMPLERPARAMETERIIVPROC) load("glSamplerParameterIiv", userptr);
    glSamplerParameterIuiv = (PFNGLSAMPLERPARAMETERIUIVPROC) load("glSamplerParameterIuiv", userptr);
    glSamplerParameterf = (PFNGLSAMPLERPARAMETERFPROC) load("glSamplerParameterf", userptr);
    glSamplerParameterfv = (PFNGLSAMPLERPARAMETERFVPROC) load("glSamplerParameterfv", userptr);
    glSamplerParameteri = (PFNGLSAMPLERPARAMETERIPROC) load("glSamplerParameteri", userptr);
    glSamplerParameteriv = (PFNGLSAMPLERPARAMETERIVPROC) load("glSamplerParameteriv", userptr);
    glVertexAttribDivisor = (PFNGLVERTEXATTRIBDIVISORPROC) load("glVertexAttribDivisor", userptr);
    glVertexAttribP1ui = (PFNGLVERTEXATTRIBP1UIPROC) load("glVertexAttribP1ui", userptr);
    glVertexAttribP1uiv = (PFNGLVERTEXATTRIBP1UIVPROC) load("glVertexAttribP1uiv", userptr);
    glVertexAttribP2ui = (PFNGLVERTEXATTRIBP2UIPROC) load("glVertexAttribP2ui", userptr);
    glVertexAttribP2uiv = (PFNGLVERTEXATTRIBP2UIVPROC) load("glVertexAttribP2uiv", userptr);
    glVertexAttribP3ui = (PFNGLVERTEXATTRIBP3UIPROC) load("glVertexAttribP3ui", userptr);
    glVertexAttribP3uiv = (PFNGLVERTEXATTRIBP3UIVPROC) load("glVertexAttribP3uiv", userptr);
    glVertexAttribP4ui = (PFNGLVERTEXATTRIBP4UIPROC) load("glVertexAttribP4ui", userptr);
    glVertexAttribP4uiv = (PFNGLVERTEXATTRIBP4UIVPROC) load("glVertexAttribP4uiv", userptr);
}
static void glad_gl_load_GL_VERSION_4_0( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_4_0) return;
    glBeginQueryIndexed = (PFNGLBEGINQUERYINDEXEDPROC) load("glBeginQueryIndexed", userptr);
    glBindTransformFeedback = (PFNGLBINDTRANSFORMFEEDBACKPROC) load("glBindTransformFeedback", userptr);
    glBlendEquationSeparatei = (PFNGLBLENDEQUATIONSEPARATEIPROC) load("glBlendEquationSeparatei", userptr);
    glBlendEquationi = (PFNGLBLENDEQUATIONIPROC) load("glBlendEquationi", userptr);
    glBlendFuncSeparatei = (PFNGLBLENDFUNCSEPARATEIPROC) load("glBlendFuncSeparatei", userptr);
    glBlendFunci = (PFNGLBLENDFUNCIPROC) load("glBlendFunci", userptr);
    glDeleteTransformFeedbacks = (PFNGLDELETETRANSFORMFEEDBACKSPROC) load("glDeleteTransformFeedbacks", userptr);
    glDrawArraysIndirect = (PFNGLDRAWARRAYSINDIRECTPROC) load("glDrawArraysIndirect", userptr);
    glDrawElementsIndirect = (PFNGLDRAWELEMENTSINDIRECTPROC) load("glDrawElementsIndirect", userptr);
    glDrawTransformFeedback = (PFNGLDRAWTRANSFORMFEEDBACKPROC) load("glDrawTransformFeedback", userptr);
    glDrawTransformFeedbackStream = (PFNGLDRAWTRANSFORMFEEDBACKSTREAMPROC) load("glDrawTransformFeedbackStream", userptr);
    glEndQueryIndexed = (PFNGLENDQUERYINDEXEDPROC) load("glEndQueryIndexed", userptr);
    glGenTransformFeedbacks = (PFNGLGENTRANSFORMFEEDBACKSPROC) load("glGenTransformFeedbacks", userptr);
    glGetActiveSubroutineName = (PFNGLGETACTIVESUBROUTINENAMEPROC) load("glGetActiveSubroutineName", userptr);
    glGetActiveSubroutineUniformName = (PFNGLGETACTIVESUBROUTINEUNIFORMNAMEPROC) load("glGetActiveSubroutineUniformName", userptr);
    glGetActiveSubroutineUniformiv = (PFNGLGETACTIVESUBROUTINEUNIFORMIVPROC) load("glGetActiveSubroutineUniformiv", userptr);
    glGetProgramStageiv = (PFNGLGETPROGRAMSTAGEIVPROC) load("glGetProgramStageiv", userptr);
    glGetQueryIndexediv = (PFNGLGETQUERYINDEXEDIVPROC) load("glGetQueryIndexediv", userptr);
    glGetSubroutineIndex = (PFNGLGETSUBROUTINEINDEXPROC) load("glGetSubroutineIndex", userptr);
    glGetSubroutineUniformLocation = (PFNGLGETSUBROUTINEUNIFORMLOCATIONPROC) load("glGetSubroutineUniformLocation", userptr);
    glGetUniformSubroutineuiv = (PFNGLGETUNIFORMSUBROUTINEUIVPROC) load("glGetUniformSubroutineuiv", userptr);
    glGetUniformdv = (PFNGLGETUNIFORMDVPROC) load("glGetUniformdv", userptr);
    glIsTransformFeedback = (PFNGLISTRANSFORMFEEDBACKPROC) load("glIsTransformFeedback", userptr);
    glMinSampleShading = (PFNGLMINSAMPLESHADINGPROC) load("glMinSampleShading", userptr);
    glPatchParameterfv = (PFNGLPATCHPARAMETERFVPROC) load("glPatchParameterfv", userptr);
    glPatchParameteri = (PFNGLPATCHPARAMETERIPROC) load("glPatchParameteri", userptr);
    glPauseTransformFeedback = (PFNGLPAUSETRANSFORMFEEDBACKPROC) load("glPauseTransformFeedback", userptr);
    glResumeTransformFeedback = (PFNGLRESUMETRANSFORMFEEDBACKPROC) load("glResumeTransformFeedback", userptr);
    glUniform1d = (PFNGLUNIFORM1DPROC) load("glUniform1d", userptr);
    glUniform1dv = (PFNGLUNIFORM1DVPROC) load("glUniform1dv", userptr);
    glUniform2d = (PFNGLUNIFORM2DPROC) load("glUniform2d", userptr);
    glUniform2dv = (PFNGLUNIFORM2DVPROC) load("glUniform2dv", userptr);
    glUniform3d = (PFNGLUNIFORM3DPROC) load("glUniform3d", userptr);
    glUniform3dv = (PFNGLUNIFORM3DVPROC) load("glUniform3dv", userptr);
    glUniform4d = (PFNGLUNIFORM4DPROC) load("glUniform4d", userptr);
    glUniform4dv = (PFNGLUNIFORM4DVPROC) load("glUniform4dv", userptr);
    glUniformMatrix2dv = (PFNGLUNIFORMMATRIX2DVPROC) load("glUniformMatrix2dv", userptr);
    glUniformMatrix2x3dv = (PFNGLUNIFORMMATRIX2X3DVPROC) load("glUniformMatrix2x3dv", userptr);
    glUniformMatrix2x4dv = (PFNGLUNIFORMMATRIX2X4DVPROC) load("glUniformMatrix2x4dv", userptr);
    glUniformMatrix3dv = (PFNGLUNIFORMMATRIX3DVPROC) load("glUniformMatrix3dv", userptr);
    glUniformMatrix3x2dv = (PFNGLUNIFORMMATRIX3X2DVPROC) load("glUniformMatrix3x2dv", userptr);
    glUniformMatrix3x4dv = (PFNGLUNIFORMMATRIX3X4DVPROC) load("glUniformMatrix3x4dv", userptr);
    glUniformMatrix4dv = (PFNGLUNIFORMMATRIX4DVPROC) load("glUniformMatrix4dv", userptr);
    glUniformMatrix4x2dv = (PFNGLUNIFORMMATRIX4X2DVPROC) load("glUniformMatrix4x2dv", userptr);
    glUniformMatrix4x3dv = (PFNGLUNIFORMMATRIX4X3DVPROC) load("glUniformMatrix4x3dv", userptr);
    glUniformSubroutinesuiv = (PFNGLUNIFORMSUBROUTINESUIVPROC) load("glUniformSubroutinesuiv", userptr);
}
static void glad_gl_load_GL_VERSION_4_1( GLADuserptrloadfunc load, void* userptr) {
    if(!GLAD_GL_VERSION_4_1) return;
    glActiveShaderProgram = (PFNGLACTIVESHADERPROGRAMPROC) load("glActiveShaderProgram", userptr);
    glBindProgramPipeline = (PFNGLBINDPROGRAMPIPELINEPROC) load("glBindProgramPipeline", userptr);
    glClearDepthf = (PFNGLCLEARDEPTHFPROC) load("glClearDepthf", userptr);
    glCreateShaderProgramv = (PFNGLCREATESHADERPROGRAMVPROC) load("glCreateShaderProgramv", userptr);
    glDeleteProgramPipelines = (PFNGLDELETEPROGRAMPIPELINESPROC) load("glDeleteProgramPipelines", userptr);
    glDepthRangeArrayv = (PFNGLDEPTHRANGEARRAYVPROC) load("glDepthRangeArrayv", userptr);
    glDepthRangeIndexed = (PFNGLDEPTHRANGEINDEXEDPROC) load("glDepthRangeIndexed", userptr);
    glDepthRangef = (PFNGLDEPTHRANGEFPROC) load("glDepthRangef", userptr);
    glGenProgramPipelines = (PFNGLGENPROGRAMPIPELINESPROC) load("glGenProgramPipelines", userptr);
    glGetDoublei_v = (PFNGLGETDOUBLEI_VPROC) load("glGetDoublei_v", userptr);
    glGetFloati_v = (PFNGLGETFLOATI_VPROC) load("glGetFloati_v", userptr);
    glGetProgramBinary = (PFNGLGETPROGRAMBINARYPROC) load("glGetProgramBinary", userptr);
    glGetProgramPipelineInfoLog = (PFNGLGETPROGRAMPIPELINEINFOLOGPROC) load("glGetProgramPipelineInfoLog", userptr);
    glGetProgramPipelineiv = (PFNGLGETPROGRAMPIPELINEIVPROC) load("glGetProgramPipelineiv", userptr);
    glGetShaderPrecisionFormat = (PFNGLGETSHADERPRECISIONFORMATPROC) load("glGetShaderPrecisionFormat", userptr);
    glGetVertexAttribLdv = (PFNGLGETVERTEXATTRIBLDVPROC) load("glGetVertexAttribLdv", userptr);
    glIsProgramPipeline = (PFNGLISPROGRAMPIPELINEPROC) load("glIsProgramPipeline", userptr);
    glProgramBinary = (PFNGLPROGRAMBINARYPROC) load("glProgramBinary", userptr);
    glProgramParameteri = (PFNGLPROGRAMPARAMETERIPROC) load("glProgramParameteri", userptr);
    glProgramUniform1d = (PFNGLPROGRAMUNIFORM1DPROC) load("glProgramUniform1d", userptr);
    glProgramUniform1dv = (PFNGLPROGRAMUNIFORM1DVPROC) load("glProgramUniform1dv", userptr);
    glProgramUniform1f = (PFNGLPROGRAMUNIFORM1FPROC) load("glProgramUniform1f", userptr);
    glProgramUniform1fv = (PFNGLPROGRAMUNIFORM1FVPROC) load("glProgramUniform1fv", userptr);
    glProgramUniform1i = (PFNGLPROGRAMUNIFORM1IPROC) load("glProgramUniform1i", userptr);
    glProgramUniform1iv = (PFNGLPROGRAMUNIFORM1IVPROC) load("glProgramUniform1iv", userptr);
    glProgramUniform1ui = (PFNGLPROGRAMUNIFORM1UIPROC) load("glProgramUniform1ui", userptr);
    glProgramUniform1uiv = (PFNGLPROGRAMUNIFORM1UIVPROC) load("glProgramUniform1uiv", userptr);
    glProgramUniform2d = (PFNGLPROGRAMUNIFORM2DPROC) load("glProgramUniform2d", userptr);
    glProgramUniform2dv = (PFNGLPROGRAMUNIFORM2DVPROC) load("glProgramUniform2dv", userptr);
    glProgramUniform2f = (PFNGLPROGRAMUNIFORM2FPROC) load("glProgramUniform2f", userptr);
    glProgramUniform2fv = (PFNGLPROGRAMUNIFORM2FVPROC) load("glProgramUniform2fv", userptr);
    glProgramUniform2i = (PFNGLPROGRAMUNIFORM2IPROC) load("glProgramUniform2i", userptr);
    glProgramUniform2iv = (PFNGLPROGRAMUNIFORM2IVPROC) load("glProgramUniform2iv", userptr);
    glProgramUniform2ui = (PFNGLPROGRAMUNIFORM2UIPROC) load("glProgramUniform2ui", userptr);
    glProgramUniform2uiv = (PFNGLPROGRAMUNIFORM2UIVPROC) load("glProgramUniform2uiv", userptr);
    glProgramUniform3d = (PFNGLPROGRAMUNIFORM3DPROC) load("glProgramUniform3d", userptr);
    glProgramUniform3dv = (PFNGLPROGRAMUNIFORM3DVPROC) load("glProgramUniform3dv", userptr);
    glProgramUniform3f = (PFNGLPROGRAMUNIFORM3FPROC) load("glProgramUniform3f", userptr);
    glProgramUniform3fv = (PFNGLPROGRAMUNIFORM3FVPROC) load("glProgramUniform3fv", userptr);
    glProgramUniform3i = (PFNGLPROGRAMUNIFORM3IPROC) load("glProgramUniform3i", userptr);
    glProgramUniform3iv = (PFNGLPROGRAMUNIFORM3IVPROC) load("glProgramUniform3iv", userptr);
    glProgramUniform3ui = (PFNGLPROGRAMUNIFORM3UIPROC) load("glProgramUniform3ui", userptr);
    glProgramUniform3uiv = (PFNGLPROGRAMUNIFORM3UIVPROC) load("glProgramUniform3uiv", userptr);
    glProgramUniform4d = (PFNGLPROGRAMUNIFORM4DPROC) load("glProgramUniform4d", userptr);
    glProgramUniform4dv = (PFNGLPROGRAMUNIFORM4DVPROC) load("glProgramUniform4dv", userptr);
    glProgramUniform4f = (PFNGLPROGRAMUNIFORM4FPROC) load("glProgramUniform4f", userptr);
    glProgramUniform4fv = (PFNGLPROGRAMUNIFORM4FVPROC) load("glProgramUniform4fv", userptr);
    glProgramUniform4i = (PFNGLPROGRAMUNIFORM4IPROC) load("glProgramUniform4i", userptr);
    glProgramUniform4iv = (PFNGLPROGRAMUNIFORM4IVPROC) load("glProgramUniform4iv", userptr);
    glProgramUniform4ui = (PFNGLPROGRAMUNIFORM4UIPROC) load("glProgramUniform4ui", userptr);
    glProgramUniform4uiv = (PFNGLPROGRAMUNIFORM4UIVPROC) load("glProgramUniform4uiv", userptr);
    glProgramUniformMatrix2dv = (PFNGLPROGRAMUNIFORMMATRIX2DVPROC) load("glProgramUniformMatrix2dv", userptr);
    glProgramUniformMatrix2fv = (PFNGLPROGRAMUNIFORMMATRIX2FVPROC) load("glProgramUniformMatrix2fv", userptr);
    glProgramUniformMatrix2x3dv = (PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC) load("glProgramUniformMatrix2x3dv", userptr);
    glProgramUniformMatrix2x3fv = (PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC) load("glProgramUniformMatrix2x3fv", userptr);
    glProgramUniformMatrix2x4dv = (PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC) load("glProgramUniformMatrix2x4dv", userptr);
    glProgramUniformMatrix2x4fv = (PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC) load("glProgramUniformMatrix2x4fv", userptr);
    glProgramUniformMatrix3dv = (PFNGLPROGRAMUNIFORMMATRIX3DVPROC) load("glProgramUniformMatrix3dv", userptr);
    glProgramUniformMatrix3fv = (PFNGLPROGRAMUNIFORMMATRIX3FVPROC) load("glProgramUniformMatrix3fv", userptr);
    glProgramUniformMatrix3x2dv = (PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC) load("glProgramUniformMatrix3x2dv", userptr);
    glProgramUniformMatrix3x2fv = (PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC) load("glProgramUniformMatrix3x2fv", userptr);
    glProgramUniformMatrix3x4dv = (PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC) load("glProgramUniformMatrix3x4dv", userptr);
    glProgramUniformMatrix3x4fv = (PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC) load("glProgramUniformMatrix3x4fv", userptr);
    glProgramUniformMatrix4dv = (PFNGLPROGRAMUNIFORMMATRIX4DVPROC) load("glProgramUniformMatrix4dv", userptr);
    glProgramUniformMatrix4fv = (PFNGLPROGRAMUNIFORMMATRIX4FVPROC) load("glProgramUniformMatrix4fv", userptr);
    glProgramUniformMatrix4x2dv = (PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC) load("glProgramUniformMatrix4x2dv", userptr);
    glProgramUniformMatrix4x2fv = (PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC) load("glProgramUniformMatrix4x2fv", userptr);
    glProgramUniformMatrix4x3dv = (PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC) load("glProgramUniformMatrix4x3dv", userptr);
    glProgramUniformMatrix4x3fv = (PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC) load("glProgramUniformMatrix4x3fv", userptr);
    glReleaseShaderCompiler = (PFNGLRELEASESHADERCOMPILERPROC) load("glReleaseShaderCompiler", userptr);
    glScissorArrayv = (PFNGLSCISSORARRAYVPROC) load("glScissorArrayv", userptr);
    glScissorIndexed = (PFNGLSCISSORINDEXEDPROC) load("glScissorIndexed", userptr);
    glScissorIndexedv = (PFNGLSCISSORINDEXEDVPROC) load("glScissorIndexedv", userptr);
    glShaderBinary = (PFNGLSHADERBINARYPROC) load("glShaderBinary", userptr);
    glUseProgramStages = (PFNGLUSEPROGRAMSTAGESPROC) load("glUseProgramStages", userptr);
    glValidateProgramPipeline = (PFNGLVALIDATEPROGRAMPIPELINEPROC) load("glValidateProgramPipeline", userptr);
    glVertexAttribL1d = (PFNGLVERTEXATTRIBL1DPROC) load("glVertexAttribL1d", userptr);
    glVertexAttribL1dv = (PFNGLVERTEXATTRIBL1DVPROC) load("glVertexAttribL1dv", userptr);
    glVertexAttribL2d = (PFNGLVERTEXATTRIBL2DPROC) load("glVertexAttribL2d", userptr);
    glVertexAttribL2dv = (PFNGLVERTEXATTRIBL2DVPROC) load("glVertexAttribL2dv", userptr);
    glVertexAttribL3d = (PFNGLVERTEXATTRIBL3DPROC) load("glVertexAttribL3d", userptr);
    glVertexAttribL3dv = (PFNGLVERTEXATTRIBL3DVPROC) load("glVertexAttribL3dv", userptr);
    glVertexAttribL4d = (PFNGLVERTEXATTRIBL4DPROC) load("glVertexAttribL4d", userptr);
    glVertexAttribL4dv = (PFNGLVERTEXATTRIBL4DVPROC) load("glVertexAttribL4dv", userptr);
    glVertexAttribLPointer = (PFNGLVERTEXATTRIBLPOINTERPROC) load("glVertexAttribLPointer", userptr);
    glViewportArrayv = (PFNGLVIEWPORTARRAYVPROC) load("glViewportArrayv", userptr);
    glViewportIndexedf = (PFNGLVIEWPORTINDEXEDFPROC) load("glViewportIndexedf", userptr);
    glViewportIndexedfv = (PFNGLVIEWPORTINDEXEDFVPROC) load("glViewportIndexedfv", userptr);
}



#if defined(GL_ES_VERSION_3_0) || defined(GL_VERSION_3_0)
#define GLAD_GL_IS_SOME_NEW_VERSION 1
#else
#define GLAD_GL_IS_SOME_NEW_VERSION 0
#endif

static int glad_gl_get_extensions( int version, const char **out_exts, unsigned int *out_num_exts_i, char ***out_exts_i) {
#if GLAD_GL_IS_SOME_NEW_VERSION
    if(GLAD_VERSION_MAJOR(version) < 3) {
#else
    (void) version;
    (void) out_num_exts_i;
    (void) out_exts_i;
#endif
        if (glGetString == NULL) {
            return 0;
        }
        *out_exts = (const char *)glGetString(GL_EXTENSIONS);
#if GLAD_GL_IS_SOME_NEW_VERSION
    } else {
        unsigned int index = 0;
        unsigned int num_exts_i = 0;
        char **exts_i = NULL;
        if (glGetStringi == NULL || glGetIntegerv == NULL) {
            return 0;
        }
        glGetIntegerv(GL_NUM_EXTENSIONS, (int*) &num_exts_i);
        if (num_exts_i > 0) {
            exts_i = (char **) malloc(num_exts_i * (sizeof *exts_i));
        }
        if (exts_i == NULL) {
            return 0;
        }
        for(index = 0; index < num_exts_i; index++) {
            const char *gl_str_tmp = (const char*) glGetStringi(GL_EXTENSIONS, index);
            size_t len = strlen(gl_str_tmp) + 1;

            char *local_str = (char*) malloc(len * sizeof(char));
            if(local_str != NULL) {
                memcpy(local_str, gl_str_tmp, len * sizeof(char));
            }

            exts_i[index] = local_str;
        }

        *out_num_exts_i = num_exts_i;
        *out_exts_i = exts_i;
    }
#endif
    return 1;
}
static void glad_gl_free_extensions(char **exts_i, unsigned int num_exts_i) {
    if (exts_i != NULL) {
        unsigned int index;
        for(index = 0; index < num_exts_i; index++) {
            free((void *) (exts_i[index]));
        }
        free((void *)exts_i);
        exts_i = NULL;
    }
}
static int glad_gl_has_extension(int version, const char *exts, unsigned int num_exts_i, char **exts_i, const char *ext) {
    if(GLAD_VERSION_MAJOR(version) < 3 || !GLAD_GL_IS_SOME_NEW_VERSION) {
        const char *extensions;
        const char *loc;
        const char *terminator;
        extensions = exts;
        if(extensions == NULL || ext == NULL) {
            return 0;
        }
        while(1) {
            loc = strstr(extensions, ext);
            if(loc == NULL) {
                return 0;
            }
            terminator = loc + strlen(ext);
            if((loc == extensions || *(loc - 1) == ' ') &&
                (*terminator == ' ' || *terminator == '\0')) {
                return 1;
            }
            extensions = terminator;
        }
    } else {
        unsigned int index;
        for(index = 0; index < num_exts_i; index++) {
            const char *e = exts_i[index];
            if(strcmp(e, ext) == 0) {
                return 1;
            }
        }
    }
    return 0;
}

static GLADapiproc glad_gl_get_proc_from_userptr(const char* name, void *userptr) {
    return (GLAD_GNUC_EXTENSION (GLADapiproc (*)(const char *name)) userptr)(name);
}

static int glad_gl_find_extensions_gl( int version) {
    const char *exts = NULL;
    unsigned int num_exts_i = 0;
    char **exts_i = NULL;
    if (!glad_gl_get_extensions(version, &exts, &num_exts_i, &exts_i)) return 0;

    GLAD_GL_ARB_texture_filter_anisotropic = glad_gl_has_extension(version, exts, num_exts_i, exts_i, "GL_ARB_texture_filter_anisotropic");
    GLAD_GL_ARB_texture_non_power_of_two = glad_gl_has_extension(version, exts, num_exts_i, exts_i, "GL_ARB_texture_non_power_of_two");

    glad_gl_free_extensions(exts_i, num_exts_i);

    return 1;
}

static int glad_gl_find_core_gl(void) {
    int i, major, minor;
    const char* version;
    const char* prefixes[] = {
        "OpenGL ES-CM ",
        "OpenGL ES-CL ",
        "OpenGL ES ",
        NULL
    };
    version = (const char*) glGetString(GL_VERSION);
    if (!version) return 0;
    for (i = 0;  prefixes[i];  i++) {
        const size_t length = strlen(prefixes[i]);
        if (strncmp(version, prefixes[i], length) == 0) {
            version += length;
            break;
        }
    }

    GLAD_IMPL_UTIL_SSCANF(version, "%d.%d", &major, &minor);

    GLAD_GL_VERSION_1_0 = (major == 1 && minor >= 0) || major > 1;
    GLAD_GL_VERSION_1_1 = (major == 1 && minor >= 1) || major > 1;
    GLAD_GL_VERSION_1_2 = (major == 1 && minor >= 2) || major > 1;
    GLAD_GL_VERSION_1_3 = (major == 1 && minor >= 3) || major > 1;
    GLAD_GL_VERSION_1_4 = (major == 1 && minor >= 4) || major > 1;
    GLAD_GL_VERSION_1_5 = (major == 1 && minor >= 5) || major > 1;
    GLAD_GL_VERSION_2_0 = (major == 2 && minor >= 0) || major > 2;
    GLAD_GL_VERSION_2_1 = (major == 2 && minor >= 1) || major > 2;
    GLAD_GL_VERSION_3_0 = (major == 3 && minor >= 0) || major > 3;
    GLAD_GL_VERSION_3_1 = (major == 3 && minor >= 1) || major > 3;
    GLAD_GL_VERSION_3_2 = (major == 3 && minor >= 2) || major > 3;
    GLAD_GL_VERSION_3_3 = (major == 3 && minor >= 3) || major > 3;
    GLAD_GL_VERSION_4_0 = (major == 4 && minor >= 0) || major > 4;
    GLAD_GL_VERSION_4_1 = (major == 4 && minor >= 1) || major > 4;

    return GLAD_MAKE_VERSION(major, minor);
}

int gladLoadGLUserPtr( GLADuserptrloadfunc load, void *userptr) {
    int version;

    glGetString = (PFNGLGETSTRINGPROC) load("glGetString", userptr);
    if(glGetString == NULL) return 0;
    if(glGetString(GL_VERSION) == NULL) return 0;
    version = glad_gl_find_core_gl();

    glad_gl_load_GL_VERSION_1_0(load, userptr);
    glad_gl_load_GL_VERSION_1_1(load, userptr);
    glad_gl_load_GL_VERSION_1_2(load, userptr);
    glad_gl_load_GL_VERSION_1_3(load, userptr);
    glad_gl_load_GL_VERSION_1_4(load, userptr);
    glad_gl_load_GL_VERSION_1_5(load, userptr);
    glad_gl_load_GL_VERSION_2_0(load, userptr);
    glad_gl_load_GL_VERSION_2_1(load, userptr);
    glad_gl_load_GL_VERSION_3_0(load, userptr);
    glad_gl_load_GL_VERSION_3_1(load, userptr);
    glad_gl_load_GL_VERSION_3_2(load, userptr);
    glad_gl_load_GL_VERSION_3_3(load, userptr);
    glad_gl_load_GL_VERSION_4_0(load, userptr);
    glad_gl_load_GL_VERSION_4_1(load, userptr);

    if (!glad_gl_find_extensions_gl(version)) return 0;



    return version;
}


int gladLoadGL( GLADloadfunc load) {
    return gladLoadGLUserPtr( glad_gl_get_proc_from_userptr, GLAD_GNUC_EXTENSION (void*) load);
}




#ifdef GLAD_GL

#ifndef GLAD_LOADER_LIBRARY_C_
#define GLAD_LOADER_LIBRARY_C_

#include <stddef.h>
#include <stdlib.h>

#if GLAD_PLATFORM_WIN32
#include <windows.h>
#else
#include <dlfcn.h>
#endif


static void* glad_get_dlopen_handle(const char *lib_names[], int length) {
    void *handle = NULL;
    int i;

    for (i = 0; i < length; ++i) {
#if GLAD_PLATFORM_WIN32
  #if GLAD_PLATFORM_UWP
        size_t buffer_size = (strlen(lib_names[i]) + 1) * sizeof(WCHAR);
        LPWSTR buffer = (LPWSTR) malloc(buffer_size);
        if (buffer != NULL) {
            int ret = MultiByteToWideChar(CP_ACP, 0, lib_names[i], -1, buffer, buffer_size);
            if (ret != 0) {
                handle = (void*) LoadPackagedLibrary(buffer, 0);
            }
            free((void*) buffer);
        }
  #else
        handle = (void*) LoadLibraryA(lib_names[i]);
  #endif
#else
        handle = dlopen(lib_names[i], RTLD_LAZY | RTLD_LOCAL);
#endif
        if (handle != NULL) {
            return handle;
        }
    }

    return NULL;
}

static void glad_close_dlopen_handle(void* handle) {
    if (handle != NULL) {
#if GLAD_PLATFORM_WIN32
        FreeLibrary((HMODULE) handle);
#else
        dlclose(handle);
#endif
    }
}

static GLADapiproc glad_dlsym_handle(void* handle, const char *name) {
    if (handle == NULL) {
        return NULL;
    }

#if GLAD_PLATFORM_WIN32
    return (GLADapiproc) GetProcAddress((HMODULE) handle, name);
#else
    return GLAD_GNUC_EXTENSION (GLADapiproc) dlsym(handle, name);
#endif
}

#endif /* GLAD_LOADER_LIBRARY_C_ */

typedef void* (GLAD_API_PTR *GLADglprocaddrfunc)(const char*);
struct _glad_gl_userptr {
    void *gl_handle;
    GLADglprocaddrfunc gl_get_proc_address_ptr;
};

static GLADapiproc glad_gl_get_proc(const char *name, void *vuserptr) {
    struct _glad_gl_userptr userptr = *(struct _glad_gl_userptr*) vuserptr;
    GLADapiproc result = NULL;

    if(userptr.gl_get_proc_address_ptr != NULL) {
        result = GLAD_GNUC_EXTENSION (GLADapiproc) userptr.gl_get_proc_address_ptr(name);
    }
    if(result == NULL) {
        result = glad_dlsym_handle(userptr.gl_handle, name);
    }

    return result;
}

int gladLoaderLoadGL(void) {
#if GLAD_PLATFORM_APPLE
    static const char *NAMES[] = {
        "../Frameworks/OpenGL.framework/OpenGL",
        "/Library/Frameworks/OpenGL.framework/OpenGL",
        "/System/Library/Frameworks/OpenGL.framework/OpenGL",
        "/System/Library/Frameworks/OpenGL.framework/Versions/Current/OpenGL"
    };
#elif GLAD_PLATFORM_WIN32
    static const char *NAMES[] = {"opengl32.dll"};
#else
    static const char *NAMES[] = {
  #if defined(__CYGWIN__)
        "libGL-1.so",
  #endif
        "libGL.so.1",
        "libGL.so"
    };
#endif

    int version = 0;
    void *handle;
    struct _glad_gl_userptr userptr;

    handle = glad_get_dlopen_handle(NAMES, sizeof(NAMES) / sizeof(NAMES[0]));
    if (handle) {
        userptr.gl_handle = handle;
#if GLAD_PLATFORM_APPLE || defined(__HAIKU__)
        userptr.gl_get_proc_address_ptr = NULL;
#elif GLAD_PLATFORM_WIN32
        userptr.gl_get_proc_address_ptr =
            (GLADglprocaddrfunc) glad_dlsym_handle(handle, "wglGetProcAddress");
#else
        userptr.gl_get_proc_address_ptr =
            (GLADglprocaddrfunc) glad_dlsym_handle(handle, "glXGetProcAddressARB");
#endif
        version = gladLoadGLUserPtr(glad_gl_get_proc, &userptr);

        glad_close_dlopen_handle(handle);
    }

    return version;
}


#endif /* GLAD_GL */
