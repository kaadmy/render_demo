
#pragma once

#include "glad.h"

#include <stdbool.h>

enum render_shader_type {
    RENDER_SHADER_TYPE_VERTEX = 0,
    RENDER_SHADER_TYPE_FRAGMENT,

    RENDER_SHADER_MAX_TYPES,
};

struct render_shader {
    enum render_shader_type type;

    bool is_compiled;

    GLenum gl_stage;
    GLuint gl_shader;
};

const char *render_shader_get_type_string(enum render_shader_type type);

void render_shader_init(struct render_shader *shader, enum render_shader_type type);
void render_shader_deinit(struct render_shader *shader);

void render_shader_load_from_string(struct render_shader *shader, const char *source);
