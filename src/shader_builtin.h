
#pragma once

extern const char *shader_builtin_default_vertex;
extern const char *shader_builtin_default_fragment;

// Post-processing

extern const char *shader_builtin_pp_vertex;

extern const char *shader_builtin_pp_ssao_fragment;

extern const char *shader_builtin_pp_bloom_extract_fragment;
extern const char *shader_builtin_pp_bloom_mix_fragment;
extern const char *shader_builtin_pp_blur_fragment;
extern const char *shader_builtin_pp_composite_fragment;
