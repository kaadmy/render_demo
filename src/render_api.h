
// Everything outside of render_*/ should include this file

#pragma once

#include "config.h"

#ifdef RENDER_DEMO_USE_OPENGL
#include "render_opengl/api.h"
#endif
