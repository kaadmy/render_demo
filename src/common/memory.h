
#pragma once

#include <stddef.h>

void *common_memory_allocate(size_t size);
void *common_memory_reallocate(void *pointer, size_t size);
void *common_memory_free(void *pointer);

void common_memory_audit(void);
