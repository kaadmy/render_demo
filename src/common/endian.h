
#pragma once

#include <stdint.h>

#ifdef _MSC_VER

#include <stdlib.h>

#ifdef BUILD_BIG_ENDIAN

#define common_lton_u16(x) (x)
#define common_lton_u32(x) (x)

#define common_bton_u16(x) _byteswap_ushort(x)
#define common_bton_u32(x) _byteswap_ulong(x)

#else // #ifdef BUILD_BIG_ENDIAN

#define common_lton_u16(x) _byteswap_ushort(x)
#define common_lton_u32(x) _byteswap_ulong(x)

#define common_bton_u16(x) (x)
#define common_bton_u32(x) (x)

#endif // #ifdef BUILD_BIG_ENDIAN

#else

#include <byteswap.h>

#ifdef BUILD_BIG_ENDIAN

#define common_lton_u16(x) (x)
#define common_lton_u32(x) (x)

#define common_bton_u16(x) bswap_16(x)
#define common_bton_u32(x) bswap_32(x)

#else // #ifdef BUILD_BIG_ENDIAN

#define common_lton_u16(x) bswap_16(x)
#define common_lton_u32(x) bswap_32(x)

#define common_bton_u16(x) (x)
#define common_bton_u32(x) (x)

#endif // #ifdef BUILD_BIG_ENDIAN

#endif
