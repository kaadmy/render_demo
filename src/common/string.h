

#pragma once

#include <stdbool.h>

#define COMMON_STRINGIFY_(s) #s
#define COMMON_STRINGIFY(s) COMMON_STRINGIFY_(s)

char *common_string_dup(const char *text);

const char *common_string_display_boolean(bool value, const char *yes, const char *no);
