
#include "string.h" // System header overlaps are fun
#include "memory.h"

#include <string.h>

char *common_string_dup(const char *text) {
    size_t size = strlen(text) + 1;

    char *new_text = common_memory_allocate(sizeof(char) * size);

    strncpy(new_text, text, size);

    return new_text;
}

const char *common_string_display_boolean(bool value, const char *yes, const char *no) {
    if(value) {
        return yes;
    }

    return no;
}
