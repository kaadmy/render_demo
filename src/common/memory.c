
#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

static struct {
    size_t allocations;
    size_t allocations_total;
} memstate;

void *common_memory_allocate(size_t size) {
    void *pointer = malloc(size);
    //assert(pointer != NULL);

    if(pointer == NULL) {
        printf("Failed to allocate %ld bytes\n", size);

        return NULL;
    }

    memstate.allocations++;
    memstate.allocations_total++;

    return pointer;
}

void *common_memory_reallocate(void *pointer, size_t size) {
    void *new_pointer = realloc(pointer, size);
    //assert(new_pointer != NULL);

    if(new_pointer == NULL) {
        printf("Failed to reallocate %ld bytes\n", size);

        return pointer;
    }

    if(pointer == NULL) {
        memstate.allocations++;
        memstate.allocations_total++;
    }

    return new_pointer;
}

void *common_memory_free(void *pointer) {
    if(pointer == NULL) {
        printf("Attempting to free a NULL pointer\n");

        return NULL;
    }

    free(pointer);

    memstate.allocations--;

    return NULL;
}

void common_memory_audit(void) {
    printf("Total memory allocations: %ld\n", memstate.allocations_total);
    printf("Unfreed memory allocations: %ld\n", memstate.allocations);
}
